Translation.prototype.createFilter = function( id ) {

    // init layers
    layerFilter[ id ] = new Kinetic.Layer();
    layerSpeaker[ id ] = new Kinetic.Layer();


    //reset value
    sceneLength = 0;

    // find all divs and save in array
    var speeches = $( this.getA() ).find('.speech'); 
    var speechesCount = speeches.length;

    // add for every text item a filterigation item
    for(var i = 0; i <= speechesCount -1 ; i++) {
    
        // add a unique ID for every item
        $(speeches[i]).attr('id', this.alias + '_' + 'id_'+ i );
        
        // count text length, get all p tags convert it to text (remove all html tags) and delete all white spaces
        // optimize shapes to fit screen


        var speechLength = Math.pow( $(speeches[i]).children('p').text().replace(/ /g,'').length , scale) * $(window).height() / 1400;              
        
        // init rect shape
        var rect = new Kinetic.Rect({   
          x: 0,
          y: sceneLength,
          width: rectWidth,
          height: speechLength,
          fill: "#000000",
          strokeWidth: filterStrokeWidth,
          stroke: filterStrokeWidthColor,
          index: 1,
          cornerRadius: 0,
          name: $(speeches[i]).children('div').text(),
          id: 'id_'+ i
          //$(speeches[i]).attr('id')
        });

        // init text object
        var speaker = new Kinetic.Text({
            text: "",
            fontFamily: "Georgia",
            fontSize: 10,
            padding: 3,
            textFill: "black",
            //fill: "black",
            alpha: 1,
            connectorible: false,
            align: "right",
            fill: 'yellow',
            x: 0,
            y: 0,
            verticalAlign: "middle",
        });


        
        // event listener
        rect.on("mousemove", function() {
            document.body.style.cursor = "pointer";
            this.setFill(filterHover);
            this.setStrokeWidth(filterStrokeWidthHover);
            this.setStroke(filterStrokeWidthColorHover);
            layerFilter[ id ].draw();

            speaker.setText( this.getName() );
            speaker.setPosition( filterPosX + 40, this.getPosition().y + 5 + filterPosY);
            speaker.show();
            layerSpeaker[ id ].draw();
        });
        
        rect.on("mouseout", function() {
            document.body.style.cursor = "default";
            this.setFill(filterNormal);

            // style active rect
            if(lastActiveFilterItem) {
                lastActiveFilterItem.setFill(filterSelected);
            }

            layerFilter[ id ].draw();

            speaker.hide();
            if(speakerActive) speakerActive.show();
            layerSpeaker[ id ].draw();
        });


        rect.on("click", function(e) {
            document.body.style.cursor = "pointer";
            e.preventDefault();

            filterStatus = true;

            // scroll content
            for(var i = 0; i < translation.length; i++) { 

                translation[i].setActiveItem( "#" + translation[i].getAlias() + '_' + this.getID() );
                translation[i].setScrollTarget( translation[i].getCurrentScrollPos() + $( translation[i].getActiveItem() ).offset().top - contentPositionTop[ id ] - this.getPosition().y - filterPosY + 2 );

                //style selected text
                if(translation[i].getLastActiveTextItem()) $( translation[i].getLastActiveTextItem() ).removeClass('textSelected');
                $( translation[i].getActiveItem() ).addClass('textSelected');
                translation[i].setLastActiveTextItem( translation[i].getActiveItem() );
                
            }
            for(var i = 0; i < translation.length; i++) { 
                if( translation[i].getCurrentScrollPos() != translation[i].getScrollTarget() ) {
                    $( translation[i].getA() ).animate({ scrollTop: translation[i].getScrollTarget() }, scrollDuration, scrollStyle, function() {
                        //window.location.href="#"+rect.getName();
                        translation[0].setCurrentScrollPos( $( translation[0].getA()).scrollTop() );
                        translation[1].setCurrentScrollPos( $( translation[1].getA()).scrollTop() );
                        
                        //connect two text parts with connector
                        //updateConnector();

                    });
                }
            }

            
            // style active rect
            if(lastActiveFilterItem) lastActiveFilterItem.setFill(filterNormal);
            if(speakerActive) speakerActive.hide();
            speaker.show();
            layerSpeaker[ id ].draw();
            //var fill = this.getFill() == "#000000" ? "#222222" : "#000000";
            this.setFill(filterSelected);
            layerFilter[ id ].draw();
            
            lastActiveFilterItem = this;
            speakerActive = speaker;
        });

        // move y value
        sceneLength += speechLength + margin;

        speaker.moveDown();

        // add the obects to the layers
        layerSpeaker[ id ].add(speaker);
        layerFilter[ id ].add(rect);
    }
    
    // position layers on stage
    layerFilter[ id ].setPosition( filterPosX, filterPosY );
    //layerSpeaker.setPosition( filterPosX-150, 00 );
    //layerSpeaker.move( 0 , 0);


    // add the layer to the stage
    filterStage[ id ].add(layerSpeaker[ id ]);
    filterStage[ id ].add(layerFilter[ id ]);

}