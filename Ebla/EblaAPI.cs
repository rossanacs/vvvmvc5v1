/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace EblaAPI
{
    /// <summary>
    /// The Ebla API exposes the services provided by Ebla - a specialised corpus management server
    /// that allows multiple versions of a document (typically, translations) to be aligned with a single
    /// base document in a variety of ways, both to analyse variation and support parallel concordance
    /// techniques. The API is designed to allow its interfaces to be exposed via SOAP, so avoids features
    /// which can make that problematic (e.g. properties, methods that return interfaces, etc.)
    /// </summary>
    internal class NamespaceDoc
    {

    }

    /// <summary>
    /// The VariationMetricTypes enum allows the type of formula to be used for variation calculation to be specified.
    /// </summary>
    public enum VariationMetricTypes
    {
        /// <summary>
        /// TypeA (based on Euclidean distance)
        /// </summary>
        metricA = 1,
        /// <summary>
        /// TypeB (original Viv/eddy formulae)
        /// </summary>
        metricB,
        /// <summary>
        /// TypeC (where Viv = standard deviation of TypeA Eddy values)
        /// </summary>
        metricC,
        /// <summary>
        /// TypeD (based on Dice's coefficient)
        /// </summary>
        metricD,
        /// <summary>
        /// TypeE (based on cosine similarity)
        /// </summary>
        metricE
    }

    /// <summary>
    /// The ICorpusStore interface exposes information and operations that concern the entire corpus
    /// store, rather than any single corpus.
    /// </summary>
    [ServiceContract]
    public interface ICorpusStore
    {
        /// <summary>
        /// Open the corpus store, specifying user credentials.
        /// </summary>
        /// <param name="Username">The username for authentication.</param>
        /// <param name="Password">The password for authentication.</param>
        /// <returns>True if credentials were valid, false otherwise.</returns>
        [OperationContract]
        bool Open(string Username, string Password);

        /// <summary>
        /// Returns a list of names of corpora in the corpus store that the user has rights to read.
        /// </summary>
        /// <returns>List of corpus names.</returns>
        [OperationContract]
        string[] GetCorpusList();

        /// <summary>
        /// Create a new corpus in the store.
        /// </summary>
        /// <param name="Name">Name of the new corpus.</param>
        /// <param name="Description">Optional corpus description.</param>
        /// <param name="BaseTextLanguageCode"></param>
        /// <remarks>The user calling this method must be an administrator.</remarks>
        [OperationContract]
        void CreateCorpus(string Name, string Description, string BaseTextLanguageCode);
        /// <summary>
        /// Delete a corpus from the corpus store.
        /// </summary>
        /// <param name="Name">Name of corpus to delete.</param>
        /// <remarks>The user calling this method must be an administrator.</remarks>
        [OperationContract]
        void DeleteCorpus(string Name);

        /// <summary>
        /// Rename a corpus in the store.
        /// </summary>
        /// <param name="OldName">The existing name of the corpus.</param>
        /// <param name="NewName">The new name for the corpus.</param>
        [OperationContract]
        void RenameCorpus(string OldName, string NewName);

        /// <summary>
        /// Create a new corpus store user.
        /// </summary>
        /// <param name="Username">Name of the new user.</param>
        /// <param name="Password">Password for the new user.</param>
        /// <param name="Email">Email address of the new user.</param>
        /// <param name="CanAdmin">True if the user is an administrator, False otherwise.</param>
        /// <remarks>The user calling this method must be an administrator. Initially the new user will have no rights, unless being created as another administrator.</remarks>
        [OperationContract]
        void CreateUser(string Username, string Password, bool CanAdmin, string Email);
        /// <summary>
        /// Delete a user from the corpus store.
        /// </summary>
        /// <param name="Username">Name of user to delete.</param>
        [OperationContract]
        void DeleteUser(string Username);

        /// <summary>
        /// Update the details of a corpus store user.
        /// </summary>
        /// <param name="Username">Name of the user to update.</param>
        /// <param name="CanAdmin">True if the user is an administrator, False otherwise.</param>
        /// <param name="Email">The user's email address.</param>
        [OperationContract]
        void UpdateUser(string Username, bool CanAdmin, string Email);

        /// <summary>
        /// Change the password of the user who opened the corpus store.
        /// </summary>
        /// <param name="NewPassword">The new password.</param>
        [OperationContract]
        void ChangePassword(string NewPassword);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Username"></param>
        /// <param name="UserEmail"></param>
        [OperationContract]
        void ResetPassword(string Username, string UserEmail);

        /// <summary>
        /// Get information on corpus store users.
        /// </summary>
        /// <param name="UsernameFilter">The name of the user for whom to return information, or null to return information for all users.</param>
        /// <param name="CorpusFilter">The name of the corpus for which to return information, or null to return information for all corpora.</param>
        /// <returns>Array of UserInfo structures, one per user, specifying user details, each containing an array specifying corpus rights.</returns>
        [OperationContract]
        UserInfo[] GetUserInfo(string UsernameFilter, string CorpusFilter);

        /// <summary>
        /// Sets a user's rights to a given corpus.
        /// </summary>
        /// <param name="CorpusName">Name of the corpus for which rights are being set.</param>
        /// <param name="Username">Name of the user for which rights are being set.</param>
        /// <param name="CanRead">True if the user should have rights to see the corpus, False otherwise.</param>
        /// <param name="CanWrite">True if the user should have rights to modify the corpus, False otherwise. If CanRead is False, this parameter is ignored.</param>
        /// <remarks>The user calling this method must be an administrator.</remarks>
        [OperationContract]
        void SetUserCorpusRights(string CorpusName, string Username, bool CanRead, bool CanWrite);

        /// <summary>
        /// Get cultures
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        SortedDictionary<string, string> GetCultures();

    }

    /// <summary>
    /// Structure providing information about a corpus user.
    /// </summary>
    [DataContract]
    public class UserInfo
    {
        /// <summary>
        /// The username.
        /// </summary>
        [DataMember]
        public string Username;

        /// <summary>
        /// True if the user is an administrator, False otherwise.
        /// </summary>
        [DataMember]
        public bool IsAdmin;

        /// <summary>
        /// The user's email address.
        /// </summary>
        [DataMember]
        public string Email;

        /// <summary>
        /// An array of CorpusRights structure specifying the user's corpus rights.
        /// </summary>
        [DataMember]
        public CorpusRights[] CorpusRights;
    }

    /// <summary>
    /// Structure providing information about corpus rights for a given user.
    /// </summary>
    [DataContract]
    public class CorpusRights
    {
        /// <summary>
        /// The name of the corpus.
        /// </summary>
        [DataMember]
        public string CorpusName;

        /// <summary>
        /// True if the user has permission to see data in the corpus, False otherwise.
        /// </summary>
        [DataMember]
        public bool CanRead;

        /// <summary>
        /// True if the user has permission to change data in the corpus, False otherwise.
        /// </summary>
        [DataMember]
        public bool CanWrite;

    }

    /// <summary>
    /// The ICorpus interface exposes information and operations for an individual corpus.
    /// Each corpus contains a 'base' document, and a number of 'versions' of that document
    /// (typically, translations of that document, all into the same target language).
    /// To use the interface for operating on a corpus, first invoke the 'Open' method.
    /// </summary>
    [ServiceContract]
    public interface ICorpus
    {
        /// <summary>
        /// Open the corpus, specifying user credentials.
        /// </summary>
        /// <param name="Name">The name of the corpus to open.</param>
        /// <param name="Username">The username for authentication.</param>
        /// <param name="Password">The password for authentication.</param>
        /// <returns>True if credentials were valid, false otherwise.</returns>
        [OperationContract]
        bool Open(string Name, string Username, string Password);

        /// <summary>
        /// Get the optional description of the corpus.
        /// </summary>
        /// <returns>The corpus description.</returns>
        [OperationContract]
        string GetDescription();

        /// <summary>
        /// Set the optional description of the corpus.
        /// </summary>
        /// <param name="Description">The new corpus description.</param>
        [OperationContract]
        void SetDescription(string Description);

        /// <summary>
        /// Upload the HTML document that acts as the 'base' within the corpus.
        /// </summary>
        /// <param name="BaseHtml">The HTML document content.</param>
        /// <remarks>The user calling this method must be an administrator. If a 'base' document has already been uploaded to the corpus, 
        /// and then a modified 'base' is uploaded thereafter, any segmentation nformation for the first 'base' is lost,
        /// as therefore is any alignment information between the first 'base' and any 'versions'.</remarks>
        [OperationContract]
        HtmlErrors UploadBaseText(string BaseHtml);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BaseHtml"></param>
        /// <param name="IDChanges"></param>
        /// <returns></returns>
        [OperationContract]
        HtmlErrors UploadBaseText(string BaseHtml, Dictionary<int, int> IDChanges);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BaseHtml"></param>
        /// <returns></returns>
        [OperationContract]
        HtmlErrors UpdateBaseText(string BaseHtml);

        /// <summary>
        /// Create storage within the corpus for a new 'version'.
        /// </summary>
        /// <param name="Name">The name to be used to refer to the new 'version'.</param>
        /// <param name="metadata">Metadata details for the version.</param>
        [OperationContract]
        void CreateVersion(string Name, DocumentMetadata metadata);

        /// <summary>
        /// Delete a 'version' from the corpus.
        /// </summary>
        /// <param name="Name">The name of the 'version' to delete.</param>
        [OperationContract]
        void DeleteVersion(string Name);

        /// <summary>
        /// Rename a 'version' in the corpus.
        /// </summary>
        /// <param name="OldName">The existing name of the 'version'.</param>
        /// <param name="NewName">The new name for the 'version'.</param>
        [OperationContract]
        void RenameVersion(string OldName, string NewName);

        /// <summary>
        /// Upload the HTML document for a given 'version'.
        /// </summary>
        /// <param name="Name">The name of the version concerned.</param>
        /// <param name="VersionHtml">The HTML document content.</param>
        /// <remarks>The user calling this method must be an administrator. If a document for this named 'version' has already been uploaded to the corpus, 
        /// and then a modified document for this named 'version' is uploaded thereafter, any segmentation nformation for this named 'version' is lost,
        /// as therefore is any alignment information between the 'base' document and this named 'version'.</remarks>
        [OperationContract]
        HtmlErrors UploadVersion(string Name, string VersionHtml);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="VersionHtml"></param>
        /// <param name="IDChanges"></param>
        /// <returns></returns>
        [OperationContract]
        HtmlErrors UploadVersion(string Name, string VersionHtml, Dictionary<int, int> IDChanges);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Name"></param>
        /// <param name="VersionHtml"></param>
        /// <returns></returns>
        [OperationContract]
        HtmlErrors UpdateVersion(string Name, string VersionHtml);

        /// <summary>
        /// Returns a list of the names of the versions in the corpus.
        /// </summary>
        /// <returns>List of version names.</returns>
        [OperationContract]
        string[] GetVersionList();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BaseTextFilter"></param>
        /// <param name="VersionFilter"></param>
        /// <param name="BaseTextAttributeFilters"></param>
        /// <returns></returns>
        [OperationContract]
        SegmentDefinition[] Search(string BaseTextFilter, string VersionFilter, SegmentAttribute[] BaseTextAttributeFilters);

        /// <summary>
        /// Used to start background calculation of variation statistics for segments, which are then stored in the database as segment attributes.
        /// Once started, ContinueSegmentVariationCalculation should be called repeatedly until it returns False.
        /// </summary>
        /// <param name="metricType">The type of variation metric to use.</param>
        [OperationContract]
        void StartSegmentVariationCalculation(VariationMetricTypes metricType);

        /// <summary>
        /// Used to continue background calculation of variation statistics for segments.
        /// </summary>
        /// <param name="progress">A percentage value expressing the amount of calculation completed so far.</param>
        /// <returns>True if there are segments outstanding for calculation, False otherwise.</returns>
        [OperationContract]
        bool ContinueSegmentVariationCalculation(out int progress);

        /// <summary>
        /// Deprecated. Used to invoke immediate calculation of variation statistics for a segment.
        /// </summary>
        /// <param name="BaseTextSegmentID"></param>
        /// <param name="metricType"></param>
        /// <param name="VariationVersionList"></param>
        /// <returns></returns>
        [OperationContract]
        SegmentVariation CalculateSegmentVariation(int BaseTextSegmentID, VariationMetricTypes metricType, List<string> VariationVersionList);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BaseTextSegmentID"></param>
        /// <param name="metricType"></param>
        /// <returns></returns>
        [OperationContract]
        SegmentVariation RetrieveSegmentVariation(int BaseTextSegmentID, VariationMetricTypes metricType);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="VersionName"></param>
        /// <param name="metricType"></param>
        /// <returns></returns>
        [OperationContract]
        double RetrieveAverageVariation(string VersionName, VariationMetricTypes metricType);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BaseTextSegmentID"></param>
        /// <param name="metricType"></param>
        /// <returns></returns>
        [OperationContract]
        SegmentVariationData RetrieveSegmentVariationData(int BaseTextSegmentID, VariationMetricTypes metricType);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BaseTextAttributeFilters"></param>
        /// <param name="BaseTextSegmentIds"></param>
        /// <param name="metricType"></param>
        /// <param name="VariationVersionList"></param>
        /// <param name="getSVDs"></param>
        /// <returns></returns>
        [OperationContract]
        MultipleSegmentVariationData RetrieveMultipleSegmentVariationData(SegmentAttribute[] BaseTextAttributeFilters, List<int> BaseTextSegmentIds, VariationMetricTypes metricType, List<string> VariationVersionList, bool getSVDs);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IncludeBuiltIn"></param>
        /// <returns></returns>
        [OperationContract]
        PredefinedSegmentAttribute[] GetPredefinedSegmentAttributes(bool IncludeBuiltIn);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PredefinedSegmentAttribute"></param>
        [OperationContract]
        void CreatePredefinedSegmentAttribute(PredefinedSegmentAttribute PredefinedSegmentAttribute);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PredefinedSegmentAttribute"></param>
        [OperationContract]
        void UpdatePredefinedSegmentAttribute(PredefinedSegmentAttribute PredefinedSegmentAttribute);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID"></param>
        [OperationContract]
        void DeletePredefinedSegmentAttribute(int ID);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [OperationContract]
        PredefinedSegmentAttribute GetPredefinedSegmentAttribute(int ID);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [OperationContract]
        HtmlErrors CreateUploadTextAndFiles(UploadedFile file);

        /// <summary>
        /// Create auto segmentation for all the documents in the corpus.
        /// </summary>
        /// <param name="corpusName"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [OperationContract]
        string CreateCorpusAutoSegmentation(string corpusName, SegmentationTypes type);

        /// <summary>
        /// Create auto alignment for all the documents in the corpus.
        /// </summary>
        /// <param name="corpusName"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <param name="algorithm"></param>
        /// <returns></returns>
        string CreateCorpusAutoAlignments(string corpusName, string userName, string password, AlignmentAlgorithms algorithm);
    }

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class SegmentVariationData
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int BaseTextSegmentID;

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string[] VersionNames;

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public double[] EddyValues;

    }

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class SVDPt
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public double X;
        /// <summary>
        /// 
        /// </summary>
        public double Y;
    }

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class MultipleSegmentVariationData
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int[] BaseTextSegmentIDs;

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string[] VersionNames;

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public double[][] EddyValues;

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public double[] VivValues;

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int[] BaseTextTotalTokenCounts;

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public List<SVDPt>[] SVDs;
    }


    /// <summary>
    /// Deprecated. Structure used to return segment variation information.
    /// </summary>
    [DataContract]
    public class SegmentVariation
    {
        /// <summary>
        /// Deprecated.
        /// </summary>
        [DataMember]
        public int BaseTextSegmentID;

        /// <summary>
        /// Deprecated.
        /// </summary>
        [DataMember]
        public string BaseTextContent;

        /// <summary>
        /// Deprecated.
        /// </summary>
        [DataMember]
        public double VivValue;

        /// <summary>
        /// Deprecated.
        /// </summary>
        [DataMember]
        public VersionSegmentVariation[] VersionSegmentVariations;

        /// <summary>
        /// Deprecated.
        /// </summary>
        [DataMember]
        public TokenInfo[] TokenInfo;
    }

    /// <summary>
    /// Deprecated.
    /// </summary>
    [DataContract]
    public class TokenInfo
    {
        /// <summary>
        /// Deprecated.
        /// </summary>
        [DataMember]
        public string Token;

        /// <summary>
        /// Deprecated.
        /// </summary>
        [DataMember]
        public double AverageCount;
    }

    /// <summary>
    /// Deprecated.
    /// </summary>
    [DataContract]
    public class VersionSegmentVariation
    {
        /// <summary>
        /// Deprecated.
        /// </summary>
        [DataMember]
        public string VersionName;

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string LanguageCode;

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int? ReferenceDate;

        /// <summary>
        /// Deprecated.
        /// </summary>
        [DataMember]
        public int[] VersionSegmentIDs;

        /// <summary>
        /// Deprecated.
        /// </summary>
        [DataMember]
        public double EddyValue;

        /// <summary>
        /// Deprecated.
        /// </summary>
        [DataMember]
        public string VersionText;

        /// <summary>
        /// Deprecated.
        /// </summary>
        [DataMember]
        public TokenInfo[] TokenInfo;
    }

    /// <summary>
    /// Used to specify attributes for a segment that has been defined for a document, above and beyond
    /// the start and end positions of the segment, such as information on what type of textual component
    /// the segment represents (chapter, scene, speech, etc.), or any other information that may be
    /// useful for selecting segments of interest.
    /// </summary>
    [DataContract]
    public class SegmentAttribute
    {
        /// <summary>
        /// Name of the attribute, such as 'Type'.
        /// </summary>
        [DataMember]
        public string Name;
        /// <summary>
        /// Value of the attribute, such as 'Chapter'.
        /// </summary>
        [DataMember]
        public string Value;

    }

    /// <summary>
    /// The type of an attribute definition.
    /// </summary>
    public enum PredefinedSegmentAttributeType
    {
        /// <summary>
        /// Text - the attribute value can be any string
        /// </summary>
        Text,
        /// <summary>
        /// Number - the attribute value can be any number
        /// </summary>
        Number,
        /// <summary>
        /// List - the attribute value must be chosen from a configured list.
        /// </summary>
        List

    }

    /// <summary>
    /// Defines an attribute (and optionally, a list of possible Values) that can be given to a segment.
    /// </summary>
    [DataContract]
    public class PredefinedSegmentAttribute
    {
        /// <summary>
        /// ID of this attribute definition.
        /// </summary>
        [DataMember]
        public int ID;

        /// <summary>
        /// Name of this attribute definition.
        /// </summary>
        [DataMember]
        public string Name;

        /// <summary>
        /// If True, segments having an attribute with this Name will generate an entry in the table of contents. The entry text will be the Value of the attribute.
        /// </summary>
        [DataMember]
        public Boolean ShowInTOC;

        /// <summary>
        /// If True, is a built-in attribute that can be emitted but not edited in the same way as other attributes
        /// </summary>
        [DataMember]
        public Boolean ReadOnly;


        /// <summary>
        /// The type of the attribute definition.
        /// </summary>
        [DataMember]
        public PredefinedSegmentAttributeType AttributeType;

        /// <summary>
        /// Where applicable, the possible Values that the attribute can have.
        /// </summary>
        [DataMember]
        public PredefinedSegmentAttributeValue[] Values;

    }

    /// <summary>
    /// For attribute definitions whose Values are taken from a list, defines one of the Values and associates other information with it.
    /// </summary>
    [DataContract]
    public class PredefinedSegmentAttributeValue
    {
        /// <summary>
        /// The Value that the attribute can have.
        /// </summary>
        [DataMember]
        public string Value;

        /// <summary>
        /// If True, indicates segments having an attribute with the given name and this Value should be shown in a different colour.
        /// </summary>
        [DataMember]
        public bool ApplyColouring;

        /// <summary>
        /// If ApplyColouring is True, the colour to apply (6-digit hexadecimal colour code, e.g. 'F2FF80')
        /// </summary>
        [DataMember]
        public string ColourCode;

    }

    /// <summary>
    /// Structure returning details of errors encountered in an HTML document when trying to upload it.
    /// </summary>
    [DataContract]
    public class HtmlErrors
    {
        /// <summary>
        /// An array of HtmlParseError structures representing errors found by the HTML parser.
        /// </summary>
        public HtmlParseError[] HtmlParseErrors;
    }

    /// <summary>
    /// Structure providing details of an error encountered in an HTML document.
    /// </summary>
    [DataContract]
    public class HtmlParseError
    {
        /// <summary>
        /// Line number on which the error occurred.
        /// </summary>
        [DataMember]
        public int Line;

        /// <summary>
        /// The text causing the error.
        /// </summary>
        [DataMember]
        public string SourceText;

        /// <summary>
        /// Position on the line at which the error was caused.
        /// </summary>
        [DataMember]
        public int LinePosition;

        /// <summary>
        /// Position within the stream of the error.
        /// </summary>
        [DataMember]
        public int StreamPosition;

        /// <summary>
        /// Description of the error.
        /// </summary>
        [DataMember]
        public string Reason;

    }


    /// <summary>
    /// Used to define a section of a document. Once defined, a segment can be aligned
    /// with segments in other documents.
    /// </summary>
    /// <remarks>Segment definitions can include mismatched HTML tags, that is, a segment might cover some text that includes an
    /// opening 'bold' tag. but not the closing 'bold' tag. </remarks>
    [DataContract]
    public class SegmentDefinition
    {
        /// <summary>
        /// Unique identifier of the segment within its document.
        /// </summary>
        [DataMember]
        public int ID;

        /// <summary>
        /// Zero-based position in the document file of where the segment starts.
        /// </summary>
        /// <remarks>The StartPosition value excludes any markup characters and all whitespace. HTML escape sequences do not count as a single character.</remarks>
        [DataMember]
        public int StartPosition;
        /// <summary>
        /// Length of the segment, in characters.
        /// </summary>
        /// <remarks>The Length value excludes any markup characters and all whitespace. HTML escape sequences do not count as a single character.</remarks>
        [DataMember]
        public int Length;
        /// <summary>
        /// Array of attributes providing additional information on the segment.
        /// </summary>
        [DataMember]
        public SegmentAttribute[] Attributes;

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public SegmentContentBehaviour ContentBehaviour;

        /// <summary>
        /// 
        /// </summary>
        public int EndPosition
        {
            get
            {
                return StartPosition + Length;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="segdef"></param>
        /// <returns></returns>
        public bool Overlaps(SegmentDefinition segdef)
        {
            if ((segdef.StartPosition + segdef.Length) <= StartPosition)
                return false;
            if (segdef.StartPosition >= (StartPosition + Length))
                return false;
            return true;
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public enum SegmentContentBehaviour
    {
        /// <summary>
        /// 
        /// </summary>
        normal = 0,
        /// <summary>
        /// 
        /// </summary>
        exclude,
        /// <summary>
        /// 
        /// </summary>
        structure
    }

    /// <summary>
    /// Represents the type of a given SegmentAlignment.
    /// </summary>
    public enum AlignmentTypes
    {
        /// <summary>
        /// A normal SegmentAlignment.
        /// </summary>
        normalAlign,
        /// <summary>
        /// A 'null' SegmentAlignment (see design documentation). Null SegmentAlignments can only between made between a SegmentDefinition 
        /// with zero length and another SegmentDefinition with non-zero length.
        /// </summary>
        nullAlign
    }

    /// <summary>
    /// Represents the status of a given SegmentAlignment.
    /// </summary>
    public enum AlignmentStatuses
    {
        /// <summary>
        /// A draft SegmentAlignment.
        /// </summary>
        draft,
        /// <summary>
        /// A confirmed SegmentAlignment.
        /// </summary>
        confirmed
    }

    /// <summary>
    /// Represents the existing alignment algorithms.
    /// </summary>
    public enum AlignmentAlgorithms
    {
        ///// <summary>
        ///// Moore Aligner.
        ///// </summary>
        //Moore,
        /// <summary>
        /// Gale and Church Alignment.
        /// </summary>
        GaleAndChurch
    }

    /// <summary>
    /// Represents the existing segmentations for auto-segmentation.
    /// </summary>
    public enum SegmentationTypes
    {
        /// <summary>
        ///Paragraph.
        /// </summary>
        Paragraph
        //,
        ///// <summary>
        ///// End of line.
        ///// </summary>
        //EndOfLine
    }

    /// <summary>
    /// Represents a single alignment between a segment definition in the 'base' document and
    /// a segment definition in one of the 'versions'.
    /// </summary>
    [DataContract]
    public class SegmentAlignment
    {
        /// <summary>
        /// Unique identifier of the SegmentAlignment within its AlignmentSet.
        /// </summary>
        [DataMember]
        public int ID;
        /// <summary>
        /// The type of the SegmentAlignment.
        /// </summary>
        [DataMember]
        public AlignmentTypes AlignmentType;
        /// <summary>
        /// The status of the SegmentAlignment.
        /// </summary>
        [DataMember]
        public AlignmentStatuses AlignmentStatus;
        /// <summary>
        /// The unique ID(s) of the segment definition(s) in the 'base' document being aligned.
        /// </summary>
        [DataMember]
        public int[] SegmentIDsInBaseText;
        /// <summary>
        /// The unique IDs of the segment definition(s) in the 'version' document being aligned.
        /// </summary>
        [DataMember]
        public int[] SegmentIDsInVersion;
        /// <summary>
        /// Any notes to associated with this SegmentAlignment.
        /// </summary>
        [DataMember]
        public string Notes;
        /// <summary>
        /// Score value in case of auto alignment.
        /// </summary>
        public float[] Scores { get; set; }
    }

    /// <summary>
    /// The IAlignmentSet interface exposes information and operations for the set of alignments between segment definitions in
    /// the 'base' document and those in a given 'version.
    /// </summary>
    [ServiceContract]
    public interface IAlignmentSet
    {
        /// <summary>
        /// Open a given alignment set within the corpus.
        /// </summary>
        /// <param name="CorpusName">Name of the corpus concerned.</param>
        /// <param name="VersionName">Name of the version concerned.</param>
        /// <param name="Username">The username for authentication.</param>
        /// <param name="Password">The password for authentication.</param>
        /// <returns>True if credentials were valid, false otherwise.</returns>
        [OperationContract]
        bool Open(string CorpusName, string VersionName, string Username, string Password);

        /// <summary>
        /// Create a new SegmentAlignment within the alignment set.
        /// </summary>
        /// <param name="NewAlignment">The SegmentAlignment to create (note: ID is ignored)</param>
        [OperationContract]
        int CreateAlignment(SegmentAlignment NewAlignment);

        /// <summary>
        /// Get a SegmentAlignment by its ID.
        /// </summary>
        /// <param name="ID">The ID of the SegmentAlignment.</param>
        /// <returns>The SegmentAlignment with the specified ID.</returns>
        [OperationContract]
        SegmentAlignment GetAlignment(int ID);

        /// <summary>
        /// Update a SegmentAlignment within the alignment set.
        /// </summary>
        /// <param name="UpdatedAlignment">The modified SegmentAlignment (note: ID identifies the SegmentAlignment to update).</param>
        [OperationContract]
        void UpdateAlignment(SegmentAlignment UpdatedAlignment);

        /// <summary>
        /// Delete a SegmentAlignment by its ID.
        /// </summary>
        /// <param name="ID">ID of the SegmentAlignment to delete.</param>
        [OperationContract]
        void DeleteAlignment(int ID);

        /// <summary>
        /// Delete alignments matching a given set of criteria
        /// </summary>
        /// <param name="DraftOnly">if True, only delete alignments with 'draft' status.</param>
        /// <param name="BaseTextAttributeFilters">If non-null, only delete alignments where the base text segment(s) has/have all of the attributes specified.</param>
        /// <param name="VersionAttributeFilters">If non-null, only delete alignments where the version segment(s) has/have all of the attributes specified.</param>
        /// <param name="basetextStartPos">If non-zero, only delete alignments where segments start at or beyond this character offset in the base text.</param>
        /// <param name="baseTextLength"></param>
        /// <param name="versionStartPos"></param>
        /// <param name="versionLength"></param>
        [OperationContract]
        void DeleteAlignments(bool DraftOnly, SegmentAttribute[] BaseTextAttributeFilters, SegmentAttribute[] VersionAttributeFilters, int basetextStartPos, int baseTextLength, int versionStartPos, int versionLength);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BaseTextAttributeFilters"></param>
        /// <param name="VersionAttributeFilters"></param>
        /// <param name="basetextStartPos"></param>
        /// <param name="baseTextLength"></param>
        /// <param name="versionStartPos"></param>
        /// <param name="versionLength"></param>
        /// <returns></returns>
        [OperationContract]
        SegmentAlignment[] FindAlignments(SegmentAttribute[] BaseTextAttributeFilters, SegmentAttribute[] VersionAttributeFilters, int basetextStartPos, int baseTextLength, int versionStartPos, int versionLength);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="segIdFilter"></param>
        /// <param name="baseText"></param>
        /// <returns></returns>
        [OperationContract]
        SegmentAlignment FindAlignment(int segIdFilter, bool baseText);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseTextSegId"></param>
        /// <param name="versionSegId"></param>
        /// <returns></returns>
        [OperationContract]
        SegmentAlignment FindAlignment(int baseTextSegId, int versionSegId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BaseTextAttributeFilters"></param>
        /// <param name="VersionAttributeFilters"></param>
        /// <param name="basetextStartPos"></param>
        /// <param name="baseTextLength"></param>
        /// <param name="versionStartPos"></param>
        /// <param name="versionLength"></param>
        [OperationContract]
        void ConfirmAlignments(SegmentAttribute[] BaseTextAttributeFilters, SegmentAttribute[] VersionAttributeFilters, int basetextStartPos, int baseTextLength, int versionStartPos, int versionLength);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="translationDocument"></param>
        /// <param name="results"></param>
        /// <param name="progress"></param>
        [OperationContract]
        bool CreateMooreAlignments(TranslationDocument translationDocument, List<BestAlignment> results, out int progress);

    }

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class DocumentMetadata
    {
        /// <summary>
        /// Version Name
        /// </summary>
        [DataMember]
        [Display(Name = nameof(Ebla.EblaResources.NameIfVersion), ResourceType = typeof(Ebla.EblaResources))]
        public string NameIfVersion;

        /// <summary>
        /// Description
        /// </summary>
        [DataMember]
        [Display(Name=nameof(Ebla.EblaResources.Description), ResourceType =typeof(Ebla.EblaResources))]
        public string Description;

        /// <summary>
        /// Information
        /// </summary>
        [DataMember]
        [Display(Name = nameof(Ebla.EblaResources.Information), ResourceType = typeof(Ebla.EblaResources))]
        public string Information;

        /// <summary>
        /// Copyright Information
        /// </summary>
        [DataMember]
        [Display(Name = nameof(Ebla.EblaResources.CopyrightInfo), ResourceType = typeof(Ebla.EblaResources))]
        public string CopyrightInfo;

        /// <summary>
        /// Author Translator
        /// </summary>
        [DataMember]
        [Display(Name = nameof(Ebla.EblaResources.AuthorTranslator), ResourceType = typeof(Ebla.EblaResources))]
        public string AuthorTranslator;

        /// <summary>
        /// Reference Date
        /// </summary>
        [DataMember]
        [Display(Name = nameof(Ebla.EblaResources.ReferenceDate), ResourceType = typeof(Ebla.EblaResources))]
        public int? ReferenceDate;

        /// <summary>
        /// Genre
        /// </summary>
        [DataMember]
        [Display(Name = nameof(Ebla.EblaResources.Genre), ResourceType = typeof(Ebla.EblaResources))]
        public string Genre;

        /// <summary>
        /// ISO 639-2 language code
        /// </summary>
        [DataMember]
        [Display(Name = nameof(Ebla.EblaResources.LanguageCode), ResourceType = typeof(Ebla.EblaResources))]
        public string LanguageCode;

    }

    /// <summary>
    /// Result from converted HTML files
    /// </summary>
    [DataContract]
    public class HtmlDocumentResult
    {
        /// <summary>
        /// File content (body)
        /// </summary>
        [DataMember]
        public string Content;

        /// <summary>
        /// Document metadata
        /// </summary>
        [DataMember]
        public DocumentMetadata Metadata;
    }

    public class SegmentFilter
    {
        public SegmentAttribute[] AttributeFilters;

        public SegmentContentBehaviour? SegmentContentBehaviourFilter;
    }

    /// <summary>
    /// The IDocument interface exposes information and operations for an individual document within the corpus.
    /// To use the interface for operating on a document, first invoke either the 'OpenBaseText' or 'OpenVersion' method.
    /// </summary>
    [ServiceContract]
    public interface IDocument
    {
        /// <summary>
        /// Open the 'base' document within a given corpus.
        /// </summary>
        /// <param name="CorpusName">Name of the corpus concerned.</param>
        /// <param name="Username">The username for authentication.</param>
        /// <param name="Password">The password for authentication.</param>
        /// <returns>True if credentials were valid, false otherwise.</returns>
        [OperationContract]
        bool OpenBaseText(string CorpusName, string Username, string Password);

        /// <summary>
        /// Open one of the 'version' documents within a given corpus.
        /// </summary>
        /// <param name="CorpusName">Name of the corpus concerned.</param>
        /// <param name="VersionName">Name of the version concerned.</param>
        /// <param name="Username">The username for authentication.</param>
        /// <param name="Password">The password for authentication.</param>
        /// <returns>True if credentials were valid, false otherwise.</returns>
        [OperationContract]
        bool OpenVersion(string CorpusName, string VersionName, string Username, string Password);

        /// <summary>
        /// Get the metadata for this document.
        /// </summary>
        /// <returns>The document metadata.</returns>
        [OperationContract]
        DocumentMetadata GetMetadata();

        /// <summary>
        /// Set the metadata for this document.
        /// </summary>
        /// <param name="Metadata">The document metadata.</param>
        [OperationContract]
        void SetMetadata(DocumentMetadata Metadata);


        /// <summary>
        /// Length of the document, in characters.
        /// </summary>
        /// <returns>The document length, or zero if no document has yet been uploaded.</returns>
        /// <remarks>The Length value excludes any markup characters. HTML escape sequences do not count as a single character. Whitespace is preserved when measuring Length.</remarks>
        [OperationContract]
        int Length();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        long GetSegmentCount();

        /// <summary>
        /// Returns a portion of the HTML document content. 
        /// </summary>
        /// <param name="StartPosition">Zero-based position within the document file of where the portion starts. Minimum value = 0 (corresponds to where HTML 'body' content began in original document uploaded).</param>
        /// <param name="Length">Length of the portion, in characters.</param>
        /// <param name="addSegmentMarkers">If True, inserts additional elements (with 'data' attributes containing segment information) to show the start and end of any segment definitions contained within the portion.</param>
        /// <param name="addSegmentFormatting">If True, inserts additional elements (with 'data' attributes containing segment information) to enclose the text any segment definitions contained within the portion.</param>
        /// <param name="AttributeFilters">Array of attributes that segment definitions must match, such as ('Type', 'Chapter'), ('Ordinal', '1') or ('Speaker', 'Othello'), etc.</param>
        /// <param name="forEdit"></param>
        /// <param name="AttributesToEmit"></param>
        /// <param name="VariationVersionList"></param>
        /// <param name="metricTypes"></param>
        /// <returns>The HTML content requested - not rendered into a complete, valid HTML document, but as content that can be inserted into the body of another HTML document.</returns>
        /// <remarks>The StartPosition and Length values exclude any markup characters. HTML escape sequences do not count as a single character. Whitespace is preserved when computing these values. If the portion includes unmatched tags, the function will attempt to append or prepend tags 
        /// so that tags match. For example, if the portion includes a closing 'bold' tag, but not the opening tag, the function will prepend the opening tag to the 
        /// string before returning it. Tags will also be appended and prepended as required to ensure that the content requested has the same formatting as when the document is viewed as a whole.</remarks>
        [OperationContract]
        string GetDocumentContent(int StartPosition, int Length, bool addSegmentMarkers, bool addSegmentFormatting, SegmentAttribute[] AttributeFilters, bool forEdit, string[] AttributesToEmit, string[] VariationVersionList, List<VariationMetricTypes> metricTypes);

        [OperationContract]
        string GetDocumentContent2(int StartPosition, int Length, bool addSegmentMarkers, bool addSegmentFormatting, SegmentFilter SegmentFilter, bool forEdit, string[] AttributesToEmit, string[] VariationVersionList, List<VariationMetricTypes> metricTypes);

        /// <summary>
        /// Returns a portion of the HTML document content, stripped of markup, inserting spaces where necessary to avoid running words together.
        /// </summary>
        /// <param name="StartPosition">Zero-based position within the document file of where the portion starts. Minimum value = 0 (corresponds to where HTML 'body' content began in original document uploaded).</param>
        /// <param name="Length">Length of the portion, in characters.</param>
        /// <returns></returns>
        [OperationContract]
        string GetDocumentContentText(int StartPosition, int Length);

        /// <summary>
        /// Create a new SegmentDefinition for the document.
        /// </summary>
        /// <param name="NewSegmentDefinition">The SegmentDefinition to create (note: ID is ignored).</param>
        /// <returns>The ID of the new segment.</returns>
        [OperationContract]
        int CreateSegmentDefinition(SegmentDefinition NewSegmentDefinition);

        /// <summary>
        /// Update a SegmentDefinition within the document.
        /// </summary>
        /// <param name="UpdatedSegmentDefinition">The modified SegmentDefinition (note: ID identifies the SegmentDefinition to update).</param>
        /// <param name="ForceRetokenisation"></param>
        [OperationContract]
        void UpdateSegmentDefinition(SegmentDefinition UpdatedSegmentDefinition, bool ForceRetokenisation);

        /// <summary>
        /// Get a SegmentDefinition by its ID.
        /// </summary>
        /// <param name="ID">ID of the segment to return.</param>
        /// <returns>The segment with the specified ID.</returns>
        [OperationContract]
        SegmentDefinition GetSegmentDefinition(int ID);

        /// <summary>
        /// Find SegmentDefinitions within a specified portion of the text, optionally matching one or more attribute filters.
        /// </summary>
        /// <param name="StartPosition">Zero-based position from which to start matching segment definitions.</param>
        /// <param name="Length">Length of the text portion, in characters, within which to start matching segment definitions.</param>
        /// <param name="AttributeFilters">Array of attributes that segment definitions must match, such as ('Type', 'Chapter'), ('Ordinal', '1') or ('Speaker', 'Othello'), etc.</param>
        /// <param name="includeStartBefore">If True, matches segment definitions that start before the portion but end within it.</param>
        /// <param name="includeEndAfter">If True, matches segment definitions that start within the portion but end after it.</param>
        /// <param name="ContentBehaviourFilters"></param>
        /// <returns>Segment definitions that match the arguments provided.</returns>
        /// <remarks>The StartPosition and Length values exclude any markup characters. HTML escape sequences do not count as a single character. Whitespace is preserved when computing these values.</remarks>
        [OperationContract]
        SegmentDefinition[] FindSegmentDefinitions(int StartPosition, int Length, bool includeStartBefore, bool includeEndAfter, SegmentAttribute[] AttributeFilters, SegmentContentBehaviour[] ContentBehaviourFilters);

        /// <summary>
        /// Delete a SegmentDefinition by its ID.
        /// </summary>
        /// <param name="ID">ID of the SegmentDefinition to delete.</param>
        [OperationContract]
        void DeleteSegmentDefinition(int ID);

        /// <summary>
        /// Delete all segment definitions.
        /// </summary>
        [OperationContract]
        void DeleteAllSegmentDefinitions();

        /// <summary>
        /// AutoSegmentationAroundTag
        /// </summary>
        /// <param name="tag"></param>
        [OperationContract]
        void AutoSegmentationAroundTag(string tag);

        /// <summary>
        /// AutoSegmentationAfterTag
        /// </summary>
        /// <param name="tag"></param>
        [OperationContract]
        void AutoSegmentationAfterTag(string tag);

        /// <summary>
        /// 
        /// </summary>
        void AutoSegmentationPlay1Format();

        /// <summary>
        /// GetSegmentDefinitionLongAttribute
        /// </summary>
        /// <param name="SegmentDefinitionID"></param>
        /// <param name="AttributeName"></param>
        /// <returns></returns>
        [OperationContract]
        string GetSegmentDefinitionLongAttribute(int SegmentDefinitionID, string AttributeName);

        /// <summary>
        /// SetSegmentDefinitionLongAttribute
        /// </summary>
        /// <param name="SegmentDefinitionID"></param>
        /// <param name="LongAttribute"></param>
        [OperationContract]
        void SetSegmentDefinitionLongAttribute(int SegmentDefinitionID, SegmentAttribute LongAttribute);

        /// <summary>
        /// RemoveSegmentDefinitionLongAttribute
        /// </summary>
        /// <param name="SegmentDefinitionID"></param>
        /// <param name="AttributeName"></param>
        [OperationContract]
        void RemoveSegmentDefinitionLongAttribute(int SegmentDefinitionID, string AttributeName);



    }

    /// <summary>
    /// The ITranslationAligner interface exposes information and operations for the set of implemantations to Alignment models between segment definitions in
    /// the 'base' document and those in a given 'version.
    /// </summary>
    [ServiceContract]
    public interface ITranslationAligner
    {
        /// <summary>
        /// Apply Moore alignment on segments.
        /// </summary>
        /// <param name="translationDocument"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="progress">Process' progress.</param>
        /// <param name="corpusName"></param>
        /// <param name="versionName"></param>
        /// <returns>True if credentials were valid, false otherwise.</returns>
        [OperationContract]
        bool ApplyMooreAlignment(TranslationDocument translationDocument, string corpusName, string versionName, string username,
            string password, out int progress);

        /// <summary>
        /// Continue the auto alignment when having multiple transactions
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="progress"></param>
        /// <param name="translationDocument"></param>
        /// <param name="corpusName"></param>
        /// <param name="versionName"></param>
        /// <returns></returns>
        [OperationContract]
        bool ContinueAutoAlignment(TranslationDocument translationDocument, string corpusName, string versionName, string username,
            string password, out int progress);

        /// <summary>
        /// Apply moore alignment and create Ebla alignment without nested transactions
        /// </summary>
        /// <param name="corpusName"></param>
        /// <param name="versionName"></param>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        List<BestAlignment> ApplyAutoAlignmentAndCreateEblaAlignment(string corpusName, string versionName, string userName, string password);
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="corpusName"></param>
        /// <param name="versionName"></param>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [OperationContract]
        TranslationDocument GetMooreTranslationDocument(string corpusName, string versionName, string username,
            string password);
    }

    /// <summary>
    /// 
    /// </summary>
    public class Vocabulary
    {
        /// <summary>
        /// Vocabulary segments
        /// </summary>
        public List<AlignmentVocabulary> list { get; set; }
        /// <summary>
        /// List of tokens
        /// </summary>
        public List<string> TokensList { get; set; }
        //Token => [id, count]
        /// <summary>
        /// Map of tokens
        /// </summary>
        public Dictionary<string,int[]> TokenMap { get; set; }
    }

    /// <summary>
    /// Moore alignment translation base class
    /// </summary>
    public class TranslationDocument
    {
        /// <summary>
        /// Name of corpus
        /// </summary>
        public string CorpusName { get; set; }
        /// <summary>
        /// Name of the Version Text
        /// </summary>
        public string VersionName { get; set; }
        /// <summary>
        /// User name
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// User password
        /// </summary>
        public string UserPassword { get; set; }
        /// <summary>
        /// Base document
        /// </summary>
        public IDocument BaseDocument { get; set; }
        /// <summary>
        /// List of segmenttokens with attribute value: :totaltokens
        /// </summary>
        public SegmentTokens[] BaseDocumentSegmentsTotalTokens { get; set; }
        /// <summary>
        /// List of segmenttokens with attribute value: :wids
        /// </summary>
        public SegmentTokens[] BaseDocumentSegmentsWids { get; set; }
        /// <summary>
        /// List of segmenttokens with attribute value: :tokens
        /// </summary>
        public SegmentTokens[] BaseDocumentSegmentsTokens { get; set; }
        /// <summary>
        /// Version document
        /// </summary>
        public IDocument VersionDocument { get; set; }
        /// <summary>
        /// List of segmenttokens with attribute value: :totaltokens
        /// </summary>
        public SegmentTokens[] VersionDocumentSegmentsTotalTokens { get; set; }
        /// <summary>
        /// List of segmenttokens with attribute value: :wids
        /// </summary>
        public SegmentTokens[] VersionDocumentSegmentsWids { get; set; }
        /// <summary>
        /// List of segmenttokens with attribute value: :tokens
        /// </summary>
        public SegmentTokens[] VersionDocumentSegmentsTokens { get; set; }
        /// <summary>
        /// Vocabulary of base text
        /// </summary>
        public Vocabulary BaseVocabulary { get; set; }
        /// <summary>
        /// Vocabulary of version text
        /// </summary>
        public Vocabulary VersionVocabulary { get; set; }
    }

    /// <summary>
    /// Vocabulary token
    /// </summary>
    public class AlignmentVocabulary
    {
        /// <summary>
        /// Token id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Token
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Count
        /// </summary>
        public int Count { get; set; }
    }

    /// <summary>
    /// Represents a single segment with the long attribute value
    /// </summary>
    [DataContract]
    public class SegmentTokens
    {
        /// <summary>
        /// Unique identifier of the SegmentToken within the Document.
        /// </summary>
        [DataMember]
        public int Id;
        /// <summary>
        /// The segment start position.
        /// </summary>
        [DataMember]
        public int StartPosition;
        /// <summary>
        /// The length of the segment.
        /// </summary>
        [DataMember]
        public int Length;
        /// <summary>
        /// The document id which this segment belongs to.
        /// </summary>
        [DataMember]
        public int DocumentId;
        /// <summary>
        /// The attribute value.
        /// </summary>
        [DataMember]
        public int TotalTokensValue;
        /// <summary>
        /// The list of attribute values.
        /// </summary>
        [DataMember]
        public SegmentAttribute[] Attributes;

        /// <summary>
        /// All segment text.
        /// </summary>
        [DataMember]
        public string Segment;
    }

    /// <summary>
    /// Best alignment - Moore's alignment
    /// </summary>
    [DataContract]
    public class BestAlignment
    {
        /// <summary>
        /// List of segments from source texts
        /// </summary>
        [DataMember]
        public SegmentTokens[] BaseSegmentList { get; set; }
        /// <summary>
        /// List of segments from target texts
        /// </summary>
        [DataMember]
        public SegmentTokens[] VersionSegmentList { get; set; }
        /// <summary>
        /// Best score according to the applied alignment.
        /// </summary>
        [DataMember]
        public float Score { get; set; }
    }

    /// <summary>
    /// Uploaded file
    /// </summary>
    public class UploadedFile
    {
        /// <summary>
        /// 
        /// </summary>
        public string CorpusName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NameIfVersion { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Encoding { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Length { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string UploadType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Stream FileStream { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DocumentMetadata FileMetadata { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FileHtml { get; set; }
    }
}
