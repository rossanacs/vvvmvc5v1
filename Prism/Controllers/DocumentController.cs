﻿//#define USE_OLD_UPLOAD
/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EblaAPI;
using EblaImpl;
using Prism.Models;
using EblaImpl.Extractor;
using System.IO;
using System.Globalization;

namespace Prism.Controllers
{
    [Authorize]
    public class DocumentController : PrismController
    {
        #region Page rendering

        public ActionResult Index(string CorpusName, string NameIfVersion)
        {
            
            try
            {
                ViewData["extract"] = string.Empty;
                ViewData["nextattribfilterrowindex"] = (0).ToString();
                ViewData["extractindented"] = string.Empty;
                ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);
                //EblaAPI.PredefinedSegmentAttribute[] atts = corpus.GetPredefinedSegmentAttributes();
                //ViewData["colourdata"] = GetColouringData(atts);
            
                IDocument doc = PrismHelpers.GetDocument(CorpusName, NameIfVersion);

                ViewData["TOCSegments"]= ConstructContentList( doc);

                DocumentExtractModel model = new DocumentExtractModel();
                //model.StartPos = 0;
                //model.Length = 500;
                //model.docModel = new DocumentModel();
                model.NameIfVersion = NameIfVersion;
                model.CorpusName = CorpusName;

                model.DocLength = doc.Length();
                DocumentMetadata meta = doc.GetMetadata();
                CorpusController.DocumentMetadataToModel(meta, model);

                return View(model);

            }
            catch (Exception ex)
            {
                string message = "An error occurred while trying to open the document: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }
        }

		public ActionResult Info(string CorpusName, string NameIfVersion)
        {
            
            try
            {
                IDocument doc = PrismHelpers.GetDocument(CorpusName, NameIfVersion);
                DocumentMetadata meta = doc.GetMetadata();

                var m = new DocInfoViewModel();
                m.CorpusName = CorpusName;
                m.NameIfVersion = NameIfVersion;
                m.MetaData = meta;

                return View(m);

            }
            catch (Exception ex)
            {
                string message = "An error occurred while trying to open the document: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }
        }

      
        public ActionResult Upload(string CorpusName, string NameIfVersion)
        {
            try
            {
                ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);
                HtmlUploadModel model = new HtmlUploadModel();
                model.CorpusName = CorpusName;
                model.NameIfVersion = NameIfVersion;
                model.encoding = PrismEncoding.UTF8;
                return View(model);

            }
            catch (Exception ex)
            {
                string message = string.Empty;
                message = "An error occurred while trying to open corpus '" + CorpusName + "': " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }

        }

        public ActionResult BaseUpload(string corpusName, string uploadType)
        {
            try
            {
                if (string.IsNullOrEmpty(corpusName) || corpusName == "undefined")
                    throw new Exception("Corpus Name was not informed.");

                var model = new UploadFilesResult
                {
                    CorpusName = corpusName,
                    UploadType = uploadType
                };

                ViewData["CorpusName"] = corpusName;
                
                return View(model);
            }
            catch (Exception ex)
            {
                var message = "An error occurred while trying to open UploadFilesResult : " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }
        }

        public ActionResult VersionUpload(string corpusName, string uploadType)
        {
            try
            {
                var model = new UploadFilesResult
                {
                    CorpusName = corpusName,
                    UploadType = uploadType
                };
                return View(model);
            }
            catch (Exception ex)
            {
                var message = "An error occurred while trying to open UploadFilesResult : " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }
        }

        [System.Web.Mvc.ValidateInput(false)]
        public ActionResult UploadContent(HtmlUploadContentModel model)
        {
            try
            {
                HtmlErrors errors = null;
                string htmlDoc = HtmlContentToHtmlDoc(model.HtmlContent);
                if (string.IsNullOrWhiteSpace(model.NameIfVersion))
                {
                    errors = PrismHelpers.GetCorpus(model.CorpusName).UploadBaseText(htmlDoc);
                }
                else
                {
                    errors = PrismHelpers.GetCorpus(model.CorpusName).UploadVersion(model.NameIfVersion, htmlDoc);
                }

                if (errors.HtmlParseErrors.Length > 0)
                {
                    string msg =  PrismResources.Upload_Error_Parsing_Html;

                    foreach (var e in errors.HtmlParseErrors)
                    {
                        string message = e.Reason + " at line " + e.Line.ToString() + ", position " + e.LinePosition.ToString();
                        msg += message + "\n";
                    }

                    throw new Exception(msg);

                }

                var results = new ResultModel {Succeeded = true};
                return MonoWorkaroundJson(results);
            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });
            }
        }

        [System.Web.Mvc.ValidateInput(false)]
        [HttpPost]
        public JsonResult UploadFiles()
        {
            try
            {               
                var r = new List<UploadFilesResult>();
                ViewBag.Message = string.Empty;

                if (ViewBag.BaseFiles == null)
                    ViewBag.BaseFiles = 0;
                if (ViewBag.VersionFiles == null)
                    ViewBag.VersionFiles = 0;
                string message;
                var model = new HtmlUploadModel
                {
                    UploadType = Request.Form["uploadType"],
                    CorpusName = Request.Form["corpusName"]
                };

                if (string.IsNullOrEmpty(model.CorpusName) || model.CorpusName == "undefined")
                {
                    message = "You need to create a corpus first.";
                    ModelState.AddModelError("CorpusName", message);
                    AddDangerAlert(string.Format("An error occurred: {0}.", message));
                    ViewBag.Message = message;
                    return Json(message);
                }

                if (ModelState.IsValid)
                {
                    foreach (string baseFile in Request.Files)
                    {
                        if (model.UploadType == "base")
                            ViewBag.BaseFiles += 1;

                        if (model.UploadType == "version")
                            ViewBag.VersionFiles += 1;

                        var uploadedFile = Request.Files[baseFile];
                        var uploadedFiles = UploadFiles(model, uploadedFile);
                        r.AddRange(uploadedFiles);
                    }

                    return Json(GetFileList(r));
                }
                else
                {
                    message = "An error occurred: The information is not valid.";
                    AddDangerAlert(message);
                    ViewBag.Message = message;
                    ModelState.AddModelError(string.Empty, message);
                    return Json(string.Empty, message);
                }
            }
            catch (Exception ex)
            {
                ViewBag.Message = string.Format("An error occurred: {0}.", ex.Message);
                AddDangerAlert(string.Format("An error occurred: {0}.", ex.Message));                
                throw ex;
            }
        }

        public List<UploadFilesResult> UploadFiles(HtmlUploadModel model, HttpPostedFileBase file)
        {
            var r = new List<UploadFilesResult>();
            try
            {
                if (file == null || file.ContentLength == 0)
                {
                    throw new Exception(PrismResources.Upload_Select_File);
                }

                DocumentMetadata metadata;
                var html = ParseFileToHtml(model, file, out metadata);
                var uploadedFile = GetUploadedFile(model, file, metadata, html);
                var errors = PrismHelpers.GetCorpus(model.CorpusName).CreateUploadTextAndFiles(uploadedFile);

                if (errors.HtmlParseErrors.Length > 0)
                {
                    ModelState.AddModelError(string.Empty, PrismResources.Upload_Error_Parsing_Html);
                    var message = string.Empty;
                    foreach (var e in errors.HtmlParseErrors)
                    {
                        message = e.Reason + " at line " + e.Line + ", position " +
                                      e.LinePosition;
                        ModelState.AddModelError(string.Empty, message);
                    }

                    throw new Exception(string.Format("An error occurred: {0}", message));
                }

                r.Add(new UploadFilesResult()
                {
                    CorpusName = uploadedFile.CorpusName,
                    Name = uploadedFile.NameIfVersion,
                    Length = uploadedFile.Length,
                    Type = uploadedFile.Type,
                    UploadType = model.UploadType
                });

                return r;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class ViewDataUploadFilesResult
        {
            public string name { get; set; }
            public int size { get; set; }
            public string type { get; set; }
            public string url { get; set; }
            public string deleteUrl { get; set; }
            public string thumbnailUrl { get; set; }
            public string deleteType { get; set; }
            public string corpusname { get; set; }
        }
       
        public class JsonFiles
        {
            public ViewDataUploadFilesResult[] files;
            public JsonFiles(List<ViewDataUploadFilesResult> filesList)
            {
                files = new ViewDataUploadFilesResult[filesList.Count];
                for (var i = 0; i < filesList.Count; i++)
                {
                    files[i] = filesList.ElementAt(i);
                }
            }
        }

        public JsonFiles GetFileList(List<UploadFilesResult> filesList)
        {
            var r = filesList.Select(file => new ViewDataUploadFilesResult
            {
                name = file.Name,
                size = file.Length,
                type = file.UploadType,
                url = file.Name,
                corpusname = file.CorpusName
            }).ToList();

            var files = new JsonFiles(r);

            return files;
        }

        private static UploadedFile GetUploadedFile(HtmlUploadModel model, HttpPostedFileBase file, 
            DocumentMetadata metadata, string html)
        {
            var uploadedFile = new UploadedFile
            {
                Name = file.FileName,
                Type = file.ContentType,
                Length = file.ContentLength > 0 ? file.ContentLength : html.Length,
                CorpusName = model.CorpusName,
                FileHtml = html,
                FileMetadata = metadata,
                NameIfVersion = Path.GetFileNameWithoutExtension(file.FileName),
                Encoding = model.encoding.ToString(),
                UploadType = model.UploadType,
                FileStream = file.InputStream
            };

            uploadedFile.FileMetadata = metadata;
            uploadedFile.FileMetadata.LanguageCode = metadata.LanguageCode;
            
            return uploadedFile;
        }


#if USE_OLD_UPLOAD
        [HttpPost]
        public ActionResult Upload(HtmlUploadModel model, HttpPostedFileBase file)
        {

            if (ModelState.IsValid)
            {
                if (file == null)
                {
                    ModelState.AddModelError(string.Empty, "You must select a file.");
                    return View(model);

                }

                try
                {
                    byte[] data = new byte[file.ContentLength];

                    file.InputStream.Read(data, 0, file.ContentLength);

                    System.IO.MemoryStream ms = new System.IO.MemoryStream(data);

                    System.Text.Encoding encoding = PrismHelpers.PrismEncodingToTextEncoding(model.encoding);

                    System.IO.StreamReader sr = new System.IO.StreamReader(ms, encoding);

                    String html = sr.ReadToEnd();

                    HtmlErrors errors = null;
                    if (string.IsNullOrWhiteSpace(model.NameIfVersion))
                    {
                        errors = PrismHelpers.GetCorpus(model.CorpusName).UploadBaseText(html);
                    }
                    else
                    {
                        errors = PrismHelpers.GetCorpus(model.CorpusName).UploadVersion(model.NameIfVersion, html);
                    }

                    if (errors.HtmlParseErrors.Length > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Errors were encountered parsing the HTML file.");

                        foreach (var e in errors.HtmlParseErrors)
                        {
                            string message = e.Reason + " at line " + e.Line.ToString() + ", position " + e.LinePosition.ToString();
                            ModelState.AddModelError(string.Empty, message);

                        }

                        return View(model);

                    }


                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);

                    return View(model);
                }

            }
            else
            {
                return View(model);

            }
            return RedirectToAction("Index", "Document", new { CorpusName = model.CorpusName, NameIfVersion = model.NameIfVersion });


        }
#else
        [HttpPost]
        public ActionResult Upload(HtmlUploadModel model, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file == null)
                {
                    ModelState.AddModelError(string.Empty, PrismResources.Upload_Select_File);

                    return View(model);
                }
                try
                {
                    DocumentMetadata metadata;
                    var html = ParseFileToHtml(model, file, out metadata);
                    var errors = string.IsNullOrWhiteSpace(model.NameIfVersion)
                        ? PrismHelpers.GetCorpus(model.CorpusName).UploadBaseText(html)
                        : PrismHelpers.GetCorpus(model.CorpusName).UploadVersion(model.NameIfVersion, html);

                    if (errors.HtmlParseErrors.Length > 0)
                    {
                        ModelState.AddModelError(string.Empty, PrismResources.Upload_Error_Parsing_Html);

                        foreach (var e in errors.HtmlParseErrors)
                        {
                            var message = e.Reason + " at line " + e.Line.ToString() + ", position " +
                                          e.LinePosition.ToString();
                            ModelState.AddModelError(string.Empty, message);
                        }

                        return View(model);
                    }

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);

                    return View(model);
                }
            }
            else
            {
                return View(model);
            }

            return RedirectToAction("Index", "Document",
                new { CorpusName = model.CorpusName, NameIfVersion = model.NameIfVersion });

        }
#endif

        private static string ParseFileToHtml(HtmlUploadModel model, HttpPostedFileBase file,
            out DocumentMetadata metadata)
        {
            var data = new byte[] {};
            string html;
            metadata = new DocumentMetadata();
            metadata.LanguageCode = "eng";

            var parsedHtml = new HtmlDocumentResult();

            using (var inputStream = file.InputStream)
            {
                var memoryStream = inputStream as MemoryStream;
                if (memoryStream == null)
                {
                    memoryStream = new MemoryStream();
                    inputStream.CopyTo(memoryStream);
                }
                data = memoryStream.ToArray();

                var tikaExtractor = new TikaExtractor();

                parsedHtml = tikaExtractor.ConvertFileToHtml(data, model.encoding.ToString());
                metadata = parsedHtml.Metadata;
            }

            if (data.Length > 0 && !string.IsNullOrEmpty(parsedHtml.Content))
            {
                html = parsedHtml.Content;
            }
            else
            {
                data = new byte[file.ContentLength];
                file.InputStream.Read(data, 0, file.ContentLength);

                var ms = new MemoryStream(data);
                var encoding = PrismHelpers.PrismEncodingToTextEncoding(model.encoding);
                var sr = new StreamReader(ms, encoding);

                html = sr.ReadToEnd();
            }

            return html;
        }

        [HttpPost]
        public ActionResult Update(HtmlUploadModel model, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file == null)
                {
                    ModelState.AddModelError(string.Empty, PrismResources.Upload_Select_File);
                    return View("Upload", model);
                }

                try
                {
                    var metadata = new DocumentMetadata();
                    var html = ParseFileToHtml(model, file, out metadata);
                    var errors = string.IsNullOrWhiteSpace(model.NameIfVersion)
                        ? PrismHelpers.GetCorpus(model.CorpusName).
                            UpdateBaseText(html)
                        : PrismHelpers.GetCorpus(model.CorpusName).
                            UpdateVersion(model.NameIfVersion, html);

                    if (errors.HtmlParseErrors.Length > 0)
                    {
                        ModelState.AddModelError(string.Empty, "Errors were encountered parsing the HTML file.");

                        foreach (var e in errors.HtmlParseErrors)
                        {
                            var message = e.Reason + " at line " + e.Line.ToString() + ", position " +
                                             e.LinePosition.ToString();
                            ModelState.AddModelError(string.Empty, message);

                        }

                        return View("Upload", model);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);
                    return View("Upload", model);
                }
            }
            else
            {
                return View("Upload", model);

            }
            return RedirectToAction("Index", "Document",
                new {CorpusName = model.CorpusName, NameIfVersion = model.NameIfVersion});

        }

        public ActionResult UploadWizard()
        {
            try
            {
                var model = new UploadFilesResult();
                return View(model);
            }
            catch (Exception ex)
            {
                var message = "An error occurred while trying to open UploadFilesResult : " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }

        }

        private HtmlErrors CreateVersion(string corpusName, string languageCode, string versionName, string htmlFile)
        {
            try
            {
                var model = new BaseDocumentModel();
                model.CorpusName = corpusName;
                model.OriginalCorpusName = corpusName;
                model.IsVersion = true;
                model.LanguageCode = languageCode;
                model.NameIfVersion = versionName;
                //model.Genre = model.Genre;
                //model.AuthorTranslator = model.AuthorTranslator;
                //model.Description = model.Description;
                //model.CopyrightInfo = model.CopyrightInfo;
                //model.ReferenceDate = model.ReferenceDate;
                //model.Information = model.Information;

                var meta = new DocumentMetadata();
                meta.LanguageCode = languageCode;

                var corpus = PrismHelpers.GetCorpus(model.CorpusName);
                corpus.CreateVersion(model.NameIfVersion, meta);

                var errors = PrismHelpers.GetCorpus(model.CorpusName).UploadVersion(versionName, htmlFile);

                return errors;

            }
            catch (Exception ex)
            {
                string message = "An error occurred while trying to open corpus '" + corpusName + "': " + ex.Message;
                throw;
            }

        }
        #endregion

        #region AJAX calls

        [HttpPost]
        public ActionResult GetBaseTextExtract(AlignModel model)
        {
            return GetExtract(model.BaseTextModel);
        }

        [HttpPost]
        public ActionResult GetVersionExtract(AlignModel model)
        {
            return GetExtract(model.VersionModel);
        }

        //[HttpPost]
        //public ActionResult GetBaseTextSegmentText(SimpleEddyChartModel model)
        //{
        //    try
        //    {
        //        SegmentTextResultModel results = new SegmentTextResultModel();

        //        var basetext = PrismHelpers.GetBaseText(model.CorpusName);

        //        var seg = basetext.GetSegmentDefinition(model.BaseTextSegmentId);

        //        results.SegmentText = basetext.GetDocumentContentText(seg.StartPosition, seg.Length);
        //        results.Succeeded = true;

        //        return MonoWorkaroundJson(results);

        //    }
        //    catch (Exception ex)
        //    {
        //        return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

        //    }

        //}

        [HttpPost]
        public ActionResult GetEddyOverviewChartTooltipText(EddyOverviewChartTooltipModel model)
        {
            try
            {
                var results = new EddyOverviewChartTooltipResultModel();

                var basetext = PrismHelpers.GetBaseText(model.CorpusName);

                var seg = basetext.GetSegmentDefinition(model.BaseTextSegmentId);

                results.BaseTextSegmentText = basetext.GetDocumentContentText(seg.StartPosition, seg.Length);


                var set = PrismHelpers.GetAlignmentSet(model.CorpusName, model.VersionName);
                var a = set.FindAlignment(model.BaseTextSegmentId, true);
                if (a != null)
                {
                    var version = PrismHelpers.GetVersion(model.CorpusName, model.VersionName);
                    string s = string.Empty;
                    foreach (int segid in a.SegmentIDsInVersion)
                    {
                        seg = version.GetSegmentDefinition(segid);
                        s += version.GetDocumentContentText(seg.StartPosition, seg.Length) + " ";
                    }

                    results.VersionSegmentText = s.Trim();
                }

                results.Succeeded = true;

                return MonoWorkaroundJson(results);

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }

        }

        [HttpGet]
        public ActionResult AutoSegmentation(string CorpusName, string VersionName)
        {
            AutoSegmentationModel m = new AutoSegmentationModel();
            m.CorpusName = CorpusName;
            m.VersionName = VersionName;

            return PartialView("AutoSegmentDialog", m);
        }

        [HttpGet]
        public ActionResult ExportDocument(string CorpusName, string VersionName)
        {
            try
            {
                
                var doc = PrismHelpers.GetDocument(CorpusName, VersionName);

                if (doc.Length() == 0)
                    throw new Exception("The document is empty.");

                string content = doc.GetDocumentContent(0, doc.Length(), true, false, null, true, null, null, null);

                content = HtmlContentToHtmlDoc(content);
                var output = new System.IO.MemoryStream();
                var sw = new System.IO.StreamWriter(output, System.Text.Encoding.UTF8);

                sw.Write(content);

                sw.Flush();
                output.Position = 0;

                string fname = CorpusName + "-";
                if (!string.IsNullOrEmpty(VersionName))
                    fname += VersionName;
                else
                    fname += "[basetext]";
                fname = PrismLib.PrismExport.SanitiseFilename(fname);
                fname += ".html";
                fname = HttpUtility.UrlPathEncode(fname);
                return File(output, "application/xml", fname);

            }
            catch (Exception ex)
            {
                string message = "An error occurred while trying to export the document: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }

        }


        [HttpPost]
        public ActionResult AutoSegmentation(AutoSegmentationModel model)
        {
            try
            {
                var doc = PrismHelpers.GetDocument(model.CorpusName, model.VersionName);

                //TODO: Add a validation if the segments are already created, then inform the user.
                //var segments = doc.GetSegmentCount();

                //if (segments > 0)
                //{
                //    return
                //        JsonPopupResult(new ResultModel
                //        {
                //            Succeeded = false,
                //            ErrorMsg = "Auto-segmentation is not possible due to the existing segments."
                //        });
                //}
                //else
                //{
                    switch (model.Option)
                    {
                        case "around":
                            doc.AutoSegmentationAroundTag(model.Tag);
                            break;
                        case "after":
                            doc.AutoSegmentationAfterTag(model.Tag);
                            break;
                        case "play1":
                            doc.AutoSegmentationPlay1Format();
                            break;
                    }

                    return JsonPopupResult(new ResultModel {Succeeded = true});
                //}
            }
            catch (Exception ex)
            {
                return JsonPopupResult(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });
            }

        }

        internal static string HtmlContentToHtmlDoc(string content)
        {
            return "<html><head></head><body>" + content + "</body></html>";
        }

        [ValidateInput(false), AcceptVerbs(HttpVerbs.Post)]
        [HttpPost]
        public ActionResult SaveExtract(DocumentExtractModel model, string newHtml)
        {
            try
            {
                string htmlDoc = HtmlContentToHtmlDoc( newHtml);

                var corpus = PrismHelpers.GetCorpus(model.CorpusName);
                HtmlErrors errors = null;
                if (string.IsNullOrEmpty(model.NameIfVersion))
                {
                    errors = corpus.UpdateBaseText(htmlDoc);
                }
                else
                {
                    errors = corpus.UpdateVersion(model.NameIfVersion, htmlDoc);
                }

                if (errors.HtmlParseErrors.Length > 0)
                {
                    string s = "Errors were encountered parsing the HTML file." + Environment.NewLine;

                    foreach (var e in errors.HtmlParseErrors)
                    {
                        s += e.Reason + " at line " + e.Line.ToString() + ", position " + e.LinePosition.ToString() + Environment.NewLine;
                        
                    }

                    return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(s) });
                }

                return MonoWorkaroundJson(new ResultModel { Succeeded = true });
            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }
        }

        [HttpPost]
        public ActionResult GetExtract(DocumentExtractModel model)
        {
            try
            {
                IDocument doc = PrismHelpers.GetDocument(model.CorpusName, model.NameIfVersion);

                int StartPosition = 0;
                int Length = 0;

                int segID = model.ExtractSegmentID;
                if (segID == -1)
                {
                    Length = doc.Length();
                }
                else
                {
                    if (model.Editing)
                        throw new Exception("Editing can currently only be performed if you are viewing the whole document, not just an extract.");
                    SegmentDefinition segdef = doc.GetSegmentDefinition(segID);
                    StartPosition = segdef.StartPosition;
                    Length = segdef.Length;
                }

                bool addSegmentFormatting = !model.Editing;

                var pds = PrismHelpers.GetCorpus(model.CorpusName).GetPredefinedSegmentAttributes(true);       
                var pdnames = new List<string>();
                foreach (var pd in pds)
                    pdnames.Add(pd.Name);
                //pdnames.Add("cb"); /// content behaviour

                var segfilter = SegmentFilterFromModel(model);// EblaAttribFilterFromModel(model).ToArray();
                if (model.Editing)
                    segfilter = null; // we need all the segments emitted when editing

                string content = doc.GetDocumentContent2(StartPosition, Length, true, addSegmentFormatting, segfilter, model.Editing, pdnames.ToArray(), null, null);

                return MonoWorkaroundJson(new GetHtmlResultModel { Succeeded = true, html = content /*System.Net.WebUtility.HtmlEncode(content)*/ });


            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }

        }

        [HttpPost]
        public ActionResult ChangeSegmentMarker(ChangeSegmentMarkerModel model)
        {
            try
            {
                IDocument doc = PrismHelpers.GetDocument(model.CorpusName, model.NameIfVersion);

                SegmentDefinition segDef = doc.GetSegmentDefinition(model.SegmentDefinitionID);

                if (segDef == null)
                    return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = "Segment definition with ID " + model.SegmentDefinitionID.ToString() + " not found." });

                if (model.start)
                {
                    if (model.newOffset > segDef.StartPosition + segDef.Length)
                        return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = "Start marker may not be placed after end marker." });

                    segDef.Length -= model.newOffset - segDef.StartPosition;
                    segDef.StartPosition = model.newOffset;
                }
                else
                {
                    if (model.newOffset < segDef.StartPosition)
                        return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = "End marker may not be placed before start marker." });

                    segDef.Length = model.newOffset - segDef.StartPosition;
                }

                doc.UpdateSegmentDefinition(segDef, false);

                return MonoWorkaroundJson(new ResultModel { Succeeded = true });

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }
        }

        [HttpPost]
        public ActionResult DeleteSegs(BaseDocumentModel model)
        {
            try
            {
                IDocument doc = PrismHelpers.GetDocument(model.CorpusName, model.NameIfVersion);

                doc.DeleteAllSegmentDefinitions();

                return MonoWorkaroundJson(new ResultModel { Succeeded = true });
            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }

        }

        [HttpPost]
        public ActionResult DeleteSeg(BaseDocumentModel model, int id)
        {
            try
            {
                IDocument doc = PrismHelpers.GetDocument(model.CorpusName, model.NameIfVersion);

                doc.DeleteSegmentDefinition(id);

                return MonoWorkaroundJson(new ResultModel { Succeeded = true });
            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }

        }

        [HttpGet]
        public ActionResult AlignerBaseTextSegmentDefinitionDialog(AlignerBaseTextDocumentExtractModel model, int? id, int? startOffset, int? length)
        {
            return SegmentDefinitionDialog(model.BaseTextModel, id, startOffset, length);
        }

        [HttpGet]
        public ActionResult AlignerVersionSegmentDefinitionDialog(AlignerVersionDocumentExtractModel model, int? id, int? startOffset, int? length)
        {
            return SegmentDefinitionDialog(model.VersionModel, id, startOffset, length);
        }

        [HttpPost]
        public ActionResult AlignerBaseTextSegmentDefinitionDialog(SegmentDefinitionModel model)
        {
            return SegmentDefinitionDialog(model);
        }

        [HttpPost]
        public ActionResult AlignerVersionSegmentDefinitionDialog(SegmentDefinitionModel model)
        {
            return SegmentDefinitionDialog(model);
        }


        [HttpGet]
        public ActionResult SegmentDefinitionDialog( DocumentExtractModel model, int? id, int? startOffset, int? length)
        {
            int nextAttribRowIndex = 0;

            IDocument doc = PrismHelpers.GetDocument(model.CorpusName, model.NameIfVersion);
            SegmentDefinitionModel m = new SegmentDefinitionModel();
            m.CorpusName = model.CorpusName;
            m.NameIfVersion = model.NameIfVersion;


            ICorpus corpus = PrismHelpers.GetCorpus(model.CorpusName);

            //m.PredefinedSegmentAttributes = new List<EblaAPI.PredefinedSegmentAttribute>( corpus.GetPredefinedSegmentAttributes());

            SegmentDefinition segdef = new SegmentDefinition();
            int maxLength = 40;
            int trimLength = maxLength / 2 - 2;
            if (id.HasValue)
            {
                segdef = doc.GetSegmentDefinition(id.Value);
            }
            else
            {
                if (!startOffset.HasValue || !length.HasValue)
                    throw new Exception("Either a non-null segment ID or valid startOffset/length must be provided.");

                // Were they viewing the whole text, or just an extract?
                if (model.ExtractSegmentID != -1)
                {
                    // An extract - so the actual start position of the new segment, with respect
                    // to the whole text, is the startoffset provided plus the extract segment start pos
                    SegmentDefinition extractSegment = doc.GetSegmentDefinition(model.ExtractSegmentID);
                    startOffset += extractSegment.StartPosition;
                }

                segdef.ID = -1;
                segdef.StartPosition = startOffset.Value;
                segdef.Length = length.Value;

                //// Add any attribs from the view filter

                //if (model.AttributeFilters != null)
                //{
                //    List<SegmentAttribute> segattribs = new List<SegmentAttribute>();
                //    foreach (var a in model.AttributeFilters)
                //    {
                //        SegmentAttribute attrib = new SegmentAttribute();
                //        attrib.Name = a.Name;
                //        attrib.Value = a.Value;
                //        segattribs.Add(attrib);
                //    }
                //    segdef.Attributes = segattribs.ToArray();
                //}

            }

            string content = doc.GetDocumentContentText(segdef.StartPosition, segdef.Length);
            m.Content = content;
            if (content.Length > maxLength)
            {
                string abbr = content.Substring(0, trimLength) + "[...]" + content.Substring(content.Length - trimLength);
                m.Content = abbr;
            }
            

            m.ID = segdef.ID;
            m.startPos = segdef.StartPosition;
            m.length = segdef.Length;
            m.ContentBehaviour = segdef.ContentBehaviour;

            List<AttributeModel> attribs = new List<AttributeModel>();

            if (segdef.Attributes != null)
                if (segdef.Attributes.Length > 0)
                    foreach (var a in segdef.Attributes)
                    {
                        AttributeModel attrib = new AttributeModel();
                        attrib.Name = a.Name;
                        attrib.Value = a.Value;
                        attribs.Add(attrib);
                        nextAttribRowIndex++;
                    }

            // Add any attribs from the view filter
            if (segdef.ID == -1)
                if (model.AttributeFilters != null)
                    attribs.AddRange(model.AttributeFilters);

            m.Attributes = attribs.ToArray();
            ViewData["nextattribrowindex"] = nextAttribRowIndex.ToString();

            return PartialView("SegmentDefinitionDialog", m);
        }


        [HttpPost]
        public ActionResult SegmentDefinitionDialog(SegmentDefinitionModel model)
        {
            try
            {
                IDocument doc = PrismHelpers.GetDocument(model.CorpusName, model.NameIfVersion);

                SegmentDefinition segDef = new SegmentDefinition();
                segDef.ID = model.ID;
                segDef.StartPosition = model.startPos;
                segDef.Length = model.length;
                segDef.ContentBehaviour = model.ContentBehaviour;
                List<SegmentAttribute> attribs = new List<SegmentAttribute>();
                if (model.Attributes != null)
                    foreach (var a in model.Attributes)
                    {
                        SegmentAttribute attrib = new SegmentAttribute();
                        attrib.Name = a.Name;
                        attrib.Value = a.Value;
                        attribs.Add(attrib);
                    }
                segDef.Attributes = attribs.ToArray();

                if (model.ID == -1)
                    doc.CreateSegmentDefinition(segDef);
                else
                    doc.UpdateSegmentDefinition(segDef, false);

                
                return JsonPopupResult(new ResultModel { Succeeded = true });

            }
            catch (Exception ex)
            {
                return JsonPopupResult(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });
            }

        }




        [HttpPost]
        public ActionResult AutoSegment2(BaseDocumentModel model)
        {
            try
            {
                IDocument doc = PrismHelpers.GetDocument(model.CorpusName, model.NameIfVersion);

                AutoSegmenter.AutoSegment2(doc);

                return MonoWorkaroundJson(new ResultModel { Succeeded = true });


            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });
            }

        }

        [HttpPost]
        public ActionResult AutoSegment1(BaseDocumentModel model)
        {
            try
            {
                IDocument doc = PrismHelpers.GetDocument(model.CorpusName, model.NameIfVersion);

                AutoSegmenter.AutoSegment1(doc);

                return MonoWorkaroundJson(new ResultModel { Succeeded = true});
                

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });
            }

        }

        #endregion

        #region Implementation

        //private class ContentsEntry
        //{
        //    public int start;
        //    public int length;
        //    public string title;
        //}

        internal static SelectList ConstructContentList(/*EblaAPI.PredefinedSegmentAttribute[] atts,*/ IDocument document)
        {
            //List<string> attsToInclude = new List<string>();
            //foreach (var a in atts)
            //{
            //    if (a.ShowInTOC)
            //        attsToInclude.Add(a.Name);
            //}

            var cbFilter = new List<SegmentContentBehaviour>() { SegmentContentBehaviour.structure };

            SegmentDefinition[] segs = document.FindSegmentDefinitions(0, document.Length(), false, false, null, cbFilter.ToArray());

            //List<ContentsEntry> entries = new List<ContentsEntry>();

            Dictionary<int, string> TOCMap = new Dictionary<int, string>();
            List<SegmentDefinition> TOCsegs = new List<SegmentDefinition>();

            foreach (var seg in segs)
            {
                var a = seg.Attributes.FirstOrDefault( x => string.Compare(x.Name, "label") == 0);
                string label = "[no label]";
                if (a != null && !string.IsNullOrEmpty(a.Value.Trim()))
                    label = a.Value;


                TOCMap.Add(seg.ID, label);
                TOCsegs.Add(seg);

                //if (seg.Attributes != null)
                //    foreach (var a in seg.Attributes)
                //    {
                //        if (attsToInclude.Contains(a.Name))
                //        {
                //            TOCMap.Add(seg.ID, a.Value);
                //            TOCsegs.Add(seg);
                //            /*ContentsEntry e = new ContentsEntry();
                //            e.title = a.Value;
                //            e.start = seg.StartPosition;
                //            e.length = seg.Length;
                //            entries.Add(e);*/
                //            break;

                //        }
                //    }
            }

            List<SegmentTreeNode> nodeList = SegmentTreeNode.SegmentsToTree(TOCsegs, false);

            List<SelectListItem> TOCitems = new List<SelectListItem>();
            TOCitems.Add(new SelectListItem { Value = (-1).ToString(), Text = "[entire text]" });

            foreach (var n in nodeList)
            {
                CreateTOCItems(TOCitems, n, TOCMap, 0);
            }

            /*ViewData["TOCSegments"] =*/
            return new SelectList(TOCitems, "Value", "Text");

        }

        static void CreateTOCItems(List<SelectListItem> TOCitems, SegmentTreeNode n, Dictionary<int, string> TOCmap, int depth)
        {
            string item = new string('-', depth);

            item += TOCmap[n.SegmentDefinition.ID];

            TOCitems.Add(new SelectListItem { Value = n.SegmentDefinition.ID.ToString(), Text = item });

            foreach (SegmentTreeNode child in n.ChildSegmentDefinitions)
            {
                CreateTOCItems(TOCitems, child, TOCmap, depth + 1);
            }

        }

        static internal SegmentFilter SegmentFilterFromModel(DocumentExtractModel model)
        {
            var segFilter = new SegmentFilter();
            segFilter.AttributeFilters = EblaAttribFilterFromModel(model).ToArray();
            segFilter.SegmentContentBehaviourFilter = model.ContentBehaviour;
            return segFilter;
        }

        static internal List<EblaAPI.SegmentAttribute> EblaAttribFilterFromModel(DocumentExtractModel model)
        {
            return EblaAttribsFromModel(model.AttributeFilters);

        }

        static internal List<EblaAPI.SegmentAttribute> EblaAttribsFromModel(AttributeModel[] attribs)
        {
            List<EblaAPI.SegmentAttribute> filterAttribs = new List<SegmentAttribute>();

            if (attribs != null)
                foreach (var a in attribs)
                {
                    SegmentAttribute attrib = new SegmentAttribute();
                    attrib.Name = a.Name;
                    attrib.Value = a.Value;
                    filterAttribs.Add(attrib);
                }

            return filterAttribs;
        }

    #endregion

    }
}
