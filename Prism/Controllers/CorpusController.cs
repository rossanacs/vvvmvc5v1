/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EblaAPI;
using Prism.Models;
using ICSharpCode.SharpZipLib.Zip;
using System.Xml;
using System.Diagnostics;
using EblaImpl;

namespace Prism.Controllers
{
    [Authorize]
    public class CorpusController : PrismController
    {
        //==============================================
        //
        //  Page rendering
        //
        //==============================================


        public ActionResult Index(string CorpusName)
        {
            try
            {
                // Since they're 'opening' this corpus, store its name in session
                // to make it the 'active' one
                SetActiveCorpus(CorpusName, false);

                return View(GetModel(CorpusName));
            }
            catch (Exception ex)
            {
                string message = "An error occurred while trying to open corpus '" + CorpusName + "': " + ex.Message;
                return RedirectToAction("Error", "Home", new  { message = message });
            }
        }

        private static string _prismactivecorpuscookiename = "prismactivecorpus";
        private static string _prismactivecorpusselectioncookiename = "prismactivecorpusselection";
        private static string _prismsegmentselectioncookiename = "prismsegmentselection";

        private static void SetActiveCorpusSelection(List<string> versions)
        {
            var sb = new System.Text.StringBuilder();
            foreach (var v in versions)
                sb.Append(v + Environment.NewLine);
            var cookie = new HttpCookie(_prismactivecorpusselectioncookiename);
            cookie.Value = System.Web.HttpUtility.UrlPathEncode(sb.ToString());
            //cookie.Value = sb.ToString();
            cookie.Path = "/";
            cookie.Expires = DateTime.Now.AddDays(7);
            System.Web.HttpContext.Current.Response.Cookies.Remove(_prismactivecorpusselectioncookiename);
            System.Web.HttpContext.Current.Response.SetCookie(cookie);

        }

        private static void SetActiveCorpus(string CorpusName, bool noCheck)
        {
            // Is this a change of active corpus?
            if (!noCheck)
                if (string.Compare(GetActiveCorpus(), CorpusName) == 0)
                    return;  // don't regenerate selection info etc.

            //System.Web.HttpContext.Current.Session["prismactivecorpus"] = CorpusName;
            HttpCookie cookie = new HttpCookie(_prismactivecorpuscookiename);
            cookie.Value = System.Web.HttpUtility.UrlPathEncode(CorpusName);
            cookie.Path = "/";
            cookie.Expires = DateTime.Now.AddDays(7);
            //RemoveRequestCookie(_prismactivecorpuscookiename);
            System.Web.HttpContext.Current.Response.Cookies.Remove(_prismactivecorpuscookiename);
            System.Web.HttpContext.Current.Response.SetCookie(cookie);

            if (!string.IsNullOrEmpty(CorpusName))
            {
                var corpus = PrismHelpers.GetCorpus(CorpusName);
                var versions = corpus.GetVersionList();
                SetActiveCorpusSelection(new List<string>(versions));

            }
            else
            {
                SetActiveCorpusSelection(new List<string>());
            }

            //var sb = new System.Text.StringBuilder();
            //foreach (var v in versions)
            //    sb.Append(v + Environment.NewLine);
            //cookie = new HttpCookie(_prismactivecorpusselectioncookiename);
            //cookie.Value = System.Web.HttpUtility.UrlPathEncode( sb.ToString());
            ////cookie.Value = sb.ToString();
            //cookie.Path = "/";
            //cookie.Expires = DateTime.Now.AddDays(7);
            //System.Web.HttpContext.Current.Response.Cookies.Remove(_prismactivecorpusselectioncookiename);
            //System.Web.HttpContext.Current.Response.SetCookie(cookie);

            cookie = new HttpCookie(_prismsegmentselectioncookiename);
            cookie.Value = string.Empty;
            //cookie.Value = sb.ToString();
            cookie.Path = "/";
            cookie.Expires = DateTime.Now.AddDays(7);

            //RemoveRequestCookie(_prismactivecorpusselectioncookiename);
            System.Web.HttpContext.Current.Response.Cookies.Remove(_prismsegmentselectioncookiename);
            System.Web.HttpContext.Current.Response.SetCookie(cookie);

        }

        //static void RemoveRequestCookie(string cookieName)
        //{
        //    var cookie = System.Web.HttpContext.Current.Request.Cookies[cookieName];
        //    if (cookie != null)
        //        cookie.Value = string.Empty;
        //}

        internal static HttpCookie GetLatestCookie(string cookieName)
        {
            //for (int i = 0; i < System.Web.HttpContext.Current.Response.Cookies.Keys.Count; i++)
            //{
            //    if (string.Compare(cookieName, System.Web.HttpContext.Current.Response.Cookies.Keys.Get(i)) == 0)
            //    {
            //        return System.Web.HttpContext.Current.Request.Cookies[cookieName];
            //    }

            //}
            
            foreach (var k in System.Web.HttpContext.Current.Response.Cookies.AllKeys)
                if (string.Compare(cookieName, k) == 0)
                {
                    return System.Web.HttpContext.Current.Response.Cookies[cookieName];
                }

            //var cookie = System.Web.HttpContext.Current.Request.Cookies[_prismactivecorpusselectioncookiename];
            //if (cookie == null || string.IsNullOrEmpty(cookie.Value))
            //    cookie = System.Web.HttpContext.Current.Response.Cookies[_prismactivecorpusselectioncookiename];
            //return cookie;

            return System.Web.HttpContext.Current.Request.Cookies[cookieName];
        }

        public static List<string> GetActiveCorpusSelection()
        {
            string corpusName = GetActiveCorpus();
            if (string.IsNullOrEmpty(corpusName))
                return new List<string>();

            var cookie = GetLatestCookie(_prismactivecorpusselectioncookiename);

            if (cookie == null)
                return new List<string>();
            if (cookie.Value == null)
                return new List<string>();

            //var decoded = System.Web.HttpUtility.UrlDecode(cookie.Value);
            var decoded = System.Uri.UnescapeDataString(cookie.Value);
            var versions = decoded.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

            // Do all these versions still exist?
            var check = new List<string>();
            try
            {
                var corpus = PrismHelpers.GetCorpus(corpusName);
                var map = new HashSet<string>(corpus.GetVersionList());
                foreach (var v in versions)
                    if (map.Contains(v))
                        check.Add(v);

                if (versions.Length != check.Count)
                {
                    System.Diagnostics.Debug.Assert(versions.Length > check.Count);
                    SetActiveCorpusSelection(check);
                }

            }
            catch (Exception ex)
            {

            }
            return check;

        }

        public static void SetSegmentSelectionList(List<int> list)
        {
            var sb = new System.Text.StringBuilder();
            foreach (var l in list)
            {
                if (sb.Length > 0)
                    sb.Append("-");
                sb.Append(l.ToString());
            }

            var cookie = new HttpCookie(_prismsegmentselectioncookiename);
            cookie.Value = sb.ToString();
            //cookie.Value = sb.ToString();
            cookie.Path = "/";
            cookie.Expires = DateTime.Now.AddDays(7);

            //RemoveRequestCookie(_prismactivecorpusselectioncookiename);
            System.Web.HttpContext.Current.Response.Cookies.Remove(_prismsegmentselectioncookiename);
            System.Web.HttpContext.Current.Response.SetCookie(cookie);

        }

        public static List<int> GetSegmentSelectionList()
        {
            if (string.IsNullOrEmpty(GetActiveCorpus()))
                return new List<int>();

            var cookie = GetLatestCookie(_prismsegmentselectioncookiename);

            if (cookie == null)
                return new List<int>();
            if (cookie.Value == null)
                return new List<int>();

            var idsHash = new HashSet<int>();

            var ids = cookie.Value.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var id in ids)
            {
                int i = 0;
                if (int.TryParse(id, out i))
                    if (!idsHash.Contains(i))
                        idsHash.Add(i);
            }

            return new List<int>(idsHash);

        }

        public static string GetCorpusCaption()
        {
            string corpusname = GetActiveCorpus();
            if (String.IsNullOrEmpty(corpusname))
                return string.Empty;

            var sb = new System.Text.StringBuilder();
            sb.Append(corpusname);

            var selection = GetActiveCorpusSelection();
            if (selection == null)
                return sb.ToString();

            sb.Append(Environment.NewLine);
            sb.Append("--");
            for (int i = 0; i < selection.Count; i++)
            {
                sb.Append(Environment.NewLine);
                sb.Append(selection[i]);

            }

            return sb.ToString();

        }

        public static string GetActiveCorpus()
        {
            if (!AccountController.IsLoggedIn)
                return string.Empty;

            var cookie = GetLatestCookie(_prismactivecorpuscookiename);

            if (cookie == null)
                return string.Empty;

            if (cookie.Value == null)
                return string.Empty;

            //var decoded = System.Web.HttpUtility.UrlDecode(cookie.Value);
            var decoded = System.Uri.UnescapeDataString(cookie.Value);

            // Does this corpus still exist?
            var store = PrismHelpers.GetCorpusStore();
            if (store == null)
            {
                // auth error - stale login cookie + password change?
                var s = new FormsAuthenticationService();
                s.SignOut();
                return string.Empty;
            }
            
            var corpora = PrismHelpers.GetCorpusStore().GetCorpusList();
            if (new HashSet<string>(corpora).Contains(decoded))
                return decoded;

            SetActiveCorpus(string.Empty, true);

            return string.Empty;

            //object o = System.Web.HttpContext.Current.Session["prismactivecorpus"];
            //if (o != null)
            //    return o.ToString();

            //return string.Empty;

        }

        public ActionResult Admin(string CorpusName)
        {

            try
            {
                ViewData["calcstatus"] = BackgroundCalculationMgr.GetCorpusStatus(CorpusName);
                ViewData["corpusbusy"] = BackgroundCalculationMgr.IsCorpusBusy(CorpusName) ? "1" : "0";
                return View(GetModel(CorpusName));

            }
            catch (Exception ex)
            {
                string message = PrismResources.An_error_occurred + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }

        }

        //void ExportDoc(IDocument doc, System.Xml.XmlWriter writer, SegmentAlignment[] alignments, string name)
        //{
        //    writer.WriteStartElement("document");
        //    writer.WriteAttributeString("name", name);
        //    var m = doc.GetMetadata();
        //    writer.WriteAttributeString("authortranslator", m.AuthorTranslator);
        //    writer.WriteAttributeString("copyrightinfo", m.CopyrightInfo);
        //    writer.WriteAttributeString("description", m.Description);

        //    writer.WriteAttributeString("genre", m.Genre);
        //    writer.WriteAttributeString("information", m.Information);
        //    writer.WriteAttributeString("langcode", m.LanguageCode);
        //    writer.WriteAttributeString("referencedate", m.ReferenceDate.ToString());

        //    writer.WriteStartElement("doccontent");

        //    writer.WriteRaw(doc.GetDocumentContent(0, doc.Length(), true, false, null, true, null, null, null));

        //    writer.WriteEndElement(); // doccontent


        //    var segdefs = doc.FindSegmentDefinitions(0, doc.Length(), false, false, null, null);

        //    writer.WriteStartElement("segmentdefinitions");

        //    foreach (var segdef in segdefs)
        //    {
        //        writer.WriteStartElement("segmentdefinition");

        //        writer.WriteAttributeString("id", segdef.ID.ToString());
        //        writer.WriteAttributeString("startpos", segdef.StartPosition.ToString());
        //        writer.WriteAttributeString("length", segdef.Length.ToString());
        //        writer.WriteAttributeString("contentbehaviour", segdef.ContentBehaviour.ToString());

        //        writer.WriteStartElement("segmentattributes");

        //        if (segdef.Attributes != null)
        //            foreach (var attrib in segdef.Attributes)
        //            {
        //                writer.WriteStartElement("segmentattribute");
        //                writer.WriteAttributeString("attribname", attrib.Name);
        //                writer.WriteAttributeString("attribval", attrib.Value);
        //                writer.WriteEndElement(); // segmentattribute
        //            }

        //        writer.WriteEndElement(); // segmentattributes

        //        writer.WriteEndElement(); // segmentdefinition
        //    }

        //    writer.WriteEndElement(); // segmentdefinitions

        //    writer.WriteStartElement("alignments");
        //    if (alignments != null)
        //    {
        //        foreach (var alignment in alignments)
        //        {
        //            writer.WriteStartElement("alignment");

        //            writer.WriteAttributeString("id", alignment.ID.ToString());
        //            writer.WriteAttributeString("notes", alignment.Notes);
        //            writer.WriteAttributeString("status", alignment.AlignmentStatus.ToString());
        //            //writer.WriteAttributeString("type", alignment.AlignmentType.ToString());

        //            System.Text.StringBuilder sbids = new System.Text.StringBuilder();
        //            foreach (var id in alignment.SegmentIDsInBaseText)
        //            {
        //                if (sbids.Length > 0)
        //                    sbids.Append(",");
        //                sbids.Append(id.ToString());
        //            }
        //            writer.WriteAttributeString("BaseTextSegIds", sbids.ToString());
        //            sbids = new System.Text.StringBuilder();
        //            foreach (var id in alignment.SegmentIDsInVersion)
        //            {
        //                if (sbids.Length > 0)
        //                    sbids.Append(",");
        //                sbids.Append(id.ToString());
        //            }
        //            writer.WriteAttributeString("VersionSegIds", sbids.ToString());
        //            writer.WriteEndElement(); // alignment
        //        }

        //    }
        //    writer.WriteEndElement(); // alignments

        //    writer.WriteEndElement(); // document
        //}

        List<int> SegIdsToArray(string idstring)
        {
            var results = new List<int>();
            var ids = idstring.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var id in ids)
            {
                int i = 0;
                if (!Int32.TryParse(id, out i))
                    throw new Exception("Invalid segment id: " + id);
                results.Add(i);
            }
            return results;
        }

        Dictionary<int, int> ImportDoc(XmlReader reader, ICorpus corpus, Dictionary<int, int> basetextIDChanges, string CorpusName, HashSet<int> discardedBaseTextSegmentIds)
        {
            bool baseText = basetextIDChanges == null;

            if (!reader.ReadToDescendant("document"))
                throw new Exception("'document' element not found.");
            var docName = reader.GetAttribute("name");

            Debug.WriteLine("Importing doc: " + docName);
            if (baseText)
                if (!string.IsNullOrEmpty(docName.Trim()))
                    throw new Exception("Unexpected non-empty 'name' attribute for base text: " + docName);
            var md = new DocumentMetadata();
            md.AuthorTranslator = reader.GetAttribute("authortranslator");
            md.CopyrightInfo = reader.GetAttribute("copyrightinfo");
            md.Description = reader.GetAttribute("description");
            md.Genre = reader.GetAttribute("genre");
            md.Information = reader.GetAttribute("information");
            md.LanguageCode = reader.GetAttribute("langcode");
            string refdatestring = reader.GetAttribute("referencedate");
            int? refdate = null;
            if (!string.IsNullOrEmpty(refdatestring))
            {
                int rf = 0;
                if (!Int32.TryParse(refdatestring, out rf))
                    throw new Exception("Invalid reference date: " + refdatestring);
                refdate = rf;
            }
            md.ReferenceDate = refdate;
            if (string.IsNullOrEmpty(md.LanguageCode))
                throw new Exception("Version '" + docName + "' has no language code");

            if (!reader.ReadToDescendant("doccontent"))
                throw new Exception("'doccontent' element not found");

            var docReader = reader.ReadSubtree();
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(docReader);

            var docIDChanges = new Dictionary<int, int>();
            IDocument doc = null;
            if (baseText)
            {
                corpus.UploadBaseText(DocumentController.HtmlContentToHtmlDoc(xmlDoc.DocumentElement.InnerXml), docIDChanges);
                doc = PrismHelpers.GetBaseText(CorpusName);
                doc.SetMetadata(md);
            }
            else
            {
                corpus.CreateVersion(docName, md);
                corpus.UploadVersion(docName, DocumentController.HtmlContentToHtmlDoc(xmlDoc.DocumentElement.InnerXml), docIDChanges);
                doc = PrismHelpers.GetVersion(CorpusName, docName);                
            }
            

            if (!reader.ReadToNextSibling("segmentdefinitions"))
                throw new Exception("'segmentdefinitions' element not found");

            var sdreader = reader.ReadSubtree();

            Debug.WriteLine("Reading segment definitions ...");
            var discardedSegIds = new HashSet<int>();
            if (baseText)
                discardedSegIds = discardedBaseTextSegmentIds;
            if (sdreader.ReadToDescendant("segmentdefinition"))
            {
                do
                {
                    string idstring = sdreader.GetAttribute("id");
                    int id = 0;
                    if (!Int32.TryParse(idstring, out id))
                        throw new Exception("Invalid 'id' attribute: " + idstring);
                    if (!docIDChanges.ContainsKey(id))
                    {
                        // this is legal during the count-whitespace to don't-count-whitespace transition
                        // as it may correspond to an ignored zero-length segment.
                        // skip for now.
                        discardedSegIds.Add(id);
                        continue;
                        //throw new Exception("ID value not found in doc content: " + idstring);
                    }

                    string cbstring = sdreader.GetAttribute("contentbehaviour");
                    SegmentContentBehaviour cb = SegmentContentBehaviour.normal;
                    if (!Enum.TryParse(cbstring, out cb))
                        throw new Exception("Invalid contentbehaviour value: " + cbstring);

                    if (!sdreader.ReadToDescendant("segmentattributes"))
                        throw new Exception("'segmentattributes' element not found");
                    var attribs = new List<SegmentAttribute>();
                    if (sdreader.ReadToDescendant("segmentattribute"))
                    {

                        do
                        {
                            string attribname = sdreader.GetAttribute("attribname");
                            string attribval = sdreader.GetAttribute("attribval");

                            var a = new SegmentAttribute();
                            a.Name = attribname;
                            a.Value = attribval;
                            attribs.Add(a);
                        }
                        while (sdreader.ReadToNextSibling("segmentattribute"));
                    }
                    if (attribs.Count > 0 || cb != SegmentContentBehaviour.normal)
                    {
                        var s = doc.GetSegmentDefinition(docIDChanges[id]);
                        s.ContentBehaviour = cb;
                        s.Attributes = attribs.ToArray();
                        doc.UpdateSegmentDefinition(s, false);
                    }

                }
                while (sdreader.ReadToFollowing("segmentdefinition"));
                
            }
            
            if (!reader.ReadToFollowing("alignments"))
                throw new Exception("'alignments' element not found");

            if (!baseText)
            {
                Debug.WriteLine("Reading alignments ...");
                var alset = PrismHelpers.GetAlignmentSet(CorpusName, docName);

                var alreader = reader.ReadSubtree();
                if (alreader.ReadToDescendant("alignment"))
                {
                    do
                    {
                        string notes = reader.GetAttribute("notes");
                        string status = reader.GetAttribute("status");
                        string btsegids = reader.GetAttribute("BaseTextSegIds");
                        string vsegids = reader.GetAttribute("VersionSegIds");

                        AlignmentStatuses st = AlignmentStatuses.confirmed;
                        if (!Enum.TryParse(status, out st))
                            throw new Exception("Invalid alignment status: " + status);

                        var btsegs = SegIdsToArray(btsegids);
                        var vsegs = SegIdsToArray(vsegids);

                        if (btsegs.Count == 0)
                            throw new Exception("Alignment with no base text segment ids");
                        if (vsegs.Count == 0)
                            throw new Exception("Alignment with no version segment ids");

                        for (int i = 0; i < btsegs.Count; i++)
                            if (discardedBaseTextSegmentIds.Contains(btsegs[i])) // we only expect this during the count-whitespace to don't-count-whitespace transition
                                // TODO - make this conditional and/or remove later
                                btsegs[i] = -1;
                            else if (!basetextIDChanges.ContainsKey(btsegs[i]))
                                throw new Exception("Alignment with unknown base text segment id: " + btsegs[i].ToString());
                            else
                                btsegs[i] = basetextIDChanges[btsegs[i]];

                        btsegs = btsegs.Where(x => x != -1).ToList();
                        if (btsegs.Count == 0)
                            continue; // all the base text segs were discarded, so no point adding this alignment

                        for (int i = 0; i < vsegs.Count; i++)
                            if (discardedSegIds.Contains(vsegs[i]))
                                vsegs[i] = -1;
                            else if (!docIDChanges.ContainsKey(vsegs[i]))
                                throw new Exception("Alignment with unknown version segment id: " + vsegs[i].ToString());
                            else
                                vsegs[i] = docIDChanges[vsegs[i]];

                        vsegs = vsegs.Where(x => x != -1).ToList();
                        if (vsegs.Count == 0)
                            continue; // all the base text segs were discarded, so no point adding this alignment

                        //foreach (var s in vsegs)
                        //    if (!docIDChanges.ContainsKey(s))
                        //        throw new Exception("Alignment with unknown version segment id: " + s.ToString());

                        var a = new SegmentAlignment();
                        a.SegmentIDsInBaseText = btsegs.ToArray();
                        a.SegmentIDsInVersion = vsegs.ToArray();
                        a.Notes = notes;
                        a.AlignmentStatus = st;

                        alset.CreateAlignment(a);
                        
                    }
                    while (alreader.ReadToNextSibling("alignment"));



                }
            }

            return docIDChanges;

        }
        

        [HttpPost]
        public ActionResult Import(CorpusModel model, HttpPostedFileBase file)
        {
            string corpName = model.CorpusName;
            if (ModelState.IsValid)
            {
                if (file == null)
                {
                    ModelState.AddModelError(string.Empty, "You must select a file.");
                    return View("Admin", model);

                }

                try
                {
                    // Get rid of existing corpus data
                    var corpus = PrismHelpers.GetCorpus(model.CorpusName);
                    var versions = corpus.GetVersionList();
                    foreach (var v in versions)
                        corpus.DeleteVersion(v);

                    var pdas = corpus.GetPredefinedSegmentAttributes(false);
                    foreach (var pda in pdas)
                        corpus.DeletePredefinedSegmentAttribute(pda.ID);

                    // Start reading input file
                    var reader = new XmlTextReader(file.InputStream);

                    if (!reader.ReadToDescendant("eblacorpus"))
                        throw new Exception("File is not a VVV corpus export");
                    //string name = reader.GetAttribute("name");
                    string desc = reader.GetAttribute("description");
                    corpName = reader.GetAttribute("name");
                    corpus.SetDescription(desc);

                    var store = PrismHelpers.GetCorpusStore();
                    store.RenameCorpus(model.CorpusName, corpName);

                    corpus = PrismHelpers.GetCorpus(corpName);

                    // TODO - some option about whether to rename corpus (uniqueness check reqd.)

                    if (!reader.ReadToDescendant("documents"))
                        throw new Exception("'documents' element not found");

                    var docsreader = reader.ReadSubtree();

                    if (!docsreader.ReadToDescendant("document"))
                        throw new Exception("'document' element not found");

                    // Import base text
                    var discardedBaseTextIds = new HashSet<int>();
                    var baseTextIDChanges = ImportDoc(docsreader.ReadSubtree(), corpus, null, corpName, discardedBaseTextIds);

                    // Import versions
                    while (docsreader.ReadToFollowing("document"))
                    {
                        ImportDoc(docsreader.ReadSubtree(), corpus, baseTextIDChanges, corpName, discardedBaseTextIds);
                    }

                    // Import predefined attribs
                    if (!reader.ReadToFollowing("predefinedattribs"))
                        throw new Exception("'predefinedattribs' element not found");

                    if (reader.ReadToDescendant("predefinedattrib"))
                    {
                        do
                        {
                            string type = reader.GetAttribute("type");
                            string name = reader.GetAttribute("name");
                            bool showintoc = false;
                            string showintocstring = reader.GetAttribute("showintoc");
                            if (!bool.TryParse(showintocstring, out showintoc))
                                throw new Exception("Invalid showintoc value: " + showintocstring);

                            PredefinedSegmentAttributeType pdatype = PredefinedSegmentAttributeType.Text;
                            if (!Enum.TryParse(type, out pdatype))
                                throw new Exception("Invalid PredefinedSegmentAttributeType value: " + type);

                            var pdareader = reader.ReadSubtree();

                            if (!pdareader.ReadToDescendant("predefinedattribvalues"))
                                throw new Exception("'predefinedattribvalues' element not found");

                            var pda = new EblaAPI.PredefinedSegmentAttribute();
                            var vals = new List<EblaAPI.PredefinedSegmentAttributeValue>();
                            pda.ShowInTOC = showintoc;
                            pda.Name = name;
                            pda.AttributeType = pdatype;

                            if (pdareader.ReadToDescendant("predefinedattribvalue"))
                            {
                                do
                                {
                                    var v = new EblaAPI.PredefinedSegmentAttributeValue();
                                    v.Value = pdareader.GetAttribute("value");
                                    v.ColourCode = pdareader.GetAttribute("colourcode");
                                    bool applyColouring = false;
                                    string applyColouringString = pdareader.GetAttribute("applycolouring");
                                    if (!bool.TryParse(applyColouringString, out applyColouring))
                                        throw new Exception("Invalid applycolouring value: " + applyColouringString);
                                    v.ApplyColouring = applyColouring;
                                    vals.Add(v);
                                }
                                while (pdareader.ReadToNextSibling("predefinedattribvalue"));
                            }
                            pda.Values = vals.ToArray();
                            corpus.CreatePredefinedSegmentAttribute(pda);

                        }
                        while (reader.ReadToFollowing("predefinedattrib"));


                    }



                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, ex.Message);

                    return View("Admin", model);
                }

            }
            else
            {
                return View("Admin", model);

            }
            return RedirectToAction("Index", "Corpus", new { CorpusName = corpName });


        }

        void ExportCorpus(System.Xml.XmlWriter writer, ICorpus corpus, string CorpusName, VariationMetricTypes metricType)
        {
            var versions = corpus.GetVersionList();

            // Are we exporting the active corpus?
            string activeCorpus = GetActiveCorpus();
            if (activeCorpus != null)
                if (string.Compare(activeCorpus, CorpusName) == 0)
                {
                    var activeversions = GetActiveCorpusSelection().ToArray();
                    if (activeversions != null && activeversions.Length > 0)
                        versions = activeversions;
                }

            var basetextDoc = PrismHelpers.GetBaseText(CorpusName);
            PrismLib.PrismExport.ExportCorpus(writer, corpus, basetextDoc, versions, CorpusName, metricType, PrismHelpers.GetVersion, PrismHelpers.GetAlignmentSet);
        }

        //void _ExportCorpus(System.Xml.XmlWriter writer, ICorpus corpus, string CorpusName, VariationMetricTypes metricType)
        //{

        //    writer.WriteStartElement("eblacorpus");
        //    writer.WriteAttributeString("name", CorpusName);
        //    writer.WriteAttributeString("description", corpus.GetDescription());

        //    writer.WriteStartElement("documents");

        //    var basetextDoc = PrismHelpers.GetBaseText(CorpusName);

        //    ExportDoc(basetextDoc, writer, null, string.Empty);

        //    var versions = corpus.GetVersionList();

        //    // Are we exporting the active corpus?
        //    string activeCorpus = GetActiveCorpus();
        //    if (activeCorpus != null)
        //        if (string.Compare(activeCorpus, CorpusName) == 0)
        //        {
        //            var activeversions = GetActiveCorpusSelection().ToArray();
        //            if (activeversions != null && activeversions.Length > 0)
        //                versions = activeversions;
        //        }

        //    foreach (var version in versions)
        //    {
        //        var versionDoc = PrismHelpers.GetVersion(CorpusName, version);

        //        var alignmentSet = PrismHelpers.GetAlignmentSet(CorpusName, version);

        //        var alignments = alignmentSet.FindAlignments(null, null, 0, basetextDoc.Length(), 0, versionDoc.Length());
                

        //        ExportDoc(versionDoc, writer, alignments, version);
        //    }


        //    writer.WriteEndElement(); // documents

        //    //var stats = ChartsController.CreateEddyOverviewChartData(CorpusName, new List<string>( versions), metricType, null);

        //    var data = corpus.RetrieveMultipleSegmentVariationData(null, null, metricType, new List<string>(versions), false);

        //    writer.WriteStartElement("vivvalues");

        //    for (int i = 0; i < data.BaseTextSegmentIDs.Length; i++)
        //    {
        //        writer.WriteStartElement("vivvalue");
        //        writer.WriteAttributeString("BaseTextSegId", data.BaseTextSegmentIDs[i].ToString());
        //        writer.WriteAttributeString("viv", data.VivValues[i].ToString());
        //        writer.WriteEndElement();
        //    }
        //    writer.WriteEndElement(); // vivvalues

        //    writer.WriteStartElement("eddyvalues");
        //    for (int j = 0; j < data.VersionNames.Length; j++)
        //    {
        //        writer.WriteStartElement("valueset");
        //        writer.WriteAttributeString("name", data.VersionNames[j]);
        //        for (int i = 0; i < data.BaseTextSegmentIDs.Length; i++)
        //        {
        //            writer.WriteStartElement("eddyvalue");
        //            writer.WriteAttributeString("BaseTextSegId", data.BaseTextSegmentIDs[i].ToString());
        //            writer.WriteAttributeString("eddy", data.EddyValues[j][i].ToString());
        //            writer.WriteEndElement();
        //        }

        //        writer.WriteEndElement(); // valueset

        //    }
        //    writer.WriteEndElement(); // eddyvalues


        //    var predefs = corpus.GetPredefinedSegmentAttributes(false);

        //    writer.WriteStartElement("predefinedattribs");

        //    foreach (var p in predefs)
        //    {
        //        writer.WriteStartElement("predefinedattrib");

        //        writer.WriteAttributeString("id", p.ID.ToString());
        //        writer.WriteAttributeString("type", p.AttributeType.ToString());
        //        writer.WriteAttributeString("name", p.Name);
        //        writer.WriteAttributeString("showintoc", p.ShowInTOC.ToString());
        //        writer.WriteStartElement("predefinedattribvalues");
        //        if (p.Values != null)
        //            foreach (var v in p.Values)
        //            {
        //                writer.WriteStartElement("predefinedattribvalue");
        //                writer.WriteAttributeString("value", v.Value);
        //                writer.WriteAttributeString("applycolouring", v.ApplyColouring.ToString());
        //                writer.WriteAttributeString("colourcode", v.ColourCode.ToString());
        //                writer.WriteEndElement(); // predefinedattribvalue
        //            }
        //        writer.WriteEndElement(); // predefinedattribvalues
        //        writer.WriteEndElement(); // predefinedattrib
        //    }

        //    writer.WriteEndElement(); // predefinedattribs

        //    writer.WriteEndElement(); // eblacorpus

        //    //writer.Flush();
        //    //sw.Flush();
        //    writer.Close();
        //}


        [HttpGet]
        public ActionResult ExportCorpusPlain(string CorpusName)
        {
            try
            {
                ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);

                var output = new System.IO.MemoryStream();

                var zip = new ZipOutputStream(output);
                
                var versions = corpus.GetVersionList();
                foreach (var version in versions)
                {
                    var versionDoc = PrismHelpers.GetVersion(CorpusName, version);
                    var md = versionDoc.GetMetadata();
                    string filename = md.ReferenceDate + "-" + version;

                    string docContent = versionDoc.GetDocumentContentText(0, versionDoc.Length());


                    //byte[] buffer = new byte[4096];
                    using (var docStream = GenerateStreamFromString(docContent, true))
                    {
                        //ICSharpCode.SharpZipLib.Core. .Copy(docStream, zip, buffer);
                        var entry = new ZipEntry(PrismLib.PrismExport.SanitiseFilename(filename) + ".txt");
                        entry.Size = docStream.Length;
                        zip.PutNextEntry(entry);
                        CopyStream(docStream, zip);
                    }
                    zip.CloseEntry();

                }
                zip.Close();

                output = new System.IO.MemoryStream(output.ToArray());


#if DEBUG
                //var output2 = new System.IO.MemoryStream(output.GetBuffer());
                var testzip = new ZipInputStream(output);
                var testentry = testzip.GetNextEntry();
                while (testentry != null)
                {

                    testentry = testzip.GetNextEntry();
                }
                testzip.Close();
                output = new System.IO.MemoryStream(output.ToArray());
#endif

                string fname = CorpusName + "-export";
                fname = PrismLib.PrismExport.SanitiseFilename(fname);
                fname += ".zip";
                return File(output, "application/zip", fname);

            }
            catch (Exception ex)
            {
                string message = "An error occurred while trying to export the document: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }

        }


        internal static System.IO.Stream GenerateStreamFromString(string s, bool utf8bom)
        {
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            System.IO.StreamWriter writer;
            if (utf8bom)
                writer = new System.IO.StreamWriter(stream, new System.Text.UTF8Encoding(true));
            else
                writer = new System.IO.StreamWriter(stream);

            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        internal static void CopyStream(System.IO.Stream input, System.IO.Stream output)
        {
            byte[] buffer = new byte[32768];
            int read;
            while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, read);
            }
        }
        [HttpGet]
        public ActionResult ExportCorpus(string CorpusName)
        {
            try
            {

                ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);

                var output = new System.IO.MemoryStream();
                var sw = new System.IO.StreamWriter(output, System.Text.Encoding.UTF8);

                //string results = corpus.ExportCorpus();
                //sw.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
                //sw.Write(results);

                System.Xml.XmlWriterSettings settings = new System.Xml.XmlWriterSettings();
                settings.Indent = true;
                settings.Encoding = System.Text.Encoding.UTF8;
                var writer = System.Xml.XmlWriter.Create(sw, settings);
                ExportCorpus(writer, corpus, CorpusName, VariationMetricTypes.metricA);


                sw.Flush();
                output.Position = 0;

                string fname = CorpusName + "-export";
                fname = PrismLib.PrismExport.SanitiseFilename(fname);
                fname += ".xml";
                return File(output , "application/xml", fname);

            }
            catch (Exception ex)
            {
                string message = "An error occurred while trying to export the document: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }

        }

        public ActionResult CreateVersion(string CorpusName)
        {
            try
            {
                BaseDocumentModel model = new BaseDocumentModel();
                model.CorpusName = CorpusName;
                model.OriginalCorpusName = CorpusName;
                model.IsVersion = true;
                SetupDocumentDetails(true, true);
                SetupLangCodes(ViewData, PrismHelpers.GetCorpusStore().GetCultures());

                return View("DocumentDetails", model);
            }
            catch (Exception ex)
            {
                string message = "An error occurred while trying to open corpus '" + CorpusName + "': " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });
            }

        }

        [HttpPost]
        public ActionResult CreateVersion(BaseDocumentModel model)
        {
            try
            {
                SetupDocumentDetails(true, true);
                SetupLangCodes(ViewData, PrismHelpers.GetCorpusStore().GetCultures());
                if (ModelState.IsValid)
                {

                    if (model.NameIfVersion.Length < 3)
                    {
                        ModelState.AddModelError("versionname", "Please enter a name for the version, at least 3 letters long.");
                        return View("DocumentDetails", model);
                    }

                    ICorpus corpus = PrismHelpers.GetCorpus(model.CorpusName);

                    DocumentMetadata meta = new DocumentMetadata();
                    ModelToDocumentMetadata(model, meta);

                    corpus.CreateVersion(model.NameIfVersion, meta);

                    return RedirectToAction("Index", "Corpus", new { CorpusName = model.CorpusName });

                }


                return View("DocumentDetails", model);
            }
            catch (Exception ex)
            {
                string message = "An error occurred while trying to create the version: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }
        }

        [HttpPost]
        public string CreateAutoSegmentation(CorpusWizardModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var corpus =
                        PrismHelpers.GetCorpus(model.CorpusName).CreateCorpusAutoSegmentation(model.CorpusName, model.SegmentationType);
                }
                catch (Exception ex)
                {
                    return "An error occurred: " + ex.Message;
                }
            }
            else
            {
                return "Invalid corpus auto segmentation.";
            }

            return "Corpus auto segmentation was performed successfully.";
        }

        [HttpPost]
        public string CreateAutoAlignments(CorpusWizardModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var corpus =
                        PrismHelpers.GetCorpus(model.CorpusName).
                        CreateCorpusAutoAlignments(model.CorpusName, 
                        AccountController.SessionEblaUsername, AccountController.SessionEblaPassword,
                        model.AlignmentAlgorithm);
                }
                catch (Exception ex)
                {
                    return "An error occurred: " + ex.Message;
                }
            }
            else
            {
                return "Invalid corpus auto alignment.";
            }

            return "Corpus auto alignment was performed successfully.";
        }

        public ActionResult DocumentMetadata(string CorpusName, string NameIfVersion)
        {
            try
            {
                SetupLangCodes(ViewData, PrismHelpers.GetCorpusStore().GetCultures());

                BaseDocumentModel model = new BaseDocumentModel();
                model.CorpusName = CorpusName;
                model.OriginalCorpusName = CorpusName;
                if (!string.IsNullOrEmpty(NameIfVersion))
                {
                    model.IsVersion = true;
                    model.NameIfVersion = NameIfVersion;
                    model.OriginalNameIfVersion = NameIfVersion;
                }
                else
                {
                    var corp = PrismHelpers.GetCorpus(CorpusName);
                    model.CorpusDescription = corp.GetDescription();
                }

                IDocument doc = PrismHelpers.GetDocument(CorpusName, NameIfVersion);
                DocumentMetadataToModel(doc.GetMetadata(), model);

                SetupDocumentDetails(model.IsVersion, false);

                return View("DocumentDetails", model);


            }
            catch (Exception ex)
            {
                string message = PrismResources.An_error_occurred + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }
        }

        [HttpPost]
        public ActionResult DocumentMetadata(BaseDocumentModel model)
        {
            try
            {
                SetupDocumentDetails(model.IsVersion, false);
                SetupLangCodes(ViewData, PrismHelpers.GetCorpusStore().GetCultures());

                if (ModelState.IsValid)
                {
                    bool rename = false;
                    bool renameCorpus = false;
                    if (model.IsVersion)
                    {
                        if (model.NameIfVersion.CompareTo(model.OriginalNameIfVersion) != 0)
                        {
                            if (model.NameIfVersion.Length < 3)
                            {
                                ModelState.AddModelError("versionname", "Please specify a version name of at least 3 characters.");
                                return View("DocumentDetails", model);
                            }
                            rename = true;
                        }
                    }
                    else
                    {
                        if (model.CorpusName.CompareTo(model.OriginalCorpusName) != 0)
                            renameCorpus = true;

                    }

                    if (renameCorpus)
                    {
                        var store = PrismHelpers.GetCorpusStore();
                        store.RenameCorpus(model.OriginalCorpusName, model.CorpusName);
                    }

                    IDocument doc = PrismHelpers.GetDocument(model.CorpusName, model.OriginalNameIfVersion);

                    DocumentMetadata meta = new DocumentMetadata();
                    ModelToDocumentMetadata(model, meta);
                    doc.SetMetadata(meta);

                    ICorpus corpus = PrismHelpers.GetCorpus(model.CorpusName);
                    corpus.SetDescription(model.CorpusDescription);
                    if (rename)
                    {
                        corpus.RenameVersion(model.OriginalNameIfVersion, model.NameIfVersion);
                    }

                    


                    return RedirectToAction("Index", "Corpus", new { CorpusName = model.CorpusName });

                }


                return View("DocumentDetails", model);
            }
            catch (Exception ex)
            {
                string message = PrismResources.An_error_occurred + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }
        }

        public ActionResult DeleteVersion(string CorpusName, string VersionName)
        {

            try
            {

                ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);


                corpus.DeleteVersion(VersionName);

                return View("Index", GetModel(CorpusName));


            }
            catch (Exception ex)
            {
                string message = "An error occurred while trying to delete the version: " + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }
        }

        public ActionResult DeleteAttribute(string CorpusName, int ID)
        {
            try
            {
                ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);

                corpus.DeletePredefinedSegmentAttribute(ID);

                return RedirectToAction("PredefinedSegmentAttributes", new { CorpusName = CorpusName });
            }
            catch (Exception ex)
            {
                string message = PrismResources.An_error_occurred + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }
        }

        public ActionResult PredefinedSegmentAttributes(string CorpusName)
        {
            try
            {
                ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);

                PredefinedSegmentAttributesModel m = new PredefinedSegmentAttributesModel();
                m.CorpusName = CorpusName;

                EblaAPI.PredefinedSegmentAttribute[] names = corpus.GetPredefinedSegmentAttributes(false);
                List<Prism.Models.PredefinedSegmentAttribute> modelAttribs = new List<Models.PredefinedSegmentAttribute>();
                foreach (var name in names)
                {
                    Prism.Models.PredefinedSegmentAttribute modelAttrib = new Models.PredefinedSegmentAttribute();
                    modelAttrib.Name = name.Name;
                    modelAttrib.ID = name.ID;
                    modelAttrib.AttributeType = name.AttributeType;
                    modelAttrib.ShowInTOC = name.ShowInTOC;
                    List<Models.PredefinedSegmentAttributeValue> vals = new List<Models.PredefinedSegmentAttributeValue>();
                    foreach (var v in name.Values)
                    {
                        Models.PredefinedSegmentAttributeValue val = new Models.PredefinedSegmentAttributeValue();
                        val.Value = v.Value;
                        val.ColourCode = v.ColourCode;
                        val.ApplyColouring = v.ApplyColouring;
                        //val.IsTOCText = v.IsTOCText;
                        //val.ShowInTOC = v.ShowInTOC;
                    }
                    modelAttrib.Values = vals.ToArray();
                    modelAttribs.Add(modelAttrib);
                }


                m.PredefinedSegmentAttributes = modelAttribs.ToArray();

                return View( m);

            }
            catch (Exception ex)
            {
                string message = PrismResources.An_error_occurred + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }

        }

        public ActionResult PredefinedSegmentAttribute(string CorpusName, int? ID)
        {
            try
            {
                ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);

                PredefinedSegmentAttributeModel m = new PredefinedSegmentAttributeModel();
                m.CorpusName = CorpusName;
                m.PredefinedSegmentAttribute = new Models.PredefinedSegmentAttribute();
                m.PredefinedSegmentAttribute.ID = -1;
                m.PredefinedSegmentAttribute.AttributeType = PredefinedSegmentAttributeType.Text;
                m.PredefinedSegmentAttribute.ShowInTOC = false;
                m.PredefinedSegmentAttribute.Values = new Models.PredefinedSegmentAttributeValue[0];
                if (ID.HasValue)
                {
                    EblaAPI.PredefinedSegmentAttribute n = corpus.GetPredefinedSegmentAttribute(ID.Value);

                    m.PredefinedSegmentAttribute.Name = n.Name;
                    m.PredefinedSegmentAttribute.ID = n.ID;
                    m.PredefinedSegmentAttribute.ShowInTOC = n.ShowInTOC;
                    List<Models.PredefinedSegmentAttributeValue> vals = new List<Models.PredefinedSegmentAttributeValue>();
                    foreach (var v in n.Values)
                    {
                        Models.PredefinedSegmentAttributeValue val = new Models.PredefinedSegmentAttributeValue();
                        val.Value = v.Value;
                        val.ColourCode = v.ColourCode;
                        val.ApplyColouring = v.ApplyColouring;
                        //val.ShowInTOC = v.ShowInTOC;
                        //val.IsTOCText = v.ShowInTOC;
                        vals.Add(val);
                    }
                    m.PredefinedSegmentAttribute.Values = vals.ToArray();
                    m.PredefinedSegmentAttribute.AttributeType = n.AttributeType;
                    
                }
                ViewData["nextvaluerowindex"] = m.PredefinedSegmentAttribute.Values.Length;

                return View(m);

            }
            catch (Exception ex)
            {
                string message = PrismResources.An_error_occurred + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }
        }

        [HttpPost]
        public ActionResult PredefinedSegmentAttribute(PredefinedSegmentAttributeModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ICorpus corpus = PrismHelpers.GetCorpus(model.CorpusName);

                    EblaAPI.PredefinedSegmentAttribute n = new EblaAPI.PredefinedSegmentAttribute();
                    n.ID = model.PredefinedSegmentAttribute.ID;
                    n.Name = model.PredefinedSegmentAttribute.Name;
                    n.ShowInTOC = model.PredefinedSegmentAttribute.ShowInTOC;
                    List<EblaAPI.PredefinedSegmentAttributeValue> vals = new List<EblaAPI.PredefinedSegmentAttributeValue>();
                    if (model.PredefinedSegmentAttribute.Values != null)
                        foreach (var v in model.PredefinedSegmentAttribute.Values)
                        {
                            EblaAPI.PredefinedSegmentAttributeValue val = new EblaAPI.PredefinedSegmentAttributeValue();
                            val.Value = v.Value;
                            val.ApplyColouring = v.ApplyColouring;
                            //val.ShowInTOC = v.ShowInTOC;
                            //val.IsTOCText = v.IsTOCText;
                            val.ColourCode = v.ColourCode;
                            vals.Add(val);
                        }
                    n.Values = vals.ToArray();
                    n.AttributeType = model.PredefinedSegmentAttribute.AttributeType;

                    if (model.PredefinedSegmentAttribute.ID == -1)
                        corpus.CreatePredefinedSegmentAttribute(n);
                    else
                        corpus.UpdatePredefinedSegmentAttribute(n);


                    return RedirectToAction("PredefinedSegmentAttributes", new { CorpusName = model.CorpusName });
                }

                ViewData["nextvaluerowindex"] = 0;

                if (model.PredefinedSegmentAttribute.Values != null)
                    ViewData["nextvaluerowindex"] = model.PredefinedSegmentAttribute.Values.Length;

                return View(model);
            }
            catch (Exception ex)
            {
                string message = PrismResources.An_error_occurred + ex.Message;
                return RedirectToAction("Error", "Home", new { message = message });

            }
        }

        [HttpGet]
        public ActionResult CorpusCss(string CorpusName)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            try
            {
                ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);

                EblaAPI.PredefinedSegmentAttribute[] atts = corpus.GetPredefinedSegmentAttributes(false);

                foreach (var a in atts)
                {
                    foreach (var v in a.Values)
                    {
                        if (v.ApplyColouring)
                        {
                            sb.AppendLine("*[data-eblatype='segformat'][data-userattrib-" + a.Name + "='" + v.Value + "']");
                            sb.AppendLine("{");
                            sb.AppendLine("background-color: #" + v.ColourCode);
                            sb.AppendLine("}");

                        }
                    }
                }

                sb.AppendLine("*[data-eblatype='segformat'][data-ebla-cb='1']");
                sb.AppendLine("{");
                sb.AppendLine("color: LightGray");
                sb.AppendLine("}");
            }
            catch (Exception ex)
            {
                // TODO - log
            }

            return Content(sb.ToString(), "text/css");
        }

        //==============================================
        //
        //  AJAX calls
        //
        //==============================================

        [HttpPost]
        public ActionResult UpdateTokenisations(string CorpusName)
        {
            try
            {
                var corpus = PrismHelpers.GetCorpus(CorpusName);
                var versions = corpus.GetVersionList();

                var basetext = PrismHelpers.GetBaseText(CorpusName);

                var cbFilter = new List<SegmentContentBehaviour>() { SegmentContentBehaviour.normal };

                var segdefs = basetext.FindSegmentDefinitions(0, basetext.Length(), false, false, null, cbFilter.ToArray());
                foreach (var segdef in segdefs)
                    basetext.UpdateSegmentDefinition(segdef, true);

                foreach (var v in versions)
                {
                    var version = PrismHelpers.GetVersion(CorpusName, v);

                    segdefs = version.FindSegmentDefinitions(0, version.Length(), false, false, null, cbFilter.ToArray());

                    foreach (var segdef in segdefs)
                        version.UpdateSegmentDefinition(segdef, true);

                }


                return MonoWorkaroundJson(new ResultModel { Succeeded = true });
            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }
        }

        [HttpPost]
        public ActionResult BeginVariationCalculation(string CorpusName, int MetricType)
        {
            try
            {
                VariationMetricTypes metricType = (VariationMetricTypes)MetricType;
                BackgroundCalculationMgr.StartCalculation(CorpusName, metricType);

                return MonoWorkaroundJson(new ResultModel { Succeeded = true });

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }

        }

        public ActionResult GetCalcStatus(string CorpusName)
        {
            try
            {

                bool b = BackgroundCalculationMgr.IsCorpusBusy(CorpusName);
                string s = BackgroundCalculationMgr.GetCorpusStatus(CorpusName);

                return MonoWorkaroundJson(new CalculationStatusResultModel { Succeeded = true, Status = s, Busy = b });

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }

        }

        public ActionResult CancelCalc(string CorpusName)
        {
            try
            {

                BackgroundCalculationMgr.Cancel(CorpusName);

                return MonoWorkaroundJson(new ResultModel { Succeeded = true });

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }

        }

        [HttpPost]
        public ActionResult GetNewAttributeRowHtml(string memberName, string CorpusName, int rowindex, string currName)
        {
            try
            {
                return MonoWorkaroundJson(new GetHtmlResultModel { Succeeded = true, html = AttributeRowHtml(memberName, CorpusName, rowindex, currName, null, false) });

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }

        }

        [HttpPost]
        public ActionResult GetNewPredefinedAttributeValueRowHtml(int rowindex)
        {
            try
            {
                return MonoWorkaroundJson(new GetHtmlResultModel { Succeeded = true, html = PredefinedAttributeValueRowHtml(rowindex, null) });

            }
            catch (Exception ex)
            {
                return MonoWorkaroundJson(new ResultModel { Succeeded = false, ErrorMsg = System.Net.WebUtility.HtmlEncode(ex.Message) });

            }

        }


        //==============================================
        //
        //  Implementation
        //
        //==============================================

        internal void SetupDocumentDetails(bool isVersion, bool creating)
        {
            System.Diagnostics.Debug.Assert(!(creating && !isVersion));
            string verb = creating ? "Create" : "Edit";
            string obj = isVersion ? " version" : " base text";

            ViewData["title"] = verb + obj;
            string subtitle = (verb + obj).ToLower();
            if (!creating)
                subtitle += " details";
            ViewData["subtitle"] = subtitle;

        }

        internal static CorpusModel GetModel(string CorpusName)
        {
            CorpusModel model = new CorpusModel();
            ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);

            model.CorpusName = CorpusName;
            model.CorpusDescription = corpus.GetDescription();
            model.CanWrite = PrismHelpers.CanWrite(CorpusName);
            model.Cultures = PrismHelpers.GetCorpusStore().GetCultures();

            DocumentModel baseText = new DocumentModel();
            IDocument baseDoc = PrismHelpers.GetBaseText(CorpusName);
            DocumentMetadata meta = baseDoc.GetMetadata();
            DocumentMetadataToModel(meta, baseText);
            baseText.DocLength = baseDoc.Length();
            model.BaseText = baseText;
            model.LanguageCode = meta.LanguageCode;

            string[] vers = corpus.GetVersionList();

            List<DocumentModel> l = new List<DocumentModel>();
            foreach (string s in vers)
            {
                IDocument d = PrismHelpers.GetDocument(CorpusName, s);
                meta = d.GetMetadata();
                DocumentModel m = new DocumentModel();
                DocumentMetadataToModel(meta, m);
                m.CorpusName = CorpusName;
                m.NameIfVersion = s;
                //meta = d.GetMetadata();
                //m.Description = meta.Description;
                m.DocLength = d.Length();
                l.Add(m);
            }

            l.Sort( (a,b) => (a.SafeDate().CompareTo(b.SafeDate()) )) ;
            model.VersionList = l.ToArray();
            return model;
        }

        internal static void SetupLangCodes(ViewDataDictionary viewdatadict, SortedDictionary<string, string> cultures)
        {
            var items = SetupLangCodes(cultures);

            viewdatadict["langcodes"] = items;
        }

        public static List<SelectListItem> SetupLangCodes(SortedDictionary<string, string> cultures)
        {
            var items = new List<SelectListItem>();

            foreach (string s in cultures.Keys)
                items.Add(new SelectListItem() { Text = s, Value = cultures[s] });

            return items;
        }

        private static string GetAlgorithm(string algorithm)
        {
            AlignmentAlgorithms newAlgorithm;
            string description = null;
            if (Enum.TryParse(algorithm, out newAlgorithm))
            {
                description = EblaHelpers.GetEnumDescription(newAlgorithm);
            }
            return description;
        }

        public static string AttributeRowHtml(string memberName, string CorpusName, int rowindex, string currName, string currVal, bool builtIn)
        {

            ICorpus corpus = PrismHelpers.GetCorpus(CorpusName);
            var atts = new List<EblaAPI.PredefinedSegmentAttribute>(corpus.GetPredefinedSegmentAttributes(true));
            atts = atts.FindAll(x => !x.ReadOnly);
            
            EblaAPI.PredefinedSegmentAttribute pre = null;

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            string indexval = rowindex.ToString();
            //string rowid = "attribrow-" + indexval;
            string nameselectname = memberName + "[" + indexval + "].Name";
            string valueinputname = memberName + "[" + indexval + "].Value";

            //sb.Append("<tr id='" + rowid + "'>");

            sb.Append("<td>");
            sb.Append("<input type='hidden' name='" + memberName + ".Index' value='" + indexval + "' />");
            sb.Append("<span class='editor-field'>");
            sb.Append(" <select class='attribnameselect' " + (builtIn ? string.Empty /* "disabled='disabled'"*/ : string.Empty) + "  name='" + nameselectname + "'>");

            bool first = true;
            foreach (var a in atts)
            {
                if (first)
                {
                    // If we're being called to add a new, empty row
                    if (currName == null)
                    {
                        // then if the default (first) predefined attrib is a list
                        if (a.AttributeType == PredefinedSegmentAttributeType.List)
                        {
                            // then record it for use in value input fixup
                            pre = a;
                        }
                    }
                    first = false;
                }

                string selected = string.Empty;
                if (currName != null)
                    if (string.Compare(currName, a.Name) == 0)
                    {
                        selected = "selected";
                        if (a.AttributeType == PredefinedSegmentAttributeType.List)
                            pre = a;
                    }
                string temp = System.Net.WebUtility.HtmlEncode(a.Name);
                sb.Append("<option " + selected + " value='" + temp + "'>" + temp + "</option>");

            }

            sb.Append("</select>");
            sb.Append("</span>");
            sb.Append("</td>");

            sb.Append("<td>");
            sb.Append("<span class='editor-field'>");

            if (pre != null)
            {
                sb.Append(" <select " + (builtIn ? string.Empty /* "disabled='disabled'" */ : string.Empty) + "  name='" + valueinputname + "'>");
                foreach (var p in pre.Values)
                {
                    string selected = string.Empty;
                    if (currVal != null)
                        if (string.Compare(currVal, p.Value) == 0)
                            selected = "selected";

                    string temp = System.Net.WebUtility.HtmlEncode(p.Value);
                    sb.Append("<option " + selected + " value='" + temp + "'>" + temp + "</option>");

                }
                sb.Append("</select>");

            }
            else
            {
                sb.Append("<input type='text' " + (builtIn ? string.Empty /* "disabled='disabled'"*/ : string.Empty) + "   class='half' name='" + valueinputname + "' value='" + (currVal == null ? string.Empty : System.Net.WebUtility.HtmlEncode(currVal)) + "' />");
            }

            sb.Append("</span>");
            sb.Append("</td>");

            sb.Append("<td>");
            if (!builtIn)
                sb.Append("<input type='button' onclick='PrismNS.Utils.DeleteAttrib(this)' value='Delete' />");
            sb.Append("</td>");

            //sb.Append("</tr>");

            return sb.ToString();
        }

        private static string Checked(bool b)
        {
            return b ? "checked='checked'" : string.Empty;
        }

        public static string PredefinedAttributeValueRowHtml(int rowindex, Models.PredefinedSegmentAttributeValue v)
        {
            if (v == null)
                v = new Models.PredefinedSegmentAttributeValue();

            System.Text.StringBuilder result = new System.Text.StringBuilder();

            result.Append("<td>");
            result.Append("<input type='hidden' name='PredefinedSegmentAttribute.Values.Index' value='" + rowindex.ToString() + "' />");
            result.Append("<span class='editor-field'> <input type='text' class='half' name='PredefinedSegmentAttribute.Values[" + rowindex.ToString() + "].Value' value='");
            result.Append(System.Net.WebUtility.HtmlEncode(v.Value));
            result.Append("' /></span>");
            result.Append("</td>");

            result.Append("<td>");
            result.Append("<span class='editor-field'>");
            result.Append("<input type='checkbox' class='ApplyColouringCheckbox' id='applycolouringcheckbox-" + rowindex.ToString() + "' ");
            result.Append(Checked(v.ApplyColouring));
            result.Append(" />");
            result.Append("<input type='hidden' id='applycolouringinput-" + rowindex.ToString() + "' name='PredefinedSegmentAttribute.Values[" + rowindex.ToString() + "].ApplyColouring' />");
            result.Append("</span");
            result.Append("</td>");

            result.Append("<td>");
            result.Append("<span class='editor-field'>");
            result.Append("<input class='color' id='colourcode-" + rowindex.ToString() + "'  name='PredefinedSegmentAttribute.Values[" + rowindex.ToString() + "].ColourCode' value='");
            result.Append(System.Net.WebUtility.HtmlEncode(v.ColourCode));
            result.Append("' />");
            result.Append("</span");
            result.Append("</td>");


            result.Append("<td>");
            result.Append("<input type='button' onclick='deletevalue(this)' value='Delete' />");
            result.Append("</td>");

            return result.ToString();
        }

        internal static void DocumentMetadataToModel(DocumentMetadata meta, BaseDocumentModel model)
        {
            model.Genre = meta.Genre;
            model.AuthorTranslator = meta.AuthorTranslator;
            model.Description = meta.Description;
            model.CopyrightInfo = meta.CopyrightInfo;
            model.ReferenceDate = meta.ReferenceDate;
            model.LanguageCode = meta.LanguageCode;
            model.Information = meta.Information;
        }

        internal static void ModelToDocumentMetadata(BaseDocumentModel model, DocumentMetadata meta)
        {
            meta.Genre = model.Genre;
            meta.AuthorTranslator = model.AuthorTranslator;
            meta.Description = model.Description;
            meta.CopyrightInfo = model.CopyrightInfo;
            meta.ReferenceDate = model.ReferenceDate;
            meta.LanguageCode = model.LanguageCode;
            meta.Information = model.Information;
        }

    }

    internal class BackgroundCalculationMgr
    {
        static object _lock = new object();

        public static bool IsCorpusBusy(string CorpusName)
        {
            lock (_lock)
            {
                if (TaskList.ContainsKey(CorpusName))
                {
                    if (TaskList[CorpusName].Status() == System.Threading.ThreadState.Running)
                        return true;

                }
                return false;

            }
                
        }

        public static void Cancel(string CorpusName)
        {
            lock (_lock)
            {
                if (TaskList.ContainsKey(CorpusName))
                {
                    TaskList[CorpusName].RequestStop();

                }

            }

        }

        public static string GetCorpusStatus(string CorpusName)
        {
            lock (_lock)
            {
                if (TaskList.ContainsKey(CorpusName))
                {
                    return TaskList[CorpusName].StatusText();

                }

                return "none.";

            }
        }

        public static void StartCalculation(string CorpusName, VariationMetricTypes metricType)
        {
            lock (_lock)
            {
                if (IsCorpusBusy(CorpusName))
                    throw new Exception("Calculation for the corpus is already in progress.");

                var c = new BackgroundCalculation();

                c.Start(CorpusName, metricType);
                if (TaskList.ContainsKey(CorpusName))
                    TaskList.Remove(CorpusName);
                TaskList.Add(CorpusName, c);
            }

        }

        static private Dictionary<string, BackgroundCalculation> TaskList
        {
            get
            {
                return _taskList;
            }
        }

        // Might be better off with ASP.NET 'application state', but in any event, this is just a cheap MVC
        // implementation of something that ought to be in a separate service
        static private Dictionary<string, BackgroundCalculation> _taskList = new Dictionary<string, BackgroundCalculation>();
    }


    internal class BackgroundCalculation
    {
        System.Threading.Thread _thread;
        ICorpus _corpus;
        string _corpusName;
        object _lock;
        DateTime _timeStarted;
        string _statusText;
        

        public void Start(string CorpusName, VariationMetricTypes metricType)
        {
            _lock = new object();

            _corpus = PrismHelpers.GetCorpus(CorpusName);
            _corpusName = CorpusName;

            _corpus.StartSegmentVariationCalculation(metricType);
            SetStatusText("0% complete.");

            _thread = new System.Threading.Thread(this.DoWork);
            _thread.Priority = System.Threading.ThreadPriority.BelowNormal;
            _timeStarted = DateTime.Now;
            _thread.Start();

        }

        public void DoWork()
        {
            while (true)
            {
                if (_shouldStop)
                {
                    SetStatusText("Cancelled.");
                    break;
                }

                try
                {
                    int progress = 0;
                    bool moreToDo = _corpus.ContinueSegmentVariationCalculation(out progress);
                    if (!moreToDo)
                    {
                        SetStatusText("Completed.");
                        break;

                    }
                    SetStatusText(progress.ToString() + "% complete.");
                }
                catch (Exception ex)
                {
                    SetStatusText("Aborted; error message: " + ex.Message);
                    break;

                }

            }

        }

        private volatile bool _shouldStop;

        public void RequestStop()
        {
            _shouldStop = true;
        }

        public System.Threading.ThreadState Status()
        {
            return _thread.ThreadState;
        }

        public string StatusText()
        {
            lock (_lock)
                return _statusText;
        }

        private void SetStatusText(string s)
        {
            lock (_lock)
                _statusText = s;
        }


    }

}
