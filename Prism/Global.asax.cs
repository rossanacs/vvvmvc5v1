﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using EblaAPI;
using EblaImpl;
using System.Configuration;
using System.Globalization;
using System.Threading;
using System.Xml;
using Prism.Controllers;

namespace Prism
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            ModelBinders.Binders.DefaultBinder = new TrimModelBinder();

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            EblaCxnMgr.Init(PrismEblaSupport.GetConnection, PrismEblaSupport.SetConnection, PrismEblaSupport.GetRequestData, PrismEblaSupport.SetRequestData);

        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            // Dispose any DB connection
            object o = PrismEblaSupport.GetConnection();
            if (o != null)
            {
                IDisposable d = o as IDisposable;
                if (d != null)
                    d.Dispose();
            }
        }

        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
            //var handler = Context.Handler as MvcHandler;
            //var routeData = handler?.RequestContext.RouteData;
            //var routeCulture = routeData?.Values["culture"].ToString();
            var languageCookie = HttpContext.Current.Request.Cookies["lang"];
            var userLanguages = HttpContext.Current.Request.UserLanguages;

            // TODO - support route cultures for better SEO if desired.
            // Meantime, just use cookie if found

            //// Set the Culture based on a route, a cookie or the browser settings,
            //// or default value if something went wrong
            //var cultureInfo = new CultureInfo(
            //    routeCulture ?? (languageCookie != null
            //        ? languageCookie.Value
            //        : userLanguages != null
            //            ? userLanguages[0]
            //            : "en")
            //);

            if (languageCookie != null)
            {
                try
                {
                    var cultureInfo = new CultureInfo(languageCookie.Value);
                    Thread.CurrentThread.CurrentUICulture = cultureInfo;
                    Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(cultureInfo.Name);
                }
                catch (Exception exception)
                {
                    //Console.WriteLine(exception);
                    //throw;
                }
            }

        }

        public class TrimModelBinder : DefaultModelBinder
        {
            protected override void SetProperty(ControllerContext controllerContext,
            ModelBindingContext bindingContext,
            System.ComponentModel.PropertyDescriptor propertyDescriptor, object value)
            {
                if (propertyDescriptor.PropertyType == typeof(string))
                {
                    var stringValue = (string)value;
                    if (!string.IsNullOrEmpty(stringValue))
                        stringValue = stringValue.Trim();

                    value = stringValue;
                }

                base.SetProperty(controllerContext, bindingContext,
                propertyDescriptor, value);
            }
        }

    }

    internal class PrismEblaSupport
    {
        internal static object GetConnection()
        {
            // Use the per-request object cache (http://www.4guysfromrolla.com/articles/060904-1.aspx)
            var current = System.Web.HttpContext.Current;
            if (current == null) return current;

            return System.Web.HttpContext.Current.Items["EblaConn"];
        }

        internal static void SetConnection(object conn)
        {
            var current = System.Web.HttpContext.Current;
            if (current != null)
                current.Items["EblaConn"] = conn;
        }

        internal static object GetRequestData()
        {
            // Use the per-request object cache (http://www.4guysfromrolla.com/articles/060904-1.aspx)
            var current = System.Web.HttpContext.Current;
            if (current == null) return current;
            return System.Web.HttpContext.Current.Items["EblaReqData"];
        }

        internal static void SetRequestData(object data)
        {
            var current = System.Web.HttpContext.Current;
            if (current != null)
                current.Items["EblaReqData"] = data;
        }

    }

    public class PrismHelpers
    {

        static public bool UseFooter
        {
            get
            {
                object o = ConfigurationManager.AppSettings["UseFooter"];
                if (o != null)
                    return bool.Parse(o.ToString());
                return false;
            }
        }
        static public bool IsPublic
        {
            get
            {
                object o = ConfigurationManager.AppSettings["IsPublic"];
                if (o != null)
                    return bool.Parse(o.ToString());
                return false;
            }
        }
        static public bool IsThirdParty
        {
            get
            {
                object o = ConfigurationManager.AppSettings["IsThirdParty"];
                if (o != null)
                    return bool.Parse(o.ToString());
                return false;
            }
        }

        internal static ICorpus GetCorpus(string name)
        {
            // TODO - consider caching in Session
            ICorpus corpus = EblaFactory.GetCorpus(ConfigurationManager.AppSettings);

            if (!corpus.Open(name, AccountController.SessionEblaUsername, AccountController.SessionEblaPassword))
                throw new Exception("You do not have permission to access the corpus.");

            return corpus;
        }

        internal static ICorpusStore GetCorpusStore()
        {
            ICorpusStore store = EblaFactory.GetCorpusStore(ConfigurationManager.AppSettings);

            if (!store.Open(AccountController.SessionEblaUsername, AccountController.SessionEblaPassword))
                return null;
            return store;
        }

        internal static bool CanWrite(string CorpusName)
        {
            if (AccountController.SessionEblaIsAdmin)
                return true;

            // TODO - something more efficient than this.
            ICorpusStore store = EblaFactory.GetCorpusStore(ConfigurationManager.AppSettings);

            store.Open(AccountController.SessionEblaUsername, AccountController.SessionEblaPassword);

            var rights = store.GetUserInfo(AccountController.SessionEblaUsername, null);

            foreach (var r in rights[0].CorpusRights)
            {
                if (string.Compare(r.CorpusName, CorpusName) == 0)
                {
                    if (r.CanWrite)
                        return true;
                    break;
                }
            }

            return false;
        }

        internal static IDocument GetBaseText(string corpusName)
        {
            // TODO - consider caching in Session
            IDocument doc = EblaFactory.GetDocument(ConfigurationManager.AppSettings);

            if (!doc.OpenBaseText(corpusName, AccountController.SessionEblaUsername, AccountController.SessionEblaPassword))
                throw new Exception("You do not have permission to access the document.");

            return doc;
        }

        internal static IDocument GetVersion(string corpusName, string versionName)
        {
            // TODO - consider caching in Session
            IDocument doc = EblaFactory.GetDocument(ConfigurationManager.AppSettings);

            if (!doc.OpenVersion(corpusName, versionName, AccountController.SessionEblaUsername, AccountController.SessionEblaPassword))
                throw new Exception("You do not have permission to access the document.");

            return doc;
        }

        internal static IAlignmentSet GetAlignmentSet(string corpusName, string versionName)
        {
            // TODO - consider caching in Session
            IAlignmentSet set = EblaFactory.GetAlignmentSet(ConfigurationManager.AppSettings);

            if (!set.Open(corpusName, versionName, AccountController.SessionEblaUsername, AccountController.SessionEblaPassword))
                throw new Exception("You do not have permission to access the alignment set.");

            return set;
        }


        internal static IDocument GetDocument(string corpusName, string nameIfVersion)
        {
            if (string.IsNullOrEmpty(nameIfVersion))
                return GetBaseText(corpusName);

            return GetVersion(corpusName, nameIfVersion);
        }

        internal static System.Text.Encoding PrismEncodingToTextEncoding(PrismEncoding encoding)
        {
            switch (encoding)
            {
                case PrismEncoding.ASCII:
                    return System.Text.Encoding.ASCII;
                case PrismEncoding.UTF8:
                    return System.Text.Encoding.UTF8;
                case PrismEncoding.UTF7:
                    return System.Text.Encoding.UTF7;
                case PrismEncoding.UTF32:
                    return System.Text.Encoding.UTF32;
                case PrismEncoding.Unicode:
                    return System.Text.Encoding.Unicode;
                case PrismEncoding.BigEndianUnicode:
                    return System.Text.Encoding.BigEndianUnicode;

            }

            throw new Exception("Unrecognised PrismEncoding: " + encoding.ToString());
        }

        internal static XmlDocument XmlDocFromContent(string content)
        {
            string xml = @"<?xml version=""1.0"" ?><body>";

            xml += content;

            xml += "</body>";

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xml);
            return xmlDoc;
        }

        internal static ITranslationAligner GetTranslationAligner(string corpusName, string versionName)
        {
            var set = EblaFactory.GetTranslationAligner(ConfigurationManager.AppSettings);

            return set;
        }

    }

    public enum PrismEncoding
    {
        ASCII,
        UTF8,
        Unicode,
        UTF7,
        UTF32,
        BigEndianUnicode
    }
}
