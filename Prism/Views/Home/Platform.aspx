﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="aboutTitle" ContentPlaceHolderID="TitleContent" runat="server">
    VVV - Technology
</asp:Content>

<asp:Content ID="aboutContent" ContentPlaceHolderID="MainContent" runat="server">
	<div class="container">	
	    <header>
			<h1>Version Variation Visualization: Technology</h1>
			<p class="lead">Explore great works with their world-wide translations</p>
		</header>
		<section id="technology">
			<div class="page-header">
				<h2>Implementation<small>Ebla and Prism</small></h2>
			</div>
			<div class="row">
				<div class="span6">
					<p>The kinds of relationship that exist between a text and its translation are complex, and their
					definitions contested. For many purposes, a text and translation can be divided up in different
					ways, and correspondences drawn between their respective parts. This process is often referred
					to as alignment. In some cases, it can lead to a straightforward and sequential one-to-one
					correspondence between a sentence in the text and its rendering in the translation. However,
					with literary translation in particular, the correspondences are often much less straightforward.</p>
					
					<p>To create a Translation Array (TA), a text must be aligned with numerous translations in a given
					language. Some of those alignments may be straightforward, and others, less so. This application
					provides tools for doing so and for working with the results. It is designed to be as generally
					useful as possible, so enables the user to define a given base text and align it with an arbitrary
					number of versions. Base text and versions are referred to as documents. The base text and set
					of versions are together referred to as a corpus.</p>
				
					<p>In this implementation, TA functionality is split into two main components:
						<ul>
							<li>Ebla: stores documents, configuration details, segment and alignment information, calculates
							variation statistics, and renders documents with segment/variation information</li>
							<li>Prism: provides a web-based interface for uploading, segmenting and aligning documents,
							then visualising document relationships.</li>
						</ul>
						Areas of interest in a document are demarcated using segments, which also can be nested or
						overlapped. Each segment can have an arbitrary number of attributes, which for a play might be
						‘type’ (with values such as ‘Speech’, ‘Stage Direction), ‘Speaker’ (with values such as ‘Othello’,
						‘Brabantio’), and so on.
					</p>
				</div>
				<div class="span6">
					<p>In a similar way to <a href="http://www.catma.de">CATMA</a>, segment positions are stored as character
					offsets within the document. (Like CATMA, this constrains editing of the document once
					segments exist for a document.) Documents uploaded can contain markup, so HTML documents
					can be stored (in which case, some sanitisation is performed). When uploaded HTML documents
					are retrieved from Ebla and displayed by Prism, their HTML markup is preserved, so providing a
					WYSIWYG rendering of the document when segmenting, aligning or visualising. If HTML
					formatting is to be applied to segments for display within Prism – which can fall anywhere within
					an HTML document structure – Ebla applies a recursive ‘greedy’ algorithm to add the minimum
					additional tags required to achieve the formatting without breaking the document structure. If only
					part of a document is to be displayed – say, a section from the middle – Ebla ensures that any
					tags required from outside that part are appended or prepended, to ensure the partial content
					rendered is both valid and correctly formatted.</p>
					
					<p>After segmentation, documents can be aligned using an interactive WYWISYG tool. Attribute
					filters can be applied to select only segments of interest, and limited ‘auto-align’ functionality
					used to expedite the process. Ebla can be used to calculate different kinds of variation statistics
					for base text segments based on aligned corpus content, which can potentially be aggregated-up
					for more coarse-grained use. The results can be navigated and explored using the visualisation
					functionality in Prism. However, translation variation is just one of the corpus properties that
					could be investigated. Once aligned, the data could be analysed in many other ways.</p>
					
				</div>
			</div>
		</section>
		<section id="design">
			<div class="page-header">
				<h2>Interface and visualization design</h2>
			</div>
			<div class="row">
				<div class="span6">
					<p>The interface design should provide a simplified and manageable way for the exploration of 
					complex data and support the advancements of new scientific findings.
					The design work consist of experimental approaches and methods and is an ongoing 
					process which is affected by the (intern) dialogue with linguists and software engineers.</p>

					<p>On the one hand, the design part consists of the high-level structure visualizations and on the other hand 
					of the text-based  view.
					The high-level structure visualizations should make it possible to find e.g. patterns and consequently 
					advance complex topics in the scientific field.
					The key aspect of the text-based view is the readability of text. Besides the text, the view offers 
					various tools to simplify the analyze and evaluation of different version variations.
					In more detail, the navigation visualization, filter and sort functions should provide a faster 
					search for a specific passage and hence enable a new way of text exploration.
					Another aspect of the text-based view is the possibility to edit text in a new collaborative way 
					which should be implemented in future releases.</p>

					<p>Finally, we are focused on an interface design which should enable a larger target group to 
					intuitively interact with and explore great works and their world-wide translations.</p>
				</div>
			</div>
		</section>
	</div>
</asp:Content>
