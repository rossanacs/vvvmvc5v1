﻿/*!

 =========================================================
 * Paper Bootstrap Wizard - v1.0.2
 =========================================================
 
 * Product Page: https://www.creative-tim.com/product/paper-bootstrap-wizard
 * Copyright 2017 Creative Tim (http://www.creative-tim.com)
 * Licensed under MIT (https://github.com/creativetimofficial/paper-bootstrap-wizard/blob/master/LICENSE.md)
 
 =========================================================
 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 */
transparent = true;
var path;
//+ "//vvvdraft";

$(document).ready(function () {
    "use strict";
    path = window.location.protocol + "//" + window.location.host + $('#siteUrl').val();
    $("#wizard").bootstrapWizard({
        'tabClass': "nav nav-pills",
        'nextSelector': ".btn-next",
        onInit: function (tab, navigation, index) {

            //check number of tabs and fill the entire row
            var $total = navigation.find("li").length;
            var $width = 100 / $total;

            navigation.find("li").css("width", $width + "%");
        },
        onTabShow: function (tab, navigation, index) {
            var $total = navigation.find("li").length;
            var $current = index + 1;

            var $wizard = navigation.closest(".wizard-card");

            // If it's the last tab then hide the last button and show the finish instead
            if ($current >= $total) {
                $($wizard).find(".btn-next").hide();
                $($wizard).find(".btn-finish").show();
            } else {
                $($wizard).find(".btn-next").show();
                $($wizard).find(".btn-finish").hide();
            }

            $wizard.find($(".wizard-card .nav-pills li.active a .icon-circle")).addClass("checked");

        }
        ,
        onNext: function (tab, navigation, index) {
            var $valid = $(".wizard-card form").valid();
            return $valid;
        }
    });

    
    $('.nav li').not('.active').find('a').removeAttr("data-toggle");

    // Prepare the preview for profile picture
    $("#wizard-picture").change(function () {
        readURL(this);
    });

    $('[data-toggle="wizard-radio"]').click(function () {
        var $wizard = $(this).closest(".wizard-card");
        $wizard.find('[data-toggle="wizard-radio"]').removeClass("active");
        $(this).addClass("active");
        $wizard.find('[type="radio"]').removeAttr("checked");
        $(this).find('[type="radio"]').attr("checked", "true");
    });

    $('[data-toggle="wizard-checkbox"]').click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).find('[type="checkbox"]').removeAttr('checked');
        } else {
            $(this).addClass('active');
            $(this).find('[type="checkbox"]').attr('checked', 'true');
        }
    });

    $(document).bind("dragover",
        function(e) {
            var dropZones = $(".dropzone"),
                timeout = window.dropZoneTimeout;
            if (timeout) {
                clearTimeout(timeout);
            } else {
                dropZones.addClass("in");
            }
            var hoveredDropZone = $(e.target).closest(dropZones);
            dropZones.not(hoveredDropZone).removeClass("hover");
            hoveredDropZone.addClass("hover");
            window.dropZoneTimeout = setTimeout(function() {
                    window.dropZoneTimeout = null;
                    dropZones.removeClass("in hover");
                },
                100);
        });

    BaseFileUploadSetUp();
    VersionFileUploadSetUp();

    $("#btnCreate").click(function() {
        var url = path + "CorpusStore/CreateCorpusWizard";
        var c = $("#CorpusName").val();
        var d = $("#Description").val();
        var e = document.getElementById("LanguageCode");
        var l = e.options[e.selectedIndex].value;

        var $valid = $('#CreateCorpusForm').valid();
        if (!$valid) {
            return;
        }

        $.post(url,
            { CorpusName: c, Description: d, SelectedLanguageCode: l },
            function(data) {
                var space = "<hr />";
                if ($("#appAlerts").html().indexOf("alert") === -1) {
                    space = "";
                }

                if ($("#appAlerts").html().indexOf("msgCreateCorpus") === -1) {
                    $("#appAlerts").html($("#appAlerts").html() +
                        space +
                        "<div id=\"msgCreateCorpus\">" +
                        data.ErrorMsg +
                        "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
                        "</div>"
                    );
                } else {
                    $("#msgCreateCorpus").html(
                        data.ErrorMsg +
                        "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>"
                    );
                }

                if (data.Succeeded) {
                    $("#CorpusName").prop("disabled", true);
                    $("#Description").prop("disabled", true);
                    $("#LanguageCode").prop("disabled", true);
                    $("#btnCreate").prop("disabled", true);
                    $("#btnCreate").addClass("btn-default");
                    $("#btnCreate").removeClass("btn-primary");
                    $("#msgCreateCorpus").addClass("alert alert-success alert-dismissable");

                    var $nextButton = $(".btn-next");
                    $nextButton.prop("disabled", false);
                    $nextButton.removeClass("btn-default");
                    $nextButton.addClass("btn-primary");
                    $('.nav li.active').next('li').find('a').attr("data-toggle", "tab");


                } else {
                    $("#CorpusName").prop("disabled", false);
                    $("#Description").prop("disabled", false);
                    $("#LanguageCode").prop("disabled", false);
                    $("#btnCreate").prop("disabled", false);
                    $("#msgCreateCorpus").addClass("alert alert-danger alert-dismissable");
                }

                window.setTimeout(function() { $(".alert").alert("close"); }, 5000);
            });
    });

    $("#btnSegmentation").click(function() {
        var url = path + "Corpus/CreateAutoSegmentation";
        var c = $("#CorpusName").val();
        var d = $("#Description").val();
        var e = document.getElementById("LanguageCode");
        var l = e.options[e.selectedIndex].value;

        $("#btnSegmentation").prop("loading", true);
        $("#imgSegmentationLoading").show();

        $.post(url,
            { CorpusName: c, Description: d, SelectedLanguageCode: l },
            function (data) {
                var space = "<hr />";
               
                if ($("#appAlerts").html().indexOf("alert") === -1) {
                    space = "";
                }

                if ($("#appAlerts").html().indexOf("msgCreateCorpus") === -1) {
                    $("#appAlerts").html($("#appAlerts").html() +
                        space +
                        "<div id=\"msgCreateCorpus\">" +
                        data +
                        "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
                        "</div>"
                    );
                } else {
                    $("#msgCreateCorpus").html(
                        data +
                        "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>"
                    );
                }

                if (data.indexOf("success") !== -1) {
                    $("#btnSegmentation").prop("disabled", true);
                    $("#segmentation").prop("disabled", true);
                    $("#btnSegmentation").removeClass("btn-primary");
                    $("#btnSegmentation").addClass("btn-default");
                    $("#alignment").prop("enabled", true);
                    $("#btnAlignment").prop("enabled", true);
                    $("#btnAlignment").removeClass("btn-default");
                    $("#btnAlignment").addClass("btn-primary");
                    $("#msgCreateCorpus").addClass("alert alert-success alert-dismissable");
                    $("#imgSegmentationLoading").hide();
                } else {
                    $("#btnSegmentation").prop("disabled", false);
                    $("#segmentation").prop("disabled", false);
                    $("#btnAlignment").prop("enabled", false);
                    $("#alignment").prop("enabled", false);
                    $("#msgCreateCorpus").addClass("alert alert-warning alert-dismissable");
                    $("#imgSegmentationLoading").hide();
                }

                
            });
    });

    $("#btnAlignment").click(function () {
        var url = path + "Corpus/CreateAutoAlignments";
        var c = $("#CorpusName").val();
        var d = $("#Description").val();
        var e = document.getElementById("LanguageCode");
        var l = e.options[e.selectedIndex].value;

        $("#btnAlignment").prop("loading", true);
        $("#imgAlignmentLoading").show();

        $.post(url,
            { CorpusName: c, Description: d, SelectedLanguageCode: l },
            function (data) {
                var space = "<hr />";

                if ($("#appAlerts").html().indexOf("alert") === -1) {
                    space = "";
                }

                if ($("#appAlerts").html().indexOf("msgCreateCorpus") === -1) {
                    $("#appAlerts").html($("#appAlerts").html() +
                        space +
                        "<div id=\"msgCreateCorpus\">" +
                        data +
                        "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>" +
                        "</div>"
                    );
                } else {
                    $("#msgCreateCorpus").html(
                        data +
                        "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>"
                    );
                }

                if (data.indexOf("success") !== -1) {
                    $("#btnAlignment").prop("disabled", true);
                    $("#alignment").prop("disabled", true);
                    $("#btnAlignment").removeClass("btn-primary");
                    $("#btnAlignment").addClass("btn-default");
                    $(".btn-finish").removeClass("btn-default");
                    $(".btn-finish").addClass("btn-primary");
                    $("#msgCreateCorpus").addClass("alert alert-success alert-dismissable");
                    $(".btn-finish").click(function () { window.location = path + 'Visualise/Alignments?CorpusName=' + $("#CorpusName").val(); });
                    $("#imgAlignmentLoading").hide();
                } else {
                    $("#btnAlignment").prop("disabled", false);
                    $("#alignment").prop("disabled", false);
                    $("#imgAlignmentLoading").hide();
                }

                window.setTimeout(function () { $(".alert").alert('close'); }, 5000);
            });
    });

});

function BaseFileUploadSetUp() {
    var baseFileCounter = 0;
    var baseTotalFiles = 0;
    var formDataValues = new FormData();
    formDataValues.append("uploadtype", "base");
    formDataValues.append("corpusName", $("#CorpusName").val());

    var urlBase = window.location.hostname === "/Document/UploadFiles",
        uploadButton = $("<button/>")
            .addClass("btn btn-primary start ")
            .prop("disabled", true)
            .text("Processing...");

    $("#baseFileUpload").fileupload({
        formData: formDataValues,
        dropZone: $("#dropzonebase"),
        url: urlBase,
        dataType: "json",
        autoUpload: true,
        maxNumberOfFiles: 1,
        maxFileSize: 5 * 1024 * 1024,
        messages: {
            maxFileSize: "File exceeds maximum allowed size of 1MB.",
            maxNumberOfFiles: "Number of files exceeds maximum allowed."
            }
        }).on("fileuploadadd",
            function (e, data) {
                var numItems = $(".basefile").length;
                if (numItems > 0) {
                    data.send.abort();
                }
                data.context = $("<div/>").appendTo("#baseFiles").addClass("basefile");
                $("#baseFileUpload").prop("disabled", true);
                $.each(data.files,
                    function (index, file) {
                        baseTotalFiles = baseTotalFiles + 1;
                        var node = $("<p/>")
                            .append($("<span/>").text(file.name));
                        if (!index) {
                            node
                                .append("<br>")
                                .append(uploadButton.clone(true).data(data));
                        }
                        node.appendTo(data.context);
                    });
            }).on("fileuploadprocessalways",
            function (e, data) {
                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.error) {
                    node
                        .append('<br>')
                        .append($('<span class="text-danger"/>').text(file.error));
                }
            }).on("fileuploadprogressall",
            function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $("#baseProgress .progress-bar").css(
                    "width",
                    progress + "%"
                );
            }).on("fileuploaddone",
            function (e, data) {
                $.each(data.result.files,
                    function (index, file) {
                        if (file.size > 0) {
                            $(data.context.children()[index]).find("button")
                                .text("Upload completed")
                                .prop("disabled", true)
                                .removeClass("btn-primary")
                                .addClass("btn-success");
                            baseFileCounter = baseFileCounter + 1;
                            if (baseFileCounter === baseTotalFiles) {
                                var $nextButtonBase = $(".btn-next");
                                $nextButtonBase.addClass("btn-primary");
                                $nextButtonBase.removeClass("btn-default");
                                $nextButtonBase.removeClass("disabled");
                                $nextButtonBase.prop("disabled", false);
                                $(".nav li.active").next("li").find("a").attr("data-toggle", "tab");
                            }
                        }

                        if (file.error) {
                            var error = $('<span class="text-danger"/>').text(file.error);
                            $(data.context.children()[index])
                                .append("<br>")
                                .append(error);
                        }
                    }
                );
                //$("#baseFilesUploaded").append('<br>');
                //$("#baseFilesUploaded").append($('<span class="text-danger"/>').text(baseFileCounter + " / " + baseTotalFiles));
            }).on("fileuploadfail",
            function (e, data) {
                $.each(data.files,
                    function (index) {
                        var error = $('<span class="text-danger"/>').text("File upload failed.");
                        $(data.context.children()[index])
                            .append('<br>')
                            .append(error);
                    });
            }).prop("disabled", !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : "disabled");

    $("#baseFileUpload").bind("fileuploadsubmit", function (e, data) {
        var formvalues = new FormData();
        formvalues.append("uploadtype", "base");
        formvalues.append("corpusName", $("#CorpusName").val());
        data.formData = formvalues;
    });

}

function VersionFileUploadSetUp() {
    var versionFileCounter = 0;
    var versionTotalFiles = 0;
    var formDataValues = new FormData();
    formDataValues.append('uploadtype', 'version');
    formDataValues.append('corpusName', $("#CorpusName").val());

    var urlVersion = window.location.hostname === "/Document/UploadFiles",
        uploadButton = $('<button/>')
            .addClass('btn btn-primary start ')
            .prop('disabled', true)
            .text('Processing...');

    $("#versionFileUpload").fileupload({
        formData: formDataValues,
        dropZone: $("#dropzoneversion"),
        url: urlVersion,
        dataType: 'json',
        autoUpload: true,
        maxNumberOfFiles: 20,
        maxFileSize: 5 * 1024 * 1024,
        messages: {
            maxFileSize: "File exceeds maximum allowed size of 1MB.",
            maxNumberOfFiles: "Number of files exceeds maximum allowed."
        }
    }).on('fileuploadadd',
            function (e, data) {
                var numItems = $(".versionfile").length;
                if (numItems > 19) {
                    data.send.abort();
                }
                data.context = $("<div/>").appendTo("#versionFiles").addClass("versionfile");
                versionTotalFiles = versionTotalFiles + 1;
                
                $("#versionFileUpload").prop("disabled", true);

                $.each(data.files,
                    function (index, file) {
                        var node = $('<p/>')
                            .append($('<span/>').text(file.name));
                        if (!index) {
                            node
                                .append('<br>')
                                .append(uploadButton.clone(true).data(data));
                        }
                        node.appendTo(data.context);
                    });
            }).on("fileuploadprocessalways",
            function (e, data) {
                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.error) {
                    node
                        .append("<br>")
                        .append($('<span class="text-danger"/>').text(file.error));
                    versionFileCounter = versionFileCounter + 1;
                }
            }).on("fileuploadprogressall",
            function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $("#versionProgress .progress-bar").css(
                    "width",
                    progress + "%"
                );
            }).on("fileuploaddone",
            function (e, data) {
                $.each(data.result.files,
                    function (index, file) {
                        versionFileCounter = versionFileCounter + 1;
                        if (file.size > 0) {
                            $(data.context.children()[index]).find('button')
                                .text('Upload completed')
                                .prop('disabled', true)
                                .removeClass("btn-primary")
                                .addClass("btn-success");
                        }
                        if (file.error) {
                            var error = $('<span class="text-danger"/>').text(file.error);
                            $(data.context.children()[index])
                                .append("<br>")
                                .append(error);
                            $(data.context.children()[index]).find("button")
                                .text("Upload failed")
                                .removeClass("btn-primary")
                                .addClass("btn-warning");
                            $("#segmentation").prop("disabled", true);
                            $("#btnSegmentation").prop("disabled", true);
                            $("#btnAlignment").prop("disabled", true);
                            $("#alignment").prop("disabled", true);
                        }

                        if (versionFileCounter >= versionTotalFiles) {
                            $("#segmentation").prop("disabled", false);
                            $("#btnSegmentation").prop("disabled", false);
                            $("#btnAlignment").prop("disabled", false);
                            $("#alignment").prop("disabled", false);
                            var $nextButtonVersion = $(".btn-next");
                            $nextButtonVersion.addClass("btn-primary");
                            $nextButtonVersion.removeClass("btn-default");
                            $nextButtonVersion.removeClass("disabled");
                            $nextButtonVersion.prop("disabled", false);
                            $(".nav li.active").next("li").find("a").attr("data-toggle", "tab");
                        }
                    });
            }).on("fileuploadfail",
            function (e, data) {
                $.each(data.files,
                    function (index) {
                        versionFileCounter = versionFileCounter + 1;
                        var error = $('<span class="text-danger"/>').text("File upload failed.");
                        $(data.context.children()[index])
                            .append("<br>")
                            .append(error);
                    });
            }).prop("disabled", !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : "disabled");

    $("#versionFileUpload").bind("fileuploadsubmit", function (e, data) {
        var formvalues = new FormData();
        formvalues.append("uploadtype", "version");
        formvalues.append("corpusName", $("#CorpusName").val());
        data.formData = formvalues;
    });
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $("#wizardPicturePreview").attr("src", e.target.result).fadeIn("slow");
        }
        reader.readAsDataURL(input.files[0]);
    }
}