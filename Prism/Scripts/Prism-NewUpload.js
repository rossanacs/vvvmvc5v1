﻿/*
Version Variation Visualisation (VVV)
http://www.delightedbeauty.org
    
Copyright (c) Kevin Flanagan, 2012.
http://www.kftrans.co.uk
  
This file is part of VVV.

VVV is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VVV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VVV.  If not, see <http://www.gnu.org/licenses/>.
*/

//var path = window.location.protocol + "//" + window.location.host + "//vvvdraft";

$(function () {
    $(document).bind("dragover",
        function (e) {
            var dropZones = $(".dropzone"),
                timeout = window.dropZoneTimeout;
            if (timeout) {
                clearTimeout(timeout);
            } else {
                dropZones.addClass("in");
            }
            var hoveredDropZone = $(e.target).closest(dropZones);
            dropZones.not(hoveredDropZone).removeClass("hover");
            hoveredDropZone.addClass("hover");
            window.dropZoneTimeout = setTimeout(function () {
                window.dropZoneTimeout = null;
                dropZones.removeClass("in hover");
            },
                100);
        });

    UploadBaseFile();
    UploadVersionFile();
});


function UploadBaseFile() {
    var baseFileCounter = 0;
    var baseTotalFiles = 0;
    var formDataValues = new FormData();
    formDataValues.append("uploadtype", "base");
    formDataValues.append("corpusName", $("#CorpusName").val());

    var urlBase = window.location.hostname === "/Document/UploadFiles",
        uploadButton = $("<button/>")
            .addClass("btn btn-primary start ")
            .prop("disabled", true)
            .text("Processing...");

    $("#baseFileUpload").fileupload({
        formData: formDataValues,
        dropZone: $("#dropzonebase"),
        url: urlBase,
        dataType: "json",
        autoUpload: true,
        maxNumberOfFiles: 1,
        maxFileSize: 5 * 1024 * 1024,
        messages: {
            maxFileSize: "File exceeds maximum allowed size of 1MB.",
            maxNumberOfFiles: "Number of files exceeds maximum allowed."
        }
    }).on("fileuploadadd",
        function (e, data) {
            var numItems = $(".basefile").length;
            if (numItems > 0) {
                data.send.abort();
            }
            data.context = $("<div/>").appendTo("#baseFiles").addClass("basefile");
            $("#baseFileUpload").prop("disabled", true);
            $.each(data.files,
                function (index, file) {
                    baseTotalFiles = baseTotalFiles + 1;
                    var node = $("<p/>")
                        .append($("<span/>").text(file.name));
                    if (!index) {
                        node
                            .append("<br>")
                            .append(uploadButton.clone(true).data(data));
                    }
                    node.appendTo(data.context);
                });
        }).on("fileuploadprocessalways",
        function (e, data) {
            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);
            if (file.error) {
                node
                    .append("<br>")
                    .append($('<span class="text-danger"/>').text(file.error));
            }
        }).on("fileuploadprogressall",
        function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $("#baseProgress .progress-bar").css(
                "width",
                progress + "%"
            );
        }).on("fileuploaddone",
        function (e, data) {
            $.each(data.result.files,
                function (index, file) {
                    if (file.size > 0) {
                        $(data.context.children()[index]).find("button")
                            .text("Upload completed")
                            .prop("disabled", true)
                            .removeClass("btn-primary")
                            .addClass("btn-success");
                        baseFileCounter = baseFileCounter + 1;
                        if (baseFileCounter === baseTotalFiles) {
                            window.location = _siteUrl + "Corpus?CorpusName=" + file.corpusname;
                        }
                    }

                    if (file.error) {
                        var error = $('<span class="text-danger"/>').text(file.error);
                        $(data.context.children()[index])
                            .append("<br>")
                            .append(error);
                    }
                }
            );
        }).on("fileuploadfail",
        function (e, data) {
            $.each(data.files,
                function (index) {
                    var error = $('<span class="text-danger"/>').text("File upload failed.");
                    $(data.context.children()[index])
                        .append("<br>")
                        .append(error);

                });
        }).prop("disabled", !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : "disabled");

    $("#baseFileUpload").bind("fileuploadsubmit", function (e, data) {
        var formvalues = new FormData();
        formvalues.append("uploadtype", "base");
        formvalues.append("corpusName", $("#CorpusName").val());
        data.formData = formvalues;
    });

}

function UploadVersionFile() {
    var versionFileCounter = 0;
    var versionTotalFiles = 0;
    var formDataValues = new FormData();
    formDataValues.append('uploadtype', 'version');
    formDataValues.append('corpusName', $("#CorpusName").val());

    //var urlVersion = window.location.hostname === "/Document/UploadFiles"

    var urlVersion = window.location.hostname === "/Document/UploadFiles",
        uploadButton = $('<button/>')
            .addClass('btn btn-primary start ')
            .prop('disabled', true)
            .text('Processing...');

    $("#versionFileUpload").fileupload({
        formData: formDataValues,
        dropZone: $("#dropzoneversion"),
        url: urlVersion,
        dataType: 'json',
        autoUpload: true,
        maxNumberOfFiles: 20,
        maxFileSize: 5 * 1024 * 1024,
        messages: {
            maxFileSize: "File exceeds maximum allowed size of 1MB.",
            maxNumberOfFiles: "Number of files exceeds maximum allowed."
        }
    }).on('fileuploadadd',
        function (e, data) {
            var numItems = $(".versionfile").length;
            if (numItems > 19) {
                data.send.abort();
            }
            data.context = $("<div/>").appendTo("#versionFiles").addClass("versionfile");
            versionTotalFiles = versionTotalFiles + 1;

            $("#versionFileUpload").prop("disabled", true);

            $.each(data.files,
                function (index, file) {
                    var node = $('<p/>')
                        .append($('<span/>').text(file.name));
                    if (!index) {
                        node
                            .append('<br>')
                            .append(uploadButton.clone(true).data(data));
                    }
                    node.appendTo(data.context);
                });
        }).on("fileuploadprocessalways",
        function (e, data) {
            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);
            if (file.error) {
                node
                    .append("<br>")
                    .append($('<span class="text-danger"/>').text(file.error));
                versionFileCounter = versionFileCounter + 1;
            }
        }).on("fileuploadprogressall",
        function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $("#versionProgress .progress-bar").css(
                "width",
                progress + "%"
            );
        }).on("fileuploaddone",
        function (e, data) {
            $.each(data.result.files,
                function (index, file) {
                    versionFileCounter = versionFileCounter + 1;
                    if (file.size > 0) {
                        $(data.context.children()[index]).find('button')
                            .text('Upload completed')
                            .prop('disabled', true)
                            .removeClass("btn-primary")
                            .addClass("btn-success");
                    }
                    if (file.error) {
                        var error = $('<span class="text-danger"/>').text(file.error);
                        $(data.context.children()[index])
                            .append("<br>")
                            .append(error);
                        $(data.context.children()[index]).find("button")
                            .text("Upload failed")
                            .removeClass("btn-primary")
                            .addClass("btn-warning");
                    }

                    if (versionFileCounter >= versionTotalFiles) {
                        window.location = _siteUrl + "Corpus?CorpusName=" + file.corpusname;
                    }
                });
        }).on("fileuploadfail",
        function (e, data) {
            $.each(data.files,
                function (index) {
                    versionFileCounter = versionFileCounter + 1;
                    var error = $('<span class="text-danger"/>').text("File upload failed.");
                    $(data.context.children()[index])
                        .append("<br>")
                        .append(error);
                });
        }).prop("disabled", !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : "disabled");

    $("#versionFileUpload").bind("fileuploadsubmit", function (e, data) {
        var formvalues = new FormData();
        formvalues.append("uploadtype", "version");
        formvalues.append("corpusName", $("#CorpusName").val());
        data.formData = formvalues;
    });
}