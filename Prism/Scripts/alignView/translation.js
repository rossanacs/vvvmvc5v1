function Translation( id ) {
    this.alias = 'txt_' + id;
    this.a = '.txt_'+ id+ ' .text';
    this.currentScrollPos = 0;
    this.activeTextItem = null;
    this.activeFilterItem = null;
    this.activeNameItem = null;
    this.id = id;
    this.allEblaIDs = new Array;
    this.totalTextLength;

    this.setAlias = function(alias){ this.alias = alias; }
    this.setA = function(a){ return this.a = a; }
    this.setActiveTextItem = function(lai){ this.activeTextItem = lai; }
    this.setActiveFilterItem = function(a){ this.activeFilterItem = a; }
    this.setActiveNameItem = function(a){ this.activeNameItem = a; }
    this.pushAllEblaIDs = function(a){ this.allEblaIDs.push(a); }
    this.setTotalTextLength = function(a){ this.totalTextLength = a; }

    this.getAlias = function(){ return this.alias; }
    this.getA = function(){ return this.a; }
    this.getActiveTextItem = function(){ return this.activeTextItem; }
    this.getActiveFilterItem = function(){ return this.activeFilterItem; }
	this.getActiveNameItem = function(){ return this.activeNameItem; }
	this.getAllEblaIDs = function(a){ return this.allEblaIDs; }
	this.popAllEblaIDs = function(a){ this.allEblaIDs.pop(a); }
	this.getTotalTextLength = function(){ return this.totalTextLength; }

}