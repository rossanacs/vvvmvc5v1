﻿/*
Version Variation Visualisation (VVV)
http://www.delightedbeauty.org
    
Copyright (c) Kevin Flanagan, 2012.
http://www.kftrans.co.uk
  
This file is part of VVV.

VVV is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

VVV is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with VVV.  If not, see <http://www.gnu.org/licenses/>.
*/

var PrismAL = PrismAL || {};

PrismAL.Aligner = {};

var _canvas = null;
var jg = null;

var _lastSelInfo = { basetextsegid: 0, versionsegid: 0, line: null };

var _basetextQuery = null;
var _versionQuery = null;

var _basetextEditorName = "basetexteditor";
var _versionEditorName = "versioneditor";


var _basetextFormId = "basetextextractform";
var _versionFormId = "versionextractform";

var _basetextDivId = "basetextenclosurediv";
var _versionDivId = "versionenclosurediv";

var _alignments = null;
var _alignmentHash = null;
var _draftAlignCount = 0;

var _linkLines = null;
var _linkPoints = null;

var _draftPen = null;
var _confirmedPen = null;

var _draftPenSelected = null;
var _confirmedPenSelected = null;

var _basetextReady = false;
var _versionReady = false;

var _basetextReadyCustom = null;
var _versionReadyCustom = null;

//var _resizeInfo = new Object();
//_resizeInfo[_basetextEditorName] = { lastEditorResizeTime: 0, editorResizeQueued: false };
//_resizeInfo[_versionEditorName] = { lastEditorResizeTime: 0, editorResizeQueued: false };
//_resizeInfo.programmaticResizeEdName = '';

var _lastScrollTime = 0;
var _scrollQueued = false;

var _redrawTimeout = 200;
var _redrawCheckInterval = 50;

$(function () {

    $("#basetextviewbutton").click(function (e) { ViewBaseTextExtract(); });
    $("#versionviewbutton").click(function (e) { ViewVersionExtract(); });

    $("#addbasetextfilterattrib").click(function (e) { PrismNS.Utils.AddAttribRow("nextbasetextattribfilterrowindex", "BaseTextModel.AttributeFilters", "basetextattribfiltertable", "basetextattribfilterrow-"); });
    $("#addversionfilterattrib").click(function (e) { PrismNS.Utils.AddAttribRow("nextversionattribfilterrowindex", "VersionModel.AttributeFilters", "versionattribfiltertable", "versionattribfilterrow-"); });

    $("#test").click(function (e) { test(); });
    $("#align").click(function (e) { Align(); });
    $("#removealign").click(function (e) { RemoveAlign(); });
    $("#removedraft").click(function (e) { RemoveDraft(); });
    $("#removeallalign").click(function (e) { RemoveAllAlign(); });
    $("#autoalign").click(function (e) { AutoAlign(); });
    $("#confirmall").click(function (e) { ConfirmAll(); });
    $("#moorealigner").click(function (e) { MooreAligner(); });


    $(window).resize(function () {
        setTimeout(function () {
            SizeEditorsToWindow();
        }, 100);
    });


//    setTimeout(function () {
//        SizeEditorsToWindow();
//    }, 500);

    _canvas = $('#graphics');

    jg = new jsGraphics(_canvas[0]);

    var col = new jsColor("red");

    _draftPenSelected = new jsPen(col, 1);
    _confirmedPenSelected = new jsPen(col, 2);

    col = new jsColor("blue");
    _draftPen = new jsPen(col, 1);
    _confirmedPen = new jsPen(col, 2);

    //EnableControls();

    SetupEditor(false, _basetextEditorName, _basetextDivId, _basetextFormId, "GetBaseTextExtract", "SaveBaseTextExtract", "AlignerBaseTextSegmentDefinitionDialog",
            function () { BaseTextSelChanged(); },
            function () { BaseTextDataReady(); },
            null /*function () { EditorResize(_basetextEditorName); }*/,
            function () { EditorScroll(_basetextEditorName) }
            );

    SetupEditor(false, _versionEditorName, _versionDivId, _versionFormId, "GetVersionExtract", "SaveVersionExtract", "AlignerVersionSegmentDefinitionDialog",
            function () { VersionSelChanged(); },
            function () { VersionDataReady(); },
            null /*function () { EditorResize(_versionEditorName); }*/,
            function () { EditorScroll(_versionEditorName) }
            );


});

function SizeEditorsToWindowIfReady() {
    if (_basetextReady && _versionReady) {
        SizeEditorsToWindow();
        ScrollToAnySegmentToView();
    }
}

function ScrollToAnySegmentToView() {

    var basetextsegid = $("#basetextSegIDToView").val();
    basetextsegid = parseInt(basetextsegid);

    if (!!basetextsegid) {
        _basetextReadyCustom = function () {
            var basetextDoc = GetEditorDocument(_basetextDivId);
            var temp = basetextDoc.getElementById(StartMarkerElementId(basetextsegid));
            temp.scrollIntoView();
            SetSelectedSegmentID(_basetextDivId, GetEditorInstanceByName(_basetextEditorName), basetextsegid);
        };

        ViewBaseTextExtract();

    }

    var versionsegid = $("#versionSegIDToView").val();
    versionsegid = parseInt(versionsegid);

    if (!!versionsegid) {
        _versionReadyCustom = function () {
            var versionDoc = GetEditorDocument(_versionDivId);
            var temp = versionDoc.getElementById(StartMarkerElementId(versionsegid));
            temp.scrollIntoView();
            SetSelectedSegmentID(_versionDivId, GetEditorInstanceByName(_versionEditorName), versionsegid);

        };
        ViewVersionExtract();
    }

   

}

function test() {

    var basetextsegid = 38100;
    var versionsegid = 37426;

    var basetextDocElm = GetEditorDocElm(_basetextDivId);
    var versionDocElm = GetEditorDocElm(_versionDivId);
    var basetextDoc = GetEditorDocument(_basetextDivId);
    var versionDoc = GetEditorDocument(_versionDivId);

    var temp = basetextDoc.getElementById(StartMarkerElementId(basetextsegid));
    temp.scrollIntoView();
    temp = versionDoc.getElementById(StartMarkerElementId(versionsegid));
    temp.scrollIntoView();

    SetSelectedSegmentID(_basetextDivId, GetEditorInstanceByName(_basetextEditorName), basetextsegid);
    SetSelectedSegmentID(_versionDivId, GetEditorInstanceByName(_versionEditorName), versionsegid);

    return;

    //var basetextsegid = GetSelectedSegmentID(GetEditorInstanceByName(versionsegid));


    //var data = { CorpusName: _corpusName, baseTextSegmentID: basetextsegid };

    ////    var url = _siteUrl + "Document/" + instance._segmentDefinitionAction;
    //var url = _siteUrl + "Aligner/SegStats";

    //showModal(url, "Test", PrismNS.modalEnum.okCancel, data);


//    GetEditorInstanceByName(_basetextEditorName).on('selectionChange', function (ev) {
//        BaseTextSelChanged();
//    });
//    GetEditorInstanceByName(_versionEditorName).on('selectionChange', function (ev) {
//        VersionSelChanged();
//    });
}

function OtherEditorName(editorname) {
    return editorname == _basetextEditorName ? _versionEditorName : _basetextEditorName;;
}

function EnclosureDivId(editorname) {
    return editorname == _basetextEditorName ? _basetextDivId : _versionDivId;

}

//function AfterEditorResize(editorname) {
//    
//    var now = new Date();
//    if (now.getTime() - _resizeInfo[editorname].lastEditorResizeTime < _redrawTimeout) {
//        // don't do it yet
//        setTimeout(function () {
//            AfterEditorResize(editorname);
//        }, _redrawCheckInterval);
//        return;
//    }

//    // Resize other editor and fixup graphics
//    var ed = $(GetEditorIFrame(editorname == _basetextEditorName ? _basetextDivId : _versionDivId));
//    var otherEdName = OtherEditorName( editorname);
//    _resizeInfo.programmaticResizeEdName = otherEdName;
//    GetEditorInstanceByName(otherEdName).resize(ed.width(), ed.height(), true);
//    _resizeInfo.programmaticResizeEdName = '';
//    FixupGraphicsDiv();
//    DrawAlignments();
//    EnableControls();
//    _resizeInfo[editorname].editorResizeQueued = false;
//}

function AfterScroll(editorname) {
    var now = new Date();
    if (now.getTime() - _lastScrollTime < _redrawTimeout) {
        // don't do it yet
        setTimeout(function () {
            AfterScroll(editorname);
        }, _redrawCheckInterval);
        return;
    }
    FixupGraphicsDiv();
    if ($('#syncscroll').is(':checked')) {
        var scrollinfo = GetEditorScrollInfo(EnclosureDivId(editorname));
        SetEditorScroll(scrollinfo, EnclosureDivId(OtherEditorName(editorname)));

    }
    DrawAlignments();
    EnableControls();
    _scrollQueued = false;
}

//function EditorResize(editorname) {
//    if (editorname == _resizeInfo.programmaticResizeEdName)
//        return;

//    var now = new Date();
//    _resizeInfo[editorname].lastEditorResizeTime = now.getTime();
//    
//    if (_resizeInfo[editorname].editorResizeQueued == false) {
//        jg.clear();
//        setTimeout(function () {
//            AfterEditorResize(editorname);
//        }, _redrawCheckInterval);
//        _resizeInfo[editorname].editorResizeQueued = true;
//    }

//}

function EditorScroll(editorname) {
    //alert('scroll');

    var now = new Date();
    _lastScrollTime = now.getTime();
    if (_scrollQueued == false) {
        jg.clear();
        setTimeout(function () {
            AfterScroll(editorname);
        }, _redrawCheckInterval);
        _scrollQueued = true;

    }
}

function SizeEditorsToWindow() {
    var editorOffset = $(GetEditorIFrame(_basetextDivId)).offset();
    if (!editorOffset)
        return;
    var availHeight = $('body').height() - editorOffset.top;
    var ed = $(GetEditorIFrame(_basetextDivId));
    GetEditorInstanceByName(_basetextEditorName).resize('100%', availHeight - 10);
    ed = $(GetEditorIFrame(_versionDivId));
    GetEditorInstanceByName(_versionEditorName).resize('100%', availHeight - 10);

    FixupGraphicsDiv();
    DrawAlignments();
    EnableControls();

}

function FixupGraphicsDiv() {

    var editorOffset = $(GetEditorIFrame(_basetextDivId)).offset();


    var currentOffset = _canvas.offset();
    currentOffset.top = editorOffset.top;
    _canvas.offset(currentOffset);
    _canvas.height($(GetEditorIFrame(_basetextDivId)).height());
}

function BaseTextDataReady() {
    EnableControls();
    FixupGraphicsDiv();
    ShowAlignments();
    if (_basetextReadyCustom) {
        _basetextReadyCustom();
        _basetextReadyCustom = null;
    }

    // Do 1-time stuff only if not alreayd done
    if (_basetextReady)
        return;
    _basetextReady = true;
    SizeEditorsToWindowIfReady();
}

function VersionDataReady() {
    EnableControls();
    FixupGraphicsDiv();
    ShowAlignments();
    if (_versionReadyCustom) {
        _versionReadyCustom();
        _versionReadyCustom = null;
    }

    // Do 1-time stuff only if not alreayd done
    if (_versionReady)
        return;

    _versionReady = true;
    SizeEditorsToWindowIfReady();
}

function ShowAlignments() {
    ClearAlignmentSelection();
    // Is there content in both editors?
    if (_basetextQuery == null || _versionQuery == null)
        return;

    FixupGraphicsDiv();

    var url = _siteUrl + "Aligner/GetAlignments?" + _basetextQuery + '&' + _versionQuery;

    CallControllerAsync(url, null, function (result) {
        if (result != null) {
            if (result.Succeeded) {
                ExtractAlignments(result.Alignments);
                DrawAlignments();
                EnableControls();
                return;

            }
            alert(PrismNS.Strings.Failed_retrieving_alignments + ": " + result.ErrorMsg);
        }
    });

}

function ExtractAlignments(alignments) {
    _alignments = new Array();
    _alignmentHash = new Object();
    var count = 0;
    _draftAlignCount = 0;
    var basetextDocElm = GetEditorDocElm(_basetextDivId);
    var versionDocElm = GetEditorDocElm(_versionDivId);
    var basetextDoc = GetEditorDocument(_basetextDivId);
    var versionDoc = GetEditorDocument(_versionDivId);
    //    var basetextDocElm = GetEditorDocElm(_basetextDivId);
//    var versionDocElm = GetEditorDocElm(_versionDivId);

    for (var i = 0; i < alignments.length; i++) {
        for (var j = 0; j < alignments[i].SegmentIDsInBaseText.length; j++)
            for (var k = 0; k < alignments[i].SegmentIDsInVersion.length; k++) {
                _alignments[count] = { basetextsegid: alignments[i].SegmentIDsInBaseText[j], versionsegid: alignments[i].SegmentIDsInVersion[k], status: alignments[i].AlignmentStatus };

                //var selector = '.' + PrismNS.Formatting._endSegClass + '[data-eblasegid="' + _alignments[count].basetextsegid + '"][data-eblatype="endmarker"]';
                //var selector = '#ebla-segstart-' + _alignments[count].basetextsegid;
                //var leftspan = $(selector, basetextDocElm);
                var temp = basetextDoc.getElementById(EndMarkerElementId(_alignments[count].basetextsegid));
                //var leftspan = $(temp);
                _alignments[count].leftspanelm = temp; // leftspan;

                //selector = '.' + PrismNS.Formatting._startSegClass + '[data-eblasegid="' + _alignments[count].versionsegid + '"][data-eblatype="startmarker"]';
                //selector = '#ebla-segend-' + _alignments[count].versionsegid;
                //var rightspan = $(selector, versionDocElm);
                temp = versionDoc.getElementById(StartMarkerElementId(_alignments[count].versionsegid));
                //var rightspan = $(temp);
                _alignments[count].rightspanelm = temp; //  rightspan;

                var key = alignments[i].SegmentIDsInBaseText[j] + ' ' + alignments[i].SegmentIDsInVersion[k];
                _alignmentHash[key] = _alignments[count];
                if (_alignments[count].status == 0)
                    _draftAlignCount++;

                //AddMarkerIds(alignments[i].SegmentIDsInBaseText[j], alignments[i].SegmentIDsInVersion[k], basetextDocElm, versionDocElm);
                count++;
            }

    }

}

function GetDrawInfo() {
    var info = new Object();

    info.leftdocoffset = $(GetEditorIFrame(_basetextDivId)).offset();
    info.rightdocoffset = $(GetEditorIFrame(_versionDivId)).offset();

    info.canvasoffset = _canvas.offset();
    info.canvasheight = _canvas.height();

    info.leftscroll = GetEditorScrollInfo(_basetextDivId).scroll; //$(GetEditorDocElm(_basetextDivId)).scrollTop();
    info.rightscroll = GetEditorScrollInfo(_versionDivId).scroll; // $(GetEditorDocElm(_versionDivId)).scrollTop();


    return info;
}

function DrawAlignments() {

    _linkLines = new Object();
    _linkPoints = new Object();

    jg.clear();

    if (_alignments == null)
        return;

    var info = GetDrawInfo();

    var basetextDocElm = GetEditorDocElm(_basetextDivId);
    var versionDocElm = GetEditorDocElm(_versionDivId);

    for (var i = 0; i < _alignments.length; i++) {
        DrawLink(_alignments[i].basetextsegid, _alignments[i].versionsegid, _alignments[i].status, info, basetextDocElm, versionDocElm, _alignments[i].leftspanelm, _alignments[i].rightspanelm);
    }
    var __debug = 0;
}

//// try to optimise link drawing by giving id attribs to the spans to be linked
//function AddMarkerIds(basetextsegid, versionsegid, basetextDocElm, versionDocElm) {

//    var selector = '.' + PrismNS.Formatting._endSegClass + '[data-eblasegid="' + basetextsegid + '"][data-eblatype="endmarker"]';
//    var leftspan = $(selector, basetextDocElm);
//    var leftspanelm = leftspan[0];

//    selector = '.' + PrismNS.Formatting._startSegClass + '[data-eblasegid="' + versionsegid + '"][data-eblatype="startmarker"]';
//    var rightspan = $(selector, versionDocElm);
//    var rightspanelm = rightspan[0];

//    if (leftspan.length == 0 || rightspan.length == 0)
//        return;

//    leftspan.attr('id', 'ebla-base-' + basetextsegid);
//    rightspan.attr('id', 'ebla-version-' + versionsegid);
//    
//}

function DrawLink(basetextsegid, versionsegid, status, info, basetextDocElm, versionDocElm, leftspanelm, rightspanelm) {

//    var selector = '.' + PrismNS.Formatting._endSegClass + '[data-eblasegid="' + basetextsegid + '"][data-eblatype="endmarker"]';
//    //var selector = '#ebla-base-' + basetextsegid;
//    var leftspan = $(selector, basetextDocElm);

//    selector = '.' + PrismNS.Formatting._startSegClass + '[data-eblasegid="' + versionsegid + '"][data-eblatype="startmarker"]';
//    //selector = '#ebla-version-' + versionsegid;
//    var rightspan = $(selector, versionDocElm);

    // The call to the controller to retrieve alignment details uses just the same filters
    // as the call to get content with segment markup. So, if a filter has been applied such that
    // only segments of with an attribute 'type=speech' are being displayed, then only alignments
    // between segments with that attribute will be retrieved, so alignments will always between
    // segments for which we do have markup, and therefore leftspan and rightspan will always be found.
    // However, because we support one-to-many alignments, it's possible for users to align a single segment
    // with (say) one segment that matches the filter criteria, and another that doesn't. 
    // Because our call to the controller retrieves all alignments containing at least one pair of segments 
    // matching the criteria, we can end up with some links that can't be drawn.
    // TODO - perhaps make the controller/back end logic more selective.
    // For the moment, we'll just ignore any links we can't draw, and assume it's for this reason.
    //if (leftspan.length == 0 || rightspan.length == 0)
        //return;

    if (leftspanelm == null || rightspanelm == null)
        return;

    // TODO - some sort of visual indication that a given alignment was only partly drawn?

    //var leftspanelm = leftspan[0];
    //var rightspanelm = rightspan[0];


    //var leftoffset = leftspan.offset();
    //var rightoffset = rightspan.offset();

    var x1 = leftspanelm.offsetLeft;//   leftoffset.left;
    var y1 = leftspanelm.offsetTop; //  leftoffset.top;
    var x2 = rightspanelm.offsetLeft; //  rightoffset.left;
    var y2 = rightspanelm.offsetTop; //  rightoffset.top;

    x1 += leftspanelm.offsetWidth; //  leftspan.width();
    y1 += leftspanelm.offsetHeight / 2; //  leftspan.height() / 2;
    y2 += rightspanelm.offsetHeight / 2; //  rightspan.height() / 2;

    y1 -= info.leftscroll;
    y2 -= info.rightscroll;

    if (y2 < 0 && y1 < 0)
        return;

    if (y1 < 0 && y2 > info.canvasheight)
        return;

    if (y2 < 0 && y1 > info.canvasheight)
        return;

    if (y2 > info.canvasheight && y1 > info.canvasheight)
        return;

    var pt1 = new jsPoint(x1 + info.leftdocoffset.left - info.canvasoffset.left,
                y1  /*info.leftdocoffset.top - info.canvasoffset.top */ );
    var pt2 = new jsPoint(x2 + info.rightdocoffset.left - info.canvasoffset.left,
                y2 /*+ info.rightdocoffset.top - info.canvasoffset.top */ );

    
    var pen = status == 0 ? _draftPen : _confirmedPen;

    var key = basetextsegid + ' ' + versionsegid;

    _linkLines[key] = jg.drawLine(pen, pt1, pt2);
    _linkPoints[key] = { point1: pt1, point2: pt2 };
//    _linkStatuses[key] = status;

}

function ViewBaseTextExtract(oncomplete) {
    _basetextQuery = $("#" + _basetextFormId).formSerialize();

    UpdateExtract(_basetextDivId, _basetextFormId, GetEditorInstanceByName(_basetextEditorName), false, oncomplete);
}

function ViewVersionExtract(oncomplete) {
    _versionQuery = $("#" + _versionFormId).formSerialize();

    UpdateExtract(_versionDivId, _versionFormId, GetEditorInstanceByName(_versionEditorName), false, oncomplete);
}

function BaseTextSelChanged() {
    EnableControls();
}

function VersionSelChanged() {
    EnableControls();
}

function ClearAlignmentSelection() {
    //if (_lastSelInfo.line) {
        // For as-yet-unascertained reasons, sometimes _lastSelInfo.line is non-null
        // despite no longer being a child of canvas
        try {
            var temp = $("#prism-sel-line");
            //_canvas[0].removeChild(_lastSelInfo.line); no longer works
            if (temp.length > 0)
                _canvas[0].removeChild(temp[0]);
        }
        catch (err) {
            var s = err;
        }
        
    //}
    _lastSelInfo = { basetextsegid: 0, versionsegid: 0, line: null };
}

function EnableControls() {


    var corpusbusy = $('#corpusbusy').val();

    var startEnabled = (corpusbusy == 0);
    PrismNS.Utils.EnableButton('startcalc', startEnabled);
    PrismNS.Utils.EnableButton('stopcalc', !startEnabled);

    EnableButton("removeallalign", _alignments != null && _alignments.length > 0);
    EnableButton("confirmall", _draftAlignCount > 0);
    EnableButton("removedraft", _draftAlignCount > 0); 

    var basesegid = GetSelectedSegmentID(GetEditorInstanceByName(_basetextEditorName));
    var versionsegid = GetSelectedSegmentID(GetEditorInstanceByName(_versionEditorName));

    // Any selection line to remove?
    if (_lastSelInfo.line) {
        if (basesegid != _lastSelInfo.basetextsegid || versionsegid != _lastSelInfo.versionsegid) {

            ClearAlignmentSelection();
        }
    }

    // Got two selected segs?
    EnableButton("moorealigner", true);
    if (!basesegid || !versionsegid || (basesegid == -1) || (versionsegid == -1)) {
        // No
        EnableButton("align", false);
        EnableButton("editalign", false);
        EnableButton("confirmalign", false);
        EnableButton("removealign", false);
        EnableButton("autoalign", false);
        return;
    }

    var aligned = false;
    var canEdit = false;
    var canConfirm = false;
    var canRemove = false;
    var canAuto = false;
    var canMoore = false;

    // Already aligned?
    var key = basesegid + " " + versionsegid;
    var alreadyaligned = false;
    if (_alignmentHash != null) // in principle never the case if two segs selected, 
                            // but can happen when scroll fired during init
        alreadyaligned = _alignmentHash[key];
    if (alreadyaligned) {
        aligned = true;
        canEdit = true;
        canConfirm = (_alignmentHash[key].status == 0);
        canRemove = true;
        canAuto = (_alignmentHash[key].status == 1);

        // Draw selected version of link

        var pen = _alignmentHash[key].status == 0 ? _draftPenSelected : _confirmedPenSelected;

        // Has the line been drawn?
        // (maybe not due to timing issue when scrolling)
        var line = null;
        if (_linkPoints[key]) {
            ClearAlignmentSelection();
            line = jg.drawLine(pen, _linkPoints[key].point1, _linkPoints[key].point2);
            $(line).attr('id', 'prism-sel-line');
        }
        else {
            // TODO
        }
        _lastSelInfo = { basetextsegid: basesegid, versionsegid: versionsegid, line: line };


    }
    else {
        ClearAlignmentSelection();
    }


    EnableButton("align", !aligned);
    EnableButton("editalign", canEdit);
    EnableButton("confirmalign", canConfirm);
    EnableButton("removealign", canRemove);
    EnableButton("autoalign", canAuto);
}

function EnableButton(id, enabled) {
    PrismNS.Utils.EnableButton(id, enabled);
//    if (enabled) {
//        $('#' + id).removeAttr('disabled');
//    }
//    else {
//        $('#' + id).attr('disabled', 'disabled');
//    }
}

function Align() {
    // Get segments to align
    var basetextsegid = GetSelectedSegmentID(GetEditorInstanceByName(_basetextEditorName));
    var versionsegid = GetSelectedSegmentID(GetEditorInstanceByName(_versionEditorName));

    if (!basetextsegid || !versionsegid || (basetextsegid == -1) || (versionsegid == -1)) {
        // button should have been disabled!
        return;
    }

    var url = _siteUrl + "Aligner/AlignSegmentPair";

    var args = { CorpusName: _corpusName, VersionName: GetVersionName(_versionFormId), BaseTextSegmentID: basetextsegid, VersionSegmentID: versionsegid };

    CallControllerAsync(url, args, function (result) {
        if (result != null) {
            if (result.Succeeded) {
                //alert('Segment deleted.');
                //UpdateExtract(enclosuredivid, formid, instance);

                // We *could* just add the alignment line, like this ...
                //_alignments[_alignments.length] = { basetextsegid: basetextsegid, versionsegid: versionsegid, status: 1 };
                //DrawLink(basetextsegid, versionsegid, _confirmedPen, GetDrawInfo());
                // ... but instead, let's minimise any chance of going out of sync
                ShowAlignments();
                return;

            }
            alert("Aligning failed: " + result.ErrorMsg);

        }

    });


}

function RemoveDraft() {
    if (!confirm(PrismNS.Strings.Confirm_remove_draft_alignments))
        return;

    var args = { DraftOnly: true };

    var url = _siteUrl + "Aligner/RemoveAlignments?" + _basetextQuery + '&' + _versionQuery;

    CallControllerAsync(url, args, function (result) {
        if (result != null) {
            if (result.Succeeded) {
                //alert('Segment deleted.');
                //UpdateExtract(enclosuredivid, formid, instance);

                ShowAlignments();
                return;

            }
            alert(PrismNS.Strings.Removing_alignments_failed + ": " + result.ErrorMsg);

        }

    });


}

function RemoveAllAlign() {
    if (!confirm(PrismNS.Strings.Confirm_remove_all_alignments))
        return;

    var args = { DraftOnly: false };

    var url = _siteUrl + "Aligner/RemoveAlignments?" + _basetextQuery + '&' + _versionQuery;

    CallControllerAsync(url, args, function (result) {
        if (result != null) {
            if (result.Succeeded) {
                //alert('Segment deleted.');
                //UpdateExtract(enclosuredivid, formid, instance);

                ShowAlignments();
                return;

            }
            alert(PrismNS.Strings.Removing_alignments_failed + ": " + result.ErrorMsg);

        }

    });


}

function RemoveAlign() {
    // Get segments to unalign
    var basetextsegid = GetSelectedSegmentID(GetEditorInstanceByName(_basetextEditorName));
    var versionsegid = GetSelectedSegmentID(GetEditorInstanceByName(_versionEditorName));

    if (!basetextsegid || !versionsegid || (basetextsegid == -1) || (versionsegid == -1)) {
        // button should have been disabled!
        return;
    }

    var url = _siteUrl + "Aligner/UnalignSegmentPair";

    var args = { CorpusName: _corpusName, VersionName: GetVersionName(_versionFormId), BaseTextSegmentID: basetextsegid, VersionSegmentID: versionsegid };

    CallControllerAsync(url, args, function (result) {
        if (result != null) {
            if (result.Succeeded) {
                //alert('Segment deleted.');
                //UpdateExtract(enclosuredivid, formid, instance);
                
                ShowAlignments();
                return;

            }
            alert(PrismNS.Strings.Removing_alignment_failed + ": " + result.ErrorMsg);

        }

    });


}

function AutoAlign() {


    var url = _siteUrl + "Aligner/AutoAlign?" + _basetextQuery + "&" + _versionQuery;

    var args = {ConfirmedAlignmentBaseTextSegId: _lastSelInfo.basetextsegid, ConfirmedAlignmentVersionSegId: _lastSelInfo.versionsegid};

    CallControllerAsync(url, args, function (result) {
        if (result != null) {
            if (result.Succeeded) {
                ShowAlignments();
                return;

            }
            alert(PrismNS.Strings.Failed_auto_aligning +  ": " + result.ErrorMsg);
        }
    });

}

function ConfirmAll() {
    if (!confirm(PrismNs.Strings.Confirm_all_alignments))
        return;

    var url = _siteUrl + "Aligner/ConfirmAlignments?" + _basetextQuery + "&" + _versionQuery;

    CallControllerAsync(url, null, function (result) {
        if (result != null) {
            if (result.Succeeded) {
                ShowAlignments();
                return;

            }
            alert(PrismNS.Strings.Failed_confirming_alignments + ": " + result.ErrorMsg);
        }
    });

}

function MooreAligner() {

    var url = _siteUrl + "Aligner/StartMooreAligner?" + _basetextQuery + "&" + _versionQuery;

    var args = {
        ConfirmedAlignmentBaseTextSegId: _lastSelInfo.basetextsegid,
        ConfirmedAlignmentVersionSegId: _lastSelInfo.versionsegid,
        TaskId: _corpusName
    };

    CallControllerAsync(url,
        args,
        function(result) {
            if (result != null) {
                if (result.Succeeded) {
                    $("#alignerBusy").val("1");
                    UpdateAlignerStatus();
                    SetAlignerTimer();
                    return;
                }
                alert(PrismNS.Strings.Failed_Moore_alignments + ": " + result.ErrorMsg);
            }
        });
}

function updateMonitor(taskId, status) {
    $("#" + taskId).html(PrismNS.Strings.Task +  " [" + taskId + "]: " + status);
}

function SetAlignerTimer() {
    setTimeout(function () {
        UpdateAlignerStatus();
        },
        3000);
}

function UpdateAlignerStatus() {
    var corpusbusy = $("#alignerBusy").val();

    var url = _siteUrl + "Aligner/GetAlignerStatus";

    var args = { TaskId: _corpusName };

    CallControllerAsync(url,
        args,
        function(result) {
            if (result != null) {
                if (result.Succeeded) {
                    if (result.Busy !== corpusbusy) {
                        $("#alignerBusy").val(result.Busy ? "1" : "0");
                        $("#divAlignerStatus").show();
                    }
                    if (result.Busy)
                        SetAlignerTimer();

                    $("#txtAlignerStatus").text(result.Status);
                    if (result.Status === "Completed.") {
                        $("#divAlignerStatus").hide();
                        ShowAlignments();
                    }
                    return;
                        
                }
                alert(PrismNS.Strings.Update_status_failed + ": " + result.ErrorMsg);
            }

        });

}

