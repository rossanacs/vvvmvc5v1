$( document ).ready( function() {
    $('#corpus-index-content').isotope(
    {
        itemSelector: '.version',
        layoutMode: 'straightDown',
        getSortData: {
            author: function($elem) {
                return $elem.find('.version-label').text().split('(')[0];
            },
            year: function($elem) {
                var yearTxt = $elem.find('.year').text();
                return parseInt(yearTxt.substring(1, yearTxt.length - 1));
            }
        }
    });

    $.each($(".po-admin"),
        function(i, e) {
            var opt = {
                trigger: "manual",
                placement: "left",
                content: $(e).next().html(),
                html: true
            };
            $(e).popover(opt);

            $(e).click(function(event) {
                event.preventDefault();
                $.each($(".po-admin"),
                    function(i, e) {
                        if (e !== event.target) {
                            $(e).popover("hide");
                        }
                    });
                $(event.target).popover("toggle");
            });
        });

    $('.optionlink').click(function() {
        var $this = $(this);
        // don't proceed if already selected
        if ($this.hasClass('disabled')) {
            return false;
        }
        var $optionSet = $this.parents('.option-set');
        $optionSet.find('.disabled').removeClass('disabled');
        $this.addClass('disabled');

        // make option object dynamically, i.e. { filter: '.my-filter-class' }
        var options = {},
            key = $optionSet.attr('data-option-key'),
            value = $this.attr('data-option-value');
        // parse 'false' as false boolean
        value = value === 'false' ? false : value;
        options[key] = value;
        if (key === 'layoutMode' && typeof changeLayoutMode === 'function') {
            // changes in layout modes need extra logic
            changeLayoutMode($this, options)
        } else {
            // otherwise, apply new options

            //console.log( goptions );
            $('#corpus-index-content').isotope(options);
        }

        return false;
    });


    $('#corpus-sort-select').change(function (event) {
        var $this = $(event.target).find('option:selected');
        var $optionSet = $this.parents('.option-set');

        var options = {},
                key = $optionSet.attr('data-option-key'),
                value = $this.attr('data-option-value');
        // parse 'false' as false boolean
        value = value === 'false' ? false : value;
        options[key] = value;

        $( '#corpus-index-content' ).isotope( options );

        return false;
    });
});