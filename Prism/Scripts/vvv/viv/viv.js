// available Viv types
var vivTypes = {
    a: {
        title: 'A - Euclidean distance',
        values: [],
        scale: d3.scale.sqrt(),
        min: 0,
        max: 0,
        metricTypeArg: 1
    },
    b: {
        title: 'B - Original Viv and Eddy formulae',
        values: [],
        scale: d3.scale.sqrt(),
        min: 0,
        max: 0,
        metricTypeArg: 2
    },
    c: {
        title: 'C - Viv=S.D. of Eddy',
        values: [],
        scale: d3.scale.sqrt(),
        min: 0,
        max: 0,
        metricTypeArg: 3
    },
    d: {
        title: 'D - Dice\'s coefficient',
        values: [],
        scale: d3.scale.sqrt(),
        min: 0,
        max: 0,
        metricTypeArg: 4
    },
    e: {
        title: 'E - Angular distance',
        values: [],
        scale: d3.scale.sqrt(),
        min: 0,
        max: 0,
        metricTypeArg: 5
    }

};

var goptions = {
    sortAscending: true,
    sortBy: 'eddy'
};

var segmentcbFilter = '[data-ebla-cb != "1"][data-ebla-cb != "2"]';

var scale = new chroma.scale(['#eaeaea', '#87c0ff']);

var segmentVivData = []; // store calculated Eddy results!

var floorslider = null;
var ceilingslider = null;

var sliderminviv = 0.0;
var slidermaxviv = 0.0;

function floorsliderchange(pos, slider) {
    //$('#floorsliderval').text(pos);
    var otherpos = ceilingslider.getValue();
    if (otherpos < pos)
        ceilingslider.setValue(pos);
    showSliderVals();
}

function ceilingsliderchange(pos, slider) {
    //$('#floorsliderval').text(pos);
    var otherpos = floorslider.getValue();
    if (otherpos > pos)
        floorslider.setValue(pos);
    showSliderVals();
}

function showSliderVals() {
    
    
    $('#floorsliderval').text(floorslider.getValue() * 1.0 / 100 * slidermaxviv);
    $('#ceilingsliderval').text(ceilingslider.getValue() * 1.0 / 100 * slidermaxviv);
}

$(document).ready(function () {

    var nav = $('.navbar').outerHeight() + $('.subnav').outerHeight();
    var h = window.innerHeight - nav - 65;

    $('.scroll-wrapper').css('height', h);

    $('#applyvivlimits').click(function (e) {

        calculateVivTypeRanges(vivTypes);

        _.each(vivTypes, function (type, key) {
            if ($vs.val() == type.title) {
                showViv(key);
            }
        });


        //var url = _siteUrl + "Visualise/Viv?CorpusName=" + $('#corpusname').val() + "&vivFloor=" + floorslider.getValue() + "&vivCeiling=" + ceilingslider.getValue();
        //window.location.href = url;
    });

    floorslider = new dhtmlxSlider("floorslider", {
        skin: "dhx_skyblue",
        min: 0,
        max: 100,
        step: 1,
        size: 200,
        vertical: false
    });
    floorslider.init();
    ceilingslider = new dhtmlxSlider("ceilingslider", {
        skin: "dhx_skyblue",
        min: 0,
        max: 100,
        step: 1,
        size: 200,
        vertical: false
    });
    ceilingslider.init();

    floorslider.attachEvent("onChange", floorsliderchange);
    ceilingslider.attachEvent("onChange", ceilingsliderchange);

    // Use some hard-coded defaults for the time being
    floorslider.setValue($('#vivfloor').val());
    ceilingslider.setValue($('#vivceiling').val());

    /**
    *   pre-init, calc ranges
    **/
    calculateVivTypeRanges(vivTypes);


    /**
    *   Init Viv colorings and type select
    **/
    var $vs = $('#viv-type-select');

    // fill options first
    _.each(vivTypes, function (type, key) {
        $vs.append('<option>' + type.title + '</option>');
        segmentVivData[type.metricTypeArg] = new Array();
    });

    // show type a by default
    showViv('a');

    showSliderVals();


    // switch viv type show on select
    $vs.change(function (event) {

        var $s = $('#eddystats');
        $s.empty();


        _.each(vivTypes, function (type, key) {
            if ($vs.val() == type.title) {
                showViv(key);
            }
        });
    });

    /**
    *   Event handlers
    **/
    $('span[data-eblatype="segformat"]' + segmentcbFilter).click(function (event) {
        var $el = null; // $(event.target);

        var domElm = event.target;
        // We may have clicked (say) text within 'em' elm in cases such as <span data-ebla...>text text <em>text text</em>text</span>
        do {
            if (domElm == null) {
                alert('Unable to find enclosing ebla span!');
                return;
            }
            if (domElm.tagName.toLowerCase() == "span") {
                $el = $(domElm);
                break;
            }
            domElm = domElm.parentElement;
        } while (true);

        var segId = $el.attr('data-eblasegid');

        var vivTypeArg = null;

        _.each(vivTypes, function (type, key) {
            if ($vs.val() == type.title) {
                vivTypeArg = type.metricTypeArg;
            }
        });

        var segmentVivDataByType = segmentVivData[vivTypeArg];

        var cachedSegment = _.find(segmentVivDataByType, function (s) {
            return segId == s.BaseTextSegmentID;
        });

        if (_.isUndefined(cachedSegment)) {





            var args = {
                CorpusName: _corpusName,
                SegmentID: segId,
                metricType: vivTypeArg
            }

            $.vvv.getSegmentVariationData(args, function (data) {

                if (goptions.sortBy == 'name') {
                    data.VersionSegmentVariations = _.sortBy(data.VersionSegmentVariations, 'VersionName');
                }
                else if (goptions.sortBy == 'length') {
                    data.VersionSegmentVariations = _.sortBy(data.VersionSegmentVariations, function (d) { return d.VersionText.length });
                }
                else if (goptions.sortBy == 'eddy') {
                    data.VersionSegmentVariations = _.sortBy(data.VersionSegmentVariations, 'EddyValue');
                }
                else if (goptions.sortBy == 'year') {
                    data.VersionSegmentVariations = _.sortBy(data.VersionSegmentVariations, 'ReferenceDate');
                }

                if (goptions.sortAscending == false) {
                    data.VersionSegmentVariations = data.VersionSegmentVariations.reverse();
                }

                segmentVivDataByType.push(data);
                fillEddyStats(data);

                $.each(data.VersionSegmentVariations, function (i, seg) {
                    if (!_.has(seg, 'Backtranslation')) {
                        //console.log( seg );

                        var versionlangcode = $('#versionlangcode').val();
                        var basetextlangcode = $('#basetextlangcode').val();

                        $.vvv.getTranslation($.trim(seg.VersionText), versionlangcode, basetextlangcode, function (result) {
                            seg.Backtranslation = result;
                            $('#backtrans-' + seg.VersionSegmentIDs[0]).html(result);
                        });
                    }
                    else {
                        $('#backtrans-' + seg.VersionSegmentIDs[0]).html(seg.Backtranslation);
                    }
                });
            });
        }
        else {
            fillEddyStats(cachedSegment);
        }
    });

    $('.optionlink').click(function () {
        var $this = $(this);
        // don't proceed if already selected
        if ($this.hasClass('disabled')) {
            return false;
        }
        var $optionSet = $this.parents('.option-set');
        $optionSet.find('.disabled').removeClass('disabled');
        $this.addClass('disabled');

        // make option object dynamically, i.e. { filter: '.my-filter-class' }
        var options = {},
            key = $optionSet.attr('data-option-key'),
            value = $this.attr('data-option-value');
        // parse 'false' as false boolean
        value = value === 'false' ? false : true;
        options[key] = value;

        _.extend(goptions, options);

        if (key === 'layoutMode' && typeof changeLayoutMode === 'function') {
            // changes in layout modes need extra logic
            changeLayoutMode($this, goptions)
        } else {
            // otherwise, apply new options

            //console.log( goptions );
            $('#eddystats > .versions').isotope(goptions);
        }

        return false;
    });


    $('#viv-sort-select').change(function (event) {
        var $this = $(event.target).find('option:selected');
        var $optionSet = $('#viv-sort-select');

        var options = {},
                key = $optionSet.attr('data-option-key'),
                value = $this.attr('data-option-value');
        // parse 'false' as false boolean
        value = value === 'false' ? false : value;
        options[key] = value;

        _.extend(goptions, options);

        $('#eddystats > .versions').isotope(goptions);

        return false;
    });

});

function calculateVivTypeRanges( vivData ) {
    _.each(vivData, function (d, type) {
        _.each($('span[data-eblatype="segformat"]' + segmentcbFilter), function (el) {
            var $el = $(el);
            var title = $el.attr('title');
            // viv value may not have been calculated/stored in db
            var vivValue = 0.0;
            if ($el.attr('data-ebla-viv-' + type))
                vivValue = parseFloat($el.attr('data-ebla-viv-' + type));
            //var vivValue = parseFloat( $el.attr( 'data-ebla-viv-' + type ).replace( /((\d+),(\d+))/g, '$2.$3' ) );
            d.values.push(vivValue);
        });

        var min = _.min(d.values, function (v) { return v; });
        var max = _.max(d.values, function (v) { return v; });

        d.min = min;
        d.max = max;

        min = max * floorslider.getValue() * 1.0 / 100;
        max = max * ceilingslider.getValue() * 1.0 / 100;


        d.scale.domain([min, max]).range([0, 1]);
    });
}

function showViv(type) {
    slidermaxviv = vivTypes.a.max;
    if (type == 'b')
        slidermaxviv = vivTypes.b.max;
    if (type == 'c')
        slidermaxviv = vivTypes.c.max;
    if (type == 'd')
        slidermaxviv = vivTypes.d.max;
    if (type == 'e')
        slidermaxviv = vivTypes.e.max;
    showSliderVals();
    _.each($('span[data-eblatype="segformat"]' + segmentcbFilter), function (el) {
        setBackgroundColorForVivValue( $( el ), type );
    });
    $( 'span[data-eblatype="segformat"]' + segmentcbFilter).tooltip();
}

function setBackgroundColorForVivValue( $el, type ) {
    var rawVivValue = 0;
    if ( !_.isUndefined( $el.attr( 'data-ebla-viv-' + type ) ) ) {
        rawVivValue = parseFloat( $el.attr( 'data-ebla-viv-' + type ).replace( /((\d+),(\d+))/g, '$2.$3' ) );
    }

    if ( _.isNaN( rawVivValue ) ) {
        rawVivValue = 0;        
    }

    var normalizedViv = scale.getColor( vivTypes[type].scale( rawVivValue ) );
    $el.css( 'background-color', normalizedViv.hex() );
    $el.attr( 'data-original-title', 'Viv: ' + rawVivValue );
    $el.attr( 'title', 'Viv: ' + rawVivValue );
}

function AddSegmentToSelection(e) {
    var segid = $(e.target).attr('data-segid');

    PrismNS.Utils.AddSegmentToSelection(segid);

//    var cookiename = 'prismsegmentselection';
//    var cookiestring = $.cookie(cookiename);
//    if (!cookiestring)
//        cookiestring = '';
//    if (cookiestring.length > 0)
//        cookiestring += '-';

//    cookiestring += segid;
//    $.cookie(cookiename, cookiestring, { expires: 7, path: '/' });

}

function fillEddyStats( data ) {
    var addselhtml = '<div class="version-eddy-stat"><span class="btn btn-mini tt " title="Add to segment selection" id="add-seg-selection" data-segid="' + data.BaseTextSegmentID + '" >+</span></div>';
    var $s = $('#eddystats');
    $s.empty();
    $s.append(addselhtml);

    $('#add-seg-selection').click(function (e) { AddSegmentToSelection(e); });

    $s.append('<div class="versions"></div>');

    var $ls = $( '#eddystats > .versions' );
    var loader = '<img src="' + _siteUrl + 'Content/img/loader.gif" />';

    var maxEddy = 0;
    var minEddy = 0;
    if (data.VersionSegmentVariations.length > 0) {
        maxEddy = _.max(data.VersionSegmentVariations, function (d) { return d.EddyValue }).EddyValue;
        minEddy = _.min(data.VersionSegmentVariations, function (d) { return d.EddyValue }).EddyValue;
    }


    var eddyBarWidth = d3.scale.linear().domain([minEddy, maxEddy]).range([0,100]);
    $.each(data.VersionSegmentVariations, function (i, e) {
        var sel = '<div class="version-eddy-stat">';
        sel += '<div class="eddy-value-bar"><div class="bar-inner" style="width: ' + eddyBarWidth(e.EddyValue) + '%"></div></div>';
        var alignerhref = _siteUrl + 'Aligner?CorpusName=' +  encodeURIComponent( _corpusName) + '&VersionName=' + encodeURIComponent( e.VersionName) + '&basetextSegIDToView=' + data.BaseTextSegmentID + '&versionSegIDToView=' + e.VersionSegmentIDs[0];
        sel += '<div class="pull-right"><a title="See this segment in the aligner" href="' + alignerhref + '">A</a></div>';
        sel += '<div class="eddy-value"><span class="label">' + e.EddyValue + '</div>';
        sel += '<div class="backtranslation pull-right">';
        if ($.vvv.useMSTrans)
            sel += '<div class="backtrans-label">Backtranslation <span class="tiny">Microsoft&copy; Translate</span></div>';
        else
            sel += '<div class="backtrans-label">Backtranslation <span class="tiny">Google&copy; Translate</span></div>';
        sel += '<div id="backtrans-' + e.VersionSegmentIDs[0] + '" class="backtrans-text">' + loader + '</div>';
        sel += '</div>';
        sel += '<div class="version">';
        sel += '<div class="version-label">' + e.VersionName + (e.ReferenceDate == null ? '' : ' <small>(' + e.ReferenceDate + ')</small>') + '</div>';
        sel += '<div id="backtrans-' + e.VersionSegmentIDs[0] + '" class="version-text">' + e.VersionText + '</div>';
        sel += '</div>';
        $ls.append(sel);
    });


    var init = {
        itemSelector: '.version-eddy-stat',
        layoutMode: 'straightDown',
        getSortData: {
            eddy: function ($elem) {
                return parseFloat($.trim($elem.find('.eddy-value').text()));
            },
            name: function ($elem) {
                return $.trim($elem.find('.version > .version-label').text().replace(/\(\d+\)/g, ''));
            },
            length: function ($elem) {
                return $.trim($elem.find('.version > .version-text').text()).length;
            },
            year: function ($elem) {
                var re = /\(\d+\)/g;
                var yt = $elem.find('.version-label > small').text();
                if (yt.length > 0) {
                    var reresult = re.exec(yt);
                    if (!reresult)
                        return 0;
                    if (!reresult.length)
                        return 0;
                    var s1 = reresult[0];
                    var s = s1.substring(1, 5);
                    return parseInt(s);
                }
                return 0;
            }
        }
    }

    _.extend( init, goptions );
    $( '#eddystats > .versions' ).isotope( init );
}