﻿using System;
using System.Web.Mvc;

namespace Prism.HtmlHelpers
{
    internal class PrismPage : IDisposable
    {
        private readonly HtmlHelper _helper;

        public PrismPage(HtmlHelper helper)
        {
            _helper = helper;
        }

        public void Dispose()
        {
            _helper.ViewContext.Writer.Write("</div>");
        }

    }
}