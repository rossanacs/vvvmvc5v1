#!/usr/bin/env python
# encoding: utf-8

import sys, os, csv, re, json, string
from bs4 import BeautifulSoup

reload( sys )
sys.setdefaultencoding( 'utf-8' )

global sentiment_category_thresholds

def map_sentiment_score( salience_sentiment_float ):
    global sentiment_category_thresholds

    if(salience_sentiment_float == 0 or salience_sentiment_float is None):
        return 0
    for i in sentiment_category_thresholds:
        if salience_sentiment_float < i['threshold']:
            return i['sentiment_category']
    return i['sentiment_category']

if __name__ == '__main__':
    try:
        input_dir = 'source/'
        output_dir = 'out/'
        
        sentiment_steps = 12
        global sentiment_category_thresholds
        sentiment_category_thresholds = json.load( open( 'scoremapping_%s.json' % sentiment_steps, 'r') )

        file_filter = re.compile( 'basetext.xml' )
        #file_filter = re.compile("*.xml")
        
        for file in os.listdir( input_dir ):
          if not file_filter.search( file ):
              print 'skipping %s' % file
              continue

          print 'working on %s' % file
          bs = BeautifulSoup( open( input_dir + file ) )

          for segment in bs.find_all( 'span' ):
            lines = []
            if segment.string is None:
              for child in segment.children:
                if child.string is not None:
                  lines.append( child.string )
            else:
              lines.append( segment.string )

            sentiment_float = 0 # to it!
            segment['data-sentiment'] = sentiment_float
            segment['data-sentiment-cat'] = map_sentiment_score( sentiment_float )

          f = open( output_dir + file, 'w' )
          f.write( str( bs.prettify() ) )

    except KeyboardInterrupt:
        sys.stdout.write( u'\n\nGoodbye!\n' )
        sys.exit()