﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EblaAPI;
using EblaImpl;
using System.Configuration;

namespace Ebla.Tests
{
    [TestClass]
    public class CorpusStoreTests
    {
        [TestMethod]
        public void ShouldOpenStore()
        {
            Helpers.GetCorpusStore();
        }

        [TestMethod]
        public void ShoulcCreateTestCorpus()
        {
            Helpers.CreateTestCorpus();
        }

        [TestMethod]
        public void ShouldDeleteTestCorpus()
        {
            Helpers.CreateTestCorpus();

            ICorpusStore store = Helpers.GetCorpusStore();

            store.DeleteCorpus(Helpers.GetTestCorpusName());
        }

    }
}
