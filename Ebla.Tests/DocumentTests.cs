﻿using System;
using System.Text;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EblaAPI;
using HtmlAgilityPack;
using System.Xml;

namespace Ebla.Tests
{
    /// <summary>
    /// Test for Document class
    /// </summary>
    [TestClass]
    public class DocumentTests
    {
        internal const int TestDocIndex = 10;
        internal string TestDocument = Helpers.TestDocs[TestDocIndex].Filename;
        internal Encoding TestDocEncoding = Helpers.TestDocs[TestDocIndex].FileEncoding;

        public DocumentTests()
        {

        }

        private TestContext _testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get { return _testContextInstance; }
            set { _testContextInstance = value; }
        }

        #region Additional test attributes

        /// <summary>
        /// Initialize test corpus
        /// </summary>
        [TestInitialize()]
        public void DocumentTestsInitialize()
        {
            Helpers.CreateTestCorpus();
        }

        #endregion

        [TestMethod]
        public void RenderLength()
        {
            string path = System.IO.Path.Combine(Helpers.GetTestDataDir(TestContext), TestDocument);

            Helpers.UploadBaseTextToTestCorpus(path, TestDocEncoding);

            IDocument doc = Helpers.GetBaseText(Helpers.GetTestCorpusName());

            int docLength = doc.Length();

            string content = doc.GetDocumentContent(0, docLength, false, false, null, false, null, null, null);

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(Helpers.DocContentToXmlDoc(content));

            int i = EblaImpl.XmlDocPos.Measure(xmlDoc.DocumentElement, false);

            Assert.AreEqual(docLength, i);

        }

        [TestMethod]
        public void BulkRenderLength()
        {
            for (int i = 0; i < Helpers.TestDocs.Count; i++)
            {
                System.Diagnostics.Debug.WriteLine("BulkRenderLength, doc " + i.ToString());
                TestDocument = Helpers.TestDocs[i].Filename;
                TestDocEncoding = Helpers.TestDocs[i].FileEncoding;
                RenderLength();
            }
        }

        [TestMethod]
        [Timeout(1000 * 60 * 60 * 24)]
        public void FineGrainedExtract1()
        {
            ExtractTest(TestDocument, 100);
        }

        [TestMethod]
        public void CheckDocsMatch()
        {
            CheckHtmlDocMatches(TestDocument);
        }


        [TestMethod]
        public void BulkFineGrainedExtract1()
        {
            for (int i = 0; i < Helpers.TestDocs.Count; i++)
            {
                System.Diagnostics.Debug.WriteLine("BulkFineGrainedExtract1, doc " + i.ToString());
                TestDocument = Helpers.TestDocs[i].Filename;
                TestDocEncoding = Helpers.TestDocs[i].FileEncoding;
                FineGrainedExtract1();
            }
        }

        [TestMethod]
        [Timeout(1000 * 60 * 60 * 24)]
        public void FineGrainedFormat1()
        {
            FormatTest(TestDocument, 1, false);
        }

        [TestMethod]
        [Timeout(1000 * 60 * 60 * 24)]
        public void MediumGrainedFormat1()
        {
            FormatTest(TestDocument, 10, false);
        }

        [TestMethod]
        [Timeout(1000 * 60 * 60 * 24)]
        public void CoarseGrainedFormat1()
        {
            FormatTest(TestDocument, 100, false);
        }

        [TestMethod]
        [Timeout(1000 * 60 * 60 * 24)]
        public void MediumGrainedHeadTailFormat1()
        {
            FormatTest(TestDocument, 50, true);
        }

        [TestMethod]
        [Timeout(1000 * 60 * 60 * 24)]
        public void ShouldFormatTestWithTikaFineGrainedFormat()
        {
            FormatTestWithTika(TestDocument, 1, false);
        }

        [TestMethod]
        [Timeout(1000 * 60 * 60 * 24)]
        public void ShouldUploadBaseTextWithTika()
        {
            UploadBaseTextWithTika(TestDocument);
        }
        

        [TestMethod]
        [Timeout(1000 * 60 * 60 * 24)]
        public void BulkMediumGrainedHeadTailFormat1()
        {
            for (int i = 0; i < Helpers.TestDocs.Count; i++)
            {
                System.Diagnostics.Debug.WriteLine("BulkMediumGrainedHeadTailFormat1, doc " + i.ToString());
                TestDocument = Helpers.TestDocs[i].Filename;
                TestDocEncoding = Helpers.TestDocs[i].FileEncoding;
                FormatTest(TestDocument, 50, true);
            }
        }

        public void FormatTest(string filename, int granularity, bool headAndTailOnly)
        {
            int headAndTailLength = 100;

            string path = System.IO.Path.Combine(Helpers.GetTestDataDir(TestContext), filename);

            Helpers.UploadBaseTextToTestCorpus(path, TestDocEncoding);

            IDocument doc = Helpers.GetBaseText(Helpers.GetTestCorpusName());

            int docLength = doc.Length();

            if (headAndTailOnly)
            {
                if (docLength < (headAndTailLength * 2 + 2))
                    headAndTailOnly = false;
            }

            SegmentDefinition segDef = new SegmentDefinition();

            bool startPosIsInTail = false;
            for (int startPos = 0; startPos < docLength; startPos += granularity)
            {
                if (headAndTailOnly)
                    if (!startPosIsInTail)
                        if (startPos > headAndTailLength)
                        {
                            startPos = docLength - headAndTailLength;
                            startPosIsInTail = true;
                        }


                int lastLengthTrace = 0;
                for (int length = 0; length < docLength - startPos; length += granularity)
                {
                    try
                    {
                        if ((length - lastLengthTrace) > 500)
                        {
                            System.Diagnostics.Trace.WriteLine("FormatTest, startPos = " + startPos.ToString() +
                                                               ", length = " + length.ToString());
                            lastLengthTrace = length;
                        }

                        doc.DeleteAllSegmentDefinitions();
                        segDef.StartPosition = startPos;
                        segDef.Length = length;

                        doc.CreateSegmentDefinition(segDef);

                        string content = doc.GetDocumentContent(0, docLength, false, true, null, false, null, null, null);

                        HtmlDocument htmlDoc = new HtmlDocument();
                        htmlDoc.LoadHtml(Helpers.DocContentToHtmlDoc(content));

                        if (htmlDoc.ParseErrors.Any())
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.AppendFormat(
                                "There were errors parsing the document with startPos {0} and length {1}.\r\n", startPos,
                                length);

                            foreach (var e in htmlDoc.ParseErrors)
                            {
                                sb.Append("Reason: " + e.Reason);
                                sb.Append(", SourceText: " + e.SourceText);
                                sb.Append("\r\n");
                            }
                            throw new Exception(sb.ToString());
                        }

                        int totalChars = 0;

                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.PreserveWhitespace = true;
                        xmlDoc.LoadXml(Helpers.DocContentToXmlDoc(content));

                        int i = EblaImpl.XmlDocPos.Measure(xmlDoc.DocumentElement, true);

                        Assert.AreEqual(docLength, i);
                        CheckFormat(startPos, length, xmlDoc.DocumentElement, ref totalChars);
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Trace.WriteLine("FormatTest1, exception thrown with startPos = " +
                                                           startPos.ToString() + ", length = " + length.ToString());
                        throw e;
                    }
                }
            }
        }

        public void CheckHtmlDocMatches(string filename)
        {
            string path = System.IO.Path.Combine(Helpers.GetTestDataDir(TestContext), filename);



            var raw = new HtmlDocument();
            raw.Load(path, Encoding.UTF8);

            Helpers.UploadBaseTextToTestCorpus(path, TestDocEncoding);

            IDocument doc = Helpers.GetBaseText(Helpers.GetTestCorpusName());

            int docLength = doc.Length();

            string content = doc.GetDocumentContent(0, docLength, false, false, null, false, null, null,
                null);

            HtmlDocument htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(Helpers.DocContentToHtmlDoc(content));

            if (htmlDoc.ParseErrors.Any())
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat(
                    "There were errors parsing the doc");

                foreach (var e in htmlDoc.ParseErrors)
                {
                    sb.Append("Reason: " + e.Reason);
                    sb.Append(", SourceText: " + e.SourceText);
                    sb.Append("\r\n");
                }
                throw new Exception(sb.ToString());
            }

            HtmlNodeCollection rawbodyNodes = raw.DocumentNode.SelectNodes("/html/body");

            if (rawbodyNodes == null || rawbodyNodes.Count != 1)
                throw new Exception("body not found");

            HtmlNodeCollection bodyNodes = htmlDoc.DocumentNode.SelectNodes("/html/body");

            if (bodyNodes == null || bodyNodes.Count != 1)
                throw new Exception("body not found");

            var rawBodyNode = rawbodyNodes[0];
            var bodyNode = bodyNodes[0];

            var rawString = rawBodyNode.InnerText.Trim();
            rawString = System.Net.WebUtility.HtmlDecode(rawString);

            var bodyString = bodyNode.InnerText.Replace("&lt;", "<").Replace("&gt;", ">").Trim();

            if (string.Compare(rawString, bodyString) != 0)
            {
                int ix = GetFirstBreakIndex(rawString, bodyString, true);

                string a = rawString.Substring(0, ix);
                //string b = rawBodyNode.InnerText.Substring(0, ix);


                throw new Exception("docs don't match");
            }
        }



        /// <summary>
        /// Gets a first different char occurence index
        /// </summary>
        /// <param name="a">First string</param>
        /// <param name="b">Second string</param>
        /// <param name="handleLengthDifference">
        /// If true will return index of first occurence even strings are of different length
        /// and same-length parts are equals otherwise -1
        /// </param>
        /// <returns>
        /// Returns first difference index or -1 if no difference is found
        /// </returns>
        public int GetFirstBreakIndex(string a, string b, bool handleLengthDifference)
        {
            int equalsReturnCode = -1;
            if (String.IsNullOrEmpty(a) || String.IsNullOrEmpty(b))
            {
                return handleLengthDifference ? 0 : equalsReturnCode;
            }

            string longest = b.Length > a.Length ? b : a;
            string shorten = b.Length > a.Length ? a : b;
            for (int i = 0; i < shorten.Length; i++)
            {
                if (shorten[i] != longest[i])
                {
                    return i;
                }
            }

            // Handles cases when length is different (a="1234", b="123")
            // index=3 would be returned for this case
            // If you do not need such behaviour - just remove this
            if (handleLengthDifference && a.Length != b.Length)
            {
                return shorten.Length;
            }

            return equalsReturnCode;
        }

        public void ExtractTest(string filename, int granularity)
        {
            string path = System.IO.Path.Combine(Helpers.GetTestDataDir(TestContext), filename);

            Helpers.UploadBaseTextToTestCorpus(path, TestDocEncoding);

            IDocument doc = Helpers.GetBaseText(Helpers.GetTestCorpusName());

            int docLength = doc.Length();

            SegmentDefinition segDef = new SegmentDefinition();

            for (int startPos = 0; startPos < docLength; startPos += granularity)
            {
                int lastLengthTrace = 0;
                for (int length = 0; length < docLength - startPos; length += granularity)
                {
                    try
                    {
                        if ((length - lastLengthTrace) > 500)
                        {
                            System.Diagnostics.Trace.WriteLine("ExtractTest, startPos = " + startPos.ToString() +
                                                               ", length = " + length.ToString());
                            lastLengthTrace = length;
                        }

                        string content = doc.GetDocumentContent(startPos, length, false, false, null, false, null, null,
                            null);

                        HtmlDocument htmlDoc = new HtmlDocument();
                        htmlDoc.LoadHtml(Helpers.DocContentToHtmlDoc(content));

                        if (htmlDoc.ParseErrors.Any())
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.AppendFormat(
                                "There were errors parsing the extract with startPos {0} and length {1}.\r\n", startPos,
                                length);

                            foreach (var e in htmlDoc.ParseErrors)
                            {
                                sb.Append("Reason: " + e.Reason);
                                sb.Append(", SourceText: " + e.SourceText);
                                sb.Append("\r\n");
                            }
                            throw new Exception(sb.ToString());
                        }
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Trace.WriteLine("FormatTest1, exception thrown with startPos = " +
                                                           startPos.ToString() + ", length = " + length.ToString());
                        throw e;
                    }
                }
            }
        }

        public static int ExcludeWhitespaceLength(string s)
        {
            int i = 0;
            foreach (char c in s)
            {
                if (!char.IsWhiteSpace(c))
                    i++;

            }
            return i;
        }



        private void CheckFormat(int startPos, int length, XmlNode xmlNode, ref int totalChars)
        {

            foreach (XmlNode n in xmlNode.ChildNodes)
            {
                switch (n.NodeType)
                {
                    case XmlNodeType.Element:
                        if (true)
                        {
                            CheckFormat(startPos, length, n, ref totalChars);
                        }
                        break;
                    case XmlNodeType.Whitespace:
                    case XmlNodeType.SignificantWhitespace:
                    case XmlNodeType.Text:
                    {
						totalChars += ExcludeWhitespaceLength(n.InnerText);


						bool formatted = HasFormatting(n);
						bool formatError = false;
						if (totalChars < startPos + 1)
						{
							if (formatted)
								formatError = true;
						}
						else if (totalChars < startPos + 1 + length)
						{
							if (!formatted)
								formatError = true;

						}
                        else
                        {
                            if (formatted)
                                formatError = true;
                        }

                        if (formatError)
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.AppendFormat(
                                "Document formatting wrong at character {2} with startPos {0} and length {1}.", startPos,
                                length, totalChars);
                            sb.Append("\r\n");
                            if (formatted)
                                sb.Append("Content was formatted that should not have been.\r\n");
                            else
                                sb.Append("Content was not formatted that should have been.\r\n");
                            sb.Append("Text node contents: " + n.InnerText + "\r\n");

                            throw new Exception(sb.ToString());
                        }
                    }
                        break;
                    default:
                        throw new Exception("Unexpected XmlNodeType: " + n.NodeType.ToString());
                }
            }
        }

        private bool HasFormatting(XmlNode n)
        {
            if (n.NodeType == XmlNodeType.Element)
                if (n.Attributes != null && n.Attributes["data-eblatype"] != null)
                    if (string.Compare(n.Attributes["data-eblatype"].Value, "segformat") == 0)
                        return true;

            if (n.ParentNode == null)
                return false;

            return HasFormatting(n.ParentNode);
        }

        public void FormatTestWithTika(string filename, int granularity, bool headAndTailOnly)
        {
            int headAndTailLength = 100;

            string path = System.IO.Path.Combine(Helpers.GetTestDataDir(TestContext), filename);

            Helpers.UploadBaseTextToTestCorpusWithTika(path, TestDocEncoding);

            IDocument doc = Helpers.GetBaseText(Helpers.GetTestCorpusName());

            int docLength = doc.Length();

            if (headAndTailOnly)
            {
                if (docLength < (headAndTailLength * 2 + 2))
                    headAndTailOnly = false;
            }

            SegmentDefinition segDef = new SegmentDefinition();

            bool startPosIsInTail = false;
            for (int startPos = 0; startPos < docLength; startPos += granularity)
            {
                if (headAndTailOnly)
                    if (!startPosIsInTail)
                        if (startPos > headAndTailLength)
                        {
                            startPos = docLength - headAndTailLength;
                            startPosIsInTail = true;
                        }


                int lastLengthTrace = 0;
                for (int length = 0; length < docLength - startPos; length += granularity)
                {
                    try
                    {
                        if ((length - lastLengthTrace) > 500)
                        {
                            System.Diagnostics.Trace.WriteLine("FormatTest, startPos = " + startPos.ToString() +
                                                               ", length = " + length.ToString());
                            lastLengthTrace = length;
                        }

                        doc.DeleteAllSegmentDefinitions();
                        segDef.StartPosition = startPos;
                        segDef.Length = length;

                        doc.CreateSegmentDefinition(segDef);

                        string content = doc.GetDocumentContent(0, docLength, false, true, null, false, null, null, null);

                        HtmlDocument htmlDoc = new HtmlDocument();
                        htmlDoc.LoadHtml(Helpers.DocContentToHtmlDoc(content));

                        if (htmlDoc.ParseErrors.Any())
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.AppendFormat(
                                "There were errors parsing the document with startPos {0} and length {1}.\r\n", startPos,
                                length);

                            foreach (var e in htmlDoc.ParseErrors)
                            {
                                sb.Append("Reason: " + e.Reason);
                                sb.Append(", SourceText: " + e.SourceText);
                                sb.Append("\r\n");
                            }
                            throw new Exception(sb.ToString());
                        }

                        int totalChars = 0;

                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.PreserveWhitespace = true;
                        xmlDoc.LoadXml(Helpers.DocContentToXmlDoc(content));

                        int i = EblaImpl.XmlDocPos.Measure(xmlDoc.DocumentElement, true);

                        Assert.AreEqual(docLength, i);
                        CheckFormat(startPos, length, xmlDoc.DocumentElement, ref totalChars);
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Trace.WriteLine("FormatTest1, exception thrown with startPos = " +
                                                           startPos.ToString() + ", length = " + length.ToString());
                        throw e;
                    }
                }
            }
        }

        public void UploadBaseTextWithTika(string filename)
        {
            string path = System.IO.Path.Combine(Helpers.GetTestDataDir(TestContext), filename);

            Helpers.UploadBaseTextToTestCorpusWithTika(path, TestDocEncoding);

            IDocument doc = Helpers.GetBaseText(Helpers.GetTestCorpusName());

        }
    }
}
