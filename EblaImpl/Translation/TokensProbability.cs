﻿
namespace EblaImpl.Translation
{
    /// <summary>
    /// 
    /// </summary>
    public class TokensProbability
    {
        /// <summary>
        /// 
        /// </summary>
        public int AttributeValueOccurenceCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int TotalAttributeValue { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public float[] TokensProbabilityValues { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public float Mean { get; set; }
    }
}
