﻿using System.Collections.Generic;
using System.Linq;

namespace EblaImpl.Translation
{
    /// <summary>
    /// Base Translation Data
    /// </summary>
    public class BaseTranslationData
    {
        /// <summary>
        /// List of version translation
        /// </summary>
        public List<VersionTranslationData> VersionTranslationDatas
        {
            get { return _versionTranslationDatas; }
            set
            {
                _versionTranslationDatas = value;
                BuildVersionTranslationDatasIndex();
            }
        }

        private void BuildVersionTranslationDatasIndex()
        {
            _versionTranslationDatasIndex = null;
            if (_versionTranslationDatas == null)
                return;
            _versionTranslationDatasIndex = new Dictionary<int, VersionTranslationData>();
            foreach (var v in _versionTranslationDatas)
                _versionTranslationDatasIndex.Add(v.Wid, v);
        }

        List<VersionTranslationData> _versionTranslationDatas;
        Dictionary<int, VersionTranslationData> _versionTranslationDatasIndex;

        /// <summary>
        /// Initial Translation probability
        /// </summary>
        /// <param name="wid"></param>
        /// <returns></returns>
        public int GetInitialTranslationProbability(int wid)
        {
            return 1;
        }

        /// <summary>
        /// Get version translation data according to the wid.
        /// </summary>
        /// <param name="wid"></param>
        /// <returns></returns>
        public VersionTranslationData GetVersionTranslationData(int wid)
        {
            if (_versionTranslationDatas == null)
                return null;
            if (_versionTranslationDatasIndex == null)
                BuildVersionTranslationDatasIndex();
            VersionTranslationData result;
            _versionTranslationDatasIndex.TryGetValue(wid, out result);
            return result;            
        }

        /// <summary>
        /// Set values for VersionTranslationData
        /// </summary>
        /// <param name="wid"></param>
        /// <param name="probability"></param>
        public void SetVersionTranslationData(int wid, double probability)
        {
            if (VersionTranslationDatas == null)
            {
                VersionTranslationDatas = new List<VersionTranslationData>();
            }

            var versionData = VersionTranslationDatas.Find(p => p.Wid == wid);
            if (versionData == null)
            {
                versionData = new VersionTranslationData()
                {
                    Wid = wid,
                    Probability = probability
                };
                VersionTranslationDatas.Add(versionData);
                _versionTranslationDatasIndex = null;
            }
            else
            {
                versionData.Probability = probability;
            }
        }

        /// <summary>
        /// Normalize translation data
        /// </summary>
        public void Normalize()
        {
            var totalProbability = 0.0d;
            if (VersionTranslationDatas == null) return;

            totalProbability += VersionTranslationDatas.Sum(data => data.Probability);
            var index = 0;
            foreach (var data in VersionTranslationDatas)
            {
                var newProbability = data.Probability / totalProbability;
                VersionTranslationDatas[index].Wid = data.Wid;
                VersionTranslationDatas[index].Probability = newProbability;

                index++;
            }
        }

        /// <summary>
        /// Sort VersionTranslationDatas based on VersionTranslationDataComparer results.
        /// </summary>
        public void Sort()
        {
            VersionTranslationDatas.Sort(new VersionTranslationDataComparer());
        }

    }
}
