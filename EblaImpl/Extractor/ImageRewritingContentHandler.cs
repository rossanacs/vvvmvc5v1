﻿using com.sun.xml.@internal.bind.util;
using javax.xml.transform.sax;
using org.apache.tika.sax;
using System;

namespace EblaImpl.Extractor
{
    public class ImageRewritingContentHandler : ContentHandlerDecorator
    {
        public ImageRewritingContentHandler(TransformerHandler handler) : base(handler)
        {
        }

        public override void startElement(string uri, string localName, string name, org.xml.sax.Attributes origAttrs)
        {
            if ("img".Equals(localName))
            {
                AttributesImpl attrs;
                if (origAttrs is AttributesImpl)
                    attrs = (AttributesImpl)origAttrs;
                else
                    attrs = new AttributesImpl(origAttrs);

                for (int i = 0; i < attrs.getLength(); i++)
                {
                    if ("src".Equals(attrs.getLocalName(i)))
                    {
                        String src = attrs.getValue(i);
                        if (src.StartsWith("embedded:"))
                        {
                            var newSrc = src.Replace("embedded:", @"images\");

                            attrs.setValue(i, newSrc);
                        }
                    }
                }
                attrs.addAttribute(null, "width", "width", "width", "100px");
                base.startElement(uri, localName, name, attrs);
            }
            else
                base.startElement(uri, localName, name, origAttrs);
        }

        //public override void startElement2(string uri, string localName, string name, org.xml.sax.Attributes origAttrs)
        //{
        //    if ("img".Equals(localName))
        //    {
        //        AttributesImpl attrs;
        //        if (origAttrs is AttributesImpl)
        //            attrs = (AttributesImpl)origAttrs;
        //        else
        //            attrs = new AttributesImpl(origAttrs);

        //        for (int i = 0; i < attrs.getLength(); i++)
        //        {
        //            if ("src".Equals(attrs.getLocalName(i)))
        //            {
        //                String src = attrs.getValue(i);
        //                if (src.StartsWith("embedded:"))
        //                {
        //                    var newSrc = src.Replace("embedded:", @"images\");

        //                    attrs.setValue(i, newSrc);
        //                }
        //            }
        //        }
        //        attrs.addAttribute(null, "width", "width", "width", "100px");
        //        base.startElement(uri, localName, name, attrs);
        //    }
        //    else
        //        base.startElement(uri, localName, name, origAttrs);
        //}
    }
}
