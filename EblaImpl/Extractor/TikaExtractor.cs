﻿using org.apache.tika.parser;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using ikvm.extensions;
using java.io;
using org.apache.tika.detect;
using org.apache.tika.metadata;
using org.apache.tika.extractor;
using javax.xml.transform.sax;
using org.apache.tika.parser.txt;
using javax.xml.transform;
using javax.xml.transform.stream;
using sun.nio.cs;
using org.apache.tika.parser.html;
using EblaAPI;
using HtmlAgilityPack;
using org.apache.tika.language.detect;
using org.apache.tika.sax;

namespace EblaImpl.Extractor
{
    public class TikaExtractor
    {
        private ParseContext _context;
        private Parser _parser;
        private List<EblaAPI.HtmlParseError> _parseErrors = new List<EblaAPI.HtmlParseError>();

        public TikaExtractor() : base()
        {
            _context = new ParseContext();
            var detector = new DefaultDetector();
            _parser = new AutoDetectParser(detector);

            _context.set(typeof(org.apache.tika.parser.Parser), _parser);
            _context.set(typeof(HtmlMapper), new CustomHtmlMapper());

            var t = typeof(com.sun.codemodel.@internal.ClassType); // IKVM.OpenJDK.Tools
            t = typeof(com.sun.org.apache.xalan.@internal.xsltc.trax.TransformerFactoryImpl); // IKVM.OpenJDK.XML.Transform
            t = typeof(com.sun.org.glassfish.external.amx.AMX); // IKVM.OpenJDK.XML.WebServices
        }

        public byte[] ConvertFileToHtmlWithEmbeddedImages(byte[] file)
        {
            var parser = new AutoDetectParser();
            var output = new ByteArrayOutputStream();
            var factory = (SAXTransformerFactory)TransformerFactory.newInstance();
            var inputStream = new ByteArrayInputStream(file);
            try
            { 
                var metaData = new Metadata();
                var encodingDetector = new UniversalEncodingDetector();

                var encode = encodingDetector.detect(inputStream, metaData) ?? new UTF_32();

                var handler = factory.newTransformerHandler();
                handler.getTransformer().setOutputProperty(OutputKeys.METHOD, "html");
                handler.getTransformer().setOutputProperty(OutputKeys.INDENT, "yes");
                handler.getTransformer().setOutputProperty(OutputKeys.ENCODING, encode.toString());
                handler.setResult(new StreamResult(output));

                var imageRewriting = new ImageRewritingContentHandler(handler);

                var context = new ParseContext();
                context.set(typeof(EmbeddedDocumentExtractor), new FileEmbeddedDocumentExtractor());

                parser.parse(inputStream, imageRewriting, new Metadata(), context);

                var array = output.toByteArray();

                return array;
            }
            finally
            {
                inputStream.close();
            }
        }

        public byte[] ConvertAnyFileToHtml(byte[] file)
        {
            var input = new ByteArrayInputStream(file);
            var ouput = new StringWriter();

            try
            {
                var metadata = new Metadata();
                var factory = (SAXTransformerFactory) TransformerFactory.newInstance();

                var handler = factory.newTransformerHandler();
                handler.getTransformer().setOutputProperty(OutputKeys.METHOD, "html");
                handler.getTransformer().setOutputProperty(OutputKeys.INDENT, "yes");
                handler.getTransformer().setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                handler.setResult(new StreamResult(ouput));

                _parser.parse(input, handler, metadata, _context);

                var extractedContent = ouput.toString();

                return extractedContent.getBytes("UTF-8");

            }
            finally
            {
                input.close();
            }
        }

        public string ConvertTextToHtml(string file)
        {
            var input = new ByteArrayInputStream(Encoding.UTF8.GetBytes(file));
            var ouput = new StringWriter();

            try
            {
                var metadata = new Metadata();
                var factory = (SAXTransformerFactory) TransformerFactory.newInstance();

                var handler = factory.newTransformerHandler();
                handler.getTransformer().setOutputProperty(OutputKeys.METHOD, "html");
                handler.getTransformer().setOutputProperty(OutputKeys.INDENT, "yes");
                handler.getTransformer().setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                handler.setResult(new StreamResult(ouput));

                _parser.parse(input, handler, metadata, _context);

                var extractedContent = ouput.toString();

                return extractedContent;
            }
            finally
            {
                input.close();
            }
        }

        public string ConvertFileToHtml(byte[] file, string encoding, out DocumentMetadata fileMetadata)
        {
            var input = new ByteArrayInputStream(file);
            var ouput = new StringWriter();
            fileMetadata = new DocumentMetadata();

            try
            {
                var factory = (SAXTransformerFactory)TransformerFactory.newInstance();
                var metadata = new Metadata();
                var newEncoding = GetPrismEncoding(encoding);
                var encodingDetector = new UniversalEncodingDetector();
                encoding = encodingDetector.detect(input, metadata) != null
                    ? encodingDetector.detect(input, metadata)?.ToString()
                    : newEncoding ?? "UTF-8";

                var handler = factory.newTransformerHandler();
                handler.getTransformer().setOutputProperty(OutputKeys.METHOD, "html");
                handler.getTransformer().setOutputProperty(OutputKeys.INDENT, "yes");
                handler.getTransformer().setOutputProperty(OutputKeys.ENCODING, encoding);
                handler.setResult(new StreamResult(ouput));

                _parser.parse(input, handler, metadata, _context);

                var extractedContent = ouput.toString();

                extractedContent = extractedContent.replaceAll("\\s+\\&\\#xD;\\s+", " ");
                extractedContent = extractedContent.replaceAll("\\s+&nbsp;", " ");
                extractedContent = extractedContent.replaceAll("&#xD;</p>", "</p>");
                extractedContent = extractedContent.replaceAll("&#xD;&#xD;+", "</p>\n      <p>");
                extractedContent = extractedContent.replaceAll("&#xD;", "<br />\n      ");

                var document = new HtmlDocument();
                document.LoadHtml(extractedContent);

                extractedContent = document.DocumentNode.OuterHtml;

                fileMetadata = ConvertMetadata(extractedContent, metadata);

                return extractedContent;
            }
            finally
            {
                input.close();
            }
        }

        public HtmlDocumentResult ConvertFileToHtml(byte[] file, string encoding)
        {
            var input = new ByteArrayInputStream(file);
            var ouput = new StringWriter();

            try
            {
                var factory = (SAXTransformerFactory)TransformerFactory.newInstance();
                var metadata = new Metadata();
                var newEncoding = GetPrismEncoding(encoding);
                var encodingDetector = new UniversalEncodingDetector();
                encoding = encodingDetector.detect(input, metadata) != null
                    ? encodingDetector.detect(input, metadata)?.ToString()
                    : newEncoding ?? "UTF-8";

                var handler = factory.newTransformerHandler();
                handler.getTransformer().setOutputProperty(OutputKeys.METHOD, "html");
                handler.getTransformer().setOutputProperty(OutputKeys.INDENT, "yes");
                handler.getTransformer().setOutputProperty(OutputKeys.ENCODING, encoding);
                handler.setResult(new StreamResult(ouput));

                _parser.parse(input, handler, metadata, _context);

                var htmlResult = GetUploadedHtmlDocument(ouput.toString());

                return htmlResult;
            }
            finally
            {
                input.close();
            }
        }

        public static CultureInfo FromIsoName(string name)
        {
            return CultureInfo
                .GetCultures(CultureTypes.NeutralCultures)
                .FirstOrDefault(c => c.TwoLetterISOLanguageName == name);
        }

        private DocumentMetadata ConvertMetadata(string content, Metadata metadata)
        {
            var documentMetadata = new DocumentMetadata();

            var doc = new HtmlDocument();
            doc.LoadHtml(content);
            var bodyNodes = doc.DocumentNode.SelectNodes("/html/body");

            if (bodyNodes != null && bodyNodes.Count > 0)
            {
                var languageHandler = new LanguageHandler();
                var detector = languageHandler.getDetector();
                var result = detector.detect(bodyNodes[0].InnerHtml);
                documentMetadata.LanguageCode = FromIsoName(result.getLanguage()).ThreeLetterISOLanguageName;
            }

            foreach (var name in metadata.names())
            {
                if (name.ToLower() == "author" || name.ToLower() == "meta:author")
                {
                    documentMetadata.AuthorTranslator = metadata.get(name);
                }
                if (name.ToLower() == "title" || name.ToLower() == "dc:title")
                {
                    documentMetadata.Information = metadata.get(name);
                }
                if (name.ToLower() == "comment")
                {
                    documentMetadata.Description = metadata.get(name);
                }
                
            }

            return documentMetadata;
        }

        private HtmlDocumentResult GetUploadedHtmlDocument(string content)
        {
            var documentMetadata = new DocumentMetadata();
            var documentResult = new HtmlDocumentResult();
            var doc = new HtmlDocument();
            doc.LoadHtml(content);
            var bodyNodes = doc.DocumentNode.SelectNodes("/html/body");

            if (bodyNodes != null && bodyNodes.Count > 0)
            {
                var languageHandler = new LanguageHandler();
                var detector = languageHandler.getDetector();
                var result = detector.detect(bodyNodes[0].InnerHtml);
                documentMetadata.LanguageCode = FromIsoName(result.getLanguage()).ThreeLetterISOLanguageName;
            }

            var metaTags = doc.DocumentNode.SelectNodes("//meta");

            if (metaTags != null)
            {
                foreach (var tag in metaTags)
                {
                    var tagName = tag.Attributes["name"];
                    var tagContent = tag.Attributes["content"];
                    if (tagName != null && tagContent != null)
                    {
                        switch (tagName.Value.ToLower())
                        {
                            case "author":
                            case "meta:author":
                                documentMetadata.AuthorTranslator = tagContent.Value;
                                break;
                            case "title":
                            case "dc:title":
                                documentMetadata.Information = tagContent.Value;
                                break;
                            case "comment":
                                documentMetadata.Description = tagContent.Value;
                                break;
                        }
                    }
                }

                foreach (var node in metaTags)
                {
                    node.Remove();
                }
            }

            if (documentMetadata.LanguageCode == null)
                documentMetadata.LanguageCode = "eng";
                    
            documentResult.Metadata = documentMetadata;
            documentResult.Content = doc.DocumentNode.InnerHtml;

            return documentResult;
        }

        private static string GetPrismEncoding(string encoding)
        {
            PrismEncoding newEncoding;
            string description = null;
            if (Enum.TryParse(encoding, out newEncoding))
            {
                description = EblaHelpers.GetEnumDescription(newEncoding);
            }
            return description;
        }

        public byte[] ConvertToHtmlWithEmbeddedFiles(byte[] file)
        {
            var input = new ByteArrayInputStream(file);
            var ouput = new StringWriter();

            try
            {
                var metadata = new Metadata();
                var factory = (SAXTransformerFactory)TransformerFactory.newInstance();

                var handler = factory.newTransformerHandler();
                handler.getTransformer().setOutputProperty(OutputKeys.METHOD, "html");
                handler.getTransformer().setOutputProperty(OutputKeys.INDENT, "yes");
                handler.getTransformer().setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                handler.setResult(new StreamResult(ouput));

                RecursiveParserWrapper wrapper = new RecursiveParserWrapper(_parser, new BasicContentHandlerFactory(
                    BasicContentHandlerFactory.HANDLER_TYPE.TEXT, 60));

                //_parser.parse(input, handler, metadata, _context);
                wrapper.parse(input, handler, metadata, _context);

                var extractedContent = ouput.toString();

                //if (metadata != null && metadata.get("dc:format") != null &&
                //    metadata.get("dc:format").toLowerCase().Contains(DocumentFormat.pdf.ToString()))
                //{
                //    extractedContent = extractedContent
                //        //.replaceAll("(&#xD;&#xA;|&#xD;|&#xA;)+", "")
                //                                    //.replaceAll("\\t[\\s \u00A0]+", " ")
                //                                    .replaceAll("\\s+\\&\\#xD;\\s+", " ")
                //                                    .replaceAll("\\s+&nbsp;", " ");
                //}

                return extractedContent.getBytes("UTF-8");

            }
            finally
            {
                input.close();
            }
        }
    }

}

internal class CustomHtmlMapper : DefaultHtmlMapper
{
    public override string mapSafeElement(string name)
    {
        return name.toLowerCase();
    }

    public override string mapSafeAttribute(string elementName, string attributeName)
    {
        return attributeName.toLowerCase();
    }
}
