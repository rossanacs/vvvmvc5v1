﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.

    This class was based on the following articles and 2006-2015 Jarek Lipski - 
    Maligna (v3.0.1-SNAPSHOT) source code (https://github.com/loomchild/maligna):

    [1] Peter F. Brown, Jennifer C. Lai, Robert L. Mercer (1991), 
    Aligning sentences in parallel corpora, Proceedings of the 29th annual meeting on Association for Computational Linguistics.
    [2] Jassem, K., & Lipski, J. (2008). A new tool for the bilingual text aligning at the sentence level. 
    Intelligent Information Systems, 279-286.
    [3] Moore, R. C. (2002, October). Fast and accurate sentence alignment of bilingual corpora. 
    In Conference of the Association for Machine Translation in the Americas (pp. 135-144). Springer Berlin Heidelberg.
 */

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using EblaAPI;
using EblaImpl.Algorithm;
using EblaImpl.Calculator;
using EblaImpl.Translation;
using java.util;
using net.loomchild.maligna.coretypes;

namespace EblaImpl.Aligner
{
    public class MalignaTranslationAligner : OpenableEblaItem, ITranslationAligner
    {
        private const string TotalTokensAttributeName = ":totaltokens";
        private const string TokensAttributeName = ":tokens";
        private const string WidsAttributeName = ":wids";
        private const int DefaultMaxWordCount = 5000;
        private const int DefaultMinOccurrenceCount = 2;
        //private SingletonDb _conn = SingletonDb.Instance;

        public MalignaTranslationAligner(NameValueCollection configProvider) : base(configProvider)
        {
        }

        private void CheckConnectionStatus()
        {
          //  _conn = SingletonDb.Instance;
          //  _conn.GetConnection();

            _open = true;
        }

        public TranslationDocument GetMooreTranslationDocument(string corpusName, string versionName, string username,
            string password)
        {
            CheckConnectionStatus();

            var corpus = new Corpus(ConfigProvider);
            corpus.OpenWithoutPrivilegeCheck(corpusName, true, GetConnection());

            var translationDocument = new TranslationDocument
            {
                CorpusName = corpusName,
                VersionName = versionName
            };

            var translationUtil = new TranslationUtil();

            translationDocument.BaseDocument = (Document) EblaFactory.GetDocument(ConfigProvider);
            if (!translationDocument.BaseDocument.OpenBaseText(corpusName, username, password))
                throw new Exception("You do not have permission to access the document.");

            var sentenceAlignment = new SegmentBasedAlignment(ConfigProvider);
            translationDocument.BaseDocumentSegmentsTotalTokens =
                sentenceAlignment.GetSegmentTokens(((Document) translationDocument.BaseDocument).ID(),
                    TotalTokensAttributeName);

            translationDocument.VersionDocument = (Document) EblaFactory.GetDocument(ConfigProvider);
            if (!translationDocument.VersionDocument.OpenVersion(corpusName, versionName, username, password))
                throw new Exception("You do not have permission to access the document.");

            translationDocument.VersionDocumentSegmentsTotalTokens =
                sentenceAlignment.GetSegmentTokens(((Document) translationDocument.VersionDocument).ID(),
                    TotalTokensAttributeName);

            translationDocument.BaseDocumentSegmentsTokens =
                sentenceAlignment.GetSegmentTokens(((Document) translationDocument.BaseDocument).ID(),
                    TokensAttributeName);

            translationDocument.BaseVocabulary =
                translationUtil.CreateAlignmentVocabularyWithoutRareWords(
                    translationDocument.BaseDocumentSegmentsTokens, DefaultMinOccurrenceCount);

            var baseDocumentWids = sentenceAlignment.GetSegmentTokens(((Document)translationDocument.BaseDocument).ID(),
                    WidsAttributeName);
            translationDocument.BaseDocumentSegmentsWids =
                translationUtil.CreateSegmentWidListWithoutRareWords(baseDocumentWids,
                    translationDocument.BaseVocabulary);

            translationDocument.VersionDocumentSegmentsTokens =
                sentenceAlignment.GetSegmentTokens(((Document) translationDocument.VersionDocument).ID(),
                    TokensAttributeName);

            translationDocument.VersionVocabulary =
                translationUtil.CreateAlignmentVocabularyWithoutRareWords(
                    translationDocument.VersionDocumentSegmentsTokens, DefaultMinOccurrenceCount);

            var versionDocumentWids = sentenceAlignment.GetSegmentTokens(((Document)translationDocument.VersionDocument).ID(),
                    WidsAttributeName);
            translationDocument.VersionDocumentSegmentsWids =
                translationUtil.CreateSegmentWidListWithoutRareWords(versionDocumentWids,
                    translationDocument.VersionVocabulary);

            return translationDocument;
        }

        private List<BestAlignment> CreateAlignments(List result, TranslationDocument td,  Dictionary<string, System.Collections.Generic.List<SegmentTokens>> bases, 
            Dictionary<string, System.Collections.Generic.List<SegmentTokens>> versions, 
            System.Collections.Generic.List<string> basesKeys, System.Collections.Generic.List<string> versionsKeys)
        {
            System.Collections.Generic.List<int> resultIndexAlreadyUsed = new System.Collections.Generic.List<int>();
            var u = new List<BestAlignment>();
            BestAlignment b = new BestAlignment();
            var keys = bases.Keys;
            int sourceIndexAlreadyUsed = 0;
            int targetIndexAlreadyUsed = 0;
            int quant = 0;

            //1-0 1-1 2-1 1-2
            foreach (var k in basesKeys)
            {
                if (quant-- > 1) continue;
                var best = FindAligmentBySource(result, k, resultIndexAlreadyUsed, bases, versions, ref sourceIndexAlreadyUsed, sourceIndexAlreadyUsed, out quant);
                if (best != null) { u.Add(best); sourceIndexAlreadyUsed++; };
            }

            //0-1
            foreach (var k in versionsKeys)
            {
                if (quant-- > 1) continue;
                var best = FindRemainingAligmentByVersion(result, k, resultIndexAlreadyUsed, bases, versions, ref targetIndexAlreadyUsed, targetIndexAlreadyUsed, out quant);
                if (best != null) {u.Add(best); targetIndexAlreadyUsed++; }
            }
            return u;
        }

        private BestAlignment FindRemainingAligmentByVersion(List result, string key, 
            System.Collections.Generic.List<int> resultIndexAlreadyUsed,
            Dictionary<string, System.Collections.Generic.List<SegmentTokens>> bases, 
            Dictionary<string, System.Collections.Generic.List<SegmentTokens>> versions, 
            ref int indexAlreadyInUse, int indexAlreadyInUseAsValue, out int quant)
        {
            quant = 0;
            for (int i = indexAlreadyInUseAsValue; i < result.size(); i++)
            {
                if (resultIndexAlreadyUsed.Contains(i)) continue;

                var alignment = (net.loomchild.maligna.coretypes.Alignment)result.get(i);
                for (int j = 0; j < alignment.getTargetSegmentList().size(); j++)
                {
                    if (((string)alignment.getTargetSegmentList().get(j)) == key)
                    {
                        //0 -> 1
                        var ba = new BestAlignment
                        {
                            BaseSegmentList = new SegmentTokens[0]
                        };

                        SegmentTokens[] vs = new SegmentTokens[alignment.getTargetSegmentList().size()];
                        for (int k = 0; k < alignment.getTargetSegmentList().size(); k++)
                        {
                            string versionText = (string)alignment.getTargetSegmentList().get(k);
                            vs[k] = versions[versionText][0];
                            versions[versionText].RemoveAt(0);//If a segment remains, it can be recovered later. We can assume that will never look for an unexisting segment.
                            quant++;

                        }
                        ba.VersionSegmentList = vs;

                        resultIndexAlreadyUsed.Add(i);
                        indexAlreadyInUse = i;

                        return ba;
                    }
                }
            }
            return null;
        }

        private BestAlignment FindAligmentBySource(List result, string key,
            System.Collections.Generic.List<int> resultIndexAlreadyUsed,
            Dictionary<string, System.Collections.Generic.List<SegmentTokens>> bases,
            Dictionary<string, System.Collections.Generic.List<SegmentTokens>> versions,
            ref int indexAlreadyUsed, int indexAlreadyUsedAsValue, out int quant)
        {
            quant = 0;
            for (int i = indexAlreadyUsedAsValue; i < result.size(); i++)
            {
                var alignment = (net.loomchild.maligna.coretypes.Alignment)result.get(i);
                for (int j = 0; j < alignment.getSourceSegmentList().size(); j++)
                {
                    if (((string)alignment.getSourceSegmentList().get(j)) == key)
                    {
                        var ba = new BestAlignment();
                        SegmentTokens[] bs = new SegmentTokens[alignment.getSourceSegmentList().size()];
                        for (int k = 0; k < alignment.getSourceSegmentList().size(); k++)
                        {
                            bs[k] = bases[(string)alignment.getSourceSegmentList().get(k)][0];
                            bases[(string)alignment.getSourceSegmentList().get(k)].RemoveAt(0);
                            //We can assume that will never look at a segment that does not exist. 
                            //Since all segments were used and are in the same order.
                            quant++;
                        }
                        ba.BaseSegmentList = bs;

                        SegmentTokens[] vs = new SegmentTokens[alignment.getTargetSegmentList().size()];
                        for (int k = 0; k < alignment.getTargetSegmentList().size(); k++)
                        {
                            string versionText = (string)alignment.getTargetSegmentList().get(k);
                            vs[k] = versions[versionText][0];
                            versions[versionText].RemoveAt(0);//If a segment remains, it can be recovered later. 
                            //We can assume that will never look for an unexisting segment.
                        }
                        resultIndexAlreadyUsed.Add(i);
                        ba.VersionSegmentList = vs;

                        indexAlreadyUsed = i;

                        return ba;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Moore alignment - EXE version
        /// </summary>
        /// <param name="translationDocument"></param>
        /// <param name="alignmentSet"></param>
        /// <param name="progress"></param>
        /// <returns></returns>
        public bool ApplyMooreAlignment(TranslationDocument translationDocument, string corpusName, string versionName, string userName, string password,
            out int progress)
        {
            CheckConnectionStatus();
            var unifiedAlignments = ApplyMooreAlignment(translationDocument);

            // Get a specific connection for this operation
            // (per-call connection held in Session state could easily now be null after Session timeout)
            using (var cn = EblaHelpers.GetConnection(ConfigProvider))
            {

                EblaCxnMgr.ThreadConnection = cn;
                var alignmentSet = new AlignmentSet(ConfigProvider);
                if (alignmentSet == null)
                    throw new Exception("It was not possible to instantiate auto-alignment.");

                alignmentSet.Open(corpusName, versionName, userName, password);

                var isMoreToDo = alignmentSet.CreateMooreAlignments(translationDocument, unifiedAlignments,
                    out progress);

                return isMoreToDo;
            }        
        }

        private List<BestAlignment> ApplyMooreAlignment(TranslationDocument translationDocument)
        {
            var alignmentList = new java.util.ArrayList();
            var alignment = new net.loomchild.maligna.coretypes.Alignment();
            alignmentList.add(alignment);
            var baseTokens = new Dictionary<string, System.Collections.Generic.List<SegmentTokens>>();
            var baseKeys = new System.Collections.Generic.List<string>();
            var versionTokens = new Dictionary<string, System.Collections.Generic.List<SegmentTokens>>();
            var versionKeys = new System.Collections.Generic.List<string>();
            for (int i = 0; i < translationDocument.BaseDocumentSegmentsWids.Length; i++)
            {
                var baseSegment = translationDocument.BaseDocumentSegmentsWids[i].Segment;
                if (baseTokens.ContainsKey(baseSegment))
                {
                    baseTokens[baseSegment].Add(translationDocument.BaseDocumentSegmentsWids[i]);
                }
                else
                {
                    var segments = new System.Collections.Generic.List<SegmentTokens>
                    {
                        translationDocument.BaseDocumentSegmentsWids[i]
                    };
                    baseTokens.Add(baseSegment, segments);
                }
                baseKeys.Add(baseSegment);
                alignment.addSourceSegment(baseSegment);
            }
            for (int i = 0; i < translationDocument.VersionDocumentSegmentsWids.Length; i++)
            {
                var versionSegment = translationDocument.VersionDocumentSegmentsWids[i].Segment;
                if (versionTokens.ContainsKey(versionSegment))
                {
                    versionTokens[versionSegment].Add(translationDocument.VersionDocumentSegmentsWids[i]);
                }
                else
                {
                    var segments = new System.Collections.Generic.List<SegmentTokens>
                    {
                        translationDocument.VersionDocumentSegmentsWids[i]
                    };
                    versionTokens.Add(versionSegment, segments);
                }
                versionKeys.Add(versionSegment);
                alignment.addTargetSegment(versionSegment);
            }

            var macro = new net.loomchild.maligna.filter.macro.MooreMacro();
            var result = macro.apply(alignmentList);

            var unifiedAlignments = CreateAlignments(result, translationDocument, baseTokens, versionTokens, baseKeys, versionKeys);
            return unifiedAlignments;
        }

        public bool ContinueAutoAlignment(TranslationDocument translationDocument, string corpusName, string versionName, string username,
            string password, out int progress)
        {
            CheckConnectionStatus();
            var isMoreToDo = ApplyMooreAlignment(translationDocument, corpusName, versionName, username, password, out progress);

            return isMoreToDo;
        }

        public List<BestAlignment> ApplyAutoAlignmentAndCreateEblaAlignment(string corpusName, 
            string versionName, string userName, string password)
        {
            CheckConnectionStatus();

            var aligner = new MalignaTranslationAligner(ConfigProvider);
            if (aligner == null)
                throw new Exception("It was not possible to instantiate aligner.");

            var translationDocument = aligner.GetMooreTranslationDocument(corpusName, versionName, userName, password);
            if (translationDocument == null)
                throw new Exception("It was not possible to create the translation document.");

            var alignmentSet = new AlignmentSet(ConfigProvider);
            if (alignmentSet == null)
                throw new Exception("It was not possible to instantiate auto-alignment.");

            alignmentSet.Open(corpusName, versionName, userName, password);

            var alignments = ApplyMooreAlignment(translationDocument);

            ((AlignmentSet)alignmentSet).CreateMooreAlignments(translationDocument, alignments);

            return alignments;
        }

        private List<BestAlignment> ApplyGaleAndChurchAlignment(TranslationDocument translationDocument)
        {
            var alignmentList = new java.util.ArrayList();
            var alignment = new net.loomchild.maligna.coretypes.Alignment();
            alignmentList.add(alignment);
            var baseTokens = new Dictionary<string, System.Collections.Generic.List<SegmentTokens>>();
            var baseKeys = new System.Collections.Generic.List<string>();
            var versionTokens = new Dictionary<string, System.Collections.Generic.List<SegmentTokens>>();
            var versionKeys = new System.Collections.Generic.List<string>();
            for (int i = 0; i < translationDocument.BaseDocumentSegmentsWids.Length; i++)
            {
                var baseSegment = translationDocument.BaseDocumentSegmentsWids[i].Segment;
                if (baseTokens.ContainsKey(baseSegment))
                {
                    baseTokens[baseSegment].Add(translationDocument.BaseDocumentSegmentsWids[i]);
                }
                else
                {
                    var segments = new System.Collections.Generic.List<SegmentTokens>
                    {
                        translationDocument.BaseDocumentSegmentsWids[i]
                    };
                    baseTokens.Add(baseSegment, segments);
                }
                baseKeys.Add(baseSegment);
                alignment.addSourceSegment(baseSegment);
            }
            for (int i = 0; i < translationDocument.VersionDocumentSegmentsWids.Length; i++)
            {
                var versionSegment = translationDocument.VersionDocumentSegmentsWids[i].Segment;
                if (versionTokens.ContainsKey(versionSegment))
                {
                    versionTokens[versionSegment].Add(translationDocument.VersionDocumentSegmentsWids[i]);
                }
                else
                {
                    var segments = new System.Collections.Generic.List<SegmentTokens>
                    {
                        translationDocument.VersionDocumentSegmentsWids[i]
                    };
                    versionTokens.Add(versionSegment, segments);
                }
                versionKeys.Add(versionSegment);
                alignment.addTargetSegment(versionSegment);
            }

            var macro = new net.loomchild.maligna.filter.macro.GaleAndChurchMacro();
            var result = macro.apply(alignmentList);

            var unifiedAlignments = CreateAlignments(result, translationDocument, baseTokens, versionTokens, baseKeys, versionKeys);
            return unifiedAlignments;
        }

        public List<BestAlignment> ApplyAutoAlignmentAndCreateEblaAlignmentWithGaleAndChurchMacro(string corpusName,
            string versionName, string userName, string password)
        {
            CheckConnectionStatus();

            var aligner = new MalignaTranslationAligner(ConfigProvider);
            if (aligner == null)
                throw new Exception("It was not possible to instantiate aligner.");

            var translationDocument = aligner.GetMooreTranslationDocument(corpusName, versionName, userName, password);
            if (translationDocument == null)
                throw new Exception("It was not possible to create the translation document.");

            var alignmentSet = new AlignmentSet(ConfigProvider);
            if (alignmentSet == null)
                throw new Exception("It was not possible to instantiate auto-alignment.");

            alignmentSet.Open(corpusName, versionName, userName, password);

            var alignments = ApplyGaleAndChurchAlignment(translationDocument);

            ((AlignmentSet)alignmentSet).CreateMooreAlignments(translationDocument, alignments);

            return alignments;
        }


    }
}