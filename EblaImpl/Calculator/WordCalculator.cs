﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.

    This class was based on the following articles and 2006-2015 Jarek Lipski - 
    Maligna (v3.0.1-SNAPSHOT) source code (https://github.com/loomchild/maligna):

    [1] Peter F. Brown, Jennifer C. Lai, Robert L. Mercer (1991), 
    Aligning sentences in parallel corpora, Proceedings of the 29th annual meeting on Association for Computational Linguistics.
    [2] Jassem, K., & Lipski, J. (2008). A new tool for the bilingual text aligning at the sentence level. 
    Intelligent Information Systems, 279-286.
    [3] Moore, R. C. (2002, October). Fast and accurate sentence alignment of bilingual corpora. 
    In Conference of the Association for Machine Translation in the Americas (pp. 135-144). Springer Berlin Heidelberg.
 */
using System;
using System.Collections.Generic;
using EblaAPI;
using EblaImpl.Aligner;
using EblaImpl.Translation;
using System.Text;

namespace EblaImpl.Calculator
{
    public class WordCalculator: ICalculator
    {
        private const float MinimumTranslationProbability = 1e-38f;

        internal float Mean { get; set; }
        internal TokensProbability BaseProbability { get; set; }
        internal TokensProbability VersionProbability { get; set; }
        internal Vocabulary BaseVocabulary { get; set; }
        internal Vocabulary VersionVocabulary { get; set; }
        internal TranslationData WordTranslationData { get; set; }
        internal List<BestAlignment> BestAlignments { get; set; }
        internal List<BestAlignment> InitialAlignments { get; set; }
        public TranslationDocument WordTranslationDocument { get; set; }
        Dictionary<string, float> _cachedScores = new Dictionary<string, float>();

        public WordCalculator()
        {
            if (BaseProbability != null && VersionProbability != null)
                Mean = BaseProbability.Mean / VersionProbability.Mean;
        }

        public WordCalculator(TranslationDocument translationDocument)
        {
            BaseProbability = CalculateProbabilities(translationDocument.BaseDocumentSegmentsWids,
                translationDocument.BaseVocabulary);
            VersionProbability = CalculateProbabilities(translationDocument.VersionDocumentSegmentsWids,
                translationDocument.VersionVocabulary);
            if (BaseProbability != null && VersionProbability != null)
                Mean = BaseProbability.Mean / VersionProbability.Mean;
            BaseVocabulary = translationDocument.BaseVocabulary;
            VersionVocabulary = translationDocument.VersionVocabulary;
        }

        static string SegmentTokensArrayKey(SegmentTokens[] tokens)
        {
            var sb = new StringBuilder();
            foreach (var t in tokens)
                sb.Append(t.Id + "-");
            return sb.ToString();

        }


        public float CalculateScore(SegmentTokens[] baseTokenses,
            SegmentTokens[] versionTokenses)
        {
            var translationUtil = new TranslationUtil();

            var baseWidList = translationUtil.CreateSegmentWidListWithoutRareWords(baseTokenses, BaseVocabulary, false);

            var versionWidList = translationUtil.CreateSegmentWidListWithoutRareWords(versionTokenses, VersionVocabulary, false);

            string scoreKey = SegmentTokensArrayKey(baseTokenses) + " " + SegmentTokensArrayKey(versionTokenses);

            float result;

            // As a temporary measure (otherwise a typical Shakespeare play takes a very long time to align)
            // use a simple memory cache for scores.
            // Obviously this needs finessing for larger texts.
            if (_cachedScores.TryGetValue(scoreKey, out result))
                return result;
               
            var score = CalculateScore(baseWidList, versionWidList);

            _cachedScores.Add(scoreKey, score);


            return score;
        }

        private float CalculateScore(List<Int32?> baseWidList, List<Int32?> versionWidList)
        {
            float score;
            if (baseWidList.Count == 0 && versionWidList.Count == 0)
            {
                score = 0.0f;
            }
            else if (baseWidList.Count == 0)
            {
                score = CalculateScore(versionWidList, VersionProbability);
            }
            else
            {
                score = CalculateScore(baseWidList, BaseProbability);
                if (versionWidList.Count <= 0) return score;
                var newBaseWidList = TranslationUtil.AddNullWidToList(baseWidList);//TODO Validate
                score += CalculateTranslationScore(newBaseWidList, versionWidList, BaseProbability,
                    VersionProbability);
            }

            return score;
        }

        static float singletonWordProbability = 0.2f;//TODO Change this value
        internal static float CalculateScore(List<Int32?> widList, TokensProbability probability)
        {
            
            float score = 0.0f;
            foreach (int? wid in widList)
            {
                float wordProbability;
                if (wid != null)
                {
                    wordProbability = GetWordProbability(wid, probability);
                }
                else
                {
                    wordProbability = singletonWordProbability;
                }
                score += (float)-Math.Log(wordProbability);
            }
            return score;
        }

        public static float GetWordProbability(Int32? wid, TokensProbability probability)
        {
            if (wid < probability.TokensProbabilityValues.Length)
            {
                return probability.TokensProbabilityValues[wid.Value];
            }
            else
            {
                return singletonWordProbability;
            }
        }

        internal float CalculateTranslationScore(List<Int32?> baseWidList, List<Int32?> versionWidList,
            TokensProbability baseProbability, TokensProbability versionProbability)
        {
            //TODO Time consuming issue
            var score = -(float)Math.Log(1.0 / baseWidList.Count) * versionWidList.Count;
            foreach (var versionList in versionWidList)
            {
                var translationProbability = 0.0f;
                if (versionList != null)
                {
                    foreach (var baseList in baseWidList)
                    {
                        if (baseList != null)
                        {
                            //TODO: System.Console.Write("+");
                            translationProbability += CalculateTranslationScore(baseList, versionList);
                        }
                    }
                }

                translationProbability = Math.Max(translationProbability, MinimumTranslationProbability);
                score += -(float) Math.Log(translationProbability);
            }
            return score;
        }

        private float CalculateTranslationScore(Int32? baseWidList, Int32? versionWidList)
        {
            
            var score = 0.0f;
            var baseData = IbmModelAligner.GetBaseTranslationData(WordTranslationData, baseWidList.Value);
            var versionData = baseData?.GetVersionTranslationData(versionWidList.Value);
            if (versionData != null)
            {
                score += (float)versionData.Probability;
            }
            return score;
        }

        public TokensProbability CalculateProbabilities(SegmentTokens[] segmentList,
            Vocabulary vocabulary)
        {
            var tokensProb = GetTranslationProbabilities(segmentList, vocabulary);

            for (var i = 0; i < tokensProb.TokensProbabilityValues.Length; ++i)
            {
                var probability = tokensProb.TokensProbabilityValues[i] / tokensProb.AttributeValueOccurenceCount;
                tokensProb.TokensProbabilityValues[i] = probability;
            }
            tokensProb.Mean = 1.0f / tokensProb.AttributeValueOccurenceCount;

            return tokensProb;
        }

        private static TokensProbability GetTranslationProbabilities(SegmentTokens[] segmentList,
            Vocabulary vocabulary)
        {
            var transProb = new TokensProbability { TokensProbabilityValues = new float[vocabulary.TokensList.Count + 1] };
            foreach (var segment in segmentList)
            {
                foreach (var token in segment.Attributes)
                {
                    if (!vocabulary.TokenMap.ContainsKey(token.Name)) continue;

                    transProb.TokensProbabilityValues[vocabulary.TokenMap[token.Name][0]] += 1;
                    transProb.AttributeValueOccurenceCount++;
                    transProb.TotalAttributeValue += int.Parse(token.Value);
                }
            }

            return transProb;
        }
    }
}
