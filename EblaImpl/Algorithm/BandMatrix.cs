﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.

    This class was based on the following articles and 2006-2015 Jarek Lipski - 
    Maligna (v3.0.1-SNAPSHOT) source code (https://github.com/loomchild/maligna):

    [1] Peter F. Brown, Jennifer C. Lai, Robert L. Mercer (1991), 
    Aligning sentences in parallel corpora, Proceedings of the 29th annual meeting on Association for Computational Linguistics.
    [2] Jassem, K., & Lipski, J. (2008). A new tool for the bilingual text aligning at the sentence level. 
    Intelligent Information Systems, 279-286.
    [3] Moore, R. C. (2002, October). Fast and accurate sentence alignment of bilingual corpora. 
    In Conference of the Association for Machine Translation in the Americas (pp. 135-144). Springer Berlin Heidelberg.
 */
using Jama;

namespace EblaImpl.Algorithm
{
    public class BandMatrix
    {
        private float?[,] dataArray;

        private int _width;

        private int _heigth;

        private int _bandWidth;

        private int _bandRadius;

        private float _widthHeightRatio;

        private const int DefaultBandRadius = 20;

        #region Constructors

        public BandMatrix(int bandRadius)
        {
            _bandRadius = bandRadius;
        }

        public BandMatrix() : this(DefaultBandRadius)
        {

        }

        public BandMatrix(int width, int heigth, int bandRadius)
        {
            _width = width;
            _heigth = heigth;
            _bandRadius = bandRadius;
            _bandWidth = bandRadius * 2 + 1;
            dataArray = new float?[_bandWidth,_heigth];
            for (int i = 0; i < _bandWidth; i++)
            {
                for (int j = 0; j < _heigth; j++)
                {
                    dataArray[i,j] = null;
                }
            }
            _widthHeightRatio = (float)width / heigth;
        }

        #endregion Constructors

        public int GetEquivalentWidth()
        {
            return _width;
        }

        public int GetEquivalentHeight()
        {
            return _heigth;
        }

        public int GetMatrixSize()
        {
            return _bandWidth * _heigth - (int)(_bandRadius * _bandRadius * _widthHeightRatio);
        }

        public float? GetEquivalentValue(int x, int y)
        {
            var actualX = GetActualValue(x, y);
            if (actualX >= 0 && actualX < _bandWidth)
            {
                return dataArray[actualX,y];
            }
            return null;
        }

        public void SetEquivalentValue(int x, int y, float data)
        {
            var actualX = GetActualValue(x, y);
            if (actualX >= 0 && actualX < _bandWidth)
            {
                dataArray[actualX,y] = data;
            }
        }

        private int GetActualValue(int x, int y)
        {
            return x - GetDiagonalValue(y) + _bandRadius;
        }

        public int GetDiagonalValue(int y)
        {
            return (int)(y * _widthHeightRatio);
        }

    }
}

