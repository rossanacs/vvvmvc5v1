/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EblaAPI;
using HtmlAgilityPack;
using System.Xml;
using Jama;
using EblaImpl.Aligner;
#if _USE_MYSQL
using EblaDataReader = MySql.Data.MySqlClient.MySqlDataReader;
using EblaConnection = MySql.Data.MySqlClient.MySqlConnection;
#else
using System.Data.SQLite;
using EblaDataReader = System.Data.SQLite.SQLiteDataReader;
using EblaConnection = System.Data.SQLite.SQLiteConnection;
using EblaCommand = System.Data.SQLite.SQLiteCommand;
using EblaParameter = System.Data.SQLite.SQLiteParameter;
using EblaTransaction = System.Data.SQLite.SQLiteTransaction;
 

#endif

namespace EblaImpl
{
    public class Corpus : OpenableEblaItem, ICorpus
    {
        #region "ICorpus"

        static List<PredefinedSegmentAttribute> _builtinPredefinedSegmentAttributes = new List<PredefinedSegmentAttribute>()
        {
            new PredefinedSegmentAttribute() { AttributeType= PredefinedSegmentAttributeType.Text, ID = -1, Name = "label"},
            new PredefinedSegmentAttribute() { AttributeType= PredefinedSegmentAttributeType.Text, ID = -1, Name = "cb", ReadOnly=true} // emission attribute for ContentBehaviour
        };

        public PredefinedSegmentAttribute[] GetPredefinedSegmentAttributes(bool IncludeBuiltIn)
        {
            CheckOpen();
            CheckCanRead(_name);

            string sql = "SELECT ID, Name, AttributeType, Value, ApplyColouring, ColourCode, ShowInTOC FROM predefinedsegmentattributes LEFT OUTER JOIN predefinedsegmentattributevalues ON predefinedsegmentattributes.ID = predefinedsegmentattributevalues.PredefinedSegmentAttributeID WHERE CorpusID = " + _id.ToString() + " ORDER BY predefinedsegmentattributes.ID";

            List<PredefinedSegmentAttribute> l = ReadPrefinedSegAttribs(sql);
            if (IncludeBuiltIn)
                l.AddRange(_builtinPredefinedSegmentAttributes);

            return l.ToArray();
        }

        public void CreatePredefinedSegmentAttribute(PredefinedSegmentAttribute PredefinedSegmentAttribute)
        {
            CheckOpen();
            CheckCanWrite(_name);

            if (PredefinedSegmentAttribute.AttributeType != PredefinedSegmentAttributeType.List || PredefinedSegmentAttribute.Values == null)
                PredefinedSegmentAttribute.Values = new PredefinedSegmentAttributeValue[0];

            using (var txn = GetConnection().BeginTransaction())
            {
                string sql = "INSERT INTO predefinedsegmentattributes (Name, AttributeType, CorpusID, ShowInTOC) VALUES (@name, " + ((int)PredefinedSegmentAttribute.AttributeType).ToString() + ", " + _id.ToString() + ", @showintoc)";
                using (var cmd = GetNewCommand(sql, GetConnection()))
                {
                    cmd.Transaction = txn;
                    cmd.Parameters.AddWithValue("name", PredefinedSegmentAttribute.Name);
                    cmd.Parameters.AddWithValue("showintoc", PredefinedSegmentAttribute.ShowInTOC);

                    cmd.ExecuteNonQuery();

                    PredefinedSegmentAttribute.ID = EblaHelpers.GetLastInsertID(cmd);

                    sql = "INSERT INTO predefinedsegmentattributevalues (PredefinedSegmentAttributeID, Value, ApplyColouring, ColourCode) VALUES (" + PredefinedSegmentAttribute.ID + ", @value, @applycolouring, @colourcode)";
                    cmd.CommandText = sql;
                    cmd.Parameters.Clear();
                    var valueparm = cmd.Parameters.AddWithValue("value", string.Empty);
                    var colouringparm = cmd.Parameters.AddWithValue("applycolouring", false);
                    var colourcodeparm = cmd.Parameters.AddWithValue("colourcode", string.Empty);
                    //MySqlParameter showintocparm = cmd.Parameters.AddWithValue("showintoc", false);
                    //MySqlParameter toctextparm = cmd.Parameters.AddWithValue("toctext", false);
                    foreach (PredefinedSegmentAttributeValue v in PredefinedSegmentAttribute.Values)
                    {
                        valueparm.Value = v.Value;
                        colouringparm.Value = v.ApplyColouring;
                        colourcodeparm.Value = v.ColourCode;
                        //showintocparm.Value = v.ShowInTOC;
                        //toctextparm.Value = v.IsTOCText;
                        cmd.ExecuteNonQuery();
                    }

                }


                txn.Commit();
            }

        }


        public void DeletePredefinedSegmentAttribute(int ID)
        {
            CheckOpen();
            CheckCanWrite(_name);

            string sql = "DELETE FROM predefinedsegmentattributes WHERE ID = " + ID.ToString() + " AND CorpusID = " + _id;

            using (var cmd = GetNewCommand(sql, GetConnection()))
            {
                cmd.ExecuteNonQuery();
            }
        }

        public void UpdatePredefinedSegmentAttribute(PredefinedSegmentAttribute PredefinedSegmentAttribute)
        {
            CheckOpen();
            CheckCanWrite(_name);

            PredefinedSegmentAttribute current = GetPredefinedSegmentAttribute(PredefinedSegmentAttribute.ID);

            if (current == null)
                throw new Exception("PredefinedSegmentAttribute with ID " + PredefinedSegmentAttribute.ID.ToString() + " not found.");

            if (PredefinedSegmentAttribute.AttributeType != PredefinedSegmentAttributeType.List || PredefinedSegmentAttribute.Values == null)
                PredefinedSegmentAttribute.Values = new PredefinedSegmentAttributeValue[0];

            List<PredefinedSegmentAttributeValue> valuesToDelete = new List<PredefinedSegmentAttributeValue>();
            List<PredefinedSegmentAttributeValue> valuesToInsert = new List<PredefinedSegmentAttributeValue>();
            List<PredefinedSegmentAttributeValue> valuesToUpdate = new List<PredefinedSegmentAttributeValue>();

            foreach (var v in current.Values)
            {
                bool found = false;
                foreach (var v2 in PredefinedSegmentAttribute.Values)
                {
                    if (String.CompareOrdinal(v.Value, v2.Value) == 0)
                    {
                        found = true;
                        break;
                    }
                }
                if (!found)
                    valuesToDelete.Add(v);

            }

            foreach (var v in PredefinedSegmentAttribute.Values)
            {
                bool found = false;
                foreach (var v2 in current.Values)
                {
                    if (String.CompareOrdinal(v.Value, v2.Value) == 0)
                    {
                        found = true;
                        valuesToUpdate.Add(v);
                        break;
                    }
                }
                if (!found)
                    valuesToInsert.Add(v);
            }

            string sql = "UPDATE predefinedsegmentattributes SET Name = @name, ShowInTOC = @showintoc, AttributeType = " + ((int)PredefinedSegmentAttribute.AttributeType).ToString() + " WHERE ID = " + PredefinedSegmentAttribute.ID.ToString();
            using (var txn = GetConnection().BeginTransaction())
            {
                using (var cmd = GetNewCommand(sql, GetConnection()))
                {
                    cmd.Transaction = txn;
                    cmd.Parameters.AddWithValue("name", PredefinedSegmentAttribute.Name);
                    cmd.Parameters.AddWithValue("showintoc", PredefinedSegmentAttribute.ShowInTOC);

                    cmd.ExecuteNonQuery();


                    sql = "DELETE FROM predefinedsegmentattributevalues WHERE PredefinedSegmentAttributeID = " + PredefinedSegmentAttribute.ID.ToString() + " AND Value = @value";
                    cmd.Parameters.Clear();
                    var valueparm = cmd.Parameters.AddWithValue("value", string.Empty);
                    cmd.CommandText = sql;
                    foreach (var v in valuesToDelete)
                    {
                        valueparm.Value = v.Value;
                        cmd.ExecuteNonQuery();
                    }

                    sql = "INSERT INTO predefinedsegmentattributevalues (PredefinedSegmentAttributeID, Value, ApplyColouring, ColourCode) VALUES (" + PredefinedSegmentAttribute.ID + ", @value, @applycolouring, @colourcode)";
                    cmd.CommandText = sql;
                    var colouringparm = cmd.Parameters.AddWithValue("applycolouring", false);
                    var colourcodeparm = cmd.Parameters.AddWithValue("colourcode", string.Empty);
                    //MySqlParameter showintocparm = cmd.Parameters.AddWithValue("showintoc", false);
                    //MySqlParameter toctextparm = cmd.Parameters.AddWithValue("toctext", false);
                    foreach (var v in valuesToInsert)
                    {
                        valueparm.Value = v.Value;
                        colouringparm.Value = v.ApplyColouring;
                        colourcodeparm.Value = v.ColourCode;
                        //showintocparm.Value = v.ShowInTOC;
                        //toctextparm.Value = v.IsTOCText;
                        cmd.ExecuteNonQuery();
                    }

                    sql = "UPDATE predefinedsegmentattributevalues SET ApplyColouring = @applycolouring, ColourCode = @colourcode WHERE PredefinedSegmentAttributeID = " + PredefinedSegmentAttribute.ID.ToString() + " AND Value = @value";
                    cmd.CommandText = sql;
                    foreach (var v in valuesToUpdate)
                    {
                        valueparm.Value = v.Value;
                        colouringparm.Value = v.ApplyColouring;
                        colourcodeparm.Value = v.ColourCode;
                        //showintocparm.Value = v.ShowInTOC;
                        //toctextparm.Value = v.IsTOCText;
                        cmd.ExecuteNonQuery();

                    }
                }

                txn.Commit();
            }

        }

        public PredefinedSegmentAttribute GetPredefinedSegmentAttribute(int ID)
        {
            CheckOpen();
            CheckCanRead(_name);

            string sql = "SELECT ID, Name, AttributeType, Value, ApplyColouring, ColourCode, ShowInTOC FROM predefinedsegmentattributes LEFT OUTER JOIN predefinedsegmentattributevalues ON predefinedsegmentattributes.ID = predefinedsegmentattributevalues.PredefinedSegmentAttributeID WHERE CorpusID = " + _id.ToString() + " AND predefinedsegmentattributes.ID = " + ID.ToString();

            List<PredefinedSegmentAttribute> l = ReadPrefinedSegAttribs(sql);


            if (l.Count == 0)
                return null;

            System.Diagnostics.Debug.Assert(l.Count == 1);
            return l[0];

        }

        public SegmentDefinition[] Search(string BaseTextFilter, string VersionFilter, SegmentAttribute[] BaseTextAttributeFilters)
        {
            if (string.IsNullOrWhiteSpace(BaseTextFilter))
                throw new Exception("A base text filter must be specified for searching.");

            Document baseText = new Document(ConfigProvider);
            baseText.OpenWithoutPrivilegeCheck(_name, string.Empty, true, GetConnection());

            // First make a list of all base text occurrences

            throw new NotImplementedException();
        }

        public bool Open(string CorpusName, string Username, string Password)
        {
            if (!base.Open(Username, Password))
                return false;

            OpenWithoutPrivilegeCheck(CorpusName, false, null);
            CheckCanRead(CorpusName);
            return true;
        }


        internal void OpenWithoutPrivilegeCheck(string Name, bool makeAdmin, EblaConnection cn)
        {
            if (makeAdmin)
                base.MakeAdmin();

            if (cn != null)
                SetConnection(cn);

            using (var cmd = GetNewCommand("SELECT ID, Description FROM corpora WHERE Name = @name", GetConnection()))
            {
                cmd.Parameters.AddWithValue("name", Name);
                using (var dr = cmd.ExecuteReader())
                {
                    if (!dr.Read())
                        throw new Exception("Corpus not found: " + Name);

                    _id = dr.GetInt32(dr.GetOrdinal("ID"));
                    int descOrdinal = dr.GetOrdinal("Description");
                    if (!dr.IsDBNull(descOrdinal))
                        _description = dr.GetString(descOrdinal);

                }

                ///Base document has null name.
                cmd.CommandText = "SELECT ID FROM documents WHERE CorpusID = " + _id.ToString() + " AND NAME IS NULL";

                using (var dr = cmd.ExecuteReader())
                {
                    if (!dr.Read())
                        throw new Exception("Corpus base text not found: " + Name);

                    _basetextid = dr.GetInt32(dr.GetOrdinal("ID"));

                }

            }

            _open = true;
            _name = Name;
            
        }

        public HtmlErrors UploadBaseText(string BaseTextHtml)
        {
            return UploadOrUpdateBaseText(BaseTextHtml, false, new Dictionary<int,int>());
        }

        public HtmlErrors UploadBaseText(string BaseTextHtml, Dictionary<int, int> IDChanges)
        {
            return UploadOrUpdateBaseText(BaseTextHtml, false, IDChanges);
        }

        protected HtmlErrors UploadOrUpdateBaseText(string BaseTextHtml, bool updating, Dictionary<int, int> IDChanges)
        {
            CheckOpen();
            CheckCanWrite(_name);

            return UploadDocument(_basetextid, BaseTextHtml, updating, null, IDChanges);
        }

        public HtmlErrors CreateUploadTextAndFiles(UploadedFile file)
        {
            var errors = file.UploadType == "base"
                ? UploadOrUpdateBaseText(file.FileHtml, false, new Dictionary<int, int>())
                : CreateUploadVersion(file);

            return errors;
        }

        private HtmlErrors CreateUploadVersion(UploadedFile file)
        {
            if (string.IsNullOrEmpty(file?.NameIfVersion) || string.IsNullOrEmpty(file.FileHtml))
                throw new Exception("File is empty.");

            CreateVersion(file.NameIfVersion, file.FileMetadata);

            var errors = UploadVersion(file.NameIfVersion, file.FileHtml);

            return errors;

        }

        public string CreateCorpusAutoSegmentation(string corpusName, SegmentationTypes type)
        {
            if (string.IsNullOrEmpty(corpusName))
                throw new Exception(
                    "An error occurred while trying to create segmentation to the documents of corpus '" +
                    corpusName + ".");

            CheckOpen();

            var tag = GetCorrespondentTag(type);
            
            CreateDocumentSegmentation(corpusName, null, tag);

            var versionList = GetVersionList();

            foreach (var versionName in versionList)
            {
                CreateDocumentSegmentation(corpusName, versionName, tag);
            }
            return "Success.";
        }

        private string GetCorrespondentTag(SegmentationTypes type)
        {
            string tag = "p";
            switch (type)
            {
                case SegmentationTypes.Paragraph:
                    tag = "p";
                    break;
                default:
                    return tag;
            }
            return tag;
        }

        private void CreateDocumentSegmentation(string corpusName, string documentName, string tag)
        {
            var doc = new Document(ConfigProvider);
            doc.OpenWithoutPrivilegeCheck(corpusName, documentName, true, GetConnection());
            doc.AutoSegmentationAroundTag(tag);
        }

        public string CreateCorpusAutoAlignments(string corpusName, string userName, string password, AlignmentAlgorithms algorithm)
        {
            if (string.IsNullOrEmpty(corpusName))
                throw new Exception(
                    "An error occurred while trying to create auto-alignment of corpus '" +
                    corpusName + ".");

            CheckOpen();

            var versions = GetVersionList();
            if (versions.Length == 0)
                throw new Exception("No versions were found for the selected corpus.");

            foreach (var versionName in versions)
            {
                var alignments = new List<BestAlignment>();
                if (!string.IsNullOrEmpty(versionName))
                {
                    var aligner = new MalignaTranslationAligner(ConfigProvider);

                    //TODO: Add a combo, so user can select the best alignment.
                    switch (algorithm)
                    {
                       // case AlignmentAlgorithms.Moore:
                       //     alignments = aligner.ApplyAutoAlignmentAndCreateEblaAlignment(corpusName, versionName, userName,
                       //password);
                       //     break;
                        case AlignmentAlgorithms.GaleAndChurch:
                            alignments = aligner.ApplyAutoAlignmentAndCreateEblaAlignmentWithGaleAndChurchMacro(corpusName, versionName, userName,
                       password);
                            break;
                        default:
                            alignments = aligner.ApplyAutoAlignmentAndCreateEblaAlignmentWithGaleAndChurchMacro(corpusName, versionName, userName,
                       password);
                            break;
                    }
                }

                if (alignments.Count == 0)
                    throw new Exception("The auto alignment was not created to version " + versionName + ".");
            }

            return "Success.";
        }

        public string GetDescription()
        {
            return _description;
        }

        public void SetDescription(string Description)
        {
            CheckOpen();
            CheckCanWrite(_name);


            using (var cmd = GetNewCommand("UPDATE corpora SET Description = @desc WHERE id = " + _id, GetConnection()))
            {
                cmd.Parameters.AddWithValue("desc", Description);

                cmd.ExecuteNonQuery();

                _description = Description;
            }
        }


        public void CreateVersion(string Name, DocumentMetadata meta)
        {
            CheckOpen();
            CheckCanWrite(_name);

            EblaHelpers.IsVersionNameValid(Name, true);

            using (var cmd = GetNewCommand("INSERT INTO documents (CorpusID, Name, Description, Information, AuthorTranslator, Genre, CopyrightInfo, ReferenceDate, LanguageCode) VALUES (@corpusid, @name, @description, @information, @authortranslator, @genre, @copyrightinfo, @referencedate, @langcode)", GetConnection()))
            {
                cmd.Parameters.AddWithValue("corpusid", _id);
                cmd.Parameters.AddWithValue("name", Name);
                cmd.Parameters.AddWithValue("description", meta.Description);
                cmd.Parameters.AddWithValue("information", meta.Information);
                cmd.Parameters.AddWithValue("authortranslator", meta.AuthorTranslator);
                cmd.Parameters.AddWithValue("genre", meta.Genre);
                cmd.Parameters.AddWithValue("copyrightinfo", meta.CopyrightInfo);
                if (meta.ReferenceDate.HasValue)
                    cmd.Parameters.AddWithValue("referencedate", meta.ReferenceDate.Value);
                else
                    cmd.Parameters.AddWithValue("referencedate", DBNull.Value);

                cmd.Parameters.AddWithValue("langcode", meta.LanguageCode);

                cmd.ExecuteNonQuery();
            }

        }

        public void DeleteVersion(string Name)
        {
            CheckOpen();
            CheckCanWrite(_name);

            EblaHelpers.IsVersionNameValid(Name, true);

            using (var cmd = GetNewCommand("DELETE FROM documents WHERE CorpusID = @corpusid AND Name = @name", GetConnection()))
            {
                cmd.Parameters.AddWithValue("corpusid", _id);
                cmd.Parameters.AddWithValue("name", Name);

                cmd.ExecuteNonQuery();
            }

        }

        public void RenameVersion(string OldName, string NewName)
        {
            CheckOpen();
            CheckCanWrite(_name);

            EblaHelpers.IsVersionNameValid(NewName, true);

            using (var cmd = GetNewCommand("UPDATE documents SET Name = @newname WHERE Name = @oldname AND CorpusID = @corpusid", GetConnection()))
            {
                cmd.Parameters.AddWithValue("corpusid", _id);
                cmd.Parameters.AddWithValue("oldname", OldName);
                cmd.Parameters.AddWithValue("newname", NewName);

                cmd.ExecuteNonQuery();

            }

        }

        public HtmlErrors UpdateVersion(string Name, string VersionHtml)
        {
            return UploadOrUpdateVersion(Name, VersionHtml, true, new Dictionary<int,int>());

        }

        public HtmlErrors UploadVersion(string Name, string VersionHtml, Dictionary<int, int> docIDChanges)
        {
            return UploadOrUpdateVersion(Name, VersionHtml, false, docIDChanges);

        }

        public HtmlErrors UpdateBaseText(string VersionHtml)
        {
            return UploadOrUpdateBaseText(VersionHtml, true, new Dictionary<int,int>());

        }

        public HtmlErrors UploadVersion(string Name, string VersionHtml)
        {
            return UploadOrUpdateVersion(Name, VersionHtml, false, new Dictionary<int,int>());
        }

        public HtmlErrors UploadOrUpdateVersion(string Name, string VersionHtml, bool updating, Dictionary<int, int> IDChanges)
        {
            CheckOpen();
            CheckCanWrite(_name);

            EblaHelpers.IsVersionNameValid(Name, true);

            int versionid;
            using (var cmd = GetNewCommand("SELECT ID FROM documents WHERE Name = @name AND CorpusID = @corpusid", GetConnection()))
            {
                cmd.Parameters.AddWithValue("corpusid", _id);
                cmd.Parameters.AddWithValue("name", Name);

                using (var dr = cmd.ExecuteReader())
                {
                    if (!dr.Read())
                        throw new Exception("Version not found: " + Name);

                    versionid = dr.GetInt32(dr.GetOrdinal("ID"));

                }


            }

            return UploadDocument(versionid, VersionHtml, updating, Name, IDChanges);


        }

        public string[] GetVersionList()
        {
            CheckOpen();
            CheckCanRead(_name);

            List<string> versions = new List<string>();

            using (var cmd = GetNewCommand("SELECT Name FROM documents WHERE Name IS NOT NULL AND CorpusID = @corpusid", GetConnection()))
            {
                cmd.Parameters.AddWithValue("corpusid", _id);

                using (var dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        versions.Add(dr.GetString(dr.GetOrdinal("Name")));
                    }
                }

            }

            return versions.ToArray();
        }


        //private static Dictionary<string, int> _docToIdMap;

        internal static Dictionary<string, int> GetDocToIdMap(EblaConnection conn, int corpusid)
        {


            //if (_docToIdMap != null)
                //return _docToIdMap;

            var _docToIdMap = new Dictionary<string, int>();
            

            using (var cmd = GetNewCommand("SELECT Name, Id FROM documents WHERE CorpusID = @corpusid", conn))
            {
                cmd.Parameters.AddWithValue("corpusid", corpusid);

                using (var dr = cmd.ExecuteReader())
                {
                    int nameord = dr.GetOrdinal("Name");
                    int idord = dr.GetOrdinal("Id");

                    while (dr.Read())
                    {
                        if (dr.IsDBNull(nameord)) // base text
                            _docToIdMap.Add(string.Empty, dr.GetInt32(idord));
                        else
                            _docToIdMap.Add(dr.GetString(nameord), dr.GetInt32(idord));
                    }
                }

            }

            return _docToIdMap;
        }

        public void StartSegmentVariationCalculation(VariationMetricTypes metricType)
        {
            throw new NotSupportedException();

            //CheckOpen();
            //CheckCanWrite(_name);
            //_metricType = metricType;

            //// Switch to background connection mode - hold/share a cxn
            //SetConnection(EblaHelpers.GetConnection(ConfigProvider));
            //_docCache = new Dictionary<string, Document>();
            //_baseTextDoc = new Document(ConfigProvider);
            //_baseTextDoc.OpenWithoutPrivilegeCheck(_name, null, true, GetConnection());

            //_segs = _baseTextDoc.FindSegmentDefinitions(0, _baseTextDoc.Length(), false, false, null);

            //// Delete any old calculated values from the corpus
            //string sql = "DELETE FROM segmentattributes WHERE Name LIKE @eddyattribname OR Name LIKE @vivattribname AND SegmentDefinitionID IN " +
            //    "(SELECT segmentdefinitions.ID FROM segmentdefinitions INNER JOIN documents ON segmentdefinitions.DocumentID = documents.ID WHERE documents.CorpusID = " + _id.ToString() + ")";

            //using (var cmd = new MySqlCommand(sql, GetConnection()))
            //{
            //    cmd.Parameters.AddWithValue("eddyattribname", EddyAttribName(metricType));
            //    cmd.Parameters.AddWithValue("vivattribname", VivAttribName(metricType));

            //    cmd.ExecuteNonQuery();
            //}


        }

        public SegmentVariationData RetrieveSegmentVariationData(int BaseTextSegmentID, VariationMetricTypes metricType)
        {
            throw new NotSupportedException();

            //var result = new SegmentVariationData();
            //result.BaseTextSegmentID = BaseTextSegmentID;

            //result.VersionNames = GetVersionList();
            //result.EddyValues = new double[result.VersionNames.Length];

            //string eddyAttribName = EddyAttribName(metricType);

            //for (int i = 0; i < result.VersionNames.Length; i++)
            //{
            //    string version = result.VersionNames[i];
            //    result.EddyValues[i] = -1;

            //    int docid = GetDocID(version);

            //    var alignmentSet = new AlignmentSet(ConfigProvider);
            //    alignmentSet.OpenWithoutPrivilegeCheck(_name, version, true, null);

            //    var alignment = alignmentSet.FindAlignment(BaseTextSegmentID, true);

            //    if (alignment != null)
            //    {

            //        foreach (int segid in alignment.SegmentIDsInVersion)
            //        {
            //            SegmentDefinition seg = Document.GetSegmentDefinition(segid, docid, GetConnection(), true);

            //            bool gotEddyAttrib = false;
            //            foreach (var a in seg.Attributes)
            //            {
            //                if (string.Compare(a.Name, eddyAttribName) == 0)
            //                {
            //                    double v = -1;
            //                    bool b = double.TryParse(a.Value, out v);
            //                    result.EddyValues[i] = v;
            //                    gotEddyAttrib = true;
            //                    break;
            //                }

            //            }
            //            if (gotEddyAttrib)
            //                break;
                        
            //        } // foreach (version seg in alignment)


            //    }
            //}

            //return result;
        }

        class VariationEntry
        {
            public double eddyVal;
            public int versionSegID;
        }

        public MultipleSegmentVariationData RetrieveMultipleSegmentVariationData(SegmentAttribute[] BaseTextAttributeFilters, List<int> BaseTextSegmentIds, VariationMetricTypes metricType, List<string> VariationVersionList, bool getSVDs)
        {
            var result = new MultipleSegmentVariationData();
            var vernames = VariationVersionList; // new List<string>(GetVersionList());
            if (vernames == null || vernames.Count == 0)
                vernames = new List<string>(GetVersionList());
            //vernames.Sort();

            result.VersionNames = vernames.ToArray();

            //result.VersionNames = new string[] {"Bab and Levy", "Zaimoglu" };
            result.EddyValues = new double[result.VersionNames.Length][];

            var doccache = Document.BatchPreloadSegmentDefinitionTokens(ConfigProvider, GetConnection(), _name, _id, vernames);


            Document baseText = doccache[string.Empty]; // new Document(ConfigProvider);
            string eddyAttribName = EddyAttribName(metricType);

            //baseText.OpenWithoutPrivilegeCheck(_name, null, true, null);

            if (BaseTextSegmentIds == null)
            {
                BaseTextSegmentIds = new List<int>();

                // Get a list of base text segments matching the filter conditions
                var cbFilter = new List<SegmentContentBehaviour>() { SegmentContentBehaviour.normal };

                var segdefs = baseText.FindSegmentDefinitions(0, baseText.Length(), false, false, BaseTextAttributeFilters, cbFilter.ToArray());

                // Sort them in order of position in the text
                var sortedSegDefs = new List<SegmentDefinition>(segdefs);
                sortedSegDefs.Sort((a, b) => (a.StartPosition.CompareTo(b.StartPosition)));
                segdefs = sortedSegDefs.ToArray();

                foreach (var s in segdefs)
                    if (s.Length > 0)
                        BaseTextSegmentIds.Add(s.ID);
            }
            else
            {
                // check segs exist
                var temp = new List<int>();
                foreach (var id in BaseTextSegmentIds)
                    if (baseText.GetSegmentDefinition(id) != null)
                        temp.Add(id);
                BaseTextSegmentIds = temp;

            }
                  

            //// Now get the eddy values for all version segments aligned to them.
            //// Note - this reads all alignments. The values in BaseTextAttributeFilters (if any) could be incorporated in the SQL. But it's pretty fast like this.
            //string sql = "SELECT BaseTextSegmentID, VersionSegmentID, Value, documents.Name FROM segmentdefinitions " +
            //            "inner join segmentalignments on segmentalignments.BaseTextSegmentID = segmentdefinitions.id " +
            //            "inner join segmentdefinitions as versionsegmentdefinitions on versionsegmentdefinitions.ID = VersionSegmentID " +
            //            "inner join segmentattributes on segmentattributes.SegmentDefinitionID=versionsegmentid " +
            //            "inner join documents on documents.ID = versionsegmentdefinitions.DocumentID " +
            //            "where segmentdefinitions.DocumentID = " + _basetextid.ToString()  +
            //            " and segmentattributes.Name = '" + eddyAttribName + "'";

            var versionsMap = new Dictionary<string, Dictionary<int, VariationEntry>>();
            foreach (string s in result.VersionNames)
                versionsMap.Add(s, new Dictionary<int, VariationEntry>());

            List<double> vivValues = new List<double>();
            List<int> baseTextTotalTokensList = new List<int>();
            List<List<SVDPt>> svdPtLists = new List<List<SVDPt>>();
            if (true)
            {
                int segcount = 0;
                //var doccache = new Dictionary<string, Document>();

                var alsetCache = AlignmentSet.BatchPreload(ConfigProvider, GetConnection(), _name, _id, vernames);
                
                //var alsetCache = new Dictionary<string, AlignmentSet>();

               

                foreach (var id  in BaseTextSegmentIds)
                {
                    segcount++;
                    //if (segdef.Length == 0)
                    //    continue;

                    int baseTextTotalTokens = 0;
                    List<SVDPt> svdPts = null;
                    if (getSVDs)
                        svdPts = new List<SVDPt>();
                    var stats = CalculateSegmentVariation(baseText, id , doccache, alsetCache, GetConnection(), metricType, vernames, ConfigProvider, _name, false, out baseTextTotalTokens, svdPts);

                    System.Diagnostics.Debug.WriteLine("seg " + segcount.ToString() + " of " + BaseTextSegmentIds.Count.ToString());

                    if (stats != null) // check e.g. for empty base text segment
                    {
                        foreach (var v in stats.VersionSegmentVariations)
                        {
                            if (v?.VersionSegmentIDs?.Length != 1) continue;
                            var eddyMap = versionsMap[v.VersionName];
                            var ve = new VariationEntry();
                            ve.versionSegID = v.VersionSegmentIDs[0];
                            ve.eddyVal = v.EddyValue;

                            eddyMap.Add(id, ve);
                        }
                        vivValues.Add(stats.VivValue);
                    } // stats != null
                    else
                        vivValues.Add(0);

                    baseTextTotalTokensList.Add(baseTextTotalTokens);
                    svdPtLists.Add(svdPts);
                }
            }
            else
            {
                //using (MySqlCommand cmd = new MySqlCommand(sql, GetConnection()))
                //{
                //    using (var dr = cmd.ExecuteReader())
                //    {
                //        while (dr.Read())
                //        {
                //            int baseTextSegmentID = dr.GetInt32("BaseTextSegmentID");
                //            int versionSegmentID = dr.GetInt32("VersionSegmentID");
                //            string temp = dr.GetString("Value");
                //            string version = dr.GetString("Name");

                //            double eddyVal = 0;
                //            double.TryParse(temp, out eddyVal); // TODO - report any error

                //            var eddyMap = versionsMap[version];
                //            // Seen this segment ID already?
                //            if (eddyMap.ContainsKey(baseTextSegmentID))
                //            {
                //                // Yes - was it with the same versionSegID?
                //                if (eddyMap[baseTextSegmentID].versionSegID == versionSegmentID)
                //                    // Yes - this must be a many-to-one alignment. We can't represent
                //                    // those yet. Mark as not usable
                //                    eddyMap[baseTextSegmentID].eddyVal = -1;

                //                // Otherwise, it's a 1-to-many, and the eddyVal will be the same,
                //                // so just ignore it
                //            }
                //            else
                //            {
                //                var ve = new VariationEntry();
                //                ve.versionSegID = versionSegmentID;
                //                ve.eddyVal = eddyVal;
                //                eddyMap[baseTextSegmentID] = ve;
                //            }
                //        }
                //    }


                //}

            }


            result.BaseTextSegmentIDs = BaseTextSegmentIds.ToArray();// new int[BaseTextSegmentIds.Count];

            //for (int i = 0; i < BaseTextSegmentIds.Count; i++)
            //    result.BaseTextSegmentIDs[i] = BaseTextSegmentIds[i];

            result.VivValues = vivValues.ToArray();
            result.BaseTextTotalTokenCounts = baseTextTotalTokensList.ToArray();
            result.SVDs = svdPtLists.ToArray();

            for (int i = 0; i < result.VersionNames.Length; i++)
            {
                string version = result.VersionNames[i];

                var eddyMap = versionsMap[version];
                result.EddyValues[i] = new double[BaseTextSegmentIds.Count];

                for (int j = 0; j < BaseTextSegmentIds.Count; j++)
                {
                    result.EddyValues[i][j] = -1;
                    if (eddyMap.ContainsKey(BaseTextSegmentIds[j]))
                    {
                        if (eddyMap[BaseTextSegmentIds[j]].eddyVal != -1)
                            result.EddyValues[i][j] = eddyMap[BaseTextSegmentIds[j]].eddyVal;
                    }
                    
                }

            }

            return result;
        }

        public SegmentVariation RetrieveSegmentVariation(int BaseTextSegmentID, VariationMetricTypes metricType)
        {
            Document baseTextDoc = new Document(ConfigProvider);
            baseTextDoc.OpenWithoutPrivilegeCheck(_name, null, true, null);
            SegmentDefinition d = baseTextDoc.GetSegmentDefinition(BaseTextSegmentID);

            if (d.Length == 0)
                //throw new Exception("Unable to calculate variation for null alignments.");
                return null;

            return RetrieveSegmentVariation(baseTextDoc, d, metricType);

        }

        public double RetrieveAverageVariation(string VersionName, VariationMetricTypes metricType)
        {
            int id = GetDocID(VersionName);

            string sql = "select avg(segmentattributes.value - 0.0) from segmentattributes " +
                    " inner join segmentdefinitions on segmentattributes.segmentdefinitionid = segmentdefinitions.id " +
                    " where segmentdefinitions.documentid = " + id.ToString() +
                    " and segmentattributes.name = @attribname";

            using (var cmd = GetNewCommand(sql, GetConnection()))
            {
                cmd.Parameters.AddWithValue("attribname", EddyAttribName(metricType));

                object o = cmd.ExecuteScalar();
                if (o == null)
                    return 0;

                if (o == DBNull.Value)
                    return 0;

                double d = 0;
                double.TryParse(o.ToString(), out d);
                return d;
            }


        }

        public SegmentVariation CalculateSegmentVariation(int BaseTextSegmentID, VariationMetricTypes metricType, List<string> VariationVersionList)
        {
            Document baseTextDoc = new Document(ConfigProvider);
            baseTextDoc.OpenWithoutPrivilegeCheck(_name, null, true, null);
            SegmentDefinition d = baseTextDoc.GetSegmentDefinition(BaseTextSegmentID);

            if (d.Length == 0)
                throw new Exception("Unable to calculate variation for null alignments.");

            if (VariationVersionList == null || VariationVersionList.Count == 0)
                VariationVersionList = new List<string>(GetVersionList());

            int baseTextTotalTokens = 0;
            
            return CalculateSegmentVariation(baseTextDoc, d.ID, null, null, GetConnection(), metricType, VariationVersionList, ConfigProvider, _name, true, out baseTextTotalTokens, null);
        }


        public bool ContinueSegmentVariationCalculation(out int progress)
        {
            throw new NotSupportedException();

        }
        
        #endregion

        string _name = string.Empty;

        /// <summary>
        /// Corpus id. 
        /// Note: Variable instantiated by Open method.
        /// </summary>
        private int _id;

        /// <summary>
        /// Corpus description.
        /// Note: Variable instantiated by Open method.
        /// </summary>
        private string _description = string.Empty;

        /// <summary>
        /// Base document id. 
        /// Note: Variable instantiated by Open method.
        /// Note: Base document has null name.
        /// </summary>
        private int _basetextid;


        public Corpus(System.Collections.Specialized.NameValueCollection configProvider)
            : base(configProvider)
        {
        }

        internal int ID()
        {
            CheckOpen();
            return _id;
        }


        private List<PredefinedSegmentAttribute> ReadPrefinedSegAttribs(string sql)
        {
            List<PredefinedSegmentAttribute> segAttribs = new List<PredefinedSegmentAttribute>();

            using (var cmd = GetNewCommand(sql, GetConnection()))
            {
                using (var dr = cmd.ExecuteReader())
                {
                   
                    int idOrdinal = dr.GetOrdinal("ID");
                    int nameOrdinal = dr.GetOrdinal("Name");
                    int typeOrdinal = dr.GetOrdinal("AttributeType");
                    int valueOrdinal = dr.GetOrdinal("Value");
                    int applyColouringOrdinal = dr.GetOrdinal("ApplyColouring");
                    int colourCodeOrdinal = dr.GetOrdinal("ColourCode");
                    int showInTOCOrdinal = dr.GetOrdinal("ShowInTOC");
                    //int tocTextOrdinal = dr.GetOrdinal("TOCText");

                    PredefinedSegmentAttribute segAttr = null;

                    while (dr.Read())
                    {
                        int id = dr.GetInt32(idOrdinal);

                        if (segAttr != null)
                        {
                            if (id == segAttr.ID)
                            {
                                // Got another attribute for the same seg
                                AppendValueFromReader(segAttr, dr, valueOrdinal, applyColouringOrdinal, colourCodeOrdinal);//, showInTOCOrdinal, tocTextOrdinal);
                                //Array.Resize(ref segDef.Attributes, segDef.Attributes.Length + 1);
                                //segDef.Attributes[segDef.Attributes.Length - 1] = AttribFromReader(dr, nameOrdinal, valueOrdinal);
                                continue;
                            }
                        }

                        segAttr = new PredefinedSegmentAttribute();
                        segAttr.Values = new PredefinedSegmentAttributeValue[0];
                        segAttr.Name = dr.GetString(nameOrdinal);
                        segAttr.AttributeType = (PredefinedSegmentAttributeType)(dr.GetInt32(typeOrdinal));
                        segAttr.ShowInTOC = dr.IsDBNull(showInTOCOrdinal) ? false : dr.GetBoolean(showInTOCOrdinal);
                        segAttr.ID = id;
                        if (!dr.IsDBNull(valueOrdinal))
                            AppendValueFromReader(segAttr, dr, valueOrdinal, applyColouringOrdinal, colourCodeOrdinal); //, showInTOCOrdinal), tocTextOrdinal);

                        segAttribs.Add(segAttr);
                    }

                }

            }

            return segAttribs;

        }

        private void AppendValueFromReader(PredefinedSegmentAttribute segAttr, EblaDataReader dr, int valueOrdinal, int applyColouringOrdinal, int colourCodeOrdinal)//, int showInTOCOrdinal, int tocTextOrdinal)
        {
            

            string value = dr.GetString(valueOrdinal);

            PredefinedSegmentAttributeValue val = new PredefinedSegmentAttributeValue();
            val.Value = value;
            val.ApplyColouring = dr.IsDBNull(applyColouringOrdinal) ? false :  dr.GetBoolean(applyColouringOrdinal);
            //val.ShowInTOC = dr.IsDBNull(showInTOCOrdinal) ? false : dr.GetBoolean(showInTOCOrdinal);
            //val.IsTOCText = dr.IsDBNull(tocTextOrdinal) ? false : dr.GetBoolean(tocTextOrdinal);
            val.ColourCode = dr.IsDBNull(colourCodeOrdinal) ? string.Empty : dr.GetString(colourCodeOrdinal);
                 

            Array.Resize(ref segAttr.Values, segAttr.Values.Length + 1);
            segAttr.Values[segAttr.Values.Length - 1] = val; // AttribFromReader(dr, nameOrdinal, valueOrdinal);

            //return attrib;
        }

        internal const string _EddyAttribPrefix = ":eddy-";

        internal static string EddyAttribName(VariationMetricTypes metricType)
        {
            string s = _EddyAttribPrefix;
            switch (metricType)
            {
                case EblaAPI.VariationMetricTypes.metricA:
                    return s + "a";
                case EblaAPI.VariationMetricTypes.metricB:
                    return s + "b";
                case EblaAPI.VariationMetricTypes.metricC:
                    return s + "c";
                case EblaAPI.VariationMetricTypes.metricD:
                    return s + "d";
                case EblaAPI.VariationMetricTypes.metricE:
                    return s + "e";
            }
            throw new Exception("Unknown VariationMetricTypes: " + metricType.ToString());
        }

        internal const string _VivAttribPrefix = ":viv-";

        internal static string VivAttribName(VariationMetricTypes metricType)
        {
            string s = _VivAttribPrefix;
            switch (metricType)
            {
                case EblaAPI.VariationMetricTypes.metricA:
                    return s + "a";
                case EblaAPI.VariationMetricTypes.metricB:
                    return s + "b";
                case EblaAPI.VariationMetricTypes.metricC:
                    return s + "c";
                case EblaAPI.VariationMetricTypes.metricD:
                    return s + "d";
                case EblaAPI.VariationMetricTypes.metricE:
                    return s + "e";
            }
            throw new Exception("Unknown VariationMetricTypes: " + metricType.ToString());
        }

        

        internal static string VersionNamesSelectionToKey(List<string> VariationVersionList, EblaConnection conn, int corpusid)
        {
            throw new NotImplementedException();
        }

        // member vars used during background viv/eddy calculation
        int _segCalcCount = 0;
        SegmentDefinition[] _segs = null;
        Dictionary<string, Document> _docCache = null;
        Document _baseTextDoc = null;
        VariationMetricTypes _metricType;

        internal static void AddAttribute(SegmentDefinition sd, string name, string value)
        {
            List<SegmentAttribute> attribs = new List<EblaAPI.SegmentAttribute>(sd.Attributes);
            attribs.Add(new SegmentAttribute() { Name = name, Value = value });
            sd.Attributes = attribs.ToArray();

        }

        private class VariationCalcInfo
        {
            public int AlignmentID;
            public string versionName;
            //public List<int> segIDs;

        }

        internal int GetDocID(string nameIfVersion)
        {
            using (var cmd = GetNewCommand("", GetConnection()))
            {
                string sql = "SELECT ID FROM documents WHERE CorpusID = " + _id.ToString();
                if (string.IsNullOrEmpty(nameIfVersion))
                {
                    sql += " AND NAME IS NULL";
                }
                else
                {
                    sql += " AND NAME = @name";
                    cmd.Parameters.AddWithValue("name", nameIfVersion);
                }

                cmd.CommandText = sql;

                object o = cmd.ExecuteScalar();
                if (o != null)
                    if (o != DBNull.Value)
                        return (int)o;

                throw new Exception("Unable to retrieve ID for document with name: " + nameIfVersion);
            }


 
        }

        private SegmentVariation RetrieveSegmentVariation(Document baseTextDoc, SegmentDefinition d, VariationMetricTypes metricType)
        {
            SegmentVariation sv = new EblaAPI.SegmentVariation();
            sv.BaseTextContent = baseTextDoc.GetDocumentContentText(d.StartPosition, d.Length);
            sv.BaseTextSegmentID = d.ID;
            

            string[] versions = GetVersionList();
            Dictionary<string, VersionSegmentVariation> variations = new Dictionary<string, EblaAPI.VersionSegmentVariation>();

            string eddyAttribName = ":eddy-a";
            if (metricType == VariationMetricTypes.metricB)
                eddyAttribName = ":eddy-b";

            foreach (string version in versions)
            {
                var alignmentSet = new AlignmentSet(ConfigProvider);
                alignmentSet.OpenWithoutPrivilegeCheck(_name, version, true, null);


                VersionSegmentVariation e = new VersionSegmentVariation();
                e.VersionName = version;
                e.VersionText = string.Empty;
                e.EddyValue = 0;

                var alignment = alignmentSet.FindAlignment(d.ID, true);

                if (alignment != null)
                {
                    Document doc = new Document(ConfigProvider);
                    doc.OpenWithoutPrivilegeCheck(_name, version, true, null);
                    int docid = GetDocID(version);

                    bool gotEddyAttrib = false;
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    foreach (int segid in alignment.SegmentIDsInVersion)
                    {
                        SegmentDefinition seg = Document.GetSegmentDefinition(segid, docid, GetConnection(), true );
                        if (!gotEddyAttrib)
                        {
                            foreach (var a in seg.Attributes)
                            {
                                if (string.Compare(a.Name, eddyAttribName) == 0)
                                {
                                    double v = 0;
                                    bool b = double.TryParse(a.Value, out v);
                                    e.EddyValue = v;
                                    gotEddyAttrib = true;
                                    break;
                                }

                            }

                            if (sb.Length > 0)
                                sb.Append(" ");

                            sb.Append(doc.GetDocumentContentText(seg.StartPosition, seg.Length));

                        }
                    }

                    e.VersionText = sb.ToString();
                    variations[version] = e;

                }
                

            }

            List<VersionSegmentVariation> vsvs = new List<EblaAPI.VersionSegmentVariation>();
            foreach (string s in variations.Keys)
            {
                vsvs.Add(variations[s]);
            }
            sv.VersionSegmentVariations = vsvs.ToArray();
            return sv;
            
        }

        static internal SegmentVariation CalculateSegmentVariation(Document baseTextDoc, int segDefId, Dictionary<string, Document> docCache, Dictionary<string, AlignmentSet> alsetCache, EblaConnection cn, VariationMetricTypes metricType, List<string> versionsSelected, System.Collections.Specialized.NameValueCollection ConfigProvider, string corpusName, bool returnVersionText, out int baseTextTotalTokens, List<SVDPt> svdPts)
        {
            SegmentVariation sv = new EblaAPI.SegmentVariation();
            
            // TODO - dispense with
            //sv.BaseTextContent = baseTextDoc.GetDocumentContentText(d.StartPosition, d.Length);

            //string[] baseTokens = TokeniseString(sv.BaseTextContent);
            //System.Diagnostics.Debug.WriteLine("Getting base text segid tokens");

            var baseTokens = baseTextDoc.GetSegmentDefinitionTokens(segDefId);
            baseTextTotalTokens = 0;

            if (baseTokens == null) // empty?
                return null;

            int totalNonUniqueBaseTokens = baseTokens.Sum(a => a.Value);
            baseTextTotalTokens = totalNonUniqueBaseTokens;
            
            System.Diagnostics.Debug.Assert(versionsSelected != null && versionsSelected.Count > 0);

            //if (versionsSelected == null || versionsSelected.Count == 0)
            //    versionsSelected = new List<string>(GetVersionList());

            var versionsSelectedHash = new HashSet<string>(versionsSelected);

            bool preLoad = true;
            if (alsetCache == null)
            {
                // only doing 1 seg - no point preloading, slower
                alsetCache = new Dictionary<string, AlignmentSet>();
                preLoad = false;

            }
            if (docCache == null)
                docCache = new Dictionary<string, Document>();

            foreach (var v in versionsSelectedHash)
            {
                if (!alsetCache.ContainsKey(v))
                {
                    var alset = new AlignmentSet(ConfigProvider);
                    alset.OpenWithoutPrivilegeCheck(corpusName, v, true, cn);
                    if (preLoad)
                    {
                        //System.Diagnostics.Debug.WriteLine("Preloading alset for version " + v);
                        alset.Preload(null);
                        //System.Diagnostics.Debug.WriteLine("Done preloading alset");
                    }
                    alsetCache.Add(v, alset);
                }
            }

            // Now find all alignments to this baseSegID
            List<string> versionsToProcess = new List<string>();
            List<List<int>> segmentsToProcess = new List<List<int>>();

            Dictionary<string, VersionSegmentVariation> variations = new Dictionary<string, EblaAPI.VersionSegmentVariation>();

            if (true)
            {
                //System.Diagnostics.Debug.WriteLine("Finding alignments");
                foreach (var v in versionsSelectedHash)
                {
                    var alset = alsetCache[v];
                    var alignment = alset.FindAlignment(segDefId, true);

                    if (alignment == null)
                        continue;

                    // Check it isn't a many-to-one, since this simple non-aggregating first implementation
                    // can't use that
                    if (alignment.SegmentIDsInBaseText.Length == 1) // this is alwasy true with FIndAlignment, in fact - TODO review
                    {
                        versionsToProcess.Add(v);
                        segmentsToProcess.Add(new List<int>(alignment.SegmentIDsInVersion));
                    }

                }
                //System.Diagnostics.Debug.WriteLine("Done finding alignments");

            }
            else
            {
                //string sql = "SELECT DISTINCT AlignmentID," + /* versionsegmentdefinitions.ID, */ " versionsegmentdefinitions.DocumentID, documents.Name ";
                //sql += "FROM segmentalignments  ";
                //sql += "INNER JOIN segmentdefinitions as basesegmentdefinitions ON segmentalignments.BaseTextSegmentID = basesegmentdefinitions.ID ";
                //sql += "INNER JOIN segmentdefinitions as versionsegmentdefinitions ON segmentalignments.VersionSegmentID = versionsegmentdefinitions.ID ";
                //sql += "INNER JOIN documents ON documents.ID = versionsegmentdefinitions.DocumentID ";
                //sql += "WHERE basesegmentdefinitions.DocumentID = " + _basetextid.ToString() + " AND segmentalignments.BaseTextSegmentID = " + d.ID.ToString();
                //sql += " ORDER BY versionsegmentdefinitions.DocumentID";

                //int lastAlignmentID = -1;
                //int lastDocID = -1;
                //string lastDocName = string.Empty;
                //List<int> segIDs = null;
                //Dictionary<string, string> versionStrings = new Dictionary<string, string>();
                ////string versionString = string.Empty;

                ////Dictionary<string, string[]> allVersionTokens = new Dictionary<string, string[]>();


                //List<VariationCalcInfo> calcInfoList = new List<VariationCalcInfo>();

                //using (MySqlCommand cmd = new MySqlCommand(sql, GetConnection()))
                //{
                //    using (MySqlDataReader dr = cmd.ExecuteReader())
                //    {
                //        int alignmentIDOrdinal = dr.GetOrdinal("AlignmentID");
                //        //int segIDOrdinal = dr.GetOrdinal("ID");
                //        int docIDOrdinal = dr.GetOrdinal("DocumentID");
                //        int docNameOrdinal = dr.GetOrdinal("Name");

                //        while (dr.Read())
                //        {
                //            int alignmentID = dr.GetInt32(alignmentIDOrdinal);
                //            //int segID = dr.GetInt32(segIDOrdinal);
                //            int docID = dr.GetInt32(docIDOrdinal);
                //            string docName = dr.GetString(docNameOrdinal);

                //            VariationCalcInfo calcInfo = new VariationCalcInfo();
                //            calcInfo.AlignmentID = alignmentID;
                //            calcInfo.versionName = docName;

                //            if (versionsSelectedHash.Contains(docName))
                //                calcInfoList.Add(calcInfo);
                //            /*if (alignmentID == lastAlignmentID)
                //            {
                //                System.Diagnostics.Debug.Assert(docID == lastDocID);
                //                segIDs.Add(segID);

                //            }
                //            else
                //            {
                //                if (lastAlignmentID != -1)
                //                {
                //                    VariationCalcInfo calcInfo = new VariationCalcInfo();
                //                    calcInfo.segIDs = segIDs;
                //                    calcInfo.versionName = lastDocName;
                //                    calcInfoList.Add(calcInfo);


                //                }

                //                // Prepare to collect more
                //                segIDs = new List<int>();
                //                lastDocID = docID;
                //                lastDocName = docName;
                //                segIDs.Add(segID);
                //                lastAlignmentID = alignmentID;
                //            }*/
                //        } // while (reading)

                //        /*if (segIDs != null && segIDs.Count > 0)
                //        {
                //            VariationCalcInfo calcInfo = new VariationCalcInfo();
                //            calcInfo.segIDs = segIDs;
                //            calcInfo.versionName = lastDocName;
                //            calcInfoList.Add(calcInfo);
                //        }*/
                //        //ProcessAlignment(lastDocName, segIDs, versionStrings, variations);


                //    }
                //}

                //foreach (var info in calcInfoList)
                //{
                //    AlignmentSet a = new AlignmentSet(ConfigProvider);

                //    a.OpenWithoutPrivilegeCheck(_name, info.versionName, true, GetConnection());

                //    var alignment = a.GetAlignment(info.AlignmentID);

                //    // Check it isn't a many-to-one, since this simple non-aggregating first implementation
                //    // can't use that
                //    if (alignment.SegmentIDsInBaseText.Length == 1)
                //    {
                //        versionsToProcess.Add(info.versionName);
                //        segmentsToProcess.Add(new List<int>(alignment.SegmentIDsInVersion));
                //    }

                //}


            }

            // Each entry in this list records the unique tokens for each version, and how often they occur in that version
            List<Dictionary<string, int>> uniqueTokenCountsPerVersion = new List<Dictionary<string, int>>();

            //foreach (var info in calcInfoList)
            for (int i = 0; i < versionsToProcess.Count; i++)
            {
                //ProcessAlignment(versionsToProcess[i], segmentsToProcess[i], versionStrings, variations, docCache, cn);
                //System.Diagnostics.Debug.WriteLine("Processing alignment for version: " + versionsToProcess[i]);

                var tokens = ProcessAlignment2(versionsToProcess[i], segmentsToProcess[i], docCache, cn, ConfigProvider, corpusName);
                //System.Diagnostics.Debug.WriteLine("Done processing alignment for version");

                if (tokens == null)
                    tokens = new Dictionary<string, int>();
                uniqueTokenCountsPerVersion.Add(tokens);
            }

            //Dictionary<string, string[]> allVersionTokens = new Dictionary<string, string[]>();

            //foreach (string s in versionStrings.Keys)
            //{
            //    allVersionTokens.Add(s, TokeniseString(versionStrings[s]));
            //}


            //var segmentVariations = new List<EblaAPI.VersionSegmentVariation>();
            List<TokenInfo> tokenInfos = new List<EblaAPI.TokenInfo>();

            //// Figure out how many unique base text tokens there are
            //var temp = new HashSet<string>();
            //foreach (string s in baseTokens)
            //    if (!temp.Contains(s))
            //        temp.Add(s);

            //double f = CalculateViv(temp.Count, allVersionTokens, variations, tokenInfos);

            // Change args to make it easier to share code with Geng
            List<string> versionNames = versionsToProcess; // new List<string>();
            //List<string[]> tokenisedVersions = new List<string[]>();
            List<double> eddyValues = new List<double>();

            //foreach (string s in versionStrings.Keys)
            //{
            //    versionNames.Add(s);
            //    tokenisedVersions.Add(allVersionTokens[s]);
            //    //eddyValues.Add(0);
            //}

            // Use this to store counts of how often each unique token occurs in the whole corpus
            Dictionary<string, int> uniqueTokenCountsForCorpus = new Dictionary<string, int>();


            //GetTokenCounts(tokenisedVersions, uniqueTokenCountsForCorpus, uniqueTokenCountsPerVersion);
            foreach (var tokens in uniqueTokenCountsPerVersion)
                foreach (var t in tokens.Keys)
                {
                    if (uniqueTokenCountsForCorpus.ContainsKey(t))
                        uniqueTokenCountsForCorpus[t] = uniqueTokenCountsForCorpus[t] + tokens[t];
                    else
                        uniqueTokenCountsForCorpus.Add(t, tokens[t]);
                }

            

            double f = CalculateVariation(totalNonUniqueBaseTokens, uniqueTokenCountsForCorpus, uniqueTokenCountsPerVersion, eddyValues, metricType);

            if (svdPts != null)
            {
                CalculateSVD(svdPts, uniqueTokenCountsForCorpus, uniqueTokenCountsPerVersion);
            }
            //if (metricType == EblaAPI.VariationMetricTypes.metricA)
            //{
            //    // Try an adjustment based on no. of unique tokens in base text 
            //    if (temp.Count > 0)
            //    {
            //        f /=  temp.Count; // Math.Log( temp.Count);
            //        System.Diagnostics.Debug.Assert(!double.IsNaN(f));
            //    }
            //    else
            //        // No alignments?
            //        f = 0;

            //}

            // Fixup results from calculation
            for (int i = 0; i < versionNames.Count; i++)
            {
                var vsv = new VersionSegmentVariation();
                vsv.VersionSegmentIDs = segmentsToProcess[i].ToArray();
                string versionName = versionNames[i];
                vsv.VersionName = versionName;
                
                variations.Add(versionName, vsv);
                vsv.EddyValue = eddyValues[i];
                Dictionary<string, int> uniqueTokenCountsForVersion = uniqueTokenCountsPerVersion[i];
                List<TokenInfo> tokenInfoForVersion = new List<TokenInfo>();
                foreach (string s in uniqueTokenCountsForVersion.Keys)
                {
                    TokenInfo t = new TokenInfo();
                    t.Token = s;
                    t.AverageCount = uniqueTokenCountsForVersion[s];
                    tokenInfoForVersion.Add(t);
                }
                vsv.TokenInfo = tokenInfoForVersion.ToArray();
                vsv.LanguageCode = docCache[versionName].GetMetadata().LanguageCode;
				vsv.ReferenceDate = docCache[versionName].GetMetadata().ReferenceDate;

                if (returnVersionText)
                {
                    //System.Diagnostics.Debug.WriteLine("Creating version string for version: " + versionName);
                    vsv.VersionText = CreateVersionString(segmentsToProcess[i], docCache[versionName]);
                    //System.Diagnostics.Debug.WriteLine("Done creating version string");
                    
                }
            }

            foreach (string token in uniqueTokenCountsForCorpus.Keys)
            {
                var ti = new TokenInfo();
                ti.Token = token;
                if (uniqueTokenCountsPerVersion.Count > 0)
                    ti.AverageCount = uniqueTokenCountsForCorpus[token] * 1.0 / uniqueTokenCountsPerVersion.Count;
                else
                    ti.AverageCount = 0;
                tokenInfos.Add(ti);
            }


            sv.BaseTextSegmentID = segDefId;

            sv.VivValue = f;

            List<VersionSegmentVariation> vsvs = new List<EblaAPI.VersionSegmentVariation>();
            foreach (string s in variations.Keys)
            {
                vsvs.Add(variations[s]);
            }
            sv.VersionSegmentVariations = vsvs.ToArray();
            sv.TokenInfo = tokenInfos.ToArray();
            

            return sv;

        }

        private void ProcessAlignment(string docName, List<int> segIDs, Dictionary<string, string> versionStrings, Dictionary<string, VersionSegmentVariation> variations, Dictionary<string, Document> docCache, EblaConnection cn)
        {
            Document versionDoc = null;
            if (docCache.ContainsKey(docName))
                versionDoc = docCache[docName];
            else
            {
                versionDoc = new Document(ConfigProvider);
                versionDoc.OpenWithoutPrivilegeCheck(_name, docName, true, cn);
                docCache.Add(docName, versionDoc);
            }
            

            string s = CreateVersionString(segIDs, versionDoc);
                                
            versionStrings.Add(docName, s);

            var vsv = new VersionSegmentVariation();
            vsv.VersionName = docName;
            vsv.VersionSegmentIDs = segIDs.ToArray();
            vsv.VersionText = s;
            variations.Add(docName, vsv);

        }

        static private Dictionary<string, int> ProcessAlignment2(string docName, List<int> segIDs, Dictionary<string, Document> docCache, EblaConnection cn, System.Collections.Specialized.NameValueCollection ConfigProvider, string corpusName)
        {
            Document versionDoc = null;
            if (docCache.ContainsKey(docName))
                versionDoc = docCache[docName];
            else
            {
                versionDoc = new Document(ConfigProvider);
                versionDoc.OpenWithoutPrivilegeCheck(corpusName, docName, true, cn);
                docCache.Add(docName, versionDoc);
            }

            Dictionary<string, int> results = null;
            foreach (int id in segIDs)
            {
                var tokens = versionDoc.GetSegmentDefinitionTokens(id);
                if (results == null)
                    results = tokens;
                else
                {
                    foreach (var t in tokens.Keys)
                    {
                        if (!results.ContainsKey(t))
                            results.Add(t, tokens[t]);
                        else
                        {
                            results[t] = results[t] + tokens[t];
                        }
                    }
                }

            }

            return results;
        }

        static void GetTokenCounts(List<string[]> tokenisedVersions, Dictionary<string, int> uniqueTokenCountsForCorpus, List<Dictionary<string, int>> uniqueTokenCountsPerVersion)
        {
            // Go through all the versions provided, counting token occurrences
            for (int versionIndex = 0; versionIndex < tokenisedVersions.Count; versionIndex++)
            {
                string[] tokens = tokenisedVersions[versionIndex];

                Dictionary<string, int> uniqueTokenCountsForThisVersion = new Dictionary<string, int>();
                uniqueTokenCountsPerVersion.Add(uniqueTokenCountsForThisVersion);

                // Look at all the tokens in this version
                foreach (string token in tokens)
                {
                    // Increment the occurrence-count-in-whole-corpus
                    if (uniqueTokenCountsForCorpus.ContainsKey(token))
                        uniqueTokenCountsForCorpus[token] = uniqueTokenCountsForCorpus[token] + 1;
                    else
                        uniqueTokenCountsForCorpus.Add(token, 1);

                    // Increment the occurrence-count-in-this-version
                    if (uniqueTokenCountsForThisVersion.ContainsKey(token))
                        uniqueTokenCountsForThisVersion[token] = uniqueTokenCountsForThisVersion[token] + 1;
                    else
                        uniqueTokenCountsForThisVersion.Add(token, 1);

                }

            }


        }

        static void CalculateSVD(List<SVDPt> svdPts, Dictionary<string, int> uniqueTokenCountsForCorpus, List<Dictionary<string, int>> uniqueTokenCountsPerVersion)
        {

            int numDocs = uniqueTokenCountsPerVersion.Count;
            int numWords = uniqueTokenCountsForCorpus.Keys.Count;

            if (numDocs < 2)
                return;

            double[][] data = new double[numWords][];

            int i = 0;
            foreach (var word in uniqueTokenCountsForCorpus.Keys)
            {
                data[i] = new double[numDocs];
                for (int j = 0; j < numDocs; j++)
                {
                    double freq = 0;
                    Dictionary<String, int> docMap = uniqueTokenCountsPerVersion[j];

                    if (docMap.ContainsKey(word))
                        freq = docMap[word];

                    data[i][j] = freq;
                }
                i++;
            }


            Matrix matrix = new Matrix(data);

            SingularValueDecomposition svd =
                  new SingularValueDecomposition(matrix);

            Matrix v = svd.getV();

            // see http://sujitpal.blogspot.co.uk/2008/10/ir-math-in-java-cluster-visualization.html
            // but note we're only taking the first 2 cols from V, not 3, because we're only
            // doing a 2-d plot (and that way, we can still plot if we have only 2 docs)

            Matrix vRed = v.getMatrix(0, v.getRowDimension() - 1, 0, 1);

            for (i = 0; i < v.getRowDimension(); i++)
            {
                var pt = new SVDPt();
                pt.X = Math.Abs(vRed.get(i, 0));
                pt.Y = Math.Abs(vRed.get(i, 1));
                svdPts.Add(pt);
                //Console.Write(Math.Abs(vRed.get(i, 0)));
                //Console.Write("\t");
                //Console.Write(Math.Abs(vRed.get(i, 1)));
                //Console.Write("\t");
                //Console.WriteLine(i);
                ////			printf("%6.4f %6.4f %d\n", 
                ////			        Math.abs(vRed.get(i, 0)), Math.abs(vRed.get(i, 1)), i);
            }
        }

        /// <summary>
        /// Calculates Viv and Eddy values for a set of versions w.r.t. the 'corpus', that is, the set containing only those versions.
        /// </summary>
        /// <param name="uniqueTokenCountsForCorpus">A dictionary of unique tokens found in the corpus, with a count of occurrences in the corpus for each token.</param>
        /// <param name="uniqueTokenCountsPerVersion">A list of dictionaries, one per version, where each dictionary contains the unique tokens found in the version, with a count of occurrences in the version for each token.</param>
        /// <param name="eddyValues">A list of doubles, one per version, used to return the Eddy values for each version.</param>
        /// <returns>The Viv value for the corpus.</returns>
        static double CalculateVariation(int baseSegmentWordCount, Dictionary<string, int> uniqueTokenCountsForCorpus, List<Dictionary<string, int>> uniqueTokenCountsPerVersion, List<double> eddyValues, VariationMetricTypes metricType)
        {
            switch (metricType)
            {
                case EblaAPI.VariationMetricTypes.metricA:
                    return CalculateVariationTypeA(baseSegmentWordCount, uniqueTokenCountsForCorpus, uniqueTokenCountsPerVersion, eddyValues);
                case EblaAPI.VariationMetricTypes.metricB:
                    return CalculateVariationTypeB(baseSegmentWordCount, uniqueTokenCountsForCorpus, uniqueTokenCountsPerVersion, eddyValues);
                case EblaAPI.VariationMetricTypes.metricC:
                    return CalculateVariationTypeC(baseSegmentWordCount, uniqueTokenCountsForCorpus, uniqueTokenCountsPerVersion, eddyValues);
                case EblaAPI.VariationMetricTypes.metricD:
                    return CalculateVariationTypeD(baseSegmentWordCount, uniqueTokenCountsForCorpus, uniqueTokenCountsPerVersion, eddyValues);
                case EblaAPI.VariationMetricTypes.metricE:
                    return CalculateVariationTypeE(baseSegmentWordCount, uniqueTokenCountsForCorpus, uniqueTokenCountsPerVersion, eddyValues);

            }

            throw new Exception("Unknown VariationMetricTypes: " + metricType.ToString());
        }

        static double CalculateVariationTypeB(int baseSegmentWordCount, Dictionary<string, int> uniqueTokenCountsForCorpus, List<Dictionary<string, int>> uniqueTokenCountsPerVersion, List<double> eddyValues)
        {
            int noOfVersions = uniqueTokenCountsPerVersion.Count;
            if (noOfVersions == 0)
                return 0;

            eddyValues.Clear();
            // Tom's original formulae

            // For each version, eddy is sum of D/tf for each token t in the version
            double sumOfEddyValues = 0;
            for (int versionIndex = 0; versionIndex < uniqueTokenCountsPerVersion.Count; versionIndex++)
            {

                var uniqueTokenCountsForThisVersion = uniqueTokenCountsPerVersion[versionIndex];

                double eddyValue = 0;
                foreach (string token in uniqueTokenCountsForThisVersion.Keys)
                {
                    eddyValue += (1.0 * noOfVersions) / uniqueTokenCountsForCorpus[token] /* * uniqueTokenCountsForThisVersion[token] */;
                }
                sumOfEddyValues += eddyValue;
                eddyValues.Add(eddyValue);
            }

            return sumOfEddyValues / noOfVersions;
        }

        //static double CalculateVariationTypeC(Dictionary<string, int> uniqueTokenCountsForCorpus, List<Dictionary<string, int>> uniqueTokenCountsPerVersion, List<double> eddyValues)
        //{
        //    int noOfVersions = uniqueTokenCountsPerVersion.Count;
        //    if (noOfVersions == 0)
        //        return 0;

        //    eddyValues.Clear();
            
        //    // Test implementation

            
        //    double sumOfEddyValues = 0;
        //    for (int versionIndex = 0; versionIndex < uniqueTokenCountsPerVersion.Count; versionIndex++)
        //    {

        //        var uniqueTokenCountsForThisVersion = uniqueTokenCountsPerVersion[versionIndex];

        //        double eddyValue = 0;
        //        foreach (string token in uniqueTokenCountsForCorpus.Keys)
        //        {
        //            double thisVal = 0;
        //            if (uniqueTokenCountsForThisVersion.ContainsKey(token))
        //                thisVal = uniqueTokenCountsForThisVersion[token];
        //            eddyValue += Math.Abs((uniqueTokenCountsForCorpus[token] * 1.0 / noOfVersions) - thisVal);

        //        }
        //        eddyValue /= uniqueTokenCountsForCorpus.Count;

        //        sumOfEddyValues += eddyValue;
        //        eddyValues.Add(eddyValue);
        //    }

        //    return sumOfEddyValues / noOfVersions;
        //}

        static double CalculateVariationTypeC(int baseSegmentWordCount, Dictionary<string, int> uniqueTokenCountsForCorpus, List<Dictionary<string, int>> uniqueTokenCountsPerVersion, List<double> eddyValues)
        {
            var unused = CalculateVariationTypeA(baseSegmentWordCount, uniqueTokenCountsForCorpus, uniqueTokenCountsPerVersion, eddyValues);

            return StandardDeviation(eddyValues);
        }

        public static double StandardDeviation(List<double> valueList)
        {
            double M = 0.0;
            double S = 0.0;
            int k = 1;
            foreach (double value in valueList)
            {
                double tmpM = M;
                M += (value - tmpM) / k;
                S += (value - tmpM) * (value - M);
                k++;
            }
            if (k == 1)
                return 0;
            return Math.Sqrt(S / (k - 1));
        }

        static double CalculateVariationTypeD(int baseSegmentWordCount, Dictionary<string, int> uniqueTokenCountsForCorpus, List<Dictionary<string, int>> uniqueTokenCountsPerVersion, List<double> eddyValues)
        {
            // Compare every version with every other version
            int noOfVersions = uniqueTokenCountsPerVersion.Count;
            //double[,] pairScores = new double[noOfVersions, noOfVersions]; // probably don't need this ...
            eddyValues.Clear();

            if (noOfVersions == 0)
                return 0;

            double[] scoreTotals = new double[noOfVersions];

            for (int versionA = 0; versionA < (noOfVersions - 1); versionA++)
            {
                for (int versionB = versionA + 1; versionB < noOfVersions; versionB++)
                {
                    var dictA = uniqueTokenCountsPerVersion[versionA];
                    var dictB = uniqueTokenCountsPerVersion[versionB];

                    int intersectionCount = 0;
                    int totalCount = 0;
                    foreach (var kvp in dictA)
                    {
                        if (dictB.ContainsKey(kvp.Key))
                            intersectionCount += Math.Min(kvp.Value, dictB[kvp.Key]);
                        totalCount += kvp.Value;
                    }
                    totalCount += dictB.Sum(x => x.Value);

                    double score = 0;
                    if (totalCount > 0)
                        score = 2.0 * intersectionCount / totalCount;

                    System.Diagnostics.Debug.Assert(score <= 1);
                    //pairScores[versionA, versionB] = score;

                    score = 1 - score; // Dice's coeff = 1 when sets are identical; we want identity to equal 0

                    scoreTotals[versionA] += score;
                    scoreTotals[versionB] += score;

                } // versionB

            } // versionA

            double vivAcc = 0;
            for (int i = 0; i < noOfVersions; i++)
            {
                eddyValues.Add( scoreTotals[i] / noOfVersions);
                vivAcc += eddyValues[i];
            }

            double viv = vivAcc / noOfVersions;
            System.Diagnostics.Debug.Assert(viv <= 1);
            return viv;
        }

        static double VectorMagnitude(IEnumerable<double> values)
        {
            double agg = 0;
            foreach (var v in values)
                agg += v * v;
            return Math.Sqrt(agg);
        }
        static double VectorMagnitude(IEnumerable<int> values)
        {
            double agg = 0;
            foreach (var v in values)
                agg += v * v;
            return Math.Sqrt(agg);
        }

        static double DotProduct(Dictionary<string, double> tokenAverages, Dictionary<string, int> uniqueTokenCountsForThisVersion)
        {
            // Since tokens not found in both dictionaries result in a zero term, no need to include them.
            // So enumerate shorter dictionary.
            double agg = 0;
            foreach (var s in uniqueTokenCountsForThisVersion.Keys)
            {
                double avValue;
                if (tokenAverages.TryGetValue(s, out avValue))
                    agg += uniqueTokenCountsForThisVersion[s] * avValue;
            }
            return agg;
        }

        static double CalculateVariationTypeE(int baseSegmentWordCount, Dictionary<string, int> uniqueTokenCountsForCorpus, List<Dictionary<string, int>> uniqueTokenCountsPerVersion, List<double> eddyValues)
        {
            int noOfVersions = uniqueTokenCountsPerVersion.Count;
            eddyValues.Clear();

            // Calculate centre of "finite set of points" (http://en.wikipedia.org/wiki/Centroid)

            Dictionary<string, double> tokenAverages = new Dictionary<string, double>();

            foreach (string token in uniqueTokenCountsForCorpus.Keys)
                tokenAverages.Add(token, uniqueTokenCountsForCorpus[token] * 1.0 / noOfVersions);

            double centroidMagnitude = VectorMagnitude(tokenAverages.Values);

            double sumOfEddyValues = 0;
            // Calculate eddy as angular distance vs. centroid (https://en.wikipedia.org/wiki/Cosine_similarity) 
            for (int versionIndex = 0; versionIndex < uniqueTokenCountsPerVersion.Count; versionIndex++)
            {
                var uniqueTokenCountsForThisVersion = uniqueTokenCountsPerVersion[versionIndex];

                double versionMagnitude = VectorMagnitude(uniqueTokenCountsForThisVersion.Values);
                double dotProduct = DotProduct(tokenAverages, uniqueTokenCountsForThisVersion);

                double tempSim = dotProduct / (versionMagnitude * centroidMagnitude);

                // Should be between 1 and -1, but floating point can lead to small overflow
                System.Diagnostics.Debug.Assert(tempSim <= 1.001);
                System.Diagnostics.Debug.Assert(tempSim >= -1.001);
                tempSim = Math.Min(tempSim, 1);
                tempSim = Math.Max(tempSim, -1);

                // Those limits hold true for all vectors, but since ours hold only +ve attributes, it should
                // be more constrained still
                System.Diagnostics.Debug.Assert(tempSim >= 0);

                // a tempSim value of 1 means identical

                // Since 0 <= tempSim <= 1 , acos(tempsim) is between 0 and pi/2
                
                double angDistance = (2 * Math.Acos(tempSim) / Math.PI);
                System.Diagnostics.Debug.Assert(angDistance >= 0);
                System.Diagnostics.Debug.Assert(angDistance <= 1);
                eddyValues.Add(angDistance);
                sumOfEddyValues += angDistance;
            }

            double vivValue = 0;
            if (noOfVersions > 0)
                vivValue = sumOfEddyValues / noOfVersions;
            return vivValue;
        }

        static double CalculateVariationTypeA(int baseSegmentWordCount, Dictionary<string, int> uniqueTokenCountsForCorpus, List<Dictionary<string, int>> uniqueTokenCountsPerVersion, List<double> eddyValues)
        {
            int noOfVersions = uniqueTokenCountsPerVersion.Count;
            eddyValues.Clear();

            // Calculate centre of "finite set of points" (http://en.wikipedia.org/wiki/Centroid)

            Dictionary<string, double> tokenAverages = new Dictionary<string, double>();

            foreach (string token in uniqueTokenCountsForCorpus.Keys)
                tokenAverages.Add(token, uniqueTokenCountsForCorpus[token] * 1.0 / noOfVersions);

            bool useSegmentLengthNormalisationDivisor = true;
            bool computePeerDistances = false;

            double segmentLengthNormalisationDivisor = Math.Log((baseSegmentWordCount + 50) * 1.0 / 50) * 8 + 1;

            //double sumOfEddyValuesOldFormula = 0;
            double sumOfEddyValues = 0;
            double sumOfPeerDistances = 0;
            int peerDistances = 0;
            //double variance = 0;
            //int totalWordsInVersions = 0;
            // For each version, calculate distance from centre, i.e. the Eddy value
            for (int versionIndex = 0; versionIndex < uniqueTokenCountsPerVersion.Count; versionIndex++)
            {
                double temp = 0;
                var uniqueTokenCountsForThisVersion = uniqueTokenCountsPerVersion[versionIndex];

                //// work out total number of (significant) words in this version's segment
                //int segmentlength = 0;
                //foreach (string s in uniqueTokenCountsForThisVersion.Keys)
                //    segmentlength += uniqueTokenCountsForThisVersion[s];

                // Calculate n-dimensional Euclidean distance (http://en.wikipedia.org/wiki/Euclidean_distance)
                foreach (string token in uniqueTokenCountsForCorpus.Keys)
                {
                    // What is this distance between the average and this version?
                    double thisVal = 0;
                    if (uniqueTokenCountsForThisVersion.ContainsKey(token))
                        thisVal = uniqueTokenCountsForThisVersion[token];
                    double distance = (tokenAverages[token] - thisVal);
                    temp += distance * distance;
                    //variance += temp;

                }


                double eddyValue = Math.Sqrt(temp);
                //sumOfEddyValuesOldFormula += eddyValue;

                if (useSegmentLengthNormalisationDivisor)
                    eddyValue /= segmentLengthNormalisationDivisor;

                eddyValues.Add(eddyValue);

                //double divisor = Math.Log((segmentlength + 50) * 1.0 / 50) * 8 + 1;

                sumOfEddyValues += eddyValue; // / divisor;
                //variance /= uniqueTokenCountsForCorpus.Count;
                //totalWordsInVersions += segmentlength;



                if (computePeerDistances)
                {
                    for (int otherVersionIndex = 0; otherVersionIndex < uniqueTokenCountsPerVersion.Count; otherVersionIndex++)
                    {
                        // Don't measure version against itself
                        if (otherVersionIndex == versionIndex)
                            continue;
                        var uniqueTokenCountsForOtherVersion = uniqueTokenCountsPerVersion[otherVersionIndex];
                        foreach (string token in uniqueTokenCountsForCorpus.Keys)
                        {
                            // What is this distance between the average and this version?
                            double thisVal = 0;
                            double otherVal = 0;
                            if (uniqueTokenCountsForThisVersion.ContainsKey(token))
                                thisVal = uniqueTokenCountsForThisVersion[token];
                            if (uniqueTokenCountsForOtherVersion.ContainsKey(token))
                                thisVal = uniqueTokenCountsForOtherVersion[token];
                            double distance = (otherVal - thisVal);
                            temp += distance * distance;
                            //variance += temp;

                        }

                        double peerDistance = Math.Sqrt(temp);
                        if (useSegmentLengthNormalisationDivisor)
                            peerDistance /= segmentLengthNormalisationDivisor;
                        sumOfPeerDistances += peerDistance;
                        peerDistances++;
                    }

                } // if (compute peer distance)


            }

            //if (noOfVersions > 0)
            //    variance /= noOfVersions;

            double vivValue = 0;

            if (noOfVersions > 0)
            {
                if (computePeerDistances)
                {
                    vivValue = 0;
                    if (peerDistances > 0)
                        vivValue = sumOfPeerDistances / peerDistances;
                }
                else
                {
                    vivValue = sumOfEddyValues / noOfVersions;
                }


                //double averageWordCountInVersions = totalWordsInVersions * 1.0 / noOfVersions;
                //double divisorOldFormula = Math.Log((averageWordCountInVersions + 50) * 1.0 / 50) * 8 + 1;
                ////vivValue /= divisor;
                //double vivValueOldFormula = sumOfEddyValuesOldFormula / noOfVersions / divisorOldFormula;

                //if ((vivValue / vivValueOldFormula) > 1.5)
                //{
                //    string s = "gkjg";
                //}
                //if ((vivValueOldFormula / vivValue) > 1.5)
                //{
                //    string s = "gkjg";
                //}

            }

            
            //// try using s.d. instead
            //vivValue = Math.Sqrt(variance);

            return vivValue;
        }

        //static double AverageDistanceBetweenPairs(List<Dictionary<string, int>> uniqueTokenCountsPerVersion, List<string> uniqueTokensForCorpus)
        //{
        //    int pairCount = 0;
        //    double distanceAcc = 0;

        //    // Calculate distance for each possible pair
        //    for (int versionIndex = 0; versionIndex < uniqueTokenCountsPerVersion.Count - 1; versionIndex++)
        //    {
        //        for (int versionIndex2 = 1; versionIndex2 < uniqueTokenCountsPerVersion.Count; versionIndex2++)
        //        {
        //            foreach (string token in uniqueTokensForCorpus)
        //            {
        //                // What is this distance between the two versions?
        //                double val1 = 0;
        //                double val2 = 0;
        //                if (uniqueTokenCountsPerVersion[versionIndex].ContainsKey(token))
        //                    val1 = uniqueTokenCountsPerVersion[versionIndex][token];


        //                double distance = (tokenAverages[token] - thisVal);
        //                temp += distance * distance;
        //            }
        //        }
        //    }
        //}

        internal static string[] TokeniseString(string text)
        {
            // Tom may want apostrophes taken out, but for now we'll just drag out alpha sequences
            //string[] tokens = System.Text.RegularExpressions.Regex.Split(text, @"[a-zA-Z]+");

            var matches= System.Text.RegularExpressions.Regex.Matches(text, @"\w+");
            List<string> tokens = new List<string>();

            foreach (System.Text.RegularExpressions.Match match in matches)
                tokens.Add(match.Value.ToLower());

            List<string> result = new List<string>();
            foreach (string s in tokens)
                if (!_deStopWords.Contains(s))
                    result.Add(s);


            //return tokens.ToArray();
            return result.ToArray();

            //for (int i = 0; i < tokens.Length; i++)
            //    tokens[i] = tokens[i].ToLower();

            //return tokens;
        }

        static string CreateVersionString(List<int> segIDs, Document versionDoc)
        {

            StringBuilder sb = new StringBuilder();
            foreach (int id in segIDs)
            {
                if (sb.Length > 0)
                    sb.Append(" ");
                SegmentDefinition d = versionDoc.GetSegmentDefinition(id);

                if (d.Length > 0)
                    sb.Append(versionDoc.GetDocumentContentText(d.StartPosition, d.Length));
            }

            return sb.ToString();
        }

        private HtmlErrors UploadDocument(int ID, string html, bool updating, string nameIfVersion, Dictionary<int, int> IDChanges)
        {
            CheckOpen();

            HtmlErrors errors = new HtmlErrors();

            string TOC = string.Empty;
            int length = 0;

            //List<DocumentOffset> offsetList = new List<DocumentOffset>();

            List<SegmentDefinition> segdefs = new List<EblaAPI.SegmentDefinition>();

            Document doc = new Document(ConfigProvider);
            doc.OpenWithoutPrivilegeCheck(_name, nameIfVersion, true, GetConnection());

            var meta = doc.GetMetadata();
            
            string xml = ParseHtml(html, errors, out TOC, out length, segdefs, meta); //, offsetList);

            if (errors.HtmlParseErrors.Length > 0)
                return errors;           

            //SegmentDefinition[] oldsegdefs = null;

            if (updating)
            {
                // Get existing segment defs
                doc = new Document(ConfigProvider);

                doc.OpenWithoutPrivilegeCheck(_name, nameIfVersion, true, GetConnection());
                
                var oldsegdefs = doc.FindSegmentDefinitions(0, doc.Length(), false, false, null, null);

                var oldsegdefmap = new Dictionary<int, SegmentDefinition>();
                var oldsegdefids = new HashSet<int>();
                foreach (var s in oldsegdefs)
                {
                    oldsegdefmap.Add(s.ID, s);
                    oldsegdefids.Add(s.ID);
                }

                foreach (var s in segdefs)
                    if (oldsegdefids.Contains(s.ID))
                    {
                        oldsegdefids.Remove(s.ID);
                    }
                    else
                        throw new Exception("A segment definition with ID " + s.ID.ToString() + " was found, but no segment definition with that ID has been defined for the document.");

                if (oldsegdefids.Count > 0)
                {
                    var e = oldsegdefids.GetEnumerator();
                    e.MoveNext();
                    throw new Exception(oldsegdefids.Count.ToString() + " segment definitions were missing. First ID: " + e.Current);
                }

            } // if (updating)

            using (var txn = GetConnection().BeginTransaction())
            {
                Dictionary<string, int> labelMap = null;
                if (!updating && !string.IsNullOrEmpty(nameIfVersion))
                {
                    // We're uploading a version for the first time.
                    // It may contain alignment information.
                    // This can currently be provided in rather an ad-hoc way.
                    // Segment attributes with name 'alignlabel' can be provided, which match up
                    // with attributes on base text segments with name 'label'.
                    // So, load all the base text segments and make a map of any labels.
                    var basetext = new Document(ConfigProvider);

                    basetext.OpenWithoutPrivilegeCheck(_name, null, true, GetConnection());

                    var basetextsegdefs = basetext.FindSegmentDefinitions(0, basetext.Length(), false, false, null, null);

                    labelMap = new Dictionary<string, int>();
                    foreach (var seg in basetextsegdefs)
                    {
                        if (seg.Attributes != null)
                        {
                            //var a = seg.Attributes.First(x => string.Compare(x.Name, "label") == 0);
                            var a = seg.Attributes.FirstOrDefault(x => string.Compare(x.Name, "label") == 0);
                            if (a != null)
                            {
                                System.Diagnostics.Debug.Assert(!labelMap.ContainsKey(a.Value));
                                labelMap.Add(a.Value, seg.ID);
                            }
                        }

                    }

                } // if (need to check for alignment info)

                using (var cmd = GetNewCommand("UPDATE documents SET Content = @content, TOC = @toc, Length = @length WHERE ID = " + ID.ToString(), GetConnection()))
                {
                    cmd.Transaction = txn;

                    cmd.Parameters.AddWithValue("content", xml);
                    cmd.Parameters.AddWithValue("toc", TOC);
                    cmd.Parameters.AddWithValue("length", length);
                    
                    cmd.ExecuteNonQuery();

                    var alignmentMap = new Dictionary<int, List<int>>();

                    if (updating)
                    {
                        // Re-open document (length etc may have changed)
                        doc = new Document(ConfigProvider);

                        doc.OpenWithoutPrivilegeCheck(_name, nameIfVersion, true, GetConnection());


                        cmd.Parameters.Clear();
                        var startPosParm = cmd.Parameters.AddWithValue("startpos", 0);
                        var lengthParm = cmd.Parameters.AddWithValue("length", 0);
                        var idParm = cmd.Parameters.AddWithValue("id", 0);
                        
                        string sql = "UPDATE segmentdefinitions SET StartPosition = @startpos, Length = @length WHERE ID = @id";
                        cmd.CommandText = sql;

                        foreach (var segdef in segdefs)
                        {

                            idParm.Value = segdef.ID;


                            startPosParm.Value = segdef.StartPosition;
                            lengthParm.Value = segdef.Length;

                            cmd.ExecuteNonQuery();
                            doc.UpdateSegmentDefinitionTokens(segdef, txn);
                        }
                    }
                    else
                    {
                        cmd.Parameters.Clear();
                        cmd.CommandText = "DELETE FROM segmentdefinitions WHERE DocumentID = " + ID.ToString();
                        cmd.ExecuteNonQuery();

                        doc = new Document(ConfigProvider);

                        doc.OpenWithoutPrivilegeCheck(_name, nameIfVersion, true, GetConnection());
                        doc.SetMetadata(meta);

                        var alset = new AlignmentSet(ConfigProvider);
                        if (labelMap != null)
                            alset.OpenWithoutPrivilegeCheck(_name, nameIfVersion, true, GetConnection());

                        foreach (var segdef in segdefs)
                        {
                            //var exclusionChangeSpans = new SpanMerger(doc.Length());
                            int importSegID = segdef.ID;
                            int segid = doc.CreateSegmentDefinition(segdef, txn, true);
                            //Document.CreateSegmentDefinition(ID, segdef, cmd);
                            IDChanges.Add(importSegID, segid);
                            System.Diagnostics.Debug.Assert(importSegID > 0);
                            segdef.ID = segid;
                            if (labelMap != null)
                            {
                                if (segdef.Attributes != null)
                                {
                                    var a = segdef.Attributes.FirstOrDefault(x => string.Compare(x.Name, "alignlabel") == 0);
                                    if (a != null)
                                    {
                                        if (!labelMap.ContainsKey(a.Value))
                                            throw new Exception("Value for 'alignlabel' matches no 'label' in base text: " + a.Value);

                                        int basetextsegid = labelMap[a.Value];

                                        if (alignmentMap.ContainsKey(basetextsegid))
                                            alignmentMap[basetextsegid].Add(segid);
                                        else
                                            alignmentMap.Add(basetextsegid, new List<int>() { segid });


                                        //var al = new SegmentAlignment();
                                        //al.SegmentIDsInBaseText = new int[] { labelMap[a.Value] };
                                        //al.SegmentIDsInVersion = new int[] { segid };
                                        //al.AlignmentStatus = AlignmentStatuses.confirmed;
                                        //al.AlignmentType = AlignmentTypes.normalAlign;
                                        //alset.CreateAlignment(al, txn);
                                        
                                    }
                                }
                                
                            } // if (checking for alignment info)

                        } // for (each segdef found)

                        foreach (var segdef in segdefs)
                        {
                            doc.UpdateSegmentDefinitionTokens(segdef, txn);
                        }

                        foreach (var segid in alignmentMap.Keys)
                        {
                            var al = new SegmentAlignment();
                            al.SegmentIDsInBaseText = new int[] { segid };
                            al.SegmentIDsInVersion = alignmentMap[segid].ToArray();
                            al.AlignmentStatus = AlignmentStatuses.confirmed;
                            al.AlignmentType = AlignmentTypes.normalAlign;
                            alset.CreateAlignment(al, txn);

                        }

                    } // if (not updating)

                    //cmd.CommandText = "DELETE FROM documentoffsetlists WHERE DocumentID = " + ID.ToString();
                    //cmd.ExecuteNonQuery();

                    /*
                    cmd.CommandText = "INSERT INTO documentoffsetlists (DocumentID, Offset, ParentID, ChildIndex) VALUES (" + ID.ToString() + ", @offset, @parentid, @childindex)";
                    MySqlParameter offsetparm = cmd.Parameters.AddWithValue("offset", 0);
                    MySqlParameter parentidparm = cmd.Parameters.AddWithValue("parentid", 0);
                    MySqlParameter childindexparm = cmd.Parameters.AddWithValue("childindex", 0);

                    foreach (var o in offsetList)
                    {
                        offsetparm.Value = o.offset;
                        parentidparm.Value = o.parentID;
                        childindexparm.Value = o.childIndex;
                        cmd.ExecuteNonQuery();
                    }*/



                }


                txn.Commit();
            }

            
            


            return errors;
        }

        void AddUnlocatedError(string message, List<EblaAPI.HtmlParseError> parseErrors)
        {
            EblaAPI.HtmlParseError error = new EblaAPI.HtmlParseError();
            error.SourceText = string.Empty;
            error.Reason = message;
            error.StreamPosition = -1;
            error.Line = -1;
            error.LinePosition = -1;
            parseErrors.Add(error);
        }

        private string ParseHtml(string html, HtmlErrors errors, out string TOC, out int length, 
            List<SegmentDefinition> segdefs, EblaAPI.DocumentMetadata meta) //, List<DocumentOffset> offsetList)
        {
            TOC = string.Empty;
            length = 0;

            ////TODO: Extract and convert with Tika
            //var tikaExtractor = new TikaExtractor();
            //var parsedHtml = tikaExtractor.ConvertTextToHtml(html);

            //if (string.IsNullOrEmpty(parsedHtml))
            //    html = parsedHtml;

            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(html);

            List<EblaAPI.HtmlParseError> parseErrors = new List<EblaAPI.HtmlParseError>();

            foreach (var e in doc.ParseErrors)
            {
                EblaAPI.HtmlParseError error = new EblaAPI.HtmlParseError();
                error.SourceText = e.SourceText;
                error.Line = e.Line;
                error.Reason = e.Reason;
                error.StreamPosition = e.StreamPosition;
                error.LinePosition = e.LinePosition;

                parseErrors.Add(error);
            }

            errors.HtmlParseErrors = parseErrors.ToArray();

            if (parseErrors.Count > 0)
                return string.Empty;


            HtmlNodeCollection headNodes = doc.DocumentNode.SelectNodes("/html/head");

            if (headNodes == null)
            {
                AddUnlocatedError("HTML 'head' element not found.", parseErrors);
                errors.HtmlParseErrors = parseErrors.ToArray();

                return string.Empty;
            }
                
            if (headNodes.Count > 1)
            {
                AddUnlocatedError("More than one HTML 'head' element found", parseErrors);
                //EblaAPI.HtmlParseError error = new EblaAPI.HtmlParseError();
                //error.SourceText = string.Empty;
                //error.Reason = "More than one HTML 'head' element found";
                //error.StreamPosition = -1;
                //error.Line = -1;
                //error.LinePosition = -1;
                //parseErrors.Add(error);
                errors.HtmlParseErrors = parseErrors.ToArray();

                return string.Empty;
            }

            if (headNodes.Count == 1)
            {
                HtmlNode headNode = headNodes[0];

                const string prefix = "data-ebla-";

                foreach (var at in headNode.Attributes)
                {
                    if (at.Name.Length > prefix.Length)
                    {
                        if (at.Name.Substring(0, prefix.Length).CompareTo(prefix) == 0)
                        {
                            string attribVal = System.Net.WebUtility.HtmlDecode(at.Value);
                            switch (at.Name.Substring(prefix.Length))
                            {
                                case "lang":
                                    if (attribVal.Length != 3)
                                    {
                                        AddUnlocatedError("Invalid language code: " + attribVal, parseErrors);

                                        errors.HtmlParseErrors = parseErrors.ToArray();

                                        return string.Empty;
                                    }
                                    meta.LanguageCode = attribVal;
                                    break;
                                case "authtran":
                                    meta.AuthorTranslator = attribVal;
                                    break;

                                case "copyright":
                                    meta.CopyrightInfo = attribVal;
                                    break;

                                case "description":
                                    meta.Description = attribVal;
                                    break;

                                case "genre":
                                    meta.Genre = attribVal;
                                    break;
                                case "info":
                                    meta.Information = attribVal;
                                    break;
                                case "date":
                                    int year = 0;
                                    if (int.TryParse(attribVal, out year))
                                        meta.ReferenceDate = year;
                                    else
                                    {
                                        AddUnlocatedError("Invalid year value: " + attribVal, parseErrors);
                                        errors.HtmlParseErrors = parseErrors.ToArray();
                                        return string.Empty;
                                    }
                                    break;


                            }
                        }
                    }
                }
            }

            HtmlNodeCollection bodyNodes = doc.DocumentNode.SelectNodes("/html/body");

            if (bodyNodes == null || bodyNodes.Count == 0)
            {
                AddUnlocatedError("HTML 'body' element not found", parseErrors);
                //EblaAPI.HtmlParseError error = new EblaAPI.HtmlParseError();
                //error.SourceText = string.Empty;
                //error.Reason = "HTML 'body' element not found";
                //error.StreamPosition = -1;
                //error.Line = -1;
                //error.LinePosition = -1;
                //parseErrors.Add(error);
                errors.HtmlParseErrors = parseErrors.ToArray();

                return string.Empty;
            }

            if (bodyNodes.Count > 1)
            {
                AddUnlocatedError("More than one HTML 'body' element found", parseErrors);
                //EblaAPI.HtmlParseError error = new EblaAPI.HtmlParseError();
                //error.SourceText = string.Empty;
                //error.Reason = "More than one HTML 'body' element found";
                //error.StreamPosition = -1;
                //error.Line = -1;
                //error.LinePosition = -1;
                //parseErrors.Add(error);
                errors.HtmlParseErrors = parseErrors.ToArray();

                return string.Empty;

            }

            HtmlNode bodyNode = bodyNodes[0];

            XmlDocument xmlDoc = EblaHelpers.CreateXmlDocument("body");

            XmlAttribute attrib = xmlDoc.CreateAttribute("id");
            attrib.Value = "0";
            xmlDoc.DocumentElement.Attributes.Append(attrib);


            XmlDocument xmlDocTOC = EblaHelpers.CreateXmlDocument("TOC");

            int totalChars = 0;
            int id = 1;
            AppendNodes(xmlDoc.DocumentElement, bodyNode, ref id, xmlDocTOC.DocumentElement, ref totalChars);

            // Go through a to-string/from-string cycle, as this changes the doc representation.
            // For instance, <p>&nbsp;</p> in the htmlagility doc is an element containing the text node, so the 
            // node-by-node copy creates that structure in the xml doc. However, when saved to XML then reloaded,
            // the xml doc contains only <p />, *even though the &nbsp remains in the XML*. We need to have the xml
            // doc in an as-loaded-from-xml state rather than in an as-built-by-appending-nodes state before 
            // measuring any segment marker offsets etc.
            string temp = EblaHelpers.XmlDocumentToString(xmlDoc);

            xmlDoc.LoadXml(temp);
            var textNodes = new List<XmlText>();

            // Deal with any segment markers that have been put in the document
            XmlNodeList eblaSpans = xmlDoc.SelectNodes("//span[@data-eblatype='startmarker']");
            var idsUsed = new HashSet<int>();
            foreach (XmlNode segstart in eblaSpans)
            {
                XmlAttribute idattrib = segstart.Attributes["data-eblasegid"];
                if (idattrib == null)
                    throw new Exception("Start marker found with no id attrib.");

                int segid = 0;
                if (!Int32.TryParse(idattrib.Value, out segid))
                    throw new Exception("Start marker found with invalid attrib.");

                if (idsUsed.Contains(segid))
                    throw new Exception("Start marker found with repeated seg id: " + segid.ToString());


                if (segstart.ChildNodes.Count != 1)
                    throw new Exception("Start marker found with incorrect no. of child nodes");

                if (segstart.ChildNodes[0].NodeType != XmlNodeType.Text)
                    throw new Exception("Start marker found with child element that is not text");

                if (string.Compare(segstart.ChildNodes[0].Value, "[") != 0)
                    throw new Exception("Start marker found with illegal marker text");


                XmlNodeList endSegs = xmlDoc.SelectNodes("//span[@data-eblasegid='" + segid.ToString() + "' and @data-eblatype='endmarker']");
                if (endSegs == null || endSegs.Count == 0)
                    throw new Exception("End marker not found for id: " + segid.ToString());

                if (endSegs.Count > 1)
                    throw new Exception("Too many end markers for id: " + segid.ToString());

                XmlNode segend = endSegs[0];

                if (segend.ChildNodes.Count != 1)
                    throw new Exception("End marker found with incorrect no. of child nodes");

                if (segend.ChildNodes[0].NodeType != XmlNodeType.Text)
                    throw new Exception("End marker found with child element that is not text");

                if (string.Compare(segend.ChildNodes[0].Value, "]") != 0)
                    throw new Exception("End marker found with illegal marker text");




                //if (segstart.NextSibling != null)
                //    if (segstart.NextSibling.NodeType == XmlNodeType.Text)
                //    {
                //        XmlText text = (XmlText)(segstart.NextSibling);
                //        if (string.Compare(text.Value, "[") == 0)
                //        {
                //            if (text.NextSibling != null)
                //            {

                //            }
                //        }

                //    }


                List<string> attribNames = new List<string>();
                List<string> attribVals = new List<string>();
                string userattribprefix = "data-userattrib-";
                SegmentContentBehaviour cb = SegmentContentBehaviour.normal;

                foreach (XmlAttribute a in segstart.Attributes)
                {
                    if (a.Name.Length > userattribprefix.Length)
                    {
                        if (string.Compare(a.Name.Substring(0, userattribprefix.Length), userattribprefix) == 0)
                        {
                            attribNames.Add(a.Name.Substring(userattribprefix.Length));
                            attribVals.Add(a.Value);
                        }
                    }
                    else if (string.Compare(a.Name, "data-ebla-cb") == 0)
                    {
                        int cbval = 0;
                        if (!Int32.TryParse(a.Value, out cbval))
                            throw new Exception("Invalid data-ebla-cb value for segid " + segid.ToString() + ": " + a.Value);

                        if (!Enum.IsDefined(typeof(SegmentContentBehaviour), cbval))
                            throw new Exception("Invalid data-ebla-cb value for segid " + segid.ToString() + ": " + a.Value);

                        cb = (SegmentContentBehaviour)cbval;
                    }


                }

                XmlDocPos pos = new XmlDocPos();
                pos.textContent = (XmlText)(segstart.ChildNodes[0]);
                XmlAttribute eblaatt = segstart.Attributes["data-eblatype"];
                segstart.Attributes.RemoveNamedItem("data-eblatype"); // temporarily stop it being ignored, so it can be sought
                pos.offset = 0;
                

#if _VVV_PRESERVEWHITESPACE
                int startOffset = XmlDocPos.MeasureTo(pos, true, Document.IgnoreElm);
#else
                int startOffset = XmlDocPos.MeasureTo(pos, false, Document.IgnoreElm);

#endif
//                segstart.Attributes.Append(eblaatt);

                segstart.ParentNode.RemoveChild(segstart);

                var startPos = new XmlDocPos();
                startPos.offset = 0;
                startPos.textContent = pos.textContent;

                pos.textContent = (XmlText)(segend.ChildNodes[0]);
                eblaatt = segend.Attributes["data-eblatype"];
                segend.Attributes.RemoveNamedItem("data-eblatype"); // temporarily stop it being ignored, so it can be sought
                

#if _VVV_PRESERVEWHITESPACE
                int endOffset = XmlDocPos.MeasureTo(pos, true, Document.IgnoreElm);
#else
                int endOffset = XmlDocPos.MeasureTo(pos, false, Document.IgnoreElm);
#endif
//                segend.Attributes.Append(eblaatt);
                segend.ParentNode.RemoveChild(segend);

                if (endOffset < startOffset)
                    throw new Exception("End marker before start for id: " + segid.ToString());


                int seglength = endOffset - startOffset; 
                bool ignore = false;
                if (seglength == 0)
                {
                    ignore = true;
                }

                if (!ignore) 
                {
                    SegmentDefinition segdef = new SegmentDefinition();
                    segdef.StartPosition = startOffset;
                    segdef.Length = seglength;
                    segdef.ID = segid; // when updating.
                    segdef.ContentBehaviour = cb;

                    List<SegmentAttribute> segattribs = new List<EblaAPI.SegmentAttribute>();

                    for (int attindex = 0; attindex < attribNames.Count; attindex++)
                    {
                        SegmentAttribute segattrib = new SegmentAttribute();
                        segattrib.Name = attribNames[attindex];
                        segattrib.Value = attribVals[attindex];
                        segattribs.Add(segattrib);
                    }

                    segdef.Attributes = segattribs.ToArray();

                    segdefs.Add(segdef);
                }


                idsUsed.Add(segid);

                // XmlDocPos.MeasureTo(
            }

                      


            //length = totalChars;
            // totalChars is not a correct indicator of length because 1. we may have removed whitespace now and 2. it included marker chars

            XmlNode n = xmlDoc.DocumentElement;
            length = 0;
            var nextTextNode = Document.NextTextNode(xmlDoc.DocumentElement, null, Document.IgnoreElm); 
            while (nextTextNode != null)
            {
                length += XmlDocPos.ExcludeWhitespaceLength(nextTextNode.InnerText);
                nextTextNode = Document.NextTextNode(nextTextNode.NextSibling, nextTextNode.ParentNode, Document.IgnoreElm);
            }
                
            TOC = EblaHelpers.XmlDocumentToString(xmlDocTOC);

            string content = EblaHelpers.XmlDocumentToString(xmlDoc);

            //if (true)
            //{
            //    XmlDocument test = new XmlDocument();
            //    test.LoadXml(content);

            //    int origLength = XmlDocPos.Measure(xmlDoc);
            //    int newLength = XmlDocPos.Measure(test);

            //    bool same = XmlDocPos.Compare(xmlDoc.DocumentElement, test.DocumentElement);

            //    System.Diagnostics.Debug.Assert(same);

            //}

            return content;
        }

        static bool AttribPermitted(string attribName)
        {
            if (attribName.Length > 5)
            {
                if (string.Compare(attribName.Substring(0, 5), "data-") == 0)
                    return true;
            }

            if (_legalAttribs.Contains(attribName.ToLower()))
                return true;

            return false;
        }

        static bool TagRemovesWhitespace(string tagName)
        {
            if (!EblaHelpers.IsInlineTag(tagName))
                return true;

            return string.Compare(tagName, "br", true) == 0;

        }

        static private XmlNode NextTextOrBlockNode(XmlText text, XmlDocPos.IgnoreElmDelg IgnoreElmImpl)
        {
            var searchPoint = _NextSearchPoint(text, IgnoreElmImpl);

            return _NextNode(searchPoint, IgnoreElmImpl, x => x.NodeType == XmlNodeType.Text || !EblaHelpers.IsInlineTag(x.Name));

        }


        /// <summary>
        /// Return sibling of this node, or if none, sibling of parent, or if none [...] or null.
        /// </summary>
        /// <param name="node"></param>
        /// <param name="IgnoreElmImpl"></param>
        /// <returns></returns>
        internal static XmlNode _NextSearchPoint(XmlNode node, XmlDocPos.IgnoreElmDelg IgnoreElmImpl)
        {
            do
            {
                var temp = node.NextSibling;
                if (temp != null && !IgnoreElmImpl.Invoke(temp))
                    return temp;

                node = node.ParentNode;
            }
            while (node != null);

            return null;
            
        }

        static bool _NodeMatches(XmlNode node, Func<XmlNode, bool> nodeMeetsCriteria)
        {
            if (node == null)
            {
                System.Diagnostics.Debug.Assert(false);
                return false;
            }
            if (nodeMeetsCriteria == null)
                return true;
            if (nodeMeetsCriteria.Invoke(node))
                return true;
            return false;
        }

        /// <summary>
        /// Test whether this node or any descendant matches criteria
        /// </summary>
        /// <param name="node"></param>
        /// <param name="IgnoreElmImpl"></param>
        /// <param name="nodeMeetsCriteria"></param>
        /// <returns></returns>
        internal static XmlNode _MatchNodeOrDescendant(XmlNode node, XmlDocPos.IgnoreElmDelg IgnoreElmImpl /*, List<XmlElement> elmsToIgnore*/, Func<XmlNode, bool> nodeMeetsCriteria)
        {
            if (node == null)
                return null;
            if (_NodeMatches(node, nodeMeetsCriteria))
                return node;
            foreach (XmlNode child in node.ChildNodes)
            {
                var temp = _MatchNodeOrDescendant(child, IgnoreElmImpl, nodeMeetsCriteria);
                if (temp != null)
                    return temp;
            }
            return null;
        }

        internal static XmlNode _NextNode(XmlNode node, XmlDocPos.IgnoreElmDelg IgnoreElmImpl /*, List<XmlElement> elmsToIgnore*/, Func<XmlNode, bool> nodeMeetsCriteria)
        {
            if (node == null)
                return null;
            if (!IgnoreElmImpl.Invoke(node))
            {
                var temp = _MatchNodeOrDescendant(node, IgnoreElmImpl, nodeMeetsCriteria);
                if (temp != null)
                    return temp;

            }

            return _NextNode(_NextSearchPoint(node, IgnoreElmImpl), IgnoreElmImpl, nodeMeetsCriteria);
        }

        private static XmlNode GetLowestParentBlockNode(XmlNode node)
        {
            var temp = node.ParentNode;
            while (temp != null)
                if (EblaHelpers.IsInlineTag(temp.Name))
                    temp = temp.ParentNode;
                else
                    break;
            return temp;
        }

        [System.Diagnostics.Conditional("DEBUG")]
        static void MyTrace(string s)
        {
            System.Diagnostics.Debug.WriteLine(s);
        }

        public static void TrimEnd(List<XmlText> textNodes)
        {
            int i = textNodes.Count - 1;
            while (i >= 0)
            {
                textNodes[i].Value = textNodes[i].Value.TrimEnd();
                if (textNodes[i].Value.Length > 0)
                    return;
                i--;
            }

        }

        static void CheckForDoubleSpaces(List<XmlText> textNodes)
        {
            var sb = new StringBuilder();
            foreach (var t in textNodes)
                sb.Append(t.Value);
            System.Diagnostics.Debug.Assert(sb.ToString().IndexOf("  ") == -1);
            System.Diagnostics.Debug.WriteLine("TEXT SO FAR: \"" + sb.ToString() + "\"");
        }

        static bool PrevTextNodeHasTrailingWhitespace(List<XmlText> textNodes)
        {
            if (textNodes.Count < 2)
                return false;
            int i = textNodes.Count - 2;
            while (i >= 0)
            {
                if (textNodes[i].Value.Length == 0)
                {
                    i--;
                    continue;
                }
                if (char.IsWhiteSpace(textNodes[i].Value[textNodes[i].Value.Length - 1]))
                    return true;
                return false;
            }
            return false;
        }

        static bool TextIsEmpty(List<XmlText> textNodes)
        {
            foreach (var t in textNodes)
                if (t.Value.Length > 0)
                    return false;
            return true;
        }

        public static void RemoveUnnecessaryWhitespace(XmlNode node, XmlDocPos.IgnoreElmDelg IgnoreElmImpl, List<XmlText> contiguousTextNodesInThisBlock)
        {

            switch (node.NodeType)
            {
                case XmlNodeType.Element:
                    MyTrace("Element: " + node.Name);
                    // Are we something to ignore?
                    if (IgnoreElmImpl.Invoke(node))
                    {
                        MyTrace("... ignoring");
                        return;
                    }
                    //if (string.Compare(node.Name, "span") == 0)
                    //{
                    //    XmlAttribute eblaattrib = node.Attributes["data-eblatype"];
                    //    if (eblaattrib != null)
                    //        switch (eblaattrib.Value)
                    //        {
                    //            case "startmarker":
                    //            case "endmarker":
                    //                return;
                    //        }
                    //}

                    // Do we just contain something like 'nbsp;'?
                    // (The .NET XML parser tells us this is an empty element, so when counting offsets it 
                    // yields 0 text characters, even though

                    break;
                case XmlNodeType.Text:
                    MyTrace("Text: \"" + node.Value + "\"");
                    //textNodes.Add((XmlText)node);
                    // Are we the first non-empty text node in this block?
                    if (TextIsEmpty(contiguousTextNodesInThisBlock))
                    {
                        MyTrace("First non-empty text node in block - trimming start");
                        node.Value = node.Value.TrimStart();
                    }


                    // Did the previous text node have trailing whitespace?
                    if (PrevTextNodeHasTrailingWhitespace(contiguousTextNodesInThisBlock))
                    {
                        MyTrace("Last node had trailing whitespace - trimming start");
                        node.Value = node.Value.TrimStart();
                    }

                    // Do we still have leading whitespace?
                    if (node.Value.Length > 0)
                    {
                        // ensure it's the minimum
                        if (char.IsWhiteSpace(node.Value[0]))
                        {
                            MyTrace("Setting leading whitespace to single space");
                            node.Value = " " + node.Value.TrimStart();
                        }
                    }

                    // Do we have trailing whitespace?
                    if (node.Value.Length > 0)
                    {
                        if (char.IsWhiteSpace(node.Value.Substring(node.Value.Length - 1)[0]))
                        {
                            MyTrace("We have trailing whitespace - setting to single space");
                            // ensure it's the minimum
                            node.Value = node.Value.TrimEnd() + " ";
                        }
                    }

                    //System.Diagnostics.Debug.Assert(node.Value.IndexOf('\n') == -1);
                    //System.Diagnostics.Debug.Assert(node.Value.IndexOf('\r') == -1);

                    //totalChars += node.Value.Length;
                    MyTrace("--Final text: \"" + node.Value + "\"");
                    contiguousTextNodesInThisBlock.Add(node as XmlText);
                    CheckForDoubleSpaces(contiguousTextNodesInThisBlock);
                    break;

            }

            foreach (XmlNode n in node.ChildNodes)
            {
                
                // Is this node a block?
                if (EblaHelpers.IsInlineTag(n.Name))
                {
                    // no
                    RemoveUnnecessaryWhitespace(n, Document.IgnoreElm, contiguousTextNodesInThisBlock);
                }
                else
                {
                    // yes
                    var contiguousTextNodesInChildBlock = new List<XmlText>();
                    RemoveUnnecessaryWhitespace(n, Document.IgnoreElm, contiguousTextNodesInChildBlock);
                    TrimEnd(contiguousTextNodesInChildBlock);
                    contiguousTextNodesInThisBlock.Clear();
                }
                
            }
        }

        public static void AppendNodes(XmlNode xmlNode, HtmlNode htmlNode, ref int id, XmlNode rootTOC, ref int totalChars, bool addIds = true)
        {
            string tagName;
            foreach (var n in htmlNode.ChildNodes)
            {
                switch (n.NodeType)
                {
                    case HtmlNodeType.Element:
                        tagName = n.Name;
                        if (string.Compare(tagName, _segMarkerEditPlaceholder) == 0)
                            tagName = "span";
                        if (TagIsPermitted(tagName))
                        {
                            XmlElement elm = xmlNode.OwnerDocument.CreateElement(tagName.ToLower());
                            XmlAttribute attrib = null;
                            if (addIds)
                            {
                                attrib = xmlNode.OwnerDocument.CreateAttribute("id");
                                attrib.Value = id.ToString();
                                id++;
                                elm.Attributes.Append(attrib);

                            }

                            foreach(HtmlAttribute a in n.Attributes)
                            {
                                if (AttribPermitted(a.Name))
                                {
                                    attrib = xmlNode.OwnerDocument.CreateAttribute(a.Name.ToLower());
                                    attrib.Value = a.Value;
                                    elm.Attributes.Append(attrib);
                                }
                            }

                            xmlNode.AppendChild(elm);
                            int tempPos = totalChars;
                            AppendNodes(elm, n, ref id, rootTOC, ref totalChars, addIds);



                            if (rootTOC != null && IncludeTagInTOC(n))
                            {
                                XmlElement elmTOC = rootTOC.OwnerDocument.CreateElement(n.Name);
                                XmlAttribute attribTOC = null;
                                if (addIds)
                                {
                                    attribTOC = rootTOC.OwnerDocument.CreateAttribute("id");
                                    attribTOC.Value = attrib.Value;
                                    elmTOC.Attributes.Append(attribTOC);
                                }
                                attribTOC = rootTOC.OwnerDocument.CreateAttribute("pos");
                                attribTOC.Value = tempPos.ToString();
                                elmTOC.Attributes.Append(attribTOC);
                                XmlNode textElm = rootTOC.OwnerDocument.CreateTextNode(elm.InnerText);
                                elmTOC.AppendChild(textElm);
                            }
                            
                        }

                        break;
                    case HtmlNodeType.Comment:
                        break;
                   
                    case HtmlNodeType.Text:
                        {
                            // The HTML read may include escape sequences such as &lt;
                            // If we pass that to xml node creation as-is, the '&' will be turned
                            // into its own escape sequence, leaving &amp;lt;
                            // So, we have to decode the string first.
                            String s = System.Net.WebUtility.HtmlDecode(n.InnerText);

                            XmlText xmlText = xmlNode.OwnerDocument.CreateTextNode(s);
                            
                            // We'll need to post-process the doc to trim whitespace
                            // where possible from block-level elements.

                            xmlNode.AppendChild(xmlText);
                            totalChars += XmlDocPos.ExcludeWhitespaceLength(xmlText.Value);

                            //if (true)
                            //{
                            //    // diagnostic
                            //    if (totalChars > 0)
                            //    {
                            //        int temp = totalChars - 1;
                            //        XmlDocPos pos = XmlDocPos.SeekToOffset(ref temp, xmlNode.OwnerDocument.DocumentElement);
                            //        System.Diagnostics.Debug.Assert(pos != null);
                            //    }
                            //}

                        }
                        break;
                    default:
                        throw new Exception("Unexpected HtmlNodeType: " + n.NodeType.ToString());
                }

                
            }
            

        }

        public static void AppendNodes(XmlNode xmlNodeTarget, XmlNode xmlNodeSource, ref int id, ref int totalChars)
        {
            string tagName;
            foreach (XmlNode n in xmlNodeSource.ChildNodes)
            {
                switch (n.NodeType)
                {
                    case XmlNodeType.Element:
                        tagName = n.Name;
                        if (string.Compare(tagName, _segMarkerEditPlaceholder) == 0)
                            tagName = "span";
                        if (true) //TagIsPermitted(tagName))
                        {
                            XmlElement elm = xmlNodeTarget.OwnerDocument.CreateElement(tagName.ToLower());
                            XmlAttribute attrib = null;

                            foreach (XmlAttribute a in n.Attributes)
                            {
                                if (true) //AttribPermitted(a.Name))
                                {
                                    attrib = xmlNodeTarget.OwnerDocument.CreateAttribute(a.Name.ToLower());
                                    attrib.Value = a.Value;
                                    elm.Attributes.Append(attrib);
                                }
                            }

                            xmlNodeTarget.AppendChild(elm);
                            int tempPos = totalChars;
                            AppendNodes(elm, n, ref id, ref totalChars);
                        }
                        break;
                        
                    case XmlNodeType.Comment:
                        break;
                    case XmlNodeType.Text:
                        //if (string.IsNullOrWhiteSpace(n.InnerText))
                        //{
                        //    // Skip - otherwise serialisation makes it different
                        //}
                        //else
                        {

                            String s = ((XmlText)(n)).Value; // System.Net.WebUtility.HtmlDecode(n.InnerText);
                            XmlText xmlText = xmlNodeTarget.OwnerDocument.CreateTextNode(s);

                            // We'll need to post-process the doc to trim whitespace
                            // where possible from block-level elements.

                            xmlNodeTarget.AppendChild(xmlText);
                            totalChars += XmlDocPos.ExcludeWhitespaceLength(xmlText.Value);

                            
                        }
                        break;
                    default:
                        throw new Exception("Unexpected XmlNodeType: " + n.NodeType.ToString());
                }


            }

        }


        static bool IncludeTagInTOC(HtmlNode node)
        {
            if (_tocTags.Contains(node.Name.ToLower()))
                return true;

            return false;
        }

        static bool TagIsPermitted(string tagName)
        {
            if (_illegalTags.Contains(tagName.ToLower()))
                return false;

            return true;
        }

        internal const string _segMarkerEditPlaceholder = "q";

        static HashSet<string> _illegalTags = new HashSet<string>() { "script", _segMarkerEditPlaceholder };

        static HashSet<string> _legalAttribs = new HashSet<string>() { "class", "dir" };

        static HashSet<string> _tocTags = new HashSet<string>() { "h1", "h2", "h3" };

        static HashSet<string> _deStopWords = new HashSet<string>() { };

        // Entirely provisional and hard-coded
        HashSet<string> __unused = new HashSet<string>()
        {

"aber",
"alle",
"allem",
"allen",
"aller",
"alles",
"als",
"also",
"am",
"an",
"ander",
"andere",
"anderem",
"anderen",
"anderer",
"anderes",
"anderm",
"andern",
"anderr",
"anders",
"auch",
"auf",
"aus",
"bei",
"bin",
"bis",
"bist",
"da",
"damit",
"dann",
"der",
"den",
"des",
"dem",
"die",
"das",
"daß",
"derselbe",
"derselben",
"denselben",
"desselben",
"demselben",
"dieselbe",
"dieselben",
"dasselbe",
"dazu",
"dein",
"deine",
"deinem",
"deinen",
"deiner",
"deines",
"denn",
"derer",
"dessen",
"dich",
"dir",
"du",
"dies",
"diese",
"diesem",
"diesen",
"dieser",
"dieses",
"doch",
"dort",
"durch",
"ein",
"eine",
"einem",
"einen",
"einer",
"eines",
"einig",
"einige",
"einigem",
"einigen",
"einiger",
"einiges",
"einmal",
"er",
"ihn",
"ihm",
"es",
"etwas",
"euer",
"eure",
"eurem",
"euren",
"eurer",
"eures",
"für",
"gegen",
"gewesen",
"hab",
"habe",
"haben",
"hat",
"hatte",
"hatten",
"hier",
"hin",
"hinter",
"ich",
"mich",
"mir",
"ihr",
"ihre",
"ihrem",
"ihren",
"ihrer",
"ihres",
"euch",
"im",
"in",
"indem",
"ins",
"ist",
"jede",
"jedem",
"jeden",
"jeder",
"jedes",
"jene",
"jenem",
"jenen",
"jener",
"jenes",
"jetzt",
"kann",
"kein",
"keine",
"keinem",
"keinen",
"keiner",
"keines",
"können",
"könnte",
"machen",
"man",
"manche",
"manchem",
"manchen",
"mancher",
"manches",
"mein",
"meine",
"meinem",
"meinen",
"meiner",
"meines",
"mit",
"muss",
"musste",
"nach",
"nicht",
"nichts",
"noch",
"nun",
"nur",
"ob",
"oder",
"ohne",
"sehr",
"sein",
"seine",
"seinem",
"seinen",
"seiner",
"seines",
"selbst",
"sich",
"sie",
"ihnen",
"sind",
"so",
"solche",
"solchem",
"solchen",
"solcher",
"solches",
"soll",
"sollte",
"sondern",
"sonst",
"über",
"um",
"und",
"uns",
"unse",
"unsem",
"unsen",
"unser",
"unses",
"unter",
"viel",
"vom",
"von",
"vor",
"während",
"war",
"waren",
"warst",
"was",
"weg",
"weil",
"weiter",
"welche",
"welchem",
"welchen",
"welcher",
"welches",
"wenn",
"werde",
"werden",
"wie",
"wieder",
"will",
"wir",
"wird",
"wirst",
"wo",
"wollen",
"wollte",
"würde",
"würden",
"zu",
"zum",
"zur",
"zwar",
"zwischen"

        };

    }
}
