/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using EblaAPI;
using System.Xml;
using System.Collections.Specialized;
using java.lang;
using Exception = System.Exception;
using Math = System.Math;
using StringBuilder = System.Text.StringBuilder;
using System.Diagnostics;
#if _USE_MYSQL
using MySql.Data.MySqlClient;
using EblaDataReader = MySql.Data.MySqlClient.MySqlDataReader;
using EblaConnection = MySql.Data.MySqlClient.MySqlConnection;
using EblaCommand = MySql.Data.MySqlClient.MySqlCommand;
using EblaParameter = MySql.Data.MySqlClient.MySqlParameter;
using EblaTransaction = MySql.Data.MySqlClient.MySqlTransaction;
#else
using System.Data.SQLite;
using EblaDataReader = System.Data.SQLite.SQLiteDataReader;
using EblaConnection = System.Data.SQLite.SQLiteConnection;
using EblaCommand = System.Data.SQLite.SQLiteCommand;
using EblaParameter = System.Data.SQLite.SQLiteParameter;
using EblaTransaction = System.Data.SQLite.SQLiteTransaction;
#endif

namespace EblaImpl
{
    #region Class Document
    public class Document : OpenableEblaItem, IDocument
    {
        #region Properties

        private int _corpusid;
        private int _id;
        private string _name;
        private string _corpusname;
        private DocumentMetadata _meta = new DocumentMetadata();
        private XmlDocument _content; // TODO - cache in singleton.
        private int _length = 0;
        private XmlDocument _TOC;
        private bool _segmentDefinitionTokensPreloaded = false;
        private Dictionary<int, Dictionary<string, int>> _segmentDefinitionTokens;
        private List<SegmentDefinition> _contentExcludedSegments = new List<SegmentDefinition>();
        private static string _totalTokensAttributeName = ":totaltokens";
        private static string _tokensAttributeName = ":tokens";
        private static string _widsAttributeName = ":wids";
        private HashSet<string> _inlineTags = new HashSet<string>() { "a", "abbr",
                                "acronym", "b", "basefont", "bdo", "big", "br", "cite", "code", "dfn", "em", "font", "i",
                                "img", "input", "kbd", "label", "q", "s", "samp", "select", "small",
                                "span", "strike", "strong", "sub", "sup", "textarea", "tt", "u", "var",
                                "ins", "del", "applet", "button", "iframe", "map", "object", "script"}; // Strictly speaking, these can be used as block-level and 
                                                                                                        // contain other block-level elements. See comments within the code that uses 
                                                                                                        // this HashSet.

        #endregion

        #region Constructors
        public Document(System.Collections.Specialized.NameValueCollection configProvider)
            : base(configProvider)
        {
        }
        #endregion

        #region Document Internal Class CloneStatus
        internal class CloneStatus
        {
            public int LengthRemaining;
            public bool StartPosFound;
        }

        #endregion

        #region Methods
        internal int ID()
        {
            CheckOpen();
            return _id;
        }

        internal static Dictionary<string, Document> BatchPreloadSegmentDefinitionTokens(System.Collections.Specialized.NameValueCollection ConfigProvider, EblaConnection cn, string corpusName, int corpusID, List<string> versions)
        {
            var docToIdMap = Corpus.GetDocToIdMap(cn, corpusID);

            string sql = "SELECT Value, SegmentDefinitionID, DocumentID FROM segmentlongattributes ";
            sql += " INNER JOIN segmentdefinitions ON segmentlongattributes.SegmentDefinitionID = segmentdefinitions.ID ";
            sql += " WHERE Name = @name AND DocumentID IN (";
            var sb = new StringBuilder();
            var valueLists = new Dictionary<int, Dictionary<int, string>>();
            var templist = new List<string>( versions);
            templist.Add(string.Empty); // include base text
            foreach (var v in templist)
            {
                if (sb.Length > 0)
                    sb.Append(", ");
                sb.Append(docToIdMap[v].ToString());
                valueLists.Add(docToIdMap[v], new Dictionary<int, string>());
            }
            sql += sb.ToString();
            sql += ") ";

            using (EblaCommand cmd = new EblaCommand(sql, cn))
            {
                cmd.Parameters.AddWithValue("name", _tokensAttributeName);

                using (EblaDataReader dr = cmd.ExecuteReader())
                {
                    int valueOrdinal = dr.GetOrdinal("Value");
                    int segdefidOrdinal = dr.GetOrdinal("SegmentDefinitionID");
                    int docidOrdinal = dr.GetOrdinal("DocumentID");
                    while (dr.Read())
                    {
                        if (!dr.IsDBNull(valueOrdinal))
                        {
                            string value = dr.GetString(valueOrdinal);
                            int segdefid = dr.GetInt32(segdefidOrdinal);
                            int docid = dr.GetInt32(docidOrdinal);

                            var list = valueLists[docid];
                            System.Diagnostics.Debug.Assert(!list.ContainsKey(segdefid));
                            list.Add(segdefid, value);

                        }
                    }
                }
            }

            var results = new Dictionary<string, Document>();
            foreach (var v in templist)
            {
                var doc = new Document(ConfigProvider);
                int docid = docToIdMap[v];
                var segdefTokens = new Dictionary<int, Dictionary<string, int>>();
                var values = valueLists[docid];
                foreach (int segdefid in values.Keys)
                {
                    segdefTokens.Add(segdefid, StringToSegmentDefinitionTokens(values[segdefid], segdefid));
                }
                doc.OpenWithoutPrivilegeCheck(corpusName, v, true, cn);
                doc.PreloadSegmentDefinitionTokens(segdefTokens);
                results.Add(v, doc);
            }

            return results;
        }

        internal void PreloadSegmentDefinitionTokens(Dictionary<int, Dictionary<string, int>> segmentDefinitionTokens)
        {
            CheckOpen();
            _segmentDefinitionTokens = segmentDefinitionTokens;
            _segmentDefinitionTokensPreloaded = true;
        }

        private bool Open(string CorpusName, string Username, string Password, string NameIfVersion)
        {
            if (!base.Open(Username, Password))
                return false;

            OpenWithoutPrivilegeCheck(CorpusName, NameIfVersion, false, null);
            CheckCanRead(_corpusname);
            return true;
        }

        internal void OpenWithoutPrivilegeCheck(string CorpusName, string NameIfVersion, bool makeAdmin, EblaConnection cn)
        {
            if (makeAdmin)
                base.MakeAdmin();

            if (cn != null)
                SetConnection(cn);

            using (EblaCommand cmd = new EblaCommand("SELECT ID FROM corpora WHERE Name = @name", GetConnection()))
            {
                EblaParameter nameParm = cmd.Parameters.AddWithValue("name", CorpusName);
                using (EblaDataReader dr = cmd.ExecuteReader())
                {
                    if (!dr.Read())
                        throw new Exception("Corpus not found: " + CorpusName);

                    _corpusid = dr.GetInt32(dr.GetOrdinal("ID"));

                }

                //_isBaseText = true;
                cmd.CommandText = "SELECT ID, Content, Length, TOC, Description, Information, ReferenceDate, AuthorTranslator, Genre, CopyrightInfo, LanguageCode FROM documents WHERE CorpusID = " + _corpusid.ToString() + " AND ";
                string notFoundMsg = "Corpus base text text not found: " + CorpusName;
                if (string.IsNullOrEmpty(NameIfVersion))
                {
                    cmd.CommandText += "Name IS NULL";
                }
                else
                {
                    cmd.CommandText += "Name = @name";
                    nameParm.Value = NameIfVersion;
                    //_isBaseText = false;
                    notFoundMsg = "Version text not found: " + NameIfVersion;
                }

                using (EblaDataReader dr = cmd.ExecuteReader())
                {
                    if (!dr.Read())
                        throw new Exception(notFoundMsg);

                    _id = dr.GetInt32(dr.GetOrdinal("ID"));

                    int contentOrdinal = dr.GetOrdinal("Content");
                    int tocOrdinal = dr.GetOrdinal("TOC");
                    int lengthOrdinal = dr.GetOrdinal("Length");

                    if (!dr.IsDBNull(lengthOrdinal))
                        _length = dr.GetInt32(lengthOrdinal);

                    if (dr.IsDBNull(contentOrdinal))
                    {

                    }
                    else
                    {
                        string content = dr.GetString(contentOrdinal);
                        if (content.Length > 0)
                        {
                            _content = new XmlDocument();
#if _VVV_PRESERVEWHITESPACE
                            _content.PreserveWhitespace = true;
#endif
                            _content.LoadXml(content);
#if _VVV_PRESERVEWHITESPACE
                            EblaHelpers.ConvertWhitespaceNodesToTextNodes(_content.DocumentElement);
#endif
                        }
                    }
                    if (dr.IsDBNull(tocOrdinal))
                    {

                    }
                    else
                    {
                        string toc = dr.GetString(tocOrdinal);
                        if (toc.Length > 0)
                        {
                            _TOC = new XmlDocument();
                            _TOC.LoadXml(toc);
                        }
                    }

                    int descOrdinal = dr.GetOrdinal("Description");
                    if (!dr.IsDBNull(descOrdinal))
                        _meta.Description = dr.GetString(descOrdinal);
                    
                    int infoOrdinal = dr.GetOrdinal("Information");
                    if (!dr.IsDBNull(infoOrdinal))
                        _meta.Information = dr.GetString(infoOrdinal);

                    int refdateOrdinal = dr.GetOrdinal("ReferenceDate");
                    if (!dr.IsDBNull(refdateOrdinal))
                        _meta.ReferenceDate = dr.GetInt32(refdateOrdinal);

                    int authtransOrdinal = dr.GetOrdinal("AuthorTranslator");
                    if (!dr.IsDBNull(authtransOrdinal))
                        _meta.AuthorTranslator = dr.GetString(authtransOrdinal);

                    int genreOrdinal = dr.GetOrdinal("Genre");
                    if (!dr.IsDBNull(genreOrdinal))
                        _meta.Genre = dr.GetString(genreOrdinal);

                    int copyrightOrdinal = dr.GetOrdinal("CopyrightInfo");
                    if (!dr.IsDBNull(copyrightOrdinal))
                        _meta.CopyrightInfo = dr.GetString(copyrightOrdinal);

                    int langcodeOrdinal = dr.GetOrdinal("LanguageCode");
                    if (!dr.IsDBNull(langcodeOrdinal))
                        _meta.LanguageCode = dr.GetString(langcodeOrdinal);

                    _meta.NameIfVersion = NameIfVersion;
                }

            }

            string exclusionSQL = "SELECT segmentdefinitions.ID, StartPosition, Length, ContentBehaviour FROM segmentdefinitions WHERE DocumentID = " + 
                _id.ToString() + " AND ContentBehaviour = " + ((int)(SegmentContentBehaviour.exclude)).ToString()  + " ORDER BY StartPosition, Length";

            _contentExcludedSegments = ReadSegDefs(exclusionSQL, GetConnection(), false, false);

            _open = true;
            _name = NameIfVersion;
            _corpusname = CorpusName;

            
        }

        internal List<SegmentDefinition> ExclusionSegmentsOverlappingSpan(int StartPosition, int Length)
        {
            var results = new List<SegmentDefinition>();
            if (Length == 0)
                return results;

            var dummy = new SegmentDefinition();
            dummy.StartPosition = StartPosition;
            dummy.Length = Length;

            //int minStartPos = StartPosition + Length - 1;
            int maxEndPos = StartPosition + Length;

            // _contentExcludedSegments is ordered by StartPosition, Length
            for (int i = 0; i < _contentExcludedSegments.Count; i++)
            {
                var seg = _contentExcludedSegments[i];

                if (seg.StartPosition > maxEndPos)
                    break; // no others can overlap

                if (dummy.Overlaps(seg))
                    results.Add(seg);

                //if (seg.StartPosition > minStartPos)
                //    return results;
                //if (seg.StartPosition < StartPosition)
                //    continue;

                //if (seg.StartPosition + seg.Length <= maxEndPos)
                //    results.Add(seg);

            }

            return results;
        }

        public bool OpenBaseText(string CorpusName, string Username, string Password)
        {
            return Open(CorpusName, Username, Password, null);
        }

        public bool OpenVersion(string CorpusName, string VersionName, string Username, string Password)
        {
            return Open(CorpusName, Username, Password, VersionName);
        }

        public DocumentMetadata GetMetadata()
        {
            return _meta;
        }

        public long GetSegmentCount()
        {
            string sql = "SELECT count(*) FROM segmentdefinitions WHERE DocumentID = " + _id.ToString();

            using (EblaCommand cmd = new EblaCommand(sql, GetConnection()))
            {
                object o = cmd.ExecuteScalar();

                if (o != null)
                    if (o != DBNull.Value)
                        return (long)o;
            }
            return 0;
        }

        public string GetSegmentDefinitionLongAttribute(int SegmentDefinitionID, string AttributeName)
        {
            using (EblaCommand cmd = new EblaCommand("", GetConnection()))
            {
                cmd.Parameters.AddWithValue("segdefid", SegmentDefinitionID);
                cmd.Parameters.AddWithValue("name", AttributeName);

                cmd.CommandText = "SELECT Value FROM segmentlongattributes WHERE Name = @name AND SegmentDefinitionID = @segdefid";

                using (EblaDataReader dr = cmd.ExecuteReader())
                {
                    if (!dr.Read())
                        return null;

                    if (dr.IsDBNull(0))
                        return null;
                    return dr.GetString(0);
                }
            }
        }

        public void SetSegmentDefinitionLongAttribute(int SegmentDefinitionID, SegmentAttribute LongAttribute)
        {
            using (EblaTransaction txn = GetConnection().BeginTransaction())
            {
                SetSegmentDefinitionLongAttribute(SegmentDefinitionID, LongAttribute, txn);
                txn.Commit();
            }
        }

        internal void SetSegmentDefinitionLongAttribute(int SegmentDefinitionID, SegmentAttribute LongAttribute, EblaTransaction txn)
        {
            //using (EblaTransaction txn = GetConnection().BeginTransaction())
            {
                using (EblaCommand cmd = new EblaCommand("", GetConnection()))
                {
                    cmd.CommandText = "DELETE FROM segmentlongattributes WHERE Name = @name AND SegmentDefinitionID = @segdefid";

                    cmd.Transaction = txn;

                    EblaParameter nameParm = cmd.Parameters.AddWithValue("name", LongAttribute.Name);
                    EblaParameter valueParm = cmd.Parameters.AddWithValue("value", LongAttribute.Value);

                    cmd.Parameters.AddWithValue("segdefid", SegmentDefinitionID);
                    cmd.ExecuteNonQuery();

                    cmd.CommandText = "INSERT INTO segmentlongattributes (SegmentDefinitionID, Name, Value) VALUES (@segdefid, @name, @value)";
                    cmd.ExecuteNonQuery();
                }
            }

        }

        public void RemoveSegmentDefinitionLongAttribute(int SegmentDefinitionID, string AttributeName)
        {
            using (EblaTransaction txn = GetConnection().BeginTransaction())
            {
                using (EblaCommand cmd = new EblaCommand("", GetConnection()))
                {
                    cmd.CommandText = "DELETE FROM segmentlongattributes WHERE Name = @name AND SegmentDefinitionID = @segdefid";

                    cmd.Transaction = txn;

                    EblaParameter nameParm = cmd.Parameters.AddWithValue("name", AttributeName);
                    cmd.Parameters.AddWithValue("segdefid", SegmentDefinitionID);
                    cmd.ExecuteNonQuery();
                }

                txn.Commit();
            }
        }

        enum TextState
        {
            none,
            inSpeakerName,
            inSpeech,
            inSD
        }

        enum TagState
        {
            none,
            inBold,
            inItalic
        }

        public void AutoSegmentationPlay1Format()
        {
            var node = _content.DocumentElement;

            var tagStates = new HashSet<TagState>();

            var textState = TextState.none;

            var segDefs = new List<SegmentDefinition>();

            string speaker = string.Empty;
            int lastSpeechStartOffset = -1;
            int lastSDStartOffset = -1;
            int lastSpeechEndOffset = -1;
            int lastSDEndOffset = -1;
            int offset = 0;
            Play1Parse(segDefs, node, tagStates, textState, ref offset, ref speaker, ref lastSpeechStartOffset, ref lastSDStartOffset, ref lastSpeechEndOffset, ref lastSDEndOffset);

            // deal with anything trailing
            CreateSpeechSeg(ref lastSpeechStartOffset, ref lastSpeechEndOffset, ref speaker, segDefs);
            CreateSDSeg(ref lastSDStartOffset, ref lastSDEndOffset, segDefs);

            CreateSegmentDefinitions(segDefs.ToArray());

        }

        bool AllUpperCase(string s)
        {
            bool result = false;
            foreach (char c in s)
            {
                if (char.IsWhiteSpace(c))
                    continue;

                if (char.IsLetter(c))
                {
                    if (char.IsLower(c))
                        return false;
                    result = true;
                }
                //else
                  //  return false; - tolerate numbers etc. for some character names
            }
            return result;
        }

        void CreateSpeechSeg(ref int lastSpeechStartOffset, ref int lastSpeechEndOffset, ref string speaker, List<SegmentDefinition> segDefs)
        {
            if (lastSpeechEndOffset == -1)
                return;
            var segDef = new SegmentDefinition();
            Debug.Assert(speaker.Length > 0);
            segDef.StartPosition = lastSpeechStartOffset;
            segDef.Length = lastSpeechEndOffset - lastSpeechStartOffset;
            if (segDef.Length == 0)
            {
                // bad formatting? Two speaker names with whitespace between, e.g.:
                // <b>dssdf</b> <b>fdsfs</b>

            }
            Debug.Assert(segDef.StartPosition > 0);
            var attribs = new List<SegmentAttribute>();
            attribs.Add(new SegmentAttribute() { Name = "type", Value = "Speech" });
            attribs.Add(new SegmentAttribute() { Name = "speaker", Value = speaker });
            segDef.Attributes = attribs.ToArray();
            segDefs.Add(segDef);
            lastSpeechEndOffset = -1;
            lastSpeechStartOffset = -1;
            speaker = string.Empty;

        }

        void CreateSDSeg(ref int lastSDStartOffset, ref int lastSDEndOffset, List<SegmentDefinition> segDefs)
        {
            if (lastSDEndOffset == -1)
                return;
            var segDef = new SegmentDefinition();
            segDef.StartPosition = lastSDStartOffset;
            segDef.Length = lastSDEndOffset - lastSDStartOffset;
            segDef.ContentBehaviour = SegmentContentBehaviour.exclude;

            if (segDef.Length == 0) // can happen with (e.g.) whitespace only stuff
                return;

            Debug.Assert(segDef.Length > 0);
            Debug.Assert(segDef.StartPosition > 0);
            var attribs = new List<SegmentAttribute>();
            attribs.Add(new SegmentAttribute() { Name = "type", Value = "S.D." });
            segDef.Attributes = attribs.ToArray();
            segDefs.Add(segDef);
            lastSDEndOffset = -1;
            lastSDStartOffset = -1;

        }


        void Play1Parse(List<SegmentDefinition> segDefs, XmlNode node, HashSet<TagState> tagStates, TextState textState, ref int offset, ref string speaker,
            ref int lastSpeechStartOffset, ref int lastSDStartOffset,
            ref int lastSpeechEndOffset, ref int lastSDEndOffset)
        {
            if (!IgnoreElm(node))
            {
                if (node.NodeType == XmlNodeType.Text)
                {
                    int length = XmlDocPos.ExcludeWhitespaceLength(node.InnerText); 
                    
                    // bold + italic = act/scene titles
                    // bold + upper case - speaker name
                    // italic = stage direction
                    // normal text = speech, if preceded by speaker name

                    // could this be a speaker name?
                    if (tagStates.Contains(TagState.inBold) && !tagStates.Contains(TagState.inItalic))
                    {
                        // yes
                        if (AllUpperCase(node.InnerText))
                        {
                            // we think it is

                            // do we have a speech seg followed by a s.d.?
                            //if (lastSpeechEndOffset != -1 && lastSDStartOffset != -1)
                                //if ()

                            // deal with any preceding speech seg
                            CreateSpeechSeg(ref lastSpeechStartOffset, ref lastSpeechEndOffset, ref speaker, segDefs);

                            // deal with any preceding stage direction
                            CreateSDSeg(ref lastSDStartOffset, ref lastSDEndOffset, segDefs);

                            speaker = node.InnerText.Trim();
                            lastSpeechStartOffset = -1;
                            lastSDStartOffset = -1;
                            lastSpeechEndOffset = -1;
                            lastSDEndOffset = -1;
                        }
                    }
                    // what about a stage direction?
                    else if (tagStates.Contains(TagState.inItalic) && !tagStates.Contains(TagState.inBold))
                    {
                        if (length > 0)
                        {
                            if (lastSDStartOffset == -1)
                                lastSDStartOffset = offset;
                            lastSDEndOffset = offset + length;
                        }
                    }
                    else if (tagStates.Count == 0)
                    {
                        // avoid breaking s.d.s that have whitespace between them
                        //if (length > 0)
                        {
                            // deal with any preceding stage direction  (can also be embedded in speeches)
                            CreateSDSeg(ref lastSDStartOffset, ref lastSDEndOffset, segDefs);

                            // have we seen a speaker?
                            if (speaker.Length > 0)
                            {
                                if (length > 0)
                                {
                                    if (lastSpeechStartOffset == -1)
                                        lastSpeechStartOffset = offset;
                                    lastSpeechEndOffset = offset + length;
                                }
                            }

                        }
                    }
                    else if (tagStates.Contains(TagState.inBold) && tagStates.Contains(TagState.inItalic))
                    {
                        // start of scene/act
                        // deal with any preceding speech seg
                        CreateSpeechSeg(ref lastSpeechStartOffset, ref lastSpeechEndOffset, ref speaker, segDefs);
                        CreateSDSeg(ref lastSDStartOffset, ref lastSDEndOffset, segDefs);
                    }
                    offset += length;
                }
                else if (node.HasChildNodes)
                {
                    string nodeName = node.Name.ToLower();
                    switch (nodeName)
                    {
                        case "b":
                            Debug.Assert(!tagStates.Contains(TagState.inBold));
                            tagStates.Add(TagState.inBold);
                            break;
                        case "i":
                            Debug.Assert(!tagStates.Contains(TagState.inItalic));
                            tagStates.Add(TagState.inItalic);
                            break;
                    }

                    foreach (XmlNode c in node.ChildNodes)
                        Play1Parse(segDefs, c, tagStates, textState, ref offset, ref speaker, 
                            ref lastSpeechStartOffset, ref lastSDStartOffset,
                            ref lastSpeechEndOffset, ref lastSDEndOffset);

                    switch (nodeName)
                    {
                        case "b":
                            tagStates.Remove(TagState.inBold);
                            break;
                        case "i":
                            tagStates.Remove(TagState.inItalic);
                            break;
                    }

                }

            }

        }


        public void AutoSegmentationAfterTag(string tag)
        {
            XmlNodeList nodes = _content.SelectNodes("//" + tag);
            XmlText textNode = null; 
            int lastStartPos = 0;
            var pos = new XmlDocPos();
            int textOffset = 0;

            foreach (XmlNode node in nodes)
            {
 
                // get next bit of text after this node
                textNode = NextTextNode(node, node.ParentNode, IgnoreElm);

                if (textNode == null)
                    return;

                pos.textContent = textNode;
                pos.offset = 0;
                textOffset = XmlDocPos.MeasureTo(pos, false, IgnoreElm);

                if (textOffset == lastStartPos)
                    continue; // e.g. several br tags in a row, or br tag at start of doc

                if (textOffset < lastStartPos)
                {
                    System.Diagnostics.Debug.Assert(false);
                    return;
                }

                var segdef = new SegmentDefinition();
                segdef.StartPosition = lastStartPos;
                segdef.Length = textOffset - lastStartPos;

                CreateSegmentDefinition(segdef);

                lastStartPos = textOffset;
            }

            // Now seek to end
            XmlText lastTextNode = textNode;
            while (true)
            {
                var node = NextNode(textNode);
                if (node == null)
                    break;
                textNode = NextTextNode(node, node.ParentNode, IgnoreElm);
                if (textNode == null)
                    break;
                lastTextNode = textNode;
            }

            pos.textContent = lastTextNode;
            pos.offset = lastTextNode.Value.Length;
            textOffset = XmlDocPos.MeasureTo(pos, false, IgnoreElm);

            if (textOffset > lastStartPos)
            {
                var segdef = new SegmentDefinition();
                segdef.StartPosition = lastStartPos;
                segdef.Length = textOffset - lastStartPos;

                CreateSegmentDefinition(segdef);

            }

        }

        public void AutoSegmentationAroundTag(string tag)
        {
            if (_content == null)
                throw new Exception("Document is empty.");
            
            XmlNodeList nodes = _content.SelectNodes("//" + tag);
            
            foreach (XmlNode node in nodes)
            {
                var pos = new XmlDocPos();
                var segStart = -1;
                // get next bit of text after this 'p' node
                var textNode = NextTextNode(node, node.ParentNode, IgnoreElm);
                // is there one?

                if (textNode != null)
                {
                    // Is it within the 'p'?
                    if (IsParent(textNode, node))
                    {
                        pos.textContent = textNode;
                        pos.offset = 0;
                        segStart = XmlDocPos.MeasureTo(pos, false, IgnoreElm);
                        int length = 0;
                        var nextNode = NextNode(node);
                        if (nextNode != null)
                        {
                            textNode = NextTextNode(nextNode, nextNode.ParentNode, IgnoreElm);
                            if (textNode != null)
                            {
                                pos.textContent = textNode;
                                length = XmlDocPos.MeasureTo(pos, false, IgnoreElm) - segStart;
                                System.Diagnostics.Debug.Assert(length >= 0);

                            }
                        }

                        if (length == 0)
                        {
                            length = this.Length() - segStart;
                            System.Diagnostics.Debug.Assert(length >= 0);
                        }

                        if (length > 0)
                        {
                            var segdef = new SegmentDefinition();
                            segdef.StartPosition = segStart;
                            segdef.Length = length;

                            CreateSegmentDefinition(segdef);
                        }


                    }
                }
                
            }
        }

        private XmlNode NextNode(XmlNode node)
        {
            while (node != null)
            {
                if (node.NextSibling != null)
                    return node.NextSibling;
                node = node.ParentNode;
            }
            return null;
        }

        private bool IsParent(XmlNode node, XmlNode possibleParent)
        {
            while (node.ParentNode != null)
            {
                if (object.ReferenceEquals(node.ParentNode, possibleParent))
                    return true;
                node = node.ParentNode;
            }
            return false;
        }

        public void SetMetadata(DocumentMetadata meta)
        {
            using (EblaCommand cmd = new EblaCommand("UPDATE documents SET Description = @description, Information = @information, ReferenceDate = @referencedate, AuthorTranslator = @authortranslator, Genre = @genre, CopyrightInfo = @copyrightinfo, LanguageCode = @langcode WHERE ID = " + _id.ToString(), GetConnection()))
            {
                cmd.Parameters.AddWithValue("description", meta.Description);
                cmd.Parameters.AddWithValue("information", meta.Information);
                cmd.Parameters.AddWithValue("authortranslator", meta.AuthorTranslator);
                cmd.Parameters.AddWithValue("genre", meta.Genre);
                cmd.Parameters.AddWithValue("copyrightinfo", meta.CopyrightInfo);
                if (meta.ReferenceDate.HasValue)
                    cmd.Parameters.AddWithValue("referencedate", meta.ReferenceDate.Value);
                else
                    cmd.Parameters.AddWithValue("referencedate", DBNull.Value);
                cmd.Parameters.AddWithValue("langcode", meta.LanguageCode);

                cmd.ExecuteNonQuery();

            }
            
        }

        public int Length()
        {
            if (_content == null)
                return 0;

            return _length;
        }

        internal string GetDocumentContentTextRaw(int StartPosition, int Length)
        {

            int i = StartPosition;
            XmlDocPos startPos = XmlDocPos.SeekToOffset(ref i, _content.DocumentElement, IgnoreElm); // elmsToIgnore);


            var collator = new List<string>();
            int count = 0;
            XmlDocPos.Extract(startPos, null, Length, false, IgnoreElm, collator, ref count);

            var sb = new StringBuilder();
            foreach (var s in collator)
            {
                sb.Append(s);
                sb.Append(" ");
            }

            return sb.ToString().Trim();

            //i = StartPosition + Length;
            //XmlDocPos endPos = XmlDocPos.SeekToOffset(ref i, _content.DocumentElement, IgnoreElm); // elmsToIgnore);

            //XmlDocument resultsDoc = EblaHelpers.CreateXmlDocument("content");

            //CloneStatus status = new CloneStatus();
            //status.StartPosFound = false;
            //status.LengthRemaining = Length;

            //// This is a rather expensive way of getting just the text content between two positions.
            //// TODO - optimise.
            //CloneBetweenPositions(_content.DocumentElement, resultsDoc.DocumentElement, startPos, status, false, null);

            ////return resultsDoc.DocumentElement.InnerText;
            //// Can't do that, as we end up getting words joined together.
            //return System.Text.RegularExpressions.Regex.Replace(resultsDoc.DocumentElement.InnerXml, "<.*?>", " ");

        }

        public string GetDocumentContentText(int StartPosition, int Length)
        {
            CheckOpen();
            CheckNotEmpty();
            CheckCanRead(_corpusname);

            CheckPosValues(StartPosition, Length);

            if (Length == 0)
                return string.Empty;

            //var exclusions = ExclusionSegmentsWithinSpan(StartPosition, Length);
            var exclusions = ExclusionSegmentsOverlappingSpan(StartPosition, Length);
            var spanSegs = new List<SegmentDefinition>();
            if (exclusions.Count == 0)
                spanSegs.Add(new SegmentDefinition() {StartPosition = StartPosition, Length = Length});
            else
            {
                int endix = StartPosition + Length;
                // exclusion segs are ordered by start pos
                for (int j = 0; j < exclusions.Count; j++)
                {
                    System.Diagnostics.Debug.Assert(j > 0 ? exclusions[j].StartPosition >= exclusions[j - 1].StartPosition : true);
                    if (StartPosition < exclusions[j].StartPosition)
                        spanSegs.Add(new SegmentDefinition() {StartPosition = StartPosition, Length = exclusions[j].StartPosition - StartPosition});
                    // might have nested exclusion segs
                    StartPosition = Math.Max(StartPosition,  exclusions[j].StartPosition + exclusions[j].Length);
                    //System.Diagnostics.Debug.Assert(StartPosition <= endix);
                    if (StartPosition >= endix)
                        break;
                }
                if (StartPosition < endix)
                    spanSegs.Add(new SegmentDefinition() { StartPosition = StartPosition, Length = endix - StartPosition });
            }

            var sb = new StringBuilder();
            foreach (var seg in spanSegs)
            {
                sb.Append(GetDocumentContentTextRaw(seg.StartPosition, seg.Length));
                sb.Append(" ");
            }

            return sb.ToString().Trim();

            ////List<XmlElement> elmsToIgnore = new List<XmlElement>();

            //int i = StartPosition;
            //XmlDocPos startPos = XmlDocPos.SeekToOffset(ref i, _content.DocumentElement, IgnoreElm); // elmsToIgnore);


            //i = StartPosition + Length;
            //XmlDocPos endPos = XmlDocPos.SeekToOffset(ref i, _content.DocumentElement, IgnoreElm); // elmsToIgnore);

            //XmlDocument resultsDoc = EblaHelpers.CreateXmlDocument("content");

            //CloneStatus status = new CloneStatus();
            //status.StartPosFound = false;
            //status.LengthRemaining = Length;

            //// This is a rather expensive way of getting just the text content between two positions.
            //// TODO - optimise.
            //CloneBetweenPositions(_content.DocumentElement, resultsDoc.DocumentElement, startPos, status, false, null);

            ////return resultsDoc.DocumentElement.InnerText;
            //// Can't do that, as we end up getting words joined together.
            //return System.Text.RegularExpressions.Regex.Replace(resultsDoc.DocumentElement.InnerXml, "<.*?>", " ");
            //// This means we may end up with multiple spaces, but let's not worry about that given what this
            //// function is meant for.
        }

        public static bool IgnoreElm(XmlNode node)
        {
            if (node.NodeType != XmlNodeType.Element)
                return false;

            XmlElement elm = (XmlElement)node;

            string eblatype = elm.GetAttribute("data-eblatype");
            if (string.IsNullOrEmpty(eblatype))
                return false;
            switch (eblatype)
            {
                case "startmarker":
                case "endmarker":
                    return true;
            }

            return false;
        }

        private bool IsInternalAttrib(string name)
        {
            if (!string.IsNullOrWhiteSpace(name))
            {
                if (name[0] == ':')
                    return true;
            }

            return false;

        }

        public string GetDocumentContent(int StartPosition, int Length, bool addSegmentMarkers, bool addSegmentFormatting, SegmentAttribute[] AttributeFilters, bool forEdit, string[] AttributesToEmit, string[] VariationVersionList, List<VariationMetricTypes> metricTypes)
        {
            var segmentFilter = new SegmentFilter();
            segmentFilter.AttributeFilters = AttributeFilters;
            return GetDocumentContent2(StartPosition, Length, addSegmentMarkers, addSegmentFormatting, segmentFilter, forEdit, AttributesToEmit, VariationVersionList, metricTypes);
        }

        public string GetDocumentContent2(int StartPosition, int Length, bool addSegmentMarkers, bool addSegmentFormatting, SegmentFilter SegmentFilter, bool forEdit, string[] AttributesToEmit, string[] VariationVersionList, List<VariationMetricTypes> metricTypes)
        {
            bool addIds = false;

            HashSet<string> hashAttributesToEmit = new HashSet<string>();

            // Stuff we may need if variation stats requested
            Dictionary<string, Document> doccache = null;
            Dictionary<string, AlignmentSet> alsetCache = null;
            List<string> versionList = null; 

            if (AttributesToEmit != null)
            {
                if (!addSegmentMarkers && !addSegmentFormatting)
                    throw new Exception("GetDocumentContent: AttributesToEmit was non-null, but addSegmentMarkers and addSegmentFormatting were false.");
                hashAttributesToEmit = new HashSet<string>(AttributesToEmit);

            }

            if (metricTypes != null && metricTypes.Count > 0)
            {
                var corpus = new Corpus(ConfigProvider);
                corpus.OpenWithoutPrivilegeCheck(_corpusname, true, GetConnection());

                versionList = new List<string>(corpus.GetVersionList());
                if (VariationVersionList != null && VariationVersionList.Length > 0)
                    versionList = new List<string>(VariationVersionList);

                doccache = Document.BatchPreloadSegmentDefinitionTokens(ConfigProvider, GetConnection(), _corpusname, _corpusid, versionList);
                alsetCache = AlignmentSet.BatchPreload(ConfigProvider, GetConnection(), _corpusname, _corpusid, versionList);

            }

            if (forEdit)
            {
                if (!addSegmentMarkers)
                    throw new Exception("GetDocumentContent: addSegmentMarkers must be true if forEdit is true");
                if (addSegmentFormatting)
                    throw new Exception("GetDocumentContent: addSegmentFormatting must not be true if forEdit is true");
            }

            CheckOpen();
            CheckNotEmpty();
            CheckCanRead(_corpusname);

            CheckPosValues(StartPosition, Length);

            if (Length == 0)
                return string.Empty;

            int i = StartPosition;
            XmlDocPos startPos =  XmlDocPos.SeekToOffset(ref i, _content.DocumentElement, IgnoreElm); // elmsToIgnore);

            if (startPos == null)
                return string.Empty;

            i = StartPosition + Length;
            XmlDocPos endPos =  XmlDocPos.SeekToOffset(ref i, _content.DocumentElement, IgnoreElm); // elmsToIgnore);

            XmlDocument resultsDoc = EblaHelpers.CreateXmlDocument("content");

            CloneStatus status = new CloneStatus();
            status.StartPosFound = false;
            status.LengthRemaining = Length;
            
            CloneBetweenPositions(_content.DocumentElement, resultsDoc.DocumentElement, startPos, status, addIds, null);

            List<XmlAttribute> segformatAttribs = new List<XmlAttribute>();
            List<XmlAttribute> startmarkerAttribs = new List<XmlAttribute>();
            List<XmlAttribute> endmarkerAttribs = new List<XmlAttribute>();
            List<XmlAttribute> userAttribs = new List<XmlAttribute>(); 
            XmlAttribute segformatAttrib = resultsDoc.CreateAttribute("data-eblatype");
            segformatAttrib.Value = "segformat";
            XmlAttribute startmarkerAttrib = resultsDoc.CreateAttribute("data-eblatype");
            startmarkerAttrib.Value = "startmarker";
            XmlAttribute endmarkerAttrib = resultsDoc.CreateAttribute("data-eblatype");
            endmarkerAttrib.Value = "endmarker";

            List<SegmentDefinition> segDefs = new List<SegmentDefinition>();
            if (addSegmentFormatting || addSegmentMarkers)
            {
                SegmentContentBehaviour[] cbFilters = null;
                if (SegmentFilter.SegmentContentBehaviourFilter != null)
                    cbFilters = new SegmentContentBehaviour[] { SegmentFilter.SegmentContentBehaviourFilter.Value };
                segDefs.AddRange(FindSegmentDefinitions(StartPosition, Length, true, true, SegmentFilter.AttributeFilters, true, cbFilters));

                if (versionList != null)
                {
                    // If and when variation stats for version selections are cached as segment attributes, retrieve
                    // the appropriate attribs by filtering with a key like this, then ensure the attrib name emitted 
                    // omits that key
                    //string versionlistkey = Corpus.VersionNamesSelectionToKey(versionList, GetConnection(), _corpusid);

                    // Ensure emitted
                    foreach (var m in metricTypes)
                    {
                        hashAttributesToEmit.Add(Corpus.EddyAttribName(m).Substring(1)); // strip leading ':'
                        hashAttributesToEmit.Add(Corpus.VivAttribName(m).Substring(1));// strip leading ':'

                    }

                    bool isVersion = !string.IsNullOrEmpty(_name);
                    AlignmentSet alset = null;
                    if (isVersion)
                        alset = alsetCache[_name];
                    //var corpus = new Corpus(ConfigProvider);
                    //corpus.OpenWithoutPrivilegeCheck(_corpusname, true, GetConnection());
                    var baseText = new Document(ConfigProvider);
                    baseText.OpenWithoutPrivilegeCheck(_corpusname, null, true, GetConnection());
                    foreach (var segdef in segDefs)
                    {
                        if (segdef.ContentBehaviour != SegmentContentBehaviour.normal)
                            continue;

                        int baseTextSegID = segdef.ID;

                        //var segdefToUse = segdef;
                        // is this a version?
                        if (isVersion)
                        {
                            var a = alset.FindAlignment(segdef.ID, false);
                            if (a == null)
                                continue; // not aligned
                            if (a.SegmentIDsInBaseText == null || a.SegmentIDsInBaseText.Length == 0 || a.SegmentIDsInBaseText.Length > 1)
                                continue; // can't do many-to-one or null
                            baseTextSegID = a.SegmentIDsInBaseText[0];
                            
                        }
                        
                        foreach (var m in metricTypes)
                        {
                            int unused = 0;
                            var stats = Corpus.CalculateSegmentVariation(baseText, baseTextSegID, doccache, alsetCache, GetConnection(), m, versionList, ConfigProvider, _corpusname, false, out unused, null);
                            if (stats != null) // check e.g. for empty base text
                                if (isVersion)
                                {
                                    var v = stats.VersionSegmentVariations.First(a => string.Compare(a.VersionName, _name) == 0);
                                    if (v != null)
                                        Corpus.AddAttribute(segdef, Corpus.EddyAttribName(m), v.EddyValue.ToString());

                                }
                                else
                                {
                                    Corpus.AddAttribute(segdef, Corpus.VivAttribName(m), stats.VivValue.ToString());
                                }
                        }
                        
                    }

                } // if (we need to emit variation stats)
            }


            if (addSegmentFormatting)
            {
                int lastNonTextNodeOffset = 0;
                XmlNode lastNonTextNode = null;

                // Get segment defs arranged in a tree that represents hierarchical enclosure
                // (pass 'true' to get a flat list of all nodes, whose 'children' still represent enclosure
                List<SegmentTreeNode> flatTree = SegmentTreeNode.SegmentsToTree(segDefs, true);

                foreach (var node in flatTree)
                {
                    // If segment formatting for each segment were applied simply by placing a start and end tag around 
                    // the entire segment, life would be simple. But because segments can start and end anywhere in the 
                    // HTML document, trying to do that could make the document invalid (e.g. our tag pair could enclose 
                    // a div start tag without enclosing its end tag). So, our 'greedy enclosure' algorithm tries to enclose all
                    // segment content within tag pairs, using as few pairs as possible but ensuring the content remains valid. 
                    // For example, given this HTML:
                    //  <body>
                    //  The quick brown fox jumps over the lazy dog.
                    //  <div>
                    //  Sphinx of black quartz, judge my vow.
                    //  </div>
                    //  </body>
                    // If the segment definition starts with the word 'fox' and ends with the word 'black',
                    // the valid enclosure produced will be something like this:
                    //  <body>
                    //  The quick brown <seg>fox jumps over the lazy dog.</seg>
                    //  <div>
                    //  <seg>Sphinx of black</seg> quartz, judge my vow.
                    //  </div>
                    //  </body>
                    // The algorithm is 'greedy' because rather than enclosing every text node in a separate span,
                    // it attempts to enclose multiple elements (inline or block) with a single tag pair wherever possible.

                    // Given that this approach is required, a further complication arises when one segment contains another.
                    // For example, suppose we have a segment S1 with start offset 10 and length 30, and a segment S2 with start
                    // offset 20 and length 10. The range covered by S1 includes the range covered by S2. If segments were 
                    // formatting simply by adding a single enclosing tag pair for each segment, then it would be clear when 
                    // parsing the document which text was in S2 and which in S2 - and, indeed, the tag for S2 could be
                    // given a different CSS class and would be rendered differently from S1, could be given different 'hover'
                    // text, et.
                    // However, because of the requirement to use multiple tag pairs as shown above, applying greedy enclosure
                    // to S1 and S2 as-is means that some segments of text may be enclosed by (say) span tags with seemingly
                    // conflicting attributes, making it harder to identify segment membership, render segments distinctly, etc.
                    // So, before applying segment formatting, we arrange the segment definitions as a tree that expresses
                    // any segment containment. If a segment contains no 'child' segments, greedy enclosure is applied to its
                    // content as-is. If a segment does contain 'child' segments, greedy enclosure is applied only to the 
                    // sections of the segment that are not also part of a 'child' segment.


                    // For this node's SegmentDefinition, find out how many enclosure stretches are needed.
                    List<DocumentRange> enclosures = node.SubtractDescendants();

                    // Set up attributes
                    XmlAttribute eblasegidAttrib = resultsDoc.CreateAttribute("data-eblasegid");
                    eblasegidAttrib.Value = node.SegmentDefinition.ID.ToString();
                    userAttribs.Clear();
                    if (node.SegmentDefinition.Attributes != null)
                    {
                        XmlAttribute titleAttrib = resultsDoc.CreateAttribute("title");
                        string title = string.Empty;
                        var attribsToShow = new List<SegmentAttribute>(node.SegmentDefinition.Attributes);
                        if (node.SegmentDefinition.ContentBehaviour != SegmentContentBehaviour.normal)
                            attribsToShow.Add(new SegmentAttribute() { Name = ":cb", Value = ((int)node.SegmentDefinition.ContentBehaviour).ToString() });
                        foreach (var a in attribsToShow)
                        {
                            string attribName = a.Name;
                            string prefix = "data-userattrib-";
                            bool internalAttrib = IsInternalAttrib(a.Name);
                            if (internalAttrib)
                            {
                                attribName = attribName.Substring(1);
                                prefix = "data-ebla-";

                            }

                            if (hashAttributesToEmit != null)
                                if (!hashAttributesToEmit.Contains(attribName))
                                    continue;

                            // Only emit internal attribs if specifically requested.
                            // This allows us to ensure variation stats are up to date.
                            if (internalAttrib)
                                if (hashAttributesToEmit == null)
                                    continue;

                            XmlAttribute userAttrib = resultsDoc.CreateAttribute(prefix + attribName);
                            userAttrib.Value = a.Value;
                            userAttribs.Add(userAttrib);


                            // Exclude attribs beginning with ':' ... we 'reserve' that for internal use (e.g. viv/eddy)
                            if (!internalAttrib)
                            {

                                if (title.Length > 0)
                                    title += ", ";
                                title += a.Name + ": " + a.Value;
                            }
                        }
                        titleAttrib.Value = title;
                        if (title.Length > 0)
                            userAttribs.Add(titleAttrib);
                    }

                    segformatAttribs.Clear();
                    segformatAttribs.Add(segformatAttrib);
                    //segformatAttribs.Add(segformatClass);
                    segformatAttribs.Add(eblasegidAttrib);
                    segformatAttribs.AddRange(userAttribs);

                    // add a greedy enclosure for each
                    foreach (var e in enclosures)
                    {
#if DEBUG
                        //if (e.Length == 0)
                        //{
                        //    string s = "debug";

                        //}
#endif

                        // although the segment as a whole must overlap our content section somewhere, 
                        // an individual enclosure (after child subtraction) might not.
                        if ((e.StartPosition + e.Length) <= StartPosition)
                            continue;
                        if (e.StartPosition >= (StartPosition + Length))
                            continue;

                        i = e.StartPosition - StartPosition;
                        if (i < 0) i = 0;
                        startPos = XmlDocPos.SeekToOffsetEx(i, resultsDoc.DocumentElement, IgnoreElm, ref lastNonTextNode, ref lastNonTextNodeOffset);

                        if (startPos == null)
                        {
                            // TODO - log
                            throw new Exception("Unable to seek to start offset " + i.ToString() + " while adding segment enclosure markup");
                        }

                        startPos = startPos.ExcludeWhitespaceOffsetToNormalOffset();
                        TrimToStart(startPos);

                        i = e.StartPosition + e.Length - StartPosition;
                        if (i < 0) i = 0;
                        if (i > Length) i = Length;
                        endPos = XmlDocPos.SeekToOffsetEx(i, resultsDoc.DocumentElement, IgnoreElm, ref lastNonTextNode, ref lastNonTextNodeOffset);

                        if (endPos == null)
                        {
                            // TODO - log
                            throw new Exception("Unable to seek to end offset " + i.ToString() + " while adding segment enclosure markup");
                        }
                        endPos = endPos.ExcludeWhitespaceOffsetToNormalOffset();

                        // Try 'promotion' optimisation
                        bool changesMade = false;
                        XmlNode startNode = startPos.textContent;
                        XmlNode endNode = endPos.textContent;
                        int startOffset = startPos.offset;
                        int endOffset = endPos.offset;
                        bool noTextBeforeSeg = startPos.offset == 0;
                        bool noTextAfterSeg = endPos.offset == endPos.textContent.Value.Length;

                        do
                        {
                            changesMade = false;

                            // if the start pos was right at the beginning of the text elm, and if the current
                            // start node is the first child of its parent ...
                            if (noTextBeforeSeg)
                                if (startNode.ParentNode != null)
                                    if (object.ReferenceEquals(startNode.ParentNode.ChildNodes[0], startNode))
                                    {
                                        // It may be worth 'promoting' the start node.

                                        // We can promote the start node to the parent so long as the
                                        // parent is not an ancestor of the end node (because if it is,
                                        // we shouldn't enclose the entire parent)
                                        if (!IsAncestor(endNode, startNode.ParentNode))
                                        {
                                            startNode = startNode.ParentNode;
                                            startOffset = -1; // tell enclosure algorithm not to worry about it
                                            changesMade = true;
                                            continue;
                                        }
                                    }


                            if (noTextAfterSeg)
                                if (endNode.ParentNode != null)
                                    if (object.ReferenceEquals(endNode.ParentNode.ChildNodes[endNode.ParentNode.ChildNodes.Count - 1], endNode))
                                    {
                                        if (!IsAncestor(startNode, endNode.ParentNode))
                                        {
                                            endNode = endNode.ParentNode;
                                            endOffset = -1; // tell enclosure algorithm not to worry about it
                                            changesMade = true;
                                            continue;
                                        }


                                    }

                            if (noTextBeforeSeg && noTextAfterSeg)
                                if (startNode.ParentNode != null && endNode.ParentNode != null)
                                    if (object.ReferenceEquals(startNode.ParentNode, endNode.ParentNode))
                                    {

                                    }
                        }
                        while (changesMade);


                        bool endFound = GreedyEnclosure(startNode, startOffset, endNode, endOffset, segformatAttribs/*, addStartMarker, addEndMarker, startmarkerAttribs, endmarkerAttribs, elmsToIgnore*/);

                        System.Diagnostics.Debug.Assert(endFound);

                    }
                }

            }

            if (addSegmentMarkers)
            {

                int lastNonTextNodeOffset = 0;
                XmlNode lastNonTextNode = null;

                foreach (SegmentDefinition segdef in segDefs)
                {
                    XmlAttribute eblasegidAttrib = resultsDoc.CreateAttribute("data-eblasegid");
                    eblasegidAttrib.Value = segdef.ID.ToString();
                    userAttribs.Clear();
                    if (!forEdit)
                        if (segdef.Attributes != null)
                            foreach (var a in segdef.Attributes)
                            {
                                string attribName = a.Name;
                                string prefix = "data-userattrib-";
                                bool internalAttrib = IsInternalAttrib(a.Name);
                                if (internalAttrib)
                                {
                                    attribName = attribName.Substring(1);
                                    prefix = "data-ebla-";

                                }

                                if (hashAttributesToEmit != null)
                                    if (!hashAttributesToEmit.Contains(attribName))
                                        continue;

                                // Only emit internal attribs if specifically requested.
                                // This allows us to ensure variation stats are up to date.
                                if (internalAttrib)
                                    if (hashAttributesToEmit == null)
                                        continue;

                                XmlAttribute userAttrib = resultsDoc.CreateAttribute(prefix + attribName);
                                userAttrib.Value = a.Value;
                                userAttribs.Add(userAttrib);
                            }


                    endmarkerAttribs.Clear();
                    endmarkerAttribs.Add(endmarkerAttrib);
                    endmarkerAttribs.Add(eblasegidAttrib);
                    //endmarkerAttribs.AddRange(userAttribs);

                    startmarkerAttribs.Clear();
                    startmarkerAttribs.Add(startmarkerAttrib);
                    startmarkerAttribs.Add(eblasegidAttrib);
                    startmarkerAttribs.AddRange(userAttribs);

                    if (!forEdit)
                    {
                        XmlAttribute eblahtmlidAttrib = resultsDoc.CreateAttribute("id");
                        eblahtmlidAttrib.Value = "ebla-segstart-" + segdef.ID.ToString();
                        startmarkerAttribs.Add(eblahtmlidAttrib);
                        eblahtmlidAttrib = resultsDoc.CreateAttribute("id");
                        eblahtmlidAttrib.Value = "ebla-segend-" + segdef.ID.ToString();
                        endmarkerAttribs.Add(eblahtmlidAttrib);
                    }

                    int startOffset = segdef.StartPosition - StartPosition;

                    bool showStartSegElm = false;
                    bool showEndSegElm = false;
                    if (startOffset >= 0 && startOffset <= Length)
                        showStartSegElm = true;

                    int endOffset = segdef.StartPosition + segdef.Length - StartPosition;
                    if (endOffset >= 0 && endOffset <= Length)
                        showEndSegElm = true;

                    if (showStartSegElm && showEndSegElm) /// only ever show both
                    {
                        XmlDocPos pos = XmlDocPos.SeekToOffsetEx(startOffset, resultsDoc.DocumentElement, IgnoreElm, ref lastNonTextNode, ref lastNonTextNodeOffset);


                        if (pos == null)
                        {
                            // TODO - log
                            throw new Exception("Unable to seek to start offset " + i.ToString() + " while adding segment markers");
                        }

                        var tempPos = pos.ExcludeWhitespaceOffsetToNormalOffset();
                        TrimToStart(tempPos);

                        //elmsToIgnore.Add(InsertStartSegElm(pos, resultsDoc, startmarkerAttribs));
                        InsertStartSegElm(tempPos, resultsDoc, startmarkerAttribs, forEdit);

                        pos = XmlDocPos.SeekToOffsetEx(endOffset, resultsDoc.DocumentElement, IgnoreElm, ref lastNonTextNode, ref lastNonTextNodeOffset);
                        if (pos == null)
                        {
                            // TODO - log
                            throw new Exception("Unable to seek to end offset " + i.ToString() + " while adding segment markers");
                        }

                        System.Diagnostics.Debug.Assert(pos != null);

                        tempPos = pos.ExcludeWhitespaceOffsetToNormalOffset();


                        //elmsToIgnore.Add(InsertEndSegElm(pos, resultsDoc, endmarkerAttribs));
                        InsertEndSegElm(tempPos, resultsDoc, endmarkerAttribs, forEdit);

                    }

                }
            }
            return resultsDoc.DocumentElement.InnerXml; 

        }

        private void TrimToStart(XmlDocPos pos)
        {
            for (; ; )
            {
                // If we're right at the end of a text node, try to insert it at the
                // start of the next instead.
                if (pos.offset == pos.textContent.Length)
                {
                    XmlText nextText = NextTextNode(pos.textContent.NextSibling, pos.textContent.ParentNode, IgnoreElm);
                    if (nextText != null)
                    {
                        pos.textContent = nextText;
                        pos.offset = 0;
                    }
                    else
                        break;
                }

                // try to skip whitespace
                bool nonWhitespaceFound = false;
                while (pos.offset < pos.textContent.Length)
                {
                    if (char.IsWhiteSpace(pos.textContent.Value[pos.offset]))
                        pos.offset++;
                    else
                    {
                        nonWhitespaceFound = true;
                        break;
                    }
                }
                if (nonWhitespaceFound)
                    break;
            }

        }

        private bool GreedyEnclosure(XmlNode startNode, int startOffset, XmlNode endNode, int endOffset, List<XmlAttribute> segformatAttribs/*, bool addStartMarker, bool addEndMarker, List<XmlAttribute> startmarkerAttribs, List<XmlAttribute> endmarkerAttribs, List<XmlElement> elmsToIgnore*/)
        {

            // Traverse forward through siblings from startNode, looking at nodes found and checking whether endPos
            // hit. Group nodes found into those that can be enclosed in a span (inline), and those that need
            // a div (block-level). If a node is found that is an ancestor of endPos, apply span/div enclosure 
            // to whatever preceded, then recurse down into that node. If enclosure is applied to all siblings 
            // and no ancester of endPos is found, recurse up through parent siblings. If endPos itself is
            // found (necessarily while grouping inline nodes), enclose in span and quit.

            // Certain elements can be 
            // either block-level or inline. We're treating most of those as inline (see inline tag list), which
            // presumes they will not then be used to contain block-level elements. If they are, then the html
            // we emit will be invalid, so this code may need to be enhanced.


            XmlDocument doc = startNode.OwnerDocument;

            List<XmlNode> nodesForDiv = new List<XmlNode>();
            List<XmlNode> nodesForSpan = new List<XmlNode>();

            if (startOffset >= 0)
            {
                System.Diagnostics.Debug.Assert(startNode != null);
                System.Diagnostics.Debug.Assert(startNode.NodeType == XmlNodeType.Text);

            }
            else
            {
                System.Diagnostics.Debug.Assert(startOffset == -1); // value meaning 'ignore'
                // set it to a no-effect value
                startOffset = 0;
            }
            if (endOffset >= 0)
            {
                System.Diagnostics.Debug.Assert(endNode != null);
                System.Diagnostics.Debug.Assert(endNode.NodeType == XmlNodeType.Text);

            }
            else
            {
                System.Diagnostics.Debug.Assert(endOffset == -1); // value meaning 'ignore'
            }
            if (startNode.NodeType == XmlNodeType.Text)
            {
                
                // Check for trivial case
                if (object.ReferenceEquals(startNode, endNode))
                {
                    nodesForSpan.Add(startNode);
                    EncloseInSpan(nodesForSpan, startOffset, startNode.OwnerDocument, startNode.ParentNode, endOffset, segformatAttribs/*, addStartMarker, addEndMarker, startmarkerAttribs, endmarkerAttribs, elmsToIgnore*/);
                    return true;
                }
            }

            XmlNode parent = startNode.ParentNode;

            bool traversingBlockElms = IsBlockElm(startNode);
            XmlNode currentNode = startNode;
            bool endFound = false;
            int spanLength = 0;

            while (currentNode != null)
            {
                if (IsAncestor(endNode, currentNode))
                {
                    // This enclosure will have to stop at this element.
                    endFound = true;
                    break;
                }

                if (IsBlockElm(currentNode))
                {
                    if (!traversingBlockElms)
                    {
                        // We just transitioned from one or more inline elms (or text nodes)
                        // to a block elm.
                        traversingBlockElms = true;

                        // put a span around everything in nodesForSpan
                        EncloseInSpan(nodesForSpan, startOffset, doc, parent, -1, segformatAttribs/*, addStartMarker, false, startmarkerAttribs, endmarkerAttribs, elmsToIgnore*/);
                        nodesForSpan.Clear();
                        startOffset = 0;

                    }
                    nodesForDiv.Add(currentNode);

                }
                else
                {
                    if (traversingBlockElms)
                    {
                        // We just transitioned from one or more block elms
                        // to an inline elm

                        // put a div around everything in nodesforDiv
                        // and use that div to replace them
                        EncloseInDiv(nodesForDiv, doc, parent, segformatAttribs);
                        nodesForDiv.Clear();

                        traversingBlockElms = false;
                    }
                    nodesForSpan.Add(currentNode);
                    spanLength += currentNode.InnerText.Length;
                }

                if (object.ReferenceEquals(currentNode, endNode))
                {
                    //System.Diagnostics.Debug.Assert(!traversingBlockElms);
                    if (traversingBlockElms)
                    {
                        System.Diagnostics.Debug.Assert(nodesForDiv.Contains(currentNode));
                        EncloseInDiv(nodesForDiv, doc, parent, segformatAttribs);
                    }
                    else
                    {
                        System.Diagnostics.Debug.Assert(nodesForSpan.Contains(currentNode));
                        EncloseInSpan(nodesForSpan, startOffset, doc, parent, endOffset, segformatAttribs/*, addStartMarker, addEndMarker, startmarkerAttribs, endmarkerAttribs, elmsToIgnore*/);
                    }

                    return true;
                }

                currentNode = currentNode.NextSibling;
                               
            }


            if (nodesForDiv.Count > 0)
                EncloseInDiv(nodesForDiv, doc, parent, segformatAttribs);


            if (nodesForSpan.Count > 0)
            {
                // Don't bother putting in formatting/markers if span would be empty (no text-bearing elements)
                // or startOffset would mean formatting starts after the last char.
                if (spanLength - startOffset > 0)
                {
                    EncloseInSpan(nodesForSpan, startOffset, doc, parent, -1, segformatAttribs/*, addStartMarker, false, startmarkerAttribs, endmarkerAttribs, elmsToIgnore*/);
                    startOffset = 0;

                }


            }

            if (endFound)
            {
                // An enclosure traversal stopped at this element, because it's
                // an ancestor of endPos.
                // Apply greedy enclosure to its children.

                endFound = true;
                if (!GreedyEnclosure(currentNode.FirstChild, -1, endNode, endOffset, segformatAttribs/*, addStartMarker, addEndMarker, startmarkerAttribs, endmarkerAttribs, elmsToIgnore*/))
                    throw new Exception("Failed to find endPos from ancestor node");

                return true;
            }

            if (!endFound)
            {
                // Ok, so we traversed forward through all the siblings of the node provided, and didn't
                // find the end of the segment to enclose. That exhausts everything in the parent node.
                // So, start looking from the parent's next sibling, if there is one.
                currentNode = parent;
                // climb up ancestors until a next sibling is found.
                while (currentNode != null)
                {
                    if (currentNode.NextSibling != null)
                    {
                        currentNode = currentNode.NextSibling;
                        break;
                    }
                    currentNode = currentNode.ParentNode;
                }
                if (currentNode != null)
                    endFound = GreedyEnclosure(currentNode, -1, endNode, endOffset, segformatAttribs/*, addStartMarker, addEndMarker, startmarkerAttribs, endmarkerAttribs, elmsToIgnore*/);
            }


            return endFound;

        }

        internal static XmlText NextTextNode(XmlNode node, XmlNode parent, XmlDocPos.IgnoreElmDelg IgnoreElmImpl)
        {

            while (node != null)
            {
                if (!IgnoreElmImpl.Invoke(node)) //  !elmsToIgnore.Contains(node))
                {
                    if (node.NodeType == XmlNodeType.Text)
                        return (XmlText)node;

                    if (node.HasChildNodes)
                    {
                        XmlText child = NextTextNode(node.ChildNodes[0], node, IgnoreElmImpl);
                        if (child != null)
                            return child;
                    }

                }
                node = node.NextSibling;
            }

            if (parent == null)
                return null;

            return NextTextNode(parent.NextSibling, parent.ParentNode, IgnoreElmImpl);
        }

        internal static void EnumerateNodes(XmlNode node, XmlDocPos.IgnoreElmDelg IgnoreElmImpl, Action<XmlNode> act)
        {
            if (!IgnoreElmImpl.Invoke(node))
            {
                act(node);
                foreach (XmlNode c in node.ChildNodes)
                    EnumerateNodes(c, IgnoreElmImpl, act);
            }

        }


        private XmlNode EncloseInSpan(List<XmlNode> nodesForSpan, int spanStartOffset, XmlDocument doc, XmlNode parent, int spanEndOffset, List<XmlAttribute> segformatAttribs/*, bool addStartMarker, bool addEndMarker, List<XmlAttribute> startmarkerAttribs, List<XmlAttribute> endmarkerAttribs, List<XmlElement> elmsToIgnore*/)
        {
            // put a span around everything in nodesForSpan
            XmlElement span = doc.CreateElement("span");
            foreach (XmlAttribute attrib in segformatAttribs)
            {
                XmlAttribute newAttrib = doc.CreateAttribute(attrib.Name);
                newAttrib.Value = attrib.Value;
                span.Attributes.Append(newAttrib);
            }

            System.Diagnostics.Debug.Assert(nodesForSpan.Count > 0);
            XmlNode insertAfterNode = nodesForSpan[0].PreviousSibling;
            XmlNode appendToSpanNode = null;
            XmlNode appendAfterSpanNode = null;

            XmlNode lastNode = nodesForSpan[nodesForSpan.Count - 1];
            int nodesProvidedCount = nodesForSpan.Count;
            bool firstNodeSplit = false;
            if (spanStartOffset > 0)
            {
                System.Diagnostics.Debug.Assert(nodesForSpan[0].NodeType == XmlNodeType.Text);
                XmlText text = (XmlText)(nodesForSpan[0]);
                nodesForSpan.RemoveAt(0);
                insertAfterNode = text;
                System.Diagnostics.Debug.Assert(spanStartOffset <= text.Length);
                if (spanStartOffset == text.Length)
                {

                }
                else
                {
                    // Split text node
                    XmlText newText = doc.CreateTextNode(text.Value.Substring(spanStartOffset));
                    text.Value = text.Value.Substring(0, spanStartOffset);
                    span.AppendChild(newText);
                    //firstNode = newText;
                    firstNodeSplit = true;
                    if (nodesProvidedCount == 1)
                        lastNode = newText;
                }
            }

            if (spanEndOffset > -1)
            {
                System.Diagnostics.Debug.Assert(lastNode.NodeType == XmlNodeType.Text);
                XmlText text = (XmlText)(lastNode);
                if (nodesProvidedCount == 1)
                {
                    
                    // if spanStartOffset > 1, it will have been split, making remaining text shorter.
                    // Adjust end offset.
                    spanEndOffset -= spanStartOffset;
                }
                else
                {
                }

                if (spanEndOffset == 0)
                {
                    // Don't bother including in span
                    if (nodesProvidedCount == 1)
                    {
                        // The node that was originally in nodesForSpan may have been removed
                        // from it, and left as a child of the parent node. This is the case
                        // if 1. start offset was > 0, in which case it will have been split and a new node added to span, or
                        // 2. start offset == length, in which case nothing will have been added to span.

                        if (nodesForSpan.Count == 0)
                        {
                            // It has indeed been removed
                            if (span.ChildNodes.Count == 0)
                            {
                                // case 2 applies - we don't need to do anything
                            }
                            else
                            {

                                // The node that was originally in nodesForSpan has been removed
                                // from it, and left as a child of the parent node. However,
                                // its content has been truncated. The new node created with the
                                // remaining content is only in the span. We need to take it out
                                // of the span, then append it after the (seemingly empty) span

                                span.RemoveChild(lastNode);
                                appendAfterSpanNode = lastNode;
                            }

                        }
                        else
                        {
                            nodesForSpan.Clear();
                        }

                    }
                    else
                        nodesForSpan.RemoveAt(nodesForSpan.Count - 1);
                }
                else
                {
                    // Split text node if needs be
                    if (spanEndOffset < text.Value.Length)
                    {
                        //string s = text.Value;
                        if (nodesProvidedCount == 1 && firstNodeSplit)
                        {
                            // The node that was originally in nodesForSpan has been removed
                            // from it, so will be left as a child of the parent node. However,
                            // its content has been truncated. The new node created with the
                            // remaining content is only in the span. We need to truncate it,
                            // then create another node with the remainder for appending
                            // after the span
                            XmlText newText = doc.CreateTextNode(lastNode.Value.Substring(spanEndOffset));
                            lastNode.Value = lastNode.Value.Substring(0, spanEndOffset);
                            appendAfterSpanNode = newText;
                        }
                        else
                        {

                            // Create a new text node with just the leftmost content before the end offset
                            XmlText newText = doc.CreateTextNode(text.Value.Substring(0, spanEndOffset));
                            // Record it as one we'll append onto the end of the span
                            appendToSpanNode = newText;
                            // Modify the existing node so as only to include the content after the end offset
                            text.Value = text.Value.Substring(spanEndOffset);
                            // Take it out of the list so it won't be removed from the parent
                            nodesForSpan.Remove(text);

                        }

                    }

                }
            }

            foreach (var n in nodesForSpan)
            {
                parent.RemoveChild(n);
                span.AppendChild(n);
            }

            if (appendToSpanNode != null)
                span.AppendChild(appendToSpanNode);

            if (insertAfterNode != null)
                parent.InsertAfter(span, insertAfterNode);
            else
            {
                if (parent.HasChildNodes)
                    parent.InsertBefore(span, parent.ChildNodes[0]);
                else
                    parent.AppendChild(span);
            }

            if (appendAfterSpanNode != null)
                parent.InsertAfter(appendAfterSpanNode, span);


            return span;
        }

        private XmlNode EncloseInDiv(List<XmlNode> nodesForDiv, XmlDocument doc, XmlNode parent, List<XmlAttribute> attribs)
        {

            // put a div around everything in nodesForDiv
            XmlElement div = doc.CreateElement("div");
            foreach (XmlAttribute attrib in attribs)
            {
                XmlAttribute newAttrib = doc.CreateAttribute(attrib.Name);
                newAttrib.Value = attrib.Value;
                div.Attributes.Append(newAttrib);
            }
            System.Diagnostics.Debug.Assert(nodesForDiv.Count > 0);
            XmlNode insertAfterNode = nodesForDiv[0].PreviousSibling;

            foreach (var n in nodesForDiv)
            {
                parent.RemoveChild(n);
                div.AppendChild(n);
            }

            if (insertAfterNode != null)
                parent.InsertAfter(div, insertAfterNode);
            else
            {
                if (parent.HasChildNodes)
                    parent.InsertBefore(div, parent.ChildNodes[0]);
                else
                    parent.AppendChild(div);
            }


            return div;
        }

        private bool IsBlockElm(XmlNode node)
        {
            if (node.NodeType != XmlNodeType.Element)
                return false;

            return !EblaHelpers.IsInlineTag(node.Name);
        }
        
        private bool IsAncestor(XmlNode node, XmlNode putativeAncestor)
        {
            if (node.ParentNode == null)
                return false;

            if (object.ReferenceEquals(putativeAncestor, node.ParentNode))
                return true;

            return IsAncestor(node.ParentNode, putativeAncestor);

        }

        public static XmlElement InsertStartSegElm(XmlDocPos pos, XmlDocument doc, List<XmlAttribute> attribs, bool forEdit)
        {
            XmlElement elm = CreateSegElm(doc, attribs, "[", forEdit);
            InsertSegElm(elm, pos, doc);
            return elm;
        }

        public static  XmlElement InsertEndSegElm(XmlDocPos pos, XmlDocument doc, List<XmlAttribute> attribs, bool forEdit)
        {
            XmlElement elm = CreateSegElm(doc, attribs, "]", forEdit);
            InsertSegElm(elm, pos, doc);
            return elm;
        }

        public static XmlElement CreateSegElm(XmlDocument doc, List<XmlAttribute> attribs, string content, bool forEdit)
        {
            XmlElement segElm = doc.CreateElement(forEdit ? Corpus._segMarkerEditPlaceholder : "span");
            foreach (var a in attribs)
            {
                XmlAttribute attrib = doc.CreateAttribute(a.Name);
                attrib.Value = a.Value;
                segElm.Attributes.Append(attrib);
            }
            XmlText text = doc.CreateTextNode(content);
            segElm.AppendChild(text);
            return segElm;
        }

        public static void InsertSegElm(XmlElement segElm, XmlDocPos pos, XmlDocument doc)
        {
            if (pos.offset == 0)
            {
                pos.textContent.ParentNode.InsertBefore(segElm, pos.textContent);
            }
            else if (pos.offset == pos.textContent.Length)
            {
                pos.textContent.ParentNode.InsertAfter(segElm, pos.textContent);

            }
            else
            {
                XmlText textAfterTag = doc.CreateTextNode(pos.textContent.Value.Substring(pos.offset));
                pos.textContent.Value = pos.textContent.Value.Substring(0, pos.offset);
                pos.textContent.ParentNode.InsertAfter(segElm, pos.textContent);
                pos.textContent.ParentNode.InsertAfter(textAfterTag, segElm);
            }

        }        

        private static int CreateSegmentDefinition(int docid, SegmentDefinition NewSegmentDefinition, EblaCommand cmd)
        {
            int newSegID = 0;

            cmd.Parameters.Clear();
            cmd.CommandText = "INSERT INTO segmentdefinitions (StartPosition, Length, DocumentID, ContentBehaviour) VALUES (@startpos, @length, @docid, @cb)";


            cmd.Parameters.AddWithValue("startpos", NewSegmentDefinition.StartPosition);
            cmd.Parameters.AddWithValue("docid", docid);
            cmd.Parameters.AddWithValue("length", NewSegmentDefinition.Length);
            cmd.Parameters.AddWithValue("cb", NewSegmentDefinition.ContentBehaviour);

            cmd.ExecuteNonQuery();

            newSegID = EblaHelpers.GetLastInsertID(cmd);

            if (NewSegmentDefinition.Attributes != null)
                if (NewSegmentDefinition.Attributes.Length > 0)
                {

                    cmd.CommandText = "INSERT INTO segmentattributes (SegmentDefinitionID, Name, Value) VALUES (@segdefid, @name, @value)";
                    cmd.Parameters.AddWithValue("segdefid", newSegID);
                    EblaParameter nameParm = cmd.Parameters.AddWithValue("name", "");
                    EblaParameter valueParm = cmd.Parameters.AddWithValue("value", "");

                    foreach (SegmentAttribute attrib in NewSegmentDefinition.Attributes)
                    {
                        nameParm.Value = attrib.Name;
                        valueParm.Value = attrib.Value;
                        cmd.ExecuteNonQuery();
                    }
                }


            
            return newSegID;
            


        }

        public int CreateSegmentDefinition(SegmentDefinition NewSegmentDefinition)
        {
            //var exclusionChangeSpans = new SpanMerger(_length);
            using (EblaTransaction txn = GetConnection().BeginTransaction())
            {
                int i = CreateSegmentDefinition(NewSegmentDefinition, txn, false);
                if (NewSegmentDefinition.ContentBehaviour == SegmentContentBehaviour.exclude)
                    ProcessExclusionChangeSpan(NewSegmentDefinition, txn);
                txn.Commit();
                return i;
            }
        }

        public void CreateSegmentDefinitions(SegmentDefinition[] NewSegmentDefinitions)
        {
            // add these first, to invalidate fewer existing seg tokenizations
            var excluded = NewSegmentDefinitions.Where(x => x.ContentBehaviour == SegmentContentBehaviour.exclude);

            var others = new List<SegmentDefinition>();
            others.AddRange(NewSegmentDefinitions);
            foreach (var ex in excluded)
                others.Remove(ex);

            using (EblaTransaction txn = GetConnection().BeginTransaction())
            {
                foreach (var ex in excluded)
                    CreateSegmentDefinition(ex, txn, false); // no need to set true for last arg, since is 'exclude' anyway
                foreach (var seg in others)
                    CreateSegmentDefinition(seg, txn, false);
                ProcessExclusionChangeSpans(excluded.ToList(), txn);
                txn.Commit();
            }
        }

        public void ProcessExclusionChangeSpan(SegmentDefinition span, EblaTransaction txn)
        {
            ProcessExclusionChangeSpans(new List<SegmentDefinition>() {span}, txn); 
        }

        public void ProcessExclusionChangeSpans(List<SegmentDefinition> spans, EblaTransaction txn)
        {
            var idsToRetok = new HashSet<int>();
            foreach (var span in spans)
            {
                var ids = GetSegmentIds(span.StartPosition, span.Length, true, true, "ContentBehaviour = " + ((int)SegmentContentBehaviour.normal).ToString());

                foreach (var id in ids)
                    if (!idsToRetok.Contains(id))
                        idsToRetok.Add(id);
            }

            foreach (var id in idsToRetok)
            {
                var seg = GetSegmentDefinition(id);
                UpdateSegmentDefinitionTokens(seg, txn);
            }

        }

        public void ProcessExclusionChangeSpans(SpanMerger exclusionChangeSpans, EblaTransaction txn)
        {
            // The content behaviour status of these spans has changed.

            // Find out which segdefs may be affected
            var spans = exclusionChangeSpans.GetSpans();

            ProcessExclusionChangeSpans(spans, txn);
                
        }

        internal int SegDefCompare(SegmentDefinition a, SegmentDefinition b)
        {
            if (a.StartPosition < b.StartPosition)
                return -1;
            if (b.StartPosition < a.StartPosition)
                return 1;
            if (a.Length < b.Length)
                return -1;
            if (b.Length < a.Length)
                return 1;
            return 0;
        }

        internal int CreateSegmentDefinition(SegmentDefinition NewSegmentDefinition, EblaTransaction txn, /*SpanMerger exclusionChangeSpans,*/ bool skipTokenisation)
        {
            CheckNotEmpty();
            CheckCanWrite(_corpusname);

            CheckPosValues(NewSegmentDefinition.StartPosition, NewSegmentDefinition.Length);


            int newSegID = 0;

            //using (EblaTransaction txn = GetConnection().BeginTransaction())
            {
                using (EblaCommand cmd = new EblaCommand("", GetConnection()))
                {
                    newSegID = CreateSegmentDefinition(_id, NewSegmentDefinition, cmd);
                }

                NewSegmentDefinition.ID = newSegID;
                if (NewSegmentDefinition.ContentBehaviour == SegmentContentBehaviour.exclude)
                {
                    _contentExcludedSegments.Add(NewSegmentDefinition);
                    _contentExcludedSegments.Sort(SegDefCompare);
                    //exclusionChangeSpans.AddSpan(NewSegmentDefinition);
                }
                else
                    if (!skipTokenisation)
                        UpdateSegmentDefinitionTokens(NewSegmentDefinition, txn);

                //txn.Commit();
                return newSegID;
            }

            
        }

        internal void UpdateSegmentDefinitionTokens(SegmentDefinition segdef, EblaTransaction txn)
        {
            // This doc should be a 'version' not the base text
            //System.Diagnostics.Debug.Assert(!string.IsNullOrEmpty(_name));
            // Actually, no - base text is tokenised as well, since variation formulae
            // make use no. of words in base text as a term.

            if (segdef.ContentBehaviour == SegmentContentBehaviour.structure)
                return; // don't bother tokenising - not meant for analysis
            //System.Diagnostics.Debug.Assert(segdef.Length < 5000);

            if (segdef.Length > 5000)
            {
                // Provisional distinction between segments meant to demarcate text to
                // be analyses and segments meant to divide text into (say) chapters to 
                // generate a TOC.

                // Assume this is the latter
                return;
            }

            if (segdef.ContentBehaviour == SegmentContentBehaviour.exclude)
                return;

            var text = GetDocumentContentText(segdef.StartPosition, segdef.Length);

            var tokens = Corpus.TokeniseString(text);
            var longAttributes = DefineSegmentDefinitionLongAttributes(tokens);

            foreach (var segmentAttribute in longAttributes)
            {
                SetSegmentDefinitionLongAttribute(segdef.ID, segmentAttribute, txn);
            }

        }

        private static List<SegmentAttribute> DefineSegmentDefinitionLongAttributes(string[] tokens)
        {
            // Create legible string representations
            var longAttributes = new List<SegmentAttribute>();

            var totalTokensAttributes = new SegmentAttribute();
            totalTokensAttributes.Name = _totalTokensAttributeName;
            totalTokensAttributes.Value = tokens.Length.ToString();
            longAttributes.Add(totalTokensAttributes);

            var tokensAttributes = new SegmentAttribute();
            tokensAttributes.Name = _tokensAttributeName;
            tokensAttributes.Value = CalculateTokensAttribute(tokens);
            longAttributes.Add(tokensAttributes);

            var widsAttributes = new SegmentAttribute();
            widsAttributes.Name = _widsAttributeName;
            widsAttributes.Value = CalculateWidAttribute(tokens);

            longAttributes.Add(widsAttributes);
            return longAttributes;
        }

        private static string CalculateTokensAttribute(string[] tokens)
        {
            var tokensDictionary = new Dictionary<string, int>();
            foreach (var t in tokens)
            {
                if (tokensDictionary.ContainsKey(t))
                    tokensDictionary[t] = tokensDictionary[t] + 1;
                else
                    tokensDictionary.Add(t, 1);
            }

            var tokensAttributeList = new StringBuilder();
            foreach (var token in tokensDictionary.Keys)
            {
                tokensAttributeList.Append(token + "\t" + tokensDictionary[token] + Environment.NewLine);
            }

            return tokensAttributeList.ToString();
        }

        private static string CalculateWidAttribute(string[] tokens)
        {
            var widsList = new StringBuilder();
            for (var index = 0; index < tokens.Length; index++)
            {
                var token = tokens[index];
                widsList.Append(token + "\t" + (index + 1) + Environment.NewLine);
            }
            return widsList.ToString();
        }

        private static Dictionary<string, int> StringToSegmentDefinitionTokens(string attrib, int segID)
        {
            var results = new Dictionary<string, int>();

            var tokenlines = attrib.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var line in tokenlines)
            {
                var parts = line.Split(new char[] { '\t' }, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length != 2)
                    throw new Exception("Bad tokens attribute for seg id : " + segID.ToString());

                if (results.ContainsKey(parts[0]))
                    throw new Exception("Bad tokens attribute for seg id : " + segID.ToString());

                int count = 0;
                if (!int.TryParse(parts[1], out count))
                    throw new Exception("Bad tokens attribute for seg id : " + segID.ToString());

                results.Add(parts[0], count);
            }
            return results;

        }

        internal static SegmentAttribute[] StringToSegmentDefinitionWids(string attrib, int segID)
        {
            var segmentAttributes = new List<SegmentAttribute>();

            var tokenlines = attrib.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var line in tokenlines)
            {
                var parts = line.Split(new char[] { '\t' }, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length != 2)
                    throw new Exception("Bad tokens attribute for seg id : " + segID.ToString());

                int count = 0;
                if (!int.TryParse(parts[1], out count))
                    throw new Exception("Bad tokens attribute for seg id : " + segID.ToString());

                var segment = new SegmentAttribute();
                segment.Name = parts[0];
                segment.Value = count.ToString();

                segmentAttributes.Add(segment);
            }
            return segmentAttributes.ToArray();

        }

        internal Dictionary<string, int> GetSegmentDefinitionTokens(int segID)
        {
            if (_segmentDefinitionTokensPreloaded)
            {
                if (_segmentDefinitionTokens.ContainsKey(segID))
                    return _segmentDefinitionTokens[segID];
                return null;
            }

            string attrib = GetSegmentDefinitionLongAttribute(segID, _tokensAttributeName);
            if (string.IsNullOrEmpty(attrib))
                return null;

            return StringToSegmentDefinitionTokens(attrib, segID);

        }

        internal int GetSegmentDefinitionTotalTokens(int segID)
        {
            string attrib = GetSegmentDefinitionLongAttribute(segID, _totalTokensAttributeName);
            if (string.IsNullOrEmpty(attrib))
                return -1;

            int count = 0;
            if (!int.TryParse(attrib, out count))
                throw new Exception("Bad total tokens attribute for seg id : " + segID.ToString());

            return count;
        }

        // TODO - txn
        internal static  void WipeVariationStatsForVersionSegment(int segId, bool includeBase, System.Collections.Specialized.NameValueCollection configProvider,
            EblaConnection conn, string corpusName, string versionName)
        {
            
            using (EblaCommand cmd = new EblaCommand("DELETE FROM segmentattributes WHERE SegmentDefinitionID = " + segId.ToString() + " AND Name LIKE @name", conn))
            {
                var nameparm = cmd.Parameters.AddWithValue("name", Corpus._EddyAttribPrefix + "%");
                cmd.ExecuteNonQuery();
            }

            if (!includeBase)
                return;

            var alset = new AlignmentSet(configProvider);
            alset.OpenWithoutPrivilegeCheck(corpusName, versionName, true, conn);
            var al = alset.FindAlignment(segId, false);
            if (al != null)
            {

                foreach (int id in al.SegmentIDsInBaseText)
                    WipeVariationStatsForBaseTextSegment(id, true, configProvider, conn, corpusName);
            }

        }

        // TODO - txn
         internal static void WipeVariationStatsForBaseTextSegment(int segId, bool includeVersions, System.Collections.Specialized.NameValueCollection configProvider,
            EblaConnection conn, string corpusName)
        {
            using (EblaCommand cmd = new EblaCommand("DELETE FROM segmentattributes WHERE SegmentDefinitionID = @segid AND Name LIKE @name", conn))
            {
                var nameparm = cmd.Parameters.AddWithValue("name", Corpus._VivAttribPrefix + "%");
                var idparm = cmd.Parameters.AddWithValue("segid", segId);
                cmd.ExecuteNonQuery();
                if (!includeVersions)
                    return;

                var corpus = new Corpus(configProvider);
                corpus.OpenWithoutPrivilegeCheck(corpusName, true, conn);

                var versions = corpus.GetVersionList();
                foreach (var versionName in versions)
                {
                    var alset = new AlignmentSet(configProvider);
                    alset.OpenWithoutPrivilegeCheck(corpusName, versionName, true, conn);
                    var al = alset.FindAlignment(segId, true);
                    if (al != null)
                    {
                        nameparm.Value = Corpus._EddyAttribPrefix;
                        foreach (var id in al.SegmentIDsInVersion)
                        {
                            idparm.Value = id;
                            cmd.ExecuteNonQuery();
                        }

                    }
                }
            }

        }

        private void RemoveAttribute(SegmentDefinition segdef, string attribName)
        {
            if (segdef.Attributes == null)
                return;

            var attribs = new List<SegmentAttribute>(segdef.Attributes);
            var attrib = attribs.Find(a => string.Compare(a.Name, attribName) == 0);
            if (attrib == null)
                return;
            attribs.Remove(attrib);
            segdef.Attributes = attribs.ToArray();

        }

        public void UpdateSegmentDefinition(SegmentDefinition UpdatedSegmentDefinition, bool ForceRetokenisation)
        {
            CheckOpen();
            CheckCanWrite(_corpusname);
            var exclusionChangeSpans = new SpanMerger(_length);
            
            SegmentDefinition current = GetSegmentDefinition(UpdatedSegmentDefinition.ID);

            

            if (current == null)
                throw new Exception("SegmentDefinition with ID " + UpdatedSegmentDefinition.ID.ToString() + " not found.");

            CheckPosValues(UpdatedSegmentDefinition.StartPosition, UpdatedSegmentDefinition.Length);

            //// Conversion code. TODO - remove
            //var attribs = new List<SegmentAttribute>(UpdatedSegmentDefinition.Attributes);
            //if (attribs.Find(a => string.Compare(a.Name, _totalTokensAttributeName) == 0) == null)
            //    UpdateSegmentDefinitionTokens(UpdatedSegmentDefinition);


            List<SegmentAttribute> attribsToDelete = new List<SegmentAttribute>();
            List<SegmentAttribute> attribsToInsert = new List<SegmentAttribute>();
            List<SegmentAttribute> attribsToUpdate = new List<SegmentAttribute>();

            SegmentAttribute[] newAttribs = UpdatedSegmentDefinition.Attributes;
            if (newAttribs == null)
                newAttribs = new SegmentAttribute[0];

            foreach (var a in current.Attributes)
            {
                bool found = false;
                foreach (var a2 in newAttribs)
                {
                    if (string.Compare(a.Name, a2.Name) == 0)
                    {
                        found = true;
                        if (string.Compare(a.Value, a2.Value) != 0)
                            attribsToUpdate.Add(a2);
                        break;
                    }
                }

                if (!found)
                    attribsToDelete.Add(a);
            }

            foreach (var a in newAttribs)
            {
                bool found = false;
                foreach (var a2 in current.Attributes)
                {
                    if (string.Compare(a.Name, a2.Name) == 0)
                    {
                        found = true;
                        break;

                    }
                }
                if (!found)
                    attribsToInsert.Add(a);
            }

            string sql = "UPDATE segmentdefinitions SET StartPosition = @startpos, Length = @length, ContentBehaviour=@cb WHERE ID = " + UpdatedSegmentDefinition.ID.ToString();

            using (EblaTransaction txn = GetConnection().BeginTransaction())
            {
                bool spanChanged = current.StartPosition != UpdatedSegmentDefinition.StartPosition || current.Length != UpdatedSegmentDefinition.Length;
                bool exclusionStatusChanged = false;
                
                if (current.ContentBehaviour == SegmentContentBehaviour.exclude || UpdatedSegmentDefinition.ContentBehaviour == SegmentContentBehaviour.exclude)
                    exclusionStatusChanged = current.ContentBehaviour != UpdatedSegmentDefinition.ContentBehaviour;

                // Was this an 'exclude' seg?
                if (current.ContentBehaviour == SegmentContentBehaviour.exclude)
                {
                    // Anything important changed?
                    if (exclusionStatusChanged || spanChanged)
                    {
                        // Remove old exclusion seg from list
                        var torem = _contentExcludedSegments.Find(x => x.StartPosition == current.StartPosition && x.Length == current.Length);
                        if (torem == null)
                            throw new Exception("Content exclusion error");
                        _contentExcludedSegments.Remove(torem);

                        // If no longer an exclusion seg, ignore, otherwise put new exl seg in list
                        if (exclusionStatusChanged)
                        {
                        }
                        else
                        {
                            _contentExcludedSegments.Add(UpdatedSegmentDefinition);
                            _contentExcludedSegments.Sort(SegDefCompare);
                        }
                    }
                }


                if (exclusionStatusChanged || (current.ContentBehaviour == SegmentContentBehaviour.exclude && spanChanged))
                {
                    exclusionChangeSpans.AddSpan(current);
                    exclusionChangeSpans.AddSpan(UpdatedSegmentDefinition);
                }

                if (ForceRetokenisation || spanChanged)
                    if (UpdatedSegmentDefinition.ContentBehaviour == SegmentContentBehaviour.normal)
                        UpdateSegmentDefinitionTokens(UpdatedSegmentDefinition, txn);

                using (EblaCommand cmd = new EblaCommand(sql, GetConnection()))
                {
                    cmd.Transaction = txn;

                    cmd.Parameters.AddWithValue("startpos", UpdatedSegmentDefinition.StartPosition);
                    cmd.Parameters.AddWithValue("length", UpdatedSegmentDefinition.Length);
                    cmd.Parameters.AddWithValue("cb", UpdatedSegmentDefinition.ContentBehaviour);
                    cmd.ExecuteNonQuery();

                    cmd.Parameters.Clear();

                    EblaParameter nameParm = cmd.Parameters.AddWithValue("name", "");
                    EblaParameter valueParm = cmd.Parameters.AddWithValue("value", "");
                    cmd.Parameters.AddWithValue("segdefid", UpdatedSegmentDefinition.ID);
                    cmd.CommandText = "INSERT INTO segmentattributes (SegmentDefinitionID, Name, Value) VALUES (@segdefid, @name, @value)";

                    foreach (var a in attribsToInsert)
                    {
                        nameParm.Value = a.Name;
                        valueParm.Value = a.Value;

                        cmd.ExecuteNonQuery();
                    }

                    cmd.CommandText = "UPDATE segmentattributes SET Value = @value WHERE Name = @name AND SegmentDefinitionID = @segdefid";
                    foreach (var a in attribsToUpdate)
                    {
                        nameParm.Value = a.Name;
                        valueParm.Value = a.Value;

                        cmd.ExecuteNonQuery();

                    }

                    cmd.CommandText = "DELETE FROM segmentattributes WHERE Name = @name AND SegmentDefinitionID = @segdefid";
                    foreach (var a in attribsToDelete)
                    {
                        nameParm.Value = a.Name;

                        cmd.ExecuteNonQuery();

                    }
                }

                ProcessExclusionChangeSpans(exclusionChangeSpans, txn);

                txn.Commit();
            }
        }

        public SegmentDefinition GetSegmentDefinition(int ID)
        {
            CheckCanRead(_corpusname);

            return GetSegmentDefinition(ID, _id, GetConnection(), false);

        }

        internal static SegmentDefinition GetSegmentDefinition(int ID, int docID, EblaConnection cn, bool includeInternal)
        {
            string filter = " AND segmentdefinitions.ID = " + ID.ToString() + " ";
            string sql = "SELECT segmentdefinitions.ID, StartPosition, Length, Name, Value, ContentBehaviour FROM segmentdefinitions LEFT OUTER JOIN segmentattributes ON segmentdefinitions.ID = segmentattributes.SegmentDefinitionID  WHERE DocumentID = " + docID.ToString() + filter + " ORDER BY segmentdefinitions.ID";

            List<SegmentDefinition> segDefs = ReadSegDefs(sql, cn, includeInternal, true);


            if (segDefs.Count == 0)
                return null;

            System.Diagnostics.Debug.Assert(segDefs.Count == 1);

            return segDefs[0];
        }

        private static void AppendAttribFromReader(SegmentDefinition segDef, EblaDataReader dr, int nameOrdinal, int valueOrdinal, bool includeInternal)
        {
            SegmentAttribute attrib = new SegmentAttribute();

            attrib.Value = dr.GetString(valueOrdinal);
            attrib.Name = dr.GetString(nameOrdinal);

            // SKip anything beginning with ':' - we 'reserve' that for internal use (e.g. eddy/viv)
            if (!includeInternal)
                if (!string.IsNullOrWhiteSpace(attrib.Name))
                    if (attrib.Name[0] == ':')
                        return;

            Array.Resize(ref segDef.Attributes, segDef.Attributes.Length + 1);
            segDef.Attributes[segDef.Attributes.Length - 1] = attrib; // AttribFromReader(dr, nameOrdinal, valueOrdinal);


        }

        internal void CheckArgPositive(int arg, string name)
        {
            if (arg < 0)
                throw new ArgumentException("Value may not be negative", name);

        }

        internal static string AppendFilter(string oldfilter, string toAppend, bool And)
        {
            if (oldfilter.Length > 0)

                oldfilter += And ? " AND " : " OR ";

            oldfilter += "(" + toAppend + ")";
            return oldfilter;
        }

        private void CheckPosValues(int StartPosition, int Length)
        {

            CheckArgPositive(StartPosition, "StartPosition");
            CheckArgPositive(Length, "Length");

            int endPosition = StartPosition + Length;
            if ((endPosition) > _length)
                throw new Exception("Values exceed document length.");

        }

        internal static string ContentBehaviourFilter(SegmentContentBehaviour[] ContentBehaviourFilters)
        {
            if (ContentBehaviourFilters == null)
                return string.Empty;

            var sb = new StringBuilder();
            foreach (var f in ContentBehaviourFilters)
            {
                if (sb.Length > 0)
                    sb.Append(" AND ");

                sb.Append("ContentBehaviour = " + ((int)f).ToString());
            }

            return sb.ToString();
        }

        internal static string SegdefBoundsFilter(int StartPosition, int Length, bool includeStartBefore, bool includeEndAfter, string tablePrefix)
        {
            string filter = string.Empty;
            int endPosition = StartPosition + Length;
            // First construct filter assuming neither flag set

            if (StartPosition > 0)
                filter = AppendFilter(filter, tablePrefix + "StartPosition >= " + StartPosition.ToString(), true);

            if (true) //endPosition < _length)
                filter = AppendFilter(filter, "(" + tablePrefix + "StartPosition + " + tablePrefix + "Length) <= " + endPosition.ToString(), true);

            if (includeStartBefore && includeEndAfter)
            {
                filter = string.Empty;

                filter = AppendFilter(filter, "(" + tablePrefix + "StartPosition + " + tablePrefix + "Length) BETWEEN " + StartPosition.ToString() + " AND " + endPosition.ToString(), true);

                filter = AppendFilter(filter, tablePrefix + "StartPosition BETWEEN " + StartPosition.ToString() + " AND " + endPosition.ToString(), false);

                //filter = AppendFilter(filter, "StartPosition < " + FromPosition.ToString() + " AND (StartPosition + Length) > " + endPosition.ToString(), false);

            }
            else if (includeStartBefore)
            {
                filter = string.Empty;

                filter = AppendFilter(filter, "(" + tablePrefix + "StartPosition + " + tablePrefix + "Length) BETWEEN " + StartPosition.ToString() + " AND " + endPosition.ToString(), true);


            }
            else if (includeEndAfter)
            {
                filter = string.Empty;
                filter = AppendFilter(filter, tablePrefix + "StartPosition BETWEEN " + StartPosition.ToString() + " AND " + endPosition.ToString(), true);

            }

            return filter;

        }

        public SegmentDefinition[] FindSegmentDefinitions(int StartPosition, int Length, bool includeStartBefore, bool includeEndAfter, SegmentAttribute[] AttributeFilters, SegmentContentBehaviour[] ContentBehaviourFilters)
        {
            return FindSegmentDefinitions(StartPosition, Length, includeStartBefore, includeEndAfter, AttributeFilters, false, ContentBehaviourFilters);
        }

        internal List<int> GetSegmentIds(int StartPosition, int Length, bool includeStartBefore, bool includeEndAfter, string furtherFilter) //, SegmentAttribute[] AttributeFilters)
        {
            CheckOpen();
            CheckCanRead(_corpusname);


            CheckPosValues(StartPosition, Length);
            int endPosition = StartPosition + Length;

            string filter = SegdefBoundsFilter(StartPosition, Length, includeStartBefore, includeEndAfter, string.Empty);

            if (!string.IsNullOrEmpty(filter))
                filter = " AND (" + filter + ") ";

            if (!string.IsNullOrEmpty(furtherFilter))
                filter += " AND (" + furtherFilter + ") ";

            string sql = "SELECT segmentdefinitions.ID  FROM segmentdefinitions  ";
            //if (AttributeFilters != null)
            //    sql = "SELECT segmentdefinitions.ID, StartPosition, Length, Name, Value FROM segmentdefinitions LEFT OUTER JOIN segmentattributes ON segmentdefinitions.ID = segmentattributes.SegmentDefinitionID ";
            sql += " WHERE DocumentID = " + _id.ToString() + filter + " ORDER BY segmentdefinitions.ID";

            var results = new List<int>();
            using (EblaCommand cmd = new EblaCommand(sql, GetConnection()))
            {
                using (EblaDataReader dr = cmd.ExecuteReader())
                {
                   
                    int idOrdinal = dr.GetOrdinal("ID");

                    while (dr.Read())
                        results.Add(dr.GetInt32(idOrdinal));
                            
                }
            }
            return results;
        }

        private SegmentDefinition[] FindSegmentDefinitions(int StartPosition, int Length, bool includeStartBefore, bool includeEndAfter, SegmentAttribute[] AttributeFilters, bool includeInternal, SegmentContentBehaviour[] ContentBehaviourFilters)
        {
            CheckOpen();
            CheckCanRead(_corpusname);


            CheckPosValues(StartPosition, Length);
            int endPosition = StartPosition + Length;

            string filter = SegdefBoundsFilter(StartPosition, Length, includeStartBefore, includeEndAfter, string.Empty);

            string filter2 = ContentBehaviourFilter(ContentBehaviourFilters);


            if (!string.IsNullOrEmpty(filter))
                filter = " AND (" + filter + ") ";

            if (!string.IsNullOrEmpty(filter2))
                filter += " AND (" + filter2 + ") ";

            string sql = "SELECT segmentdefinitions.ID, StartPosition, Length, Name, Value, ContentBehaviour FROM segmentdefinitions LEFT OUTER JOIN segmentattributes ON segmentdefinitions.ID = segmentattributes.SegmentDefinitionID  WHERE DocumentID = " + _id.ToString() + filter + " ORDER BY segmentdefinitions.ID";

            List<SegmentDefinition> segDefs = ReadSegDefs(sql, GetConnection(), includeInternal, true);

            // Could do this in sql, like in AlignmentSet
            if (AttributeFilters != null)
                if (AttributeFilters.Length > 0)
                {
                    List<SegmentDefinition> temp = new List<SegmentDefinition>();
                    foreach (var s in segDefs)
                    {
                        bool include = false;
                        foreach (var f in AttributeFilters)
                        {
                            // Does the seg def have a matching attrib?
                            bool found = false;
                            foreach (var a in s.Attributes)
                            {
                                if (string.Compare(a.Name, f.Name) == 0)
                                {
                                    found = true;
                                    // Does the value match?
                                    include = string.Compare(a.Value, f.Value) == 0;

                                    break;
                                }

                            }
                            if (!found)
                                include = false;
                            if (!include || !found)
                                break;
                        }

                        if (include)
                            temp.Add(s);
                    }

                    segDefs.Clear();
                    segDefs.AddRange(temp);
                }

            return segDefs.ToArray();
        }

        private static List<SegmentDefinition> ReadSegDefs(string sql, EblaConnection cn, bool includeInternal, bool includeAttribs)
        {
            List<SegmentDefinition> segDefs = new List<SegmentDefinition>();

            using (EblaCommand cmd = new EblaCommand(sql, cn))
            {
                using (EblaDataReader dr = cmd.ExecuteReader())
                {
                    SegmentDefinition segDef = null;
                    int idOrdinal = dr.GetOrdinal("ID");
                    int lengthOrdinal = dr.GetOrdinal("Length");
                    int startOrdinal = dr.GetOrdinal("StartPosition");

                    int nameOrdinal = 0;
                    int valueOrdinal = 0;
                    if (includeAttribs)
                    {
                        nameOrdinal = dr.GetOrdinal("Name");
                        valueOrdinal = dr.GetOrdinal("Value");
                    }
                    int cbOrdinal = dr.GetOrdinal("ContentBehaviour");
                    while (dr.Read())
                    {
                        int id = dr.GetInt32(idOrdinal);

                        if (segDef != null)
                        {
                            if (id == segDef.ID)
                            {
                                System.Diagnostics.Debug.Assert(includeAttribs);
                                // Got another attribute for the same seg
                                AppendAttribFromReader(segDef, dr, nameOrdinal, valueOrdinal, includeInternal);
                                continue;
                            }
                        }

                        segDef = new SegmentDefinition();
                        segDef.Attributes = new SegmentAttribute[0];

                        segDef.ID = id;
                        segDef.StartPosition = dr.GetInt32(startOrdinal);
                        segDef.Length = dr.GetInt32(lengthOrdinal);
                        segDef.ContentBehaviour = (SegmentContentBehaviour)  dr.GetInt32(cbOrdinal);
                        if (includeAttribs)
                            if (!dr.IsDBNull(nameOrdinal))
                                AppendAttribFromReader(segDef, dr, nameOrdinal, valueOrdinal, includeInternal);

                        segDefs.Add(segDef);
                    }

                }
                
            }

            return segDefs;

        }

        public void DeleteSegmentDefinition(int ID)
        {
            CheckOpen();
            CheckCanWrite(_corpusname);

            string sql = "DELETE FROM segmentdefinitions WHERE DocumentID = " + _id.ToString() + " AND ID = " + ID.ToString();

            SegmentDefinition current = GetSegmentDefinition(ID);
            using (EblaCommand cmd = new EblaCommand(sql, GetConnection()))
            {
                cmd.ExecuteNonQuery();
            }
            if (current.ContentBehaviour == SegmentContentBehaviour.exclude)
            {
                //var exclusionSpansChanged = new SpanMerger(_length);
                //exclusionSpansChanged.AddSpan(current);
                ProcessExclusionChangeSpan(current, null);
                var torem = _contentExcludedSegments.Find(x => x.StartPosition == current.StartPosition && x.Length == current.Length);
                if (torem == null)
                    throw new Exception("Content exclusion error");
                _contentExcludedSegments.Remove(torem);

            }
        }

        public void DeleteAllSegmentDefinitions()
        {
            CheckOpen();
            CheckCanWrite(_corpusname);
            string sql = "DELETE FROM segmentdefinitions WHERE DocumentID = " + _id.ToString();

            using (EblaCommand cmd = new EblaCommand(sql, GetConnection()))
            {
                cmd.ExecuteNonQuery();
            }
        }

        internal static void CloneBetweenPositions(XmlNode sourceNode, XmlNode targetNode, XmlDocPos startPos, CloneStatus status, bool cloneIds, XmlDocPos.IgnoreElmDelg IgnoreElmImpl)
        {

            foreach (XmlNode sourceChild in sourceNode.ChildNodes)
            {
                XmlNode targetChild = null;
                bool ignore = false;
                switch (sourceChild.NodeType)
                {
                    case XmlNodeType.Text:
                        XmlText sourceText = (XmlText) sourceChild;
                        XmlText targetText = null;
                        if (!(status.StartPosFound))
                        {
                            if (object.ReferenceEquals(sourceText, startPos.textContent))
                            {
                                status.StartPosFound = true;
                                string text = sourceText.Value.Substring(XmlDocPos.ExcludeWhitespaceOffsetToNormalOffset(sourceText.Value, startPos.offset));
                                int excludeWhitespaceLength = XmlDocPos.ExcludeWhitespaceLength(text);
                                if (excludeWhitespaceLength > status.LengthRemaining)
                                    text = text.Substring(0, XmlDocPos.ExcludeWhitespaceOffsetToNormalOffset(text, status.LengthRemaining));
                                targetText = targetNode.OwnerDocument.CreateTextNode(text);
                                status.LengthRemaining -= excludeWhitespaceLength;

                            }
                        }
                        else
                        {
                            if (status.LengthRemaining > 0)
                            {
                                string text = sourceText.Value;
                                int excludeWhitespaceLength = XmlDocPos.ExcludeWhitespaceLength(text);
                                if (XmlDocPos.ExcludeWhitespaceLength(text) > status.LengthRemaining)
                                    text = text.Substring(0, XmlDocPos.ExcludeWhitespaceOffsetToNormalOffset(text, status.LengthRemaining));
                                targetText = targetNode.OwnerDocument.CreateTextNode(text);
                                status.LengthRemaining -= excludeWhitespaceLength;
                            }

                        }
                        targetChild = targetText;
                        break;
                    case XmlNodeType.Whitespace:
                        if (status.LengthRemaining > 0)
                        {
                            XmlWhitespace sourceWhitespace = (XmlWhitespace)sourceChild;
                            XmlWhitespace targetWhitespace = targetNode.OwnerDocument.CreateWhitespace(sourceWhitespace.Value);
                            targetChild = targetWhitespace;

                        }
                        break;
                    case XmlNodeType.Element:
                        if (IgnoreElmImpl != null)
                            ignore = IgnoreElmImpl.Invoke(sourceChild);
                        
                        if (!ignore)
                        if (status.LengthRemaining > 0)
                        {
                            XmlElement sourceElm = (XmlElement)sourceChild;
                            XmlElement targetElm = targetNode.OwnerDocument.CreateElement(sourceElm.Name);
                            if (cloneIds)
                            {
                                XmlAttribute attrib = targetNode.OwnerDocument.CreateAttribute("id");
                                attrib.Value = EblaHelpers.GetNodeId(sourceElm).ToString();
                                targetElm.Attributes.Append(attrib);
                            }
                            foreach (XmlAttribute attrib in sourceElm.Attributes)
                            {
                                if (string.Compare(attrib.Name, "id") != 0)
                                {
                                    XmlAttribute a = targetElm.OwnerDocument.CreateAttribute(attrib.Name);
                                    a.Value = attrib.Value;
                                    targetElm.Attributes.Append(a);

                                }
                            }
                            targetChild = targetElm;
                        }
                        break;
                    default:
                        throw new Exception("Unexpected XmlNodeType in Clone: " + sourceChild.NodeType.ToString());
                }

                if (!ignore)
                CloneBetweenPositions(sourceChild, targetChild, startPos, status, cloneIds, IgnoreElmImpl);

                if (targetChild != null && status.StartPosFound)
                    targetNode.AppendChild(targetChild);

                if (status.LengthRemaining == 0)
                    break;

            }
        }

        private void CheckNotEmpty()
        {
            if (_content == null)
                throw new Exception("The document is empty.");
        }

        #endregion Methods
    }
    #endregion Class Document

    #region Class SpanMerger
    public class SpanMerger
    {
        #region Properties
        private System.Collections.BitArray _bitarray;
        private int _highestIndexSet = -1;
        private int _lowestIndexSet = -1;
        #endregion

        #region Constructors
        public SpanMerger(int size)
        {
            _bitarray = new System.Collections.BitArray(size);
        }
        #endregion

        #region Methods
        public int GetLowestIndex()
        {
            //// There are some bithacky ways to do this much faster.
            //// TODO - implement if they can work with bitarrays of arbitrary size
            //for (int i = 0; i < _bitarray.Length; i++)
            //    if (_bitarray.Get(i))
            //        return i;

            //return -1;
            return _lowestIndexSet;
        }

        public int GetHighestIndex()
        {
            //// There are some bithacky ways to do this much faster.
            //// TODO - implement if they can work with bitarrays of arbitrary size
            //for (int i = _bitarray.Length - 1; i >= 0; i--)
            //    if (_bitarray.Get(i))
            //        return i;

            //return -1;

            return _highestIndexSet;
        }

        public void AddSpan(SegmentDefinition s)
        {

            for (int i = 0; i < s.Length; i++)
                _bitarray.Set(s.StartPosition + i, true);

            if (s.Length > 0)
            {
                _highestIndexSet = Math.Max(_highestIndexSet, s.EndPosition);
                if (_lowestIndexSet == -1)
                    _lowestIndexSet = s.StartPosition;
                else
                    _lowestIndexSet = Math.Min(_lowestIndexSet, s.StartPosition);
            }

        }

        public List<SegmentDefinition> GetInverseSpans(int startFrom, int length)
        {
            return GetSpans(startFrom, length, true);
        }

        public List<SegmentDefinition> GetInverseSpans()
        {
            if (_bitarray.Length == 0)
                return new List<SegmentDefinition>();
            return GetSpans(0, _bitarray.Length, true);

        }

        public List<SegmentDefinition> GetSpans()
        {
            if (_bitarray.Length == 0)
                return new List<SegmentDefinition>();
            return GetSpans(0, _bitarray.Length, false);
        }

        public List<SegmentDefinition> GetSpans(int startFrom, int length)
        {
            return GetSpans(startFrom, length, false);
        }

        private List<SegmentDefinition> GetSpans(int startFrom, int length, bool inverse)
        {

            var spans = new List<SegmentDefinition>();

            bool bitValToCollate = !inverse;
            int startIndex = startFrom;
            bool previousBitVal = false;
            for (int i = 0; i < length; i++)
            {
                bool thisBitVal = _bitarray.Get(startFrom + i);

                // If this bit differs from the previous bit, we've reached the end of a span
                if (thisBitVal != previousBitVal && i > 0)
                {
                    if (previousBitVal == bitValToCollate)
                    {
                        var span = new SegmentDefinition();
                        span.StartPosition = startIndex;
                        span.Length = i - (startIndex - startFrom);
                        spans.Add(span);
                    }
                    startIndex = startFrom + i;
                }
                previousBitVal = thisBitVal;
            }

            if (length > 0)
            {
                if (previousBitVal == bitValToCollate)
                {
                    var span = new SegmentDefinition();
                    span.StartPosition = startIndex;
                    span.Length = length - (startIndex - startFrom);
                    spans.Add(span);
                }

            }

            return spans;
        }

        #endregion
    }
    #endregion Class SpanMerger
}
