﻿/*
    Version Variation Visualisation (VVV)
    http://www.delightedbeauty.org
    
    Copyright (c) Kevin Flanagan, 2012.
    http://www.kftrans.co.uk
  
    This file is part of VVV.

    VVV is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    VVV is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with VVV.  If not, see <http://www.gnu.org/licenses/>.
 */


using System;

namespace EblaImpl
{
    public class EblaCxnMgr
    {
        public delegate object GetConnectionDelg();
        public delegate void SetConnectionDelg(object o);

        public delegate object GetRequestDataDelg();
        public delegate void SetRequestDataDelg(object o);

        [ThreadStatic]
        public static object ThreadConnection = null;

        static GetConnectionDelg _getConnectionImpl;
        static SetConnectionDelg _setConnectionImpl;

        static GetRequestDataDelg _getRequestDataImpl;
        static SetRequestDataDelg _setRequestDataImpl;

        static bool _initted;

        public static bool Initialised()
        {
            return _initted;
        }



        static public void Init(GetConnectionDelg getConnectionImpl, SetConnectionDelg setConnectionImpl, GetRequestDataDelg getRequestDataImpl, SetRequestDataDelg setRequestDataImpl)
        {
            _getConnectionImpl = getConnectionImpl;
            _setConnectionImpl = setConnectionImpl;
            _getRequestDataImpl = getRequestDataImpl;
            _setRequestDataImpl = setRequestDataImpl;
            _initted = true;
        }

        static internal object GetConnection()
        {
            // Has the implementation been overridden for this thread?
            if (ThreadConnection != null)
                return ThreadConnection;

            return _getConnectionImpl.Invoke();
        }

        static internal void SetConnection(object conn)
        {
            _setConnectionImpl.Invoke(conn);
        }

        static internal object GetRequestData()
        {
            return _getRequestDataImpl.Invoke();
        }

        static internal void SetRequestData(object data)
        {
            _setRequestDataImpl.Invoke(data);
        }

    }
}
