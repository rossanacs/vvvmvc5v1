

--
-- Table structure for table `alignmentdetails`
--

DROP TABLE IF EXISTS `alignmentdetails`;
CREATE TABLE `alignmentdetails` (
  `ID` INTEGER PRIMARY KEY  AUTOINCREMENT NOT NULL,
  `AlignmentType` INTEGER NOT NULL,
  `Notes` TEXT ,
  `AlignmentStatus` INTEGER NOT NULL
);


--
-- Table structure for table `corpora`
--

DROP TABLE IF EXISTS `corpora`;
CREATE TABLE `corpora` (
  `ID` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  `Name` TEXT UNIQUE NOT NULL,
  `Description` TEXT 
) ;


--
-- Table structure for table `documents`
--

DROP TABLE IF EXISTS `documents`;
CREATE TABLE `documents` (
  `ID` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  `CorpusID` INTEGER NOT NULL references corpora(id) on delete cascade,
  `Content` TEXT ,
  `Name` TEXT  DEFAULT NULL,
  `Length` INTEGER DEFAULT NULL,
  `TOC` TEXT ,
  `Description` TEXT ,
  `ReferenceDate` INTEGER DEFAULT NULL,
  `AuthorTranslator` TEXT ,
  `CopyrightInfo` TEXT ,
  `genre` TEXT  DEFAULT NULL,
  `LanguageCode` TEXT DEFAULT NULL,
  `Information` TEXT ,
  UNIQUE (`CorpusID`,`Name`)
);


--
-- Table structure for table `predefinedsegmentattributes`
--

DROP TABLE IF EXISTS `predefinedsegmentattributes`;
CREATE TABLE `predefinedsegmentattributes` (
  `CorpusID` INTEGER NOT NULL references corpora(id) on delete cascade,
  `Name` TEXT  NOT NULL,
  `AttributeType` INTEGER NOT NULL,
  `ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `ShowInTOC` TINYINT DEFAULT NULL
);

--
-- Table structure for table `predefinedsegmentattributevalues`
--

DROP TABLE IF EXISTS `predefinedsegmentattributevalues`;
CREATE TABLE `predefinedsegmentattributevalues` (
  `PredefinedSegmentAttributeID` INTEGER PRIMARY KEY NOT NULL REFERENCES `predefinedsegmentattributes` (`ID`) ON DELETE CASCADE,
  `Value` TEXT  NOT NULL,
  `ApplyColouring` TINYINT DEFAULT NULL,
  `ColourCode` TEXT  DEFAULT NULL
);

--
-- Table structure for table `segmentalignments`
--

DROP TABLE IF EXISTS `segmentalignments`;
CREATE TABLE `segmentalignments` (
  `ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `BaseTextSegmentID` INTEGER NOT NULL REFERENCES `segmentdefinitions` (`ID`) ON DELETE CASCADE,
  `VersionSegmentID` INTEGER NOT NULL REFERENCES `segmentdefinitions` (`ID`) ON DELETE CASCADE,
  `AlignmentID` INTEGER NOT NULL REFERENCES `alignmentdetails` (`ID`) ON DELETE CASCADE ,
  UNIQUE  (`BaseTextSegmentID`,`VersionSegmentID`)
);


--
-- Table structure for table `segmentattributes`
--

DROP TABLE IF EXISTS `segmentattributes`;
CREATE TABLE `segmentattributes` (
  `ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `SegmentDefinitionID` INTEGER NOT NULL REFERENCES `segmentdefinitions` (`ID`) ON DELETE CASCADE,
  `Name` TEXT  NOT NULL,
  `Value` TEXT  NOT NULL
);


--
-- Table structure for table `segmentdefinitions`
--

DROP TABLE IF EXISTS `segmentdefinitions`;
CREATE TABLE `segmentdefinitions` (
  `ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `StartPosition` INTEGER NOT NULL,
  `Length` INTEGER NOT NULL,
  `DocumentID` INTEGER NOT NULL REFERENCES `documents` (`ID`) ON DELETE CASCADE,
  `ContentBehaviour` INTEGER NOT NULL DEFAULT '0'
);


--
-- Table structure for table `segmentlongattributes`
--

DROP TABLE IF EXISTS `segmentlongattributes`;
CREATE TABLE `segmentlongattributes` (
  `ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `SegmentDefinitionID` INTEGER DEFAULT NULL REFERENCES `segmentdefinitions` (`ID`) ON DELETE CASCADE,
  `Name` TEXT  DEFAULT NULL,
  `Value` TEXT 

);


--
-- Table structure for table `userrights`
--

DROP TABLE IF EXISTS `userrights`;
CREATE TABLE `userrights` (
  `ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `UserID` INTEGER NOT NULL REFERENCES `users` (`ID`) ON DELETE CASCADE,
  `CorpusID` INTEGER NOT NULL REFERENCES `corpora` (`ID`) ON DELETE CASCADE,
  `CanRead` TINYINT NOT NULL,
  `CanWrite` TINYINT NOT NULL
);


--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `ID` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `Username` TEXT NOT NULL,
  `Password` TEXT NOT NULL,
  `IsAdmin` TINYINT DEFAULT NULL,
  `UserEmail` TEXT DEFAULT NULL,
  `Salt` TEXT DEFAULT NULL

);

INSERT INTO `users` VALUES (1,'admin','3182AB4E777DDC0D759E25D30468984EC41B3401',1,'admin@admin.com','X5XlCQ==');
